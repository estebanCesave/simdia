@php
    $rolUser = Session::get('rol');
@endphp
@extends('master')
@section('content')
<section class="content">
    <div class="card" style="margin-top: 5px !important;">
        <div class="card-header">
            <h3 class="card-title">Usuarios | 
            @if(isset($junta->junta))
                {{ $junta->junta }}
            @elseif(isset($estado->estado))
                {{ $estado->estado }} 
            @endif
            </h3>
            <div class="card-tools">
            @if(Auth::user()->hasRole('Super Admin') && $rolUser == 'Super Admin')
                <a id="newUser" class="btn btn-success" title="Agregar Usuario" style="cursor: pointer;">
                    <i class="fas fa-user-plus"></i>               
                </a>
                <a id="addRole" class="btn btn-warning" title="Agregar Rol" style="cursor: pointer;">
                    <i class="fas fa-user-edit"></i>
                </a>
                <a id="addPer" class="btn btn-info" title="Agregar Permiso" style="cursor: pointer;">
                    <i class="fas fa-edit"></i>                
                </a>
               
            @endif
            </div>
            <div class="card-body">       
                <table id='usuarios' class="table table-responsive table-bordered table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Username</th>
                            <th scope="col">Personal id</th>
                            @if((Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado') || ( Auth::user()->hasRole('Super Admin') && $rolUser == 'Super Admin'))
                            <th scope="col">Estado</th>
                            <th scope="col">Junta</th>
                            @endif
                            <!--<th scope="col">Rol</th>-->
                            <th scope="col">Estatus</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($usuarios as $u)
                         <tr>
                            <td>{{ $u->name }} {{ $u->apellido_paterno }} {{ $u->apellido_materno }}</td>
                            <td>{{ $u->email }}</td>
                            <td width='20%'>{{ $u->personal_id }}</td>
                            @if((Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado') || ( Auth::user()->hasRole('Super Admin') && $rolUser == 'Super Admin'))                                 
                                <td>{{ $u->estado }}</td>
                                <td width='20%'>{{ $u->junta }}</td>
                            @endif
                            {{--<td> 
                                <label class="badge badge-success">{{ $u->description }}</label>
                            </td>--}}
                            <td> 
                                @if($u->status == 1)
                                    <label class="badge badge-success">Activo</label>
                                @else
                                    <label class="badge badge-danger">Inactivo</label>
                                @endif
                            </td>
                            <td width='20%'>
                                @if(Auth::user()->hasRole('Super Admin') && $rolUser == 'Super Admin')
                                    <a data-toggle="modal" data-target="#modalAssingRol" data-id="{{ $u->id }}" class="btn btn-sm btn-success editar" title="Agregar Rol" style="cursor: pointer;">
                                        <i class="fas fa-user-edit"></i>
                                    </a>
                                @endif
                                <a data-toggle="modal" data-target="#editUserModal" data-id="{{ $u->id }}" data-username='{{ $u->username }}' data-name='{{ $u->name }}{{ $u->apellido_paterno }} {{ $u->apellido_materno }}'  class="btn btn-sm btn-warning editar" title="Editar Usuario" style="cursor: pointer;">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a data-toggle="modal" data-target="#modalAddImei" data-id="{{ $u->id }}"  class="btn btn-sm btn-info borrar"  title="Imeis" style="cursor: pointer;">
                                    <i class="fas fa-mobile-alt"></i>
                                </a>
                             </td>
                         </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>

    @hasrole('Super Admin')
        @include('chunks.modals.createRol')
        @include('chunks.modals.createPermision')
        @include('chunks.modals.selectRol')
    @endrole

    @include('chunks.modals.addUser')

    @include('chunks.modals.editUser')
    @include('chunks.modals.addImei')

<script>
    const base_url = "{{ URL::to('/').'/usuarios/' }}";
    $(function () {
        $("body").addClass('sidebar-collapse');
    });
    $(document).ready(function(){

        $("#usuarios").DataTable({
            language:{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros por página",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
                },
                "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });  
    
        var id;

        $('#addRole').on('click', function(){
           $("#modalCreateRol").modal('show');
        });
        $('#addPer').on('click', function(){
            $("#modalCreatePermiso").modal('show');
        });
        $('#modalAssingRol').on('show.bs.modal', function (e) {
            id = $(e.relatedTarget).data().id;
        });
        $('#modalAddImei').on('show.bs.modal', function (e) {
            id = $(e.relatedTarget).data().id;
            var json = {
                userId : id
            }
            axios.post(base_url + 'imeis',json)
            .then(function(response){
                armar_tabla(response.data);
            }).catch(function(error){
                console.log(error);
            })
        });
        $('#newUser').on('click', function (e) {
            $("#addUserModal").modal({
                backdrop: 'static',
                keyboard: false            
            });
            $('#addUserModal').find("input,textarea").val('').end().find("input[type=checkbox], input[type=radio]").prop("checked", "").end();
            $('#showErrors').hide();
        });
        $('#editUserModal').on('show.bs.modal', function (e) {
            id = $(e.relatedTarget).data().id;
            var username = $(e.relatedTarget).data().username;
            var name = $(e.relatedTarget).data().name;
            $('#editTittle').html(name +' (' + username + ') ');

            $('#editUser').on('click', function(){
                var contra = $('#contraEdit').val();
                console.log(contra);
                var json = {
                    userId : id,
                    contra : contra
                 }
                 axios.post(base_url + 'update',json)
                .then(function(response){
                    if(response.data.status == 'error')
                    {
                        Swal.fire({
                            position: 'top-end',
                            type: 'error',
                            title: response.data.message,
                            showConfirmButton: false,
                            timer: 3000
                        })
                    }
                    if(response.data.status == 'success')
                    {
                        Swal.fire({
                            position: 'top-end',
                            type: 'success',
                            title: response.data.message,
                            showConfirmButton: false,
                            timer: 3000
                        })
                    }

                }).catch(function(error){
                    console.log(error);
                });
            });
        });
        $('#selectRole').on('click', function(){
            var rolUser = $('#rolUser').val();
            var json = {
                userId : id,
                rolId : rolUser
            }
            axios.post(base_url + 'assignRole',json)
                .then(function(response){
                    if(response.data.status == 'success')
                    {
                        $("#modalAssingRol").modal('hide');
                        //location.reload();
                    }
                    else{
                        Swal.fire({
                            position: 'top-end',
                            type: 'error',
                            title: response.data.message,
                            showConfirmButton: false,
                            timer: 3000
                        })
                    }
                }).catch(function(error){
                    console.log(error);
                })
        });
        function armar_tabla(dato)
        {
            myTable = $('#tblImeis').dataTable();       
            myTable.fnClearTable();
            $.each(dato.data, function(i,item)
            {
                var opciones = '<a class="btn btn-sm btn-danger borrar" data-id="'+dato.data[i]['id']+'" data-indice="' + i + '" title="Eliminar" style="cursor: pointer;"><i class="fa fa-trash"></i></a>';
                var rowNode = myTable
                    .fnAddData( [ 
                    '<i>' + dato.data[i]['name'] + '</i>',
                    '<i><label class="badge badge-success">Activo</label></i>',
                    opciones
                ]);
            });
        }
        $('#saveImei').on('click', function(){
            var json = {
                name : $('#imei').val(),
                userId : id
            }
            axios.post(base_url + 'addImei',json)
                .then(function (response) {
                     if(response.data.status == 'error')
                    {
                        Swal.fire({
                            position: 'top-end',
                            type: 'error',
                            title: response.data.message,
                            showConfirmButton: false,
                            timer: 3000
                        })
                    }
                    if(response.data.status == 'success')
                    {
                        Swal.fire({
                            position: 'top-end',
                            type: 'success',
                            title: response.data.message,
                            showConfirmButton: false,
                            timer: 3000
                        })
                        $("#modalAddImei").modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();

                    }
                }).catch(function(error){
                    console.log(error);
                });
        });
        $("#tblImeis").on("click",".borrar", function(){
            imei_id = $(this).data("id");
            console.log(id);
            var json = {
                imei_id : imei_id,
                userId : id
            }
            Swal.fire({
                title: 'Eliminar',
                text: "¿Seguro que desea desactivar este imei?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
            }).then((result) => {
                console.log(result.value);
                if (result.value == true) 
                {
                    axios.post(base_url + 'deleteImei',json)
                        .then(function(response){
                            $("#modalAddImei").modal('hide');
                            //location.reload();
                            if(response.data.status == 'success')
                            {
                                Swal.fire({
                                    position: 'top-end',
                                    type: 'success',
                                    title: response.data.message,
                                    showConfirmButton: false,
                                    timer: 3000
                                })
                                $("#modalAddImei").modal('hide');
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                            }        
                        }).catch(function(error){
                            console.log('Error',error);
                        });

                }
            })  

        });

        $('#imei').on('input', function () { 
            this.value = this.value.replace(/[^0-9]/g,'');
        });


    });    
</script>
</section>
@endsection
