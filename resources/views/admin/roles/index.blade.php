@extends('master')
@section('content')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<section class="content">
    <div class="card" style="margin-top: 5px !important;">
        <div class="card-header">
            <h3 class="card-title">Roles</h3>
            <div class="card-tools">
               
            </div>
            <div class="card-body">       
                <table class="table table-responsive table-bordered table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Rol</th>
                            <th scope="col">Permisos</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $r)
                        <tr>
                             <td>{{ $r->name }}</td>
                             <td>
                                @foreach($r->getAllPermissions() as $v)
                                    <label class="badge badge-success">{{ $v->name }}</label>
                                @endforeach
                            </td>
                            <td>
                                <a data-toggle="modal" data-target="#permissionRole" data-id="{{ $r->id }}"  class="btn btn-primary" title="Agregar Rol" style="cursor: pointer;">
                                    <i class="fas fa-plus"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('chunks.modals.permissionsRoles')

<script>
    const base_url = "{{ URL::to('/').'/roles/' }}";
    $(function () {
        $("body").addClass('sidebar-collapse');
    })
    $(document).ready(function(){
        var id;

        $('#permissionRole').on('show.bs.modal', function (e) {
            id = $(e.relatedTarget).data().id;
        });
        //$('.selectpicker').selectpicker();
        $('#saveRolPermission').on('click', function (e) {
            var rolPermiso = $('#permisosRol').val();
            var json = {
                rolId : id,
                permissionId : rolPermiso
            }
            axios.post(base_url + 'assignPermission',json)
                .then(function(response){
                    if(response.data.status == 'success')
                    {
                        $("#permissionRole").modal('hide');
                        location.reload();
                    }
                    else{
                        Swal.fire({
                            position: 'top-end',
                            type: 'error',
                            title: response.data.message,
                            showConfirmButton: false,
                            timer: 3000
                        })
                    }
                }).catch(function(error){
                    console.log(error);
                });
        });

    });
</script>
</section>
@endsection
