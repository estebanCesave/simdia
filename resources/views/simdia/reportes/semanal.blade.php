@extends('master')
@section('content')
<section class="content">
    <div class="card" style="margin-top: 10px !important;">
        <div class="card-header">
            <h3 class="card-title">Reporte semanal</h3>
        </div>
        <div class="card-body">       
            <table id="reporte" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Estado_id</th>
                    <th>Junta_id</th>
                    <th>Arco</th>
                    <th>Año</th>
                    <th>Semana</th>
                    <th>Revisada</th>
                    <th>No revisadas</th>
                    <th>Capturas</th>
                    <th>Trampas con capturas</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Modified</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data as $d)
                    <tr>
                        <td>{{ $d->estado_id }}</td>
                        <td>{{ $d->junta_id }}</td>
                        <td>{{ $d->arco }}</td>
                        <td>{{ $d->ano }}</td>
                        <td>{{ $d->semana }}</td>
                        <td>{{ $d->revisadas }}</td>
                        <td>{{ $d->norevisadas }}</td>
                        <td>{{ $d->capturas }}</td>
                        <td>{{ $d->trampasconcapturas }}</td>
                        <td>{{ $d->status }}</td>
                        <td>{{ $d->created }}</td>
                        <td>{{ $d->modified }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
         </div>
    </div>
</section>
@endsection
<script src="https://code.jquery.com/jquery-git.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#reporte').DataTable({
            'language': {
                'loadingRecords': '&nbsp;',
                'processing': 'Cargando...',
                "sLengthMenu":     "Mostrar _MENU_ registros por página",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    });
</script>