@php
     $base_url = URL::to('/');
@endphp
@extends('master')
@section('content')
    <reporte-arco :base_url='{{ json_encode($base_url) }}'></reporte-avance>
@endsection