@php
  $rolUser = Session::get('rol');
  $nombre = Auth::user()->persona->name .' '.  Auth::user()->persona->apellido_paterno .' '.  Auth::user()->persona->apellido_materno;
@endphp
@extends('master')
@section('content')
    <reporte-avance :img="{{ json_encode(asset('images/markers_10')) }}" :usuario='{{ json_encode($nombre) }}' :id='{{ json_encode($id) }}'></reporte-avance>     
@endsection