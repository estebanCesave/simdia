@php
  $rolUser = Session::get('rol');
@endphp
@extends('master')
@section('content')
  <!-- Content Header (Page header) -->
  <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">
                        <i class="ion ion-clipboard mr-1"></i>
                            Seleccione una huerta 
                    </h1>
                 </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="list-group text-center">
                @foreach($campos as $c)
                    <a href="{{ route('huerta', [ 'id' => $c->campo_id ]) }}" class="list-group-item list-group-item-action list-group-item-light"><i class="fas fa-feather-alt"></i> {{ $c->name }}</a>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
       $("body").addClass('sidebar-collapse');

    });
</script>
@endsection
