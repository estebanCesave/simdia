@php
  $rolUser = Session::get('rol');
@endphp
@extends('master')
@section('content')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?lang=en_RU&amp;apikey=c35c6842-1adc-47b9-9296-f8fdeb06114e" type="text/javascript"></script>
<script src="{{ asset('assets/leaflet-plugins-3.3.1\layer\tile\Yandex.js') }}"></script>
   <!-- Content Header (Page header) -->
   <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ $campo->name }}</h1>
                 </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class='row'>
                <div class="col-lg-2">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            @if(is_null($arco))
                                <h3>--</h3>
                            @else
                                <h3>{{ $arco->arco }}</h3>
                            @endif                            
                            <p>Arco</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-tree"></i>
                        </div>
                     </div>
                </div>
                <div class="col-lg-2">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ $trampas }}</h3>
                            <p>Trampas</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-home"></i>
                        </div>
                     </div>
                </div>
                <div class="col-lg-2">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            @if(is_null($superficie->superficie))
                                <h3>---</h3>
                            @else    
                                <h3>{{ $superficie->superficie }} Has.</h3>
                            @endif
                            <p>Superficie</p>
                        </div>
                        <div class="icon">
                            <!--<i class="ion ion-bag"></i>-->
                        </div>
                     </div>
                </div>
                <div class="col-lg-2">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ $estado->estado }}</h3>
                            <p>Estado</p>
                        </div>
                        <div class="icon">
                            <!--<i class="ion ion-bag"></i>-->
                        </div>
                     </div>
                </div>
                <div class="col-lg-2">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <div class="row">
                                <div class='col-sm-6'>
                                    <h3 class="semanaTitle"></h3>
                                </div>
                                <div class="col-sm-6" style="padding: 10px;">
                                    <select class="form-control" id="slcSemana"></select>
                                </div>
                            </div>
                            <p>Semana</p> 
                        </div>
                        <div class="icon">
                            <!--<i class="ion ion-bag"></i>-->
                        </div>
                     </div>
                </div>
                <div class="col-lg-2">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <div class="row">
                                <div class='col-sm-6'>
                                    <h3>{{ date('Y') }}</h3>
                                </div>
                                <div class="col-sm-6" style="padding: 10px;">
                                    <select class="form-control" id="slAno"></select>
                                </div>
                            </div>
                            <p>Año</p>
                        </div>
                        <div class="icon">
                            <!--<i class="ion ion-bag"></i>-->
                        </div>
                     </div>
                </div>
            </div>
        </div>
        <div class="row">
            <section class='col-lg-12'>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-center"><strong>Aplicación Química.</strong></h5><br>
                        @if(!is_null($aplicacionQuimica))
                        <div class='row'>
                            <div class="col-lg-3">
                            <!-- small box -->
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <h3>{{ date('d/M/Y', strtotime($aplicacionQuimica->fecha)) }}</h3>
                                        <p>Fecha</p>
                                    </div>
                                    <div class="icon">
                                        <!--<i class="fas fa-globe-americas"></i>-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                            <!-- small box -->
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <h3> {{ $aplicacionQuimica->semana }}</h3>
                                        <p>Semana</p>
                                    </div>
                                    <div class="icon">
                                        <!-- <i class="fas fa-tree"></i> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                            <!-- small box -->
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <h3>{{ $aplicacionQuimica->ano }}</h3>
                                        <p>Año</p>
                                    </div>
                                    <div class="icon">
                                       <!-- <i class="fas fa-home"></i> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                            <!-- small box -->
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <h3>{{ $aplicacionQuimica->tipo }}</h3>
                                        <p>Tipo</p>
                                    </div>
                                    <div class="icon">
                                        <!--<i class="fas fa-home"></i>-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                            <!-- small box -->
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <h3>{{ $aplicacionQuimica->producto }}</h3>
                                        <p>Producto</p>
                                    </div>
                                    <div class="icon">
                                        <!--<i class="fas fa-home"></i>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col-lg-6 connectedSortable">    
                <div class="card bg-gradient-success">
                    <div class="card-header border-0">
                        <h3 class="card-title">
                            <i class="fas fa-chart-line"></i>
                            Promedio de Adultos por trampa. 
                        </h3>
                    </div>
                    <div class="card-body">
                        @include('chunks.charts.productor.promedioTrampa')
                    </div>
                </div>
            </section>
            <section class="col-lg-6 connectedSortable">
                <div class="card bg-gradient-info">
                    <div class="card-header border-0">
                        <h3 class="card-title">
                            <i class="fas fa-chart-line"></i>
                            Información sobre la actividad de golpeteo en la Huerta 
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('chunks.charts.productor.promedioGolpeteo')
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col-lg-6 connectedSortable">
                <div class="card bg-gradient-danger">
                    <div class="card-header border-0">
                        <h3 class="card-title">
                            <i class="fas fa-map-marked-alt"></i>                               
                            Distribución Espacial de las trampas en la Huerta (Umbral). 
                        </h3>
                    </div>
                    <div class="card-body">
                        @include('chunks.maps.productor.umbral')
                    </div>
                </div>
            </section>
            <section class="col-lg-6 connectedSortable">
                <div class="card bg-gradient-warning">
                    <div class="card-header border-0">
                        <h3 class="card-title">
                            <i class="fas fa-map-marked-alt"></i>                               
                            Distribución Espacial de las trampas en la Huerta (Promedio del Arco). 
                        </h3>
                    </div>
                    <div class="card-body">
                        @include('chunks.maps.productor.arco')
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col-lg-12 connectedSortable">
                <div class="card bg-gradient-dark">
                    <div class="card-header border-0">
                        <h3 class="card-title">
                            <i class="fas fa-map-marked-alt"></i>      
                            Información sobre la actividad de golpeteo en la Huerta 
                            Semana ( <span class='semanaTitle golp'></span> )
                        </h3>
                    </div>
                    <div class="card-body">
                        @include('chunks.maps.productor.golpeteo')
                    </div>
                </div>
            </section>
        </div>   
    </section>
   
<script>

    const base_url = "{{ URL::to('/').'/productor/huerta' }}";
    var campo_id = '{!! $campo->campo_id !!}';
    var semana;
 
    $(document).ready(function () {
       if(semana == null){
           semana = moment().format("W");
       }
       
       $('.semanaTitle').html(semana);
       $('.golp').html(semana - 1);
        
       $("body").addClass('sidebar-collapse');
        getSemanas();
        $('#slcSemana').on('change',function(){
            semana = $('#slcSemana').val();
            $('.semanaTitle').html(semana)
        });

        Highcharts.theme = 
        {
            "colors": ["#00AACC", "#FF4E00", "#B90000", "#5F9B0A", "#CD6723"],
            "chart": {
                "backgroundColor": {
                "linearGradient": [
                    0,
                    0,
                    0,
                    150
                ],
                "stops": [
                        [0, "#CAE1F4"],
                        [1, "#EEEEEE"]
                    ]
                },
                "style": {
                    "fontFamily": 'bold 136px "Trebuchet MS", Verdana, sans-serif'
                }
            },
            "title": {
                "align": "center"
            },
            "subtitle": {
                "align": "center"
            },
            "legend": {
                "align": "center",
                "verticalAlign": "bottom"
            },
            "xAxis": {
                "gridLineWidth": 1,
                "gridLineColor": "#F3F3F3",
                "lineColor": "#F3F3F3",
                "minorGridLineColor": "#F3F3F3",
                "tickColor": "#F3F3F3",
                "tickWidth": 1,
                "fontFamily": 'bold 96px "Trebuchet MS", Verdana, sans-serif'

            },
            "yAxis": {
                "gridLineColor": "#F3F3F3",
                "lineColor": "#F3F3F3",
                "minorGridLineColor": "#F3F3F3",
                "tickColor": "#F3F3F3",
                "tickWidth": 1
            }
        };

        Highcharts.setOptions(Highcharts.theme);



    });
    function getSemanas(){
        axios.get(base_url+'/semanas/'+ campo_id)
        .then(function (response) {
            crear_lista("slcSemana", response.data);
         }).catch(function (error) { 
            console.log(error); 
        });
    }
    function crear_lista(slc, dato, selected) 
    {
        $("#" + slc).html('');
        $("#" + slc).append('<option value="0" selected> -- SELECCIONE -- </option>');
        
        $.each(dato, function (i, item) {
            $("#" + slc).append('<option value=' + dato[i]['semana'] + '>' + dato[i]['semana'] + '</option>');
        });
        $("#" + slc).append('<option value="2"> 2 </option>');
        $("#" + slc).append('<option value="20"> 20 </option>');

        if (selected){ 
            $("#" + slc).val(selected); 
        }
    }
</script>
@endsection