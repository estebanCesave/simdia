@php
  $rolUser = Session::get('rol');
@endphp
@extends('master')
@section('content')
<style>
 /* .ui-datepicker-calendar tr:hover td a {
    background-color: #808080;
}
.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
	 border: 1px solid #808080; 
	 background: #808080; 
	color: #ffff;
}
.dataTables_wrapper .dataTables_filter {
  float: left;
  text-align: right;
  visibility: hidden;
}*/
tfoot 
{
        display: table-header-group;
}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('actividad')}}">Home</a></li>
              <li class="breadcrumb-item active">Recuperar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<section class="content">
 <!-- Default box -->
      <div class="card">
        <center>
        <div class="card-header">
          <h3 class="card-title">Recuperar Registros</h3>
          <div class="card-tools">
            {{-- <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> --}}
          </div>
        </div>
        <div class="card-body">
              <div class="row">
                  <div class='col-md-4'>
                      @if((Auth::user()->hasRole('Admin Junta') && $rolUser == 'Admin Junta') || (Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado'))
                        <div class="form-group" id="div_fenologia">
                            <label class="col-xs-4 text-right control-label">Tecnicos:</label>
                              <select class="form-control formulario_general" id="slcUser" name="slcUser">
                              </select>
                        </div>
                      @endif      
                  </div>
                  <div class='col-md-4'>
                      <div class="form-group" id="div_trampa">
                          <label class="col-xs-4 text-right control-label"> Imei:</label>
                            <select class="form-control formulario_general" id="slcImei" name="slcImei">
                            </select>
                      </div> 
                  </div>
                  <div class='col-md-4'>
                      <div class="form-group" id="div_fenologia">
                          <label class="col-xs-4 text-right control-label"> Base de Datos:</label>
                            <select class="form-control formulario_general" id="slc_archivo" name="hospedante">
                            </select>
                      </div>
                  </div>
              </div> 
        </div> 
        </div>
        <div id="fileTable" style="display: none">
            <h6><strong>Registros Recuperados</strong>  </h6>
            <div class="row">
              <!--<div class="col-md-4">
                  <input type="text" id="datepicker" placeholder="Buscar Semana" class="form-control input-sm">
              </div>-->
              <div class="col-md-12 text-right">
                <button class="btn btn-success" id='guardar'><i class="fa fa-qrcode"></i> GUARDAR REGISTRO's</button>
              </div>
            </div>
            <table id="tbl_trampas" class="table table-responsive table-bordered table-striped">
                <tfoot>
                  <tr>
                    <th>o</th>
                    <th width="10%">SiembraID</th>
                    <th width="10%">IMEI</th>
                    <th width="10%">Fecha</th>
                    <th width="10%">Fecha Hora</th>
                    <th width="10%">Año</th>
                    <th width="10%">Semana</th>
                    <th>Latitud</th>
                    <th>Longitud</th>
                    <th>Precision</th>
                    <th width="10%">Distancia QR</th>
                    <th width="5%">status</th>
                    <th width="5%">IdCel</th>
                    <th width="5%">Trampa ID</th>
                    <th width="5%">Capturas</th>
                    <th width="5%">Fenologia ID</th>
                    <th width="5%">Instalada</th>
                    <th width="5%">Norte Adultos</th>
                    <th width="5%">Norte Ninfas</th>
                    <th width="5%">Norte Fenologia</th>
                    <th width="5%">Sur Adultos</th>
                    <th width="5%">Sur Ninfas</th>
                    <th width="5%">Sur Fenologia</th>
                    <th width="5%">Este Adultos</th>
                    <th width="5%">Este Ninfas</th>
                    <th width="5%">Este Fenologia</th>
                    <th width="5%">Oeste Adultos</th>
                    <th width="5%">Oeste Ninfas</th>
                    <th width="5%">Oeste Fenologia</th>
                  </tr>
                </tfoot>
                <thead>
                <tr>
                    <th><input id="checkAll" type="checkbox"/></th>
                    <th width="10%">SiembraID</th>
                    <th width="10%">IMEI</th>
                    <th width="10%">Fecha</th>
                    <th width="10%">Fecha Hora</th>
                    <th width="10%">Año</th>
                    <th width="10%">Semana</th>
                    <th>Latitud</th>
                    <th>Longitud</th>
                    <th>Precision</th>
                    <th width="10%">Distancia QR</th>
                    <th width="5%">status</th>
                    <th width="5%">IdCel</th>
                    <th width="5%">Trampa ID</th>
                    <th width="5%">Capturas</th>
                    <th width="5%">Fenologia ID</th>
                    <th width="5%">Instalada</th>
                    <th width="5%">Norte Adultos</th>
                    <th width="5%">Norte Ninfas</th>
                    <th width="5%">Norte Fenologia</th>
                    <th width="5%">Sur Adultos</th>
                    <th width="5%">Sur Ninfas</th>
                    <th width="5%">Sur Fenologia</th>
                    <th width="5%">Este Adultos</th>
                    <th width="5%">Este Ninfas</th>
                    <th width="5%">Este Fenologia</th>
                    <th width="5%">Oeste Adultos</th>
                    <th width="5%">Oeste Ninfas</th>
                    <th width="5%">Oeste Fenologia</th>
                </tr>
                </thead>
                <tbody></tbody>
                
            </table>
        </div>
        </div>
        <!-- /.card-body -->
        <!-- /.card-footer-->
      </center>
      </div>
      <!-- /.card -->
      </section>
<script>
const base_url = "{{ URL::to('/')."/" }}";
$(document).ready(function(){
  $('#datepicker').val('');
  var data = [];
  var json;
  //moment().format();
  getImeis();
  getUsers();
  $("body").addClass('sidebar-collapse');
  $("#tbl_trampas").DataTable({ 
      pageLength : 50, 
      lengthChange: false, 
      select: true, 
      searching: true,
      language:{ 
        "sProcessing": "Procesando...", 
        "sLengthMenu": "Mostrar _MENU_ registros por página", 
        "sZeroRecords": "No se encontraron resultados", 
        "sEmptyTable": "Ningún dato disponible en esta tabla", 
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", 
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", 
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", 
        "sInfoPostFix": "", 
        "sSearch": "Buscar:", 
        "sUrl": "", 
        "sInfoThousands": ",",
         "sLoadingRecords": "Cargando...", 
         "oPaginate": 
          { 
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
          }, 
          "oAria": 
          {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          } 
        } 
      });

    $("#checkAll").click(function(){
      $('input:checkbox.ads_Checkbox').not(this).prop('checked', this.checked);
      if(this.checked == true){
        $('#tbl_trampas tbody tr').toggleClass('selected');
      }
      else{
        $('#tbl_trampas tbody tr').removeClass('selected');
      }
    });
    $('#guardar').click( function () 
    {
      var val = [];
      $(':checkbox.ads_Checkbox:checked').each(function(i)
      {
          val[i] = $(this).val();
          var self = $('#ad_Checkbox'+val[i]).parents('tr').map(function(res) 
          {
            $(this).toggleClass( "selected" );
          });   
            
            var myTable;
            myTable = $('#tbl_trampas').DataTable();       
            var myTableSelect = myTable.rows('.selected').data().toArray();
            $.each(myTableSelect, function ( value, index ) 
            {
                json = {
                  siembra_id : index[1],
                  imei : index[2],
                  fecha : index[3],
                  fechaHora : index[4],
                  ano : index[5],
                  semana : index[6],
                  latitud : index[7],
                  longitud : index[8],
                  accuracy : index[9],
                  distancia_qr : index[10],
                  status : index[11],
                  id_bd_cel : index[12],
                  trampa_id : index[13],
                  capturas : index[14],
                  fenologia_id : index[15],
                  instalada : index[16],
                  noa : index[17],
                  non : index[18],
                  nof : index[19],
                  sua : index[20],
                  sun : index[21],
                  suf : index[22],
                  esa : index[23],
                  esn : index[24],
                  esf : index[25],
                  oea : index[26],
                  oen : index[27],
                  oef : index[28]
                }
            });
            data.push(json);
      });
      axios.post(base_url + 'saveFile',data)
        .then(function (response) {
          if(response.data.data >= 1)
          {
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Registros guardados',
              showConfirmButton: false,
              timer: 3000
            });
              $('#tbl_trampas tbody tr').removeClass('selected');
              data.length=0;
          }
          else{
            Swal.fire({
              position: 'top-end',
              type: 'error',
              title: 'Oops...',
              text: response.data.data,
              showConfirmButton: false,
              timer: 3000
            })   
          }
        }).catch(function (error) { 
          console.log(error); 
        });
    });
});
$("#slcImei").on("change", function(){
  var EspSelect = $('#slcImei').find('option:selected');

  var imei = EspSelect.data('imei');
    var json = {
      imei : ''+imei+''
    };
    axios.post(base_url + 'archivos',json)
      .then(function (response) {
        crear_lista_base("slc_archivo", response.data.data);
    }).catch(function (error) { 
      console.log(error); 
    });
});
$("#slcUser").on("change", function(){
  getImeis();
});
$("#slc_archivo").on("change", function(){
    var EspSelect = $('#slcImei').find('option:selected');
    var fileSelect = $('#slc_archivo').find('option:selected');
    var file = fileSelect.data('file');
    var imei = EspSelect.data('imei');
    var json ={
      file: file,
      imei : ''+imei+''
    }
    $('#fileTable').show();
    axios.post(base_url + 'readFile',json)
      .then(function(response){
        if(response.data.length > 0)
        {
          $('#fileTable').show();
          armar_tabla(response.data);
        }
        else{
          Swal.fire({
            position: 'top-end',
            type: 'error',
            title: 'Oops...',
            text: 'El archivo no contiene registros',
            showConfirmButton: false,
            timer: 3000
          })   
        }
      }).catch(function(error){
        console.log(error);
      })
});
function getImeis(){
  var userSelect = $('#slcUser').find('option:selected');
  var id = userSelect.data('id');

  var json ={
      userId: id,
  }

  axios.post(base_url + 'getImeis',json)
    .then(function (response) {
       crear_lista("slcImei", response.data);
    }).catch(function (error) { console.log(error); });
}
function getUsers(){
  axios.post(base_url + 'usuarios/getUsers')
        .then(function (response) {
          user_id = response.data;
          console.log(response.data);
          $("#slcUser").append('<option value="0" selected> -- SELECCIONE -- </option>');
          $.each(response.data, function (i, item) {
            console.log(item);
            var nombre = response.data[i]['name'] + ' '+ response.data[i]['apellido_paterno'] + ' '+ response.data[i]['apellido_materno'];
            $('#slcUser').append('<option value="'+response.data[i]['id'] + '" data-id="' + response.data[i]['id'] +'">'+ nombre +'</option>');           
          });
        }).catch(function (error){ 
          console.log(error); 
        });
}
function crear_lista(slc, dato, selected) {
  $("#" + slc).html('');
  $("#" + slc).append('<option value="0" selected> -- SELECCIONE -- </option>');
  $.each(dato, function (i, item) {
    $("#" + slc).append('<option value=' + dato[i]['value'] + ' data-imei='+dato[i]['name']+'>' + dato[i]['name'].toUpperCase() + '</option>');
  });
  if (selected){ 
    $("#" + slc).val(selected); 
  }
}
function crear_lista_base(slc, dato, selected){
  $("#" + slc).html('');
  $("#" + slc).append('<option value="0" selected> -- SELECCIONE -- </option>');
  $.each(dato, function (i, item) {
    $("#" + slc).append('<option value=' + item + ' data-file='+ item +'>'+ item.toUpperCase() + '</option>');
  });
  if (selected){ 
    $("#" + slc).val(selected); 
  }
}
function armar_tabla(dato)
{  
    var checkedRows = [];
    myTable = $('#tbl_trampas').dataTable();

    $('#tbl_trampas tfoot th').each(function(i,item){
         var title = $(this).text();
         console.log(title);
         if(title == 'o' || title == 'Fenologia ID' || title == 'Norte Adultos' || title == 'Norte Ninfas' || title == 'Norte Fenologia' || title == 'Sur Adultos' || title == 'Sur Ninfas' || title == 'Sur Fenologia' || title == 'Este Adultos' || title == 'Este Ninfas' || title == 'Este Fenologia' || title == 'Oeste Adultos' || title == 'Oeste Ninfas' || title == 'Oeste Fenologia'){
            $(this).html('');
          }
          else{
            $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
          }
    });
    myTable.api().columns().every(function() 
    {
        var that = this;
        $('input', this.footer()).on('keyup change clear', function () 
        {
            if (that.search() !== this.value) 
            {
              that.search(this.value).draw();
            }
        });
    });


    myTable.fnClearTable();
    $.each(dato, function(i,item){
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
      var rowNode = myTable
        .fnAddData( [
          '<input name="selector[]" id="ad_Checkbox' + dato[i]['id'] + '" class="ads_Checkbox" type="checkbox" value="' + dato[i]['id'] + '" />',
          '' + dato[i]['siembra_id'] + '',
          '' + dato[i]['imei'] + '',
          '' + dato[i]['fecha'] + '',
          '' + dato[i]['fechaHora'] + '',
          '' + dato[i]['ano'] + '',
          '' + dato[i]['semana'] + '',
          '' + dato[i]['latitud'] + '',
          '' + dato[i]['longitud'] + '',
          '' + dato[i]['accuracy'] + '',
          '' + dato[i]['distancia_qr'] + '',
          '' + dato[i]['status'] + '',
          '' + dato[i]['id_bd_cel'] + '',
          '' + dato[i]['trampa_id'] + '',
          '' + dato[i]['captura'] + '',
          '' + dato[i]['fenologia_id'] + '',
          '' + dato[i]['instalada'] + '',
          '' + dato[i]['noa'] + '',
          '' + dato[i]['non'] + '',
          '' + dato[i]['nof'] + '',
          '' + dato[i]['sua'] + '',
          '' + dato[i]['sun'] + '',
          '' + dato[i]['suf'] + '',
          '' + dato[i]['esa'] + '',
          '' + dato[i]['esn'] + '',
          '' + dato[i]['esf'] + '',
          '' + dato[i]['oea'] + '',
          '' + dato[i]['oen'] + '',
          '' + dato[i]['oef'] + '',
        ]);
      });

      $( "#datepicker" ).datepicker({
        showWeek: true,
        firstDay: 1,
        dateFormat: 'yy-mm-dd',
        changeMonth: false,
        changeYear: false,
        stepMonths: 0,
        onClose: function(selectedDate) {
          var value = selectedDate;
          var firstDate = moment(value).week();

          $("#datepicker").val(firstDate);
          myTable.dataTable().fnFilter(firstDate,6);
        }
      });
}
</script>
@endsection