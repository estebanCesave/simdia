
@php
  $rolUser = Session::get('rol');
@endphp
@extends('master')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Reporte de Autoridad Nacional | México</h1>
                 </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content connectedSortable">
        <div class="container-fluid">
            <div class='row'>
                <div class="col-lg-3">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ number_format(count($estados)) }}</h3>
                            <p>Estados</p>
                        </div>
                        <div class="icon">
                            <!--<i class="fas fa-globe-americas"></i>-->
                        </div>
                     </div>
                </div>
                <div class="col-lg-3">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ number_format(count($arcos)) }}</h3>
                            <p>Arcos</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-tree"></i>
                        </div>
                     </div>
                </div>
                <div class="col-lg-3">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ number_format(count($traspatios)) }}</h3>
                            <p>Zonas Urbanas</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-home"></i>
                        </div>
                     </div>
                </div>
                <div class="col-lg-3">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ number_format($superficie->round) }} Has.</h3>
                            <p>Superficie</p>
                        </div>
                        <div class="icon">
                            <!--<i class="ion ion-bag"></i>-->
                        </div>
                     </div>
                </div>
            </div>
            <div class="row">
                <section class='col-lg-6 connectedSortable'>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title text-center"><strong>Trampas</strong></h5><br>
                            <div class='row'>
                                <div class="col-lg-4">
                                    <!-- small box -->
                                    <div class="small-box bg-success">
                                        <div class="inner">
                                            <h3>{{ number_format($activas) }}</h3>
                                            <p>Activas</p>
                                        </div>
                                        <div class="icon">
                                            <!--<i class="fas fa-globe-americas"></i>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <!-- small box -->
                                    <div class="small-box bg-success">
                                        <div class="inner">
                                            <h3>{{ number_format($trampaArcos) }}</h3>
                                            <p>Arcos</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fas fa-tree"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <!-- small box -->
                                    <div class="small-box bg-success">
                                        <div class="inner">
                                            <h3>{{ number_format($urbanas) }}</h3>
                                            <p>Zonas Urbanas</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fas fa-home"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class='col-lg-6 connectedSortable'>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title text-center"><strong>Técnicos</strong></h5><br>
                            <div class='row'>
                                <div class="col-lg-4">
                                    <!-- small box -->
                                    <div class="small-box bg-danger">
                                        <div class="inner">
                                            <h3>{{ number_format(count($tecnicosArcos)) }}</h3>
                                            <p>Arcos</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fas fa-tree"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <!-- small box -->
                                    <div class="small-box bg-danger">
                                        <div class="inner">
                                            <h3>{{ number_format(count($tecnicosZU)) }}</h3>
                                            <p>Zonas Urbanas</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fas fa-home"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <!-- small box -->
                                    <div class="small-box bg-danger">
                                        <div class="inner">
                                            <h3>{{ number_format((count($tecnicosArcos) + count($tecnicosZU)))  }}</h3>
                                            <p>Total</p>
                                        </div>
                                        <div class="icon">
                                            <!--<i class="fas fa-home"></i>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="col-lg-6 connectedSortable">    
                    <div class="card bg-gradient-success">
                        <div class="card-header border-0">
                            <h3 class="card-title">
                                <i class="fas fa-chart-line"></i>
                                Media móvil
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('chunks.charts.autoridad.nacional.media')
                        </div>
                    </div>
                </section>
                <section class="col-lg-6 connectedSortable">
                    <div class="card bg-gradient-warning">
                        <div class="card-header border-0">
                            <h3 class="card-title">
                                <i class="fas fa-map-marked-alt"></i>                               
                                Media
                            </h3>
                        </div>
                        <div class="card-body">
                           @include('chunks.maps.autoridad.media')
                        </div>
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="col-lg-6 connectedSortable">
                    <div class="card bg-gradient-info">
                        <div class="card-header border-0">
                            <h3 class="card-title">
                                <i class="fas fa-chart-line"></i>
                                Promedios
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('chunks.charts.autoridad.nacional.promedios')
                        </div>
                    </div>
                </section>
                <!--<section class="col-lg-6 connectedSortable">
                    <div class="card bg-gradient-success">
                        <div class="card-header border-0">
                            <h3 class="card-title">
                                <i class="fas fa-map-marked-alt"></i>                               
                                Presencia
                            </h3>
                        </div>
                        <div class="card-body">
                           @include('chunks.maps.autoridad.presencia')
                        </div>
                    </div>
                </section>-->
            </div>
            <div class="row">  
                <section class="col-lg-6 connectedSortable">
                    <div class="card bg-gradient-danger">
                        <div class="card-header border-0">
                            <h3 class="card-title">
                                <i class="fas fa-chart-line"></i>
                                Comparativa años anteriores
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('chunks.charts.autoridad.nacional.anos')
                        </div>
                    </div>
                </section>
            </div>
            <div class="row">
                <section class="col-lg-12 connectedSortable">
                    <div class="card">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3">
                                <i class="fas fa-clipboard-list"></i>
                                    Tabla Principal
                            </h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content p-0">
                                <table id='principall' class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Año</th>
                                            <th>Semana</th>
                                            <th>Revisadas</th>
                                            <th>No revisadas</th>
                                            <th>Adultos diaforina</th>
                                            <th>Trampas con adultos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- /.card-body -->
                    </div>
                </section>
            </div>
    </section>
 <script>
    $(document).ready(function () {
       $("body").addClass('sidebar-collapse');
      
       $('#principall').DataTable({
        'serverSide' : true,
        'responsive' :true,
        'processing': true,
        'stateSave': false,
        'destroy' : true,
        "aLengthMenu": [ [17, 26, 52, -1], [17, 26, 52, "Todas"]],
        "ajax" : {
          url: "{{ route('tabla-principal') }}",
          type: 'POST',
          deferRender: true,
          data : {
            "_token" : "{{ csrf_token() }}",
            } 
        }, 
        'language': {
          'loadingRecords': '&nbsp;',
          'processing': 'Cargando...',
          "sLengthMenu":     "Mostrar _MENU_ trampas por página",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando trampas del _START_ al _END_ de un total de _TOTAL_ trampas",
          "sInfoEmpty":      "Mostrando trampas del 0 al 0 de un total de 0 trampas",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ trampas)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
            },
            "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          },
          "columns" : [
              { data: 'ano', name: 'ano', searchable: true },
              { data: 'semana', name: 'semana', searchable: true },
              { data: 'revisadas', name: 'revisadas', searchable: false },
              { data: 'norevisadas', name: 'norevisadas', searchable: false },
              { data: 'adultosdiaforina', name: 'adultosdiaforina', searchable: false },
              { data: 'trampasadultos', name: 'trampasadultos', searchable: false }
            ]
      });

     Highcharts.theme = 
     {
            "colors": ["#00AACC", "#FF4E00", "#B90000", "#5F9B0A", "#CD6723"],
            "chart": {
                "backgroundColor": {
                "linearGradient": [
                    0,
                    0,
                    0,
                    150
                ],
                "stops": [
                        [0, "#CAE1F4"],
                        [1, "#EEEEEE"]
                    ]
                },
                "style": {
                    "fontFamily": 'bold 136px "Trebuchet MS", Verdana, sans-serif'
                }
            },
            "title": {
                "align": "center"
            },
            "subtitle": {
                "align": "center"
            },
            "legend": {
                "align": "center",
                "verticalAlign": "bottom"
            },
            "xAxis": {
                "gridLineWidth": 1,
                "gridLineColor": "#F3F3F3",
                "lineColor": "#F3F3F3",
                "minorGridLineColor": "#F3F3F3",
                "tickColor": "#F3F3F3",
                "tickWidth": 1,
                "fontFamily": 'bold 96px "Trebuchet MS", Verdana, sans-serif'

            },
            "yAxis": {
                "gridLineColor": "#F3F3F3",
                "lineColor": "#F3F3F3",
                "minorGridLineColor": "#F3F3F3",
                "tickColor": "#F3F3F3",
                "tickWidth": 1
            }
        };

        Highcharts.setOptions(Highcharts.theme);
     
    });
</script>
@endsection
