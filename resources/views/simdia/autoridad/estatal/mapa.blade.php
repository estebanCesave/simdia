
@extends('master')
@section('content')
<div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Seleccione un Estado</h1>
                 </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
            <div class="text-center">
                <img src="{{ asset('images/mapa_mexico.png') }}" usemap="#Map" /></div>
                <map id="Map" name="Map">
                    <area coords="144,121,215,160" href="{{ route('estado', [ 'id' => 1 ]) }}" shape="rect" alt="Sonora"/> <!-- SONORA -->
                    <area coords="85,196,137,220" href="{{ route('estado', [ 'id' => 2 ]) }}" shape="rect" alt="BCS" /> <!-- BCS -->
					<area coords="39,73,76,98" href="{{ route('estado', [ 'id' => 21 ]) }}" shape="rect" alt="BC" /> <!-- BCN -->
					<area coords="273,345,318,366" href="{{ route('estado', [ 'id' => 3 ]) }}" shape="rect" alt="NAYARIT" /> <!-- NAYARIT -->
					<area coords="670,359,717,376" href="{{ route('estado', [ 'id' => 4 ]) }}" shape="rect" alt="YUCATAN" /> <!-- YUCATAN -->
					<area coords="492,480,548,502" href="{{ route('estado', [ 'id' => 5 ]) }}" shape="rect" alt="OAXACA" /> <!-- OAXACA -->
					<area coords="291,428,334,442" href="{{ route('estado', [ 'id' => 6 ]) }}" shape="rect" alt="COLIMA" /> <!-- COLIMA -->
					<area coords="347,428,407,448" href="{{ route('estado', [ 'id' => 7 ]) }}" shape="rect" alt="MICHOACAN" /> <!-- MICHOACAN -->
					<area coords="439,433,455,448" href="{{ route('estado', [ 'id' => 8 ]) }}" shape="rect" alt="MORELOS" /> <!-- MORELOS -->
					<area coords="457,427,499,450" href="{{ route('estado', [ 'id' => 9 ]) }}" shape="rect" alt="PUEBLA" /> <!-- PUEBLA -->
					<area coords="295,390,344,409" href="{{ route('estado', [ 'id' => 10 ]) }}" shape="rect" alt="JALISCO" /> <!-- JALISCO -->
					<area coords="620,410,685,428" href="{{ route('estado', [ 'id' => 11 ]) }}" shape="rect" alt="CAMPECHE" /> <!-- CAMPECHE -->
					<area coords="387,333,426,353" href="{{ route('estado', [ 'id' => 12 ]) }}" shape="rect" alt="SLP" /> <!-- SLP -->
					<area coords="195,239,248,264" href="{{ route('estado', [ 'id' => 13 ]) }}" shape="rect" alt="SINALOA" /> <!-- SINALOA -->
					<area coords="593,443,624,459" href="{{ route('estado', [ 'id' => 14 ]) }}" shape="rect" alt="TABASCO" /> <!-- TABASCO -->
					<area coords="590,479,644,498" href="{{ route('estado', [ 'id' => 15 ]) }}" shape="rect" alt="CHIAPAS" /> <!-- CHIAPAS -->
					<area coords="435,384,465,403" href="{{ route('estado', [ 'id' => 16 ]) }}" shape="rect" alt="HIDALGO" /> <!-- HIDALGO -->
					<area coords="490,409,546,433" href="{{ route('estado', [ 'id' => 17 ]) }}" shape="rect" alt="VERACRUZ" /> <!-- VERACRUZ -->
					<area coords="398,456,458,478" href="{{ route('estado', [ 'id' => 22 ]) }}" shape="rect" alt="GUERRERO" /> <!-- GUERRERO -->
					<area coords="397,237,448,271" href="{{ route('estado', [ 'id' => 18 ]) }}" shape="rect" alt="NUEVO LEON" /> <!-- NUEVO LEON -->
					<area coords="690,400,739,427" href="{{ route('estado', [ 'id' => 24 ]) }}" shape="rect" alt="QUINTANA ROO" /> <!-- QUINTANA ROO -->
					<area coords="411,382,430,398" href="{{ route('estado', [ 'id' => 23]) }}" shape="rect" alt="QUERETARO" /> <!-- QUERETARO -->
					<area coords="427,309,490,329" href="{{ route('estado', [ 'id' => 25 ]) }}" shape="rect" alt="TAMAULIPAS" /> <!-- TAMAULIPAS -->
					<area coords="316,304,371,334" href="{{ route('estado', [ 'id' => 72 ]) }}" shape="rect" alt="ZACATECAS" /> <!-- ZACATECAS -->
                </map>
            </div>
        </div>
    </section>
</div>
<script>
$(document).ready(function () {
    $("body").addClass('sidebar-collapse');
});
</script>
@endsection
