
@extends('master')
@section('content')
<div class="container">
    <br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="row justify-content-center">
                <div class="card">
                    <div style="color: #FE9A2E;" class="card-header">Seleccionar Nivel</div>
                    <div class="card-body" style="width: 300px !important;">
                        <form method="POST" action="{{ route('save-rol') }}">
                            @csrf
                            <div class="form-group row">
                                 <div class="col-md-12">
                                    <select id='rolUser' name='rolUser' class="form-control formulario_general"> 
                                        @foreach($roles as $r)
                                            <option value="{{ $r->name }}">{{ $r->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-12">
                                    <button type="submit" class="btn btn-primary" style="width: 100%; background-color: #FE9A2E;">
                                        {{ __('Entrar') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection