@php
  $rolUser = Session::get('rol');
@endphp
@extends('master')
@section('content')
  <etiquetas :rol='{{ json_encode($rolUser) }}'></etiquetas>
@endsection