@php
  $rolUser = Session::get('rol');
  $user_id = Auth::user()->id;
@endphp
@extends('master')
@section('content')
    <actividad-control :rol='{{ json_encode($rolUser) }}' :user_id='{{ json_encode($user_id) }}'></actividad-control>
@endsection