@php
  $rolUser = Session::get('rol');
  $user_id = Auth::user()->id;
@endphp
@extends('master')
@section('content')
    <actividad-trampeo :rol='{{ json_encode($rolUser) }}' :user_id='{{ json_encode($user_id) }}'></actividad-trampeo>
@endsection