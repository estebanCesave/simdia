@php
  $rolUser = Session::get('rol');
  $base_url = URL::to('/');

@endphp
@extends('master')
@section('content')
    <usuarios :rol='{{ json_encode($rolUser) }}' :base_url='{{ json_encode($base_url) }}'></usuarios>
@endsection