<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td class="content-cell" align="center">
                    <a href="http://www.siafeson.com/">
                        <img src="http://www.siafeson.com/assets/customs/simdia/img/logos/siafeson.png" width="40px" height="40px">
                    </a>
                </td>
                <td class="content-cell" align="center">
                    © {{ date('Y') }} siafeson. @lang('All rights reserved.')
                </td>
                <td class="content-cell" align="center">
                    <a href="http://www.cesaveson.com/">
                        <img src='http://www.siafeson.com/assets/customs/simdia/img/logos/cesave.png' width="40px" height="40px">
                    </a>
                </td>
            </tr>
        </table>
    </td>
</tr>
