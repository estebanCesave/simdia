<div class="table">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Fecha</th>
                <th scope="col">Campo</th>
                <th scope="col">Asignado</th>
                <th scope="col">Promedio</th>
            </tr>
        </thead>
        <tbody>
            @foreach($focos as $f)
            <tr>
                <td align="center">{{ date('d-m-Y',strtotime($f->fecha_det) ) }}</td>
                <td align="center">{{ $f->campo }}</td>
                <td align="center">{{ $f->nombre }} {{ $f->apellido_paterno }} {{ $f->apellido_materno }}</td>
                <td align="center">{{ $f->promedio }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
