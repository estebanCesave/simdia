{{-- @extends('layouts.app') --}}
@extends('master')
@section('content')
<div class="container">
    <div>
        <br>
        <br>
        <br>
        <br>
    </div>
    <div class="row justify-content-center">
        <div >
            <div class="card">
                <div style="color: #FE9A2E;" class="card-header">{{ __('Sistema de Monitoreo de Diaphorina') }}</div>

                <div class="card-body" style="width: 300px !important;">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" style="width: 260px !important;" placeholder="Usuario" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">                          
                            <div class="col-md-9">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Contraseña" name="password" required autocomplete="current-password" style="width: 260px !important;">
                                <!--<input placeholder='Contraseña' type='text' name="fake_pass" id="fake_pass" class="form-control" required="" style="display:none; width: 260px !important;">-->

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-3">
                                <!--<i id="password_reveal" style="margin-top:10px" class="fa fa-eye"></i>-->
                            </div>
                        
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-12">
                                <button type="submit" class="btn btn-primary" style="width: 100%; background-color: #FE9A2E;">
                                    {{ __('Entrar') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{-- {{ __('Forgot Your Password?') }} --}}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script>
  $(document).ready(function(){

    $("#password").on("keypress, keyup", function(){
      $("#fake_pass").val($("#password").val());
    })
  
    $("#password_reveal").on("mousedown", function(){
      $("#fake_pass").show();
      $("#password").hide();
    });

    $("#password_reveal").on("mouseup", function(){
      $("#fake_pass").hide();
      $("#password").show();
    });

    $("#password").trigger('keypress').trigger('keyup');

  });
</script>-->

@endsection
