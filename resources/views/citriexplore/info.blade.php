<link rel="stylesheet" href="{{ URL::to('/') }}/css/app.css">
<style>
    
</style>
<table class="table table-bordered">
    <tr>
        <th class="p-2 m-0">Campo:</th>
        <th class="p-2 m-0">{{ $arbol->campo }}</th>
    </tr>
    <tr>
        <th class="p-2 m-0">Propietario:</th>
        <th class="p-2 m-0">{{ $arbol->propietario }}</th>
    </tr>
    <tr>
        <th class="p-2 m-0">Variedad:</th>
        <th class="p-2 m-0">{{ $arbol->variedad }}</th>
    </tr>
    <tr>
        <th class="p-2 m-0">Superficie:</th>
        <th class="p-2 m-0">{{ $arbol->superficie }}</th>
    </tr>
    <tr>
        <th class="p-2 m-0">Árbol N°:</th>
        <th class="p-2 m-0">{{ $arbol->secuencial }}</th>
    </tr>
    <tr>
        <th class="p-2 m-0">Latitud:</th>
        <th class="p-2 m-0">{{ $arbol->lat }}</th>
    </tr>
    <tr>
        <th class="p-2 m-0">Longitud:</th>
        <th class="p-2 m-0">{{ $arbol->lng }}</th>
    </tr>
    <tr>
        <th colspan="2" class="p-2 m-0"><center><strong>Exploración</strong></center></th>
    </tr>
    <tr>
        <th class="p-2 m-0">Sínt. HLB:</th>
        <th class="p-2 m-0">
            @if ($arbol->hlb > 0)
                <span><img src="{{ URL::to('/') }}/images/markers_10/FF4500.png" alt=""> POSITIVO</span>
            @else
                <span><img src="{{ URL::to('/') }}/images/markers_10/00FA9A.png" alt=""> NEGATIVO</span>
            @endif
        </th>
    </tr>
    <tr>
        <th colspan="2" class="p-2 m-0"><center><strong>Monitoreo de psílidos</strong></center></th>
    </tr>
    <tr>
        <th class="p-2 m-0">Adultos:</th>
        <th class="p-2 m-0">
            @if ($arbol->psa > 0)
                <span><img src="{{ URL::to('/') }}/images/markers_10/FF4500.png" alt=""> POSITIVO</span>
            @else
                <span><img src="{{ URL::to('/') }}/images/markers_10/00FA9A.png" alt=""> NEGATIVO</span>
            @endif
        </th>
    </tr>
    <tr>
        <th class="p-2 m-0">Ninfas:</th>
        <th class="p-2 m-0">
            @if ($arbol->psn > 0)
                <span><img src="{{ URL::to('/') }}/images/markers_10/FF4500.png" alt=""> POSITIVO</span>
            @else
                <span><img src="{{ URL::to('/') }}/images/markers_10/00FA9A.png" alt=""> NEGATIVO</span>
            @endif
        </th>
    </tr>
    <tr>
        <th colspan="2" class="p-2 m-0"><center><strong>Monitoreo estratificado</strong></center></th>
    </tr>
    <tr>
        <th class="p-2 m-0">Alto Adul.:</th>
        <th class="p-2 m-0">
            @if ($arbol->eaa > 0)
                <span><img src="{{ URL::to('/') }}/images/markers_10/FF4500.png" alt=""> POSITIVO</span>
            @else
                <span><img src="{{ URL::to('/') }}/images/markers_10/00FA9A.png" alt=""> NEGATIVO</span>
            @endif
        </th>
    </tr>
    <tr>
        <th class="p-2 m-0">Alto Ninf.:</th>
        <th class="p-2 m-0">
            @if ($arbol->ean > 0)
                <span><img src="{{ URL::to('/') }}/images/markers_10/FF4500.png" alt=""> POSITIVO</span>
            @else
                <span><img src="{{ URL::to('/') }}/images/markers_10/00FA9A.png" alt=""> NEGATIVO</span>
            @endif
        </th>
    </tr>
    <tr>
        <th class="p-2 m-0">Medio Adul.:</th>
        <th class="p-2 m-0">
            @if ($arbol->ema > 0)
                <span><img src="{{ URL::to('/') }}/images/markers_10/FF4500.png" alt=""> POSITIVO</span>
            @else
                <span><img src="{{ URL::to('/') }}/images/markers_10/00FA9A.png" alt=""> NEGATIVO</span>
            @endif
        </th>
    </tr>
    <tr>
        <th class="p-2 m-0">Medio Ninf.:</th>
        <th class="p-2 m-0">
            @if ($arbol->emn > 0)
                <span><img src="{{ URL::to('/') }}/images/markers_10/FF4500.png" alt=""> POSITIVO</span>
            @else
                <span><img src="{{ URL::to('/') }}/images/markers_10/00FA9A.png" alt=""> NEGATIVO</span>
            @endif
        </th>
    </tr>
    <tr>
        <th class="p-2 m-0">Bajo Adul.:</th>
        <th class="p-2 m-0">
            @if ($arbol->eba > 0)
                <span><img src="{{ URL::to('/') }}/images/markers_10/FF4500.png" alt=""> POSITIVO</span>
            @else
                <span><img src="{{ URL::to('/') }}/images/markers_10/00FA9A.png" alt=""> NEGATIVO</span>
            @endif
        </th>
    </tr>
    <tr>
        <th class="p-2 m-0">Bajo Ninf.:</th>
        <th class="p-2 m-0">
            @if ($arbol->ebn > 0)
                <span><img src="{{ URL::to('/') }}/images/markers_10/FF4500.png" alt=""> POSITIVO</span>
            @else
                <span><img src="{{ URL::to('/') }}/images/markers_10/00FA9A.png" alt=""> NEGATIVO</span>
            @endif
        </th>
    </tr>
</table>
<script src="{{ URL::to('/') }}/js/app.js"></script>