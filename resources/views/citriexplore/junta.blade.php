<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CITRIEXPLORE | SIAFESON</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ URL::to('/') }}/css/app.css">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJJNwg8QiNyKdljatuh7J8sia-3-H4vL4&sensor=false"></script>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/theme/plugins/sweetalert2/sweetalert2.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>
<style>
    body {
        background-image: url("{{ URL::to('/') }}/images/fondoblanco1.png");
        background-color: #ffffff;
    }
    #home { margin-top: 20px !important; }
    #infomacion { margin-top: 18px !important; }
    .titulos{ font-weight: bold !important; }
    .subtitulo{ color: #D0CCBF !important; }
    .izq{ text-align: right !important; }
    .map {height: 450px; border-style: solid; }
    .right{ float: right; margin-right: 10px; }
    .giralo90{
	    -ms-transform: rotate(90deg); /* IE 9 */
        -webkit-transform: rotate(90deg); /* Chrome, Safari, Opera */
         transform: rotate(90deg);
    }
</style>
<body>
    <div>
        
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <!-- Brand -->
            <img src="{{ URL::to('/') }}/images/orange.png" alt="Logo" style="width:40px;">&nbsp;&nbsp;<a class="navbar-brand" style="color: #ff6f00 !important; font-weight: bold !important;">CITRIXPLOR</a>
            <!-- <ul class="nav navbar-nav ml-auto">
                <form class="form-inline" class="ml-auto">
                    <input id="imei" value="867970030568428" class="form-control mr-sm-2" type="text"  autocomplete="off" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" maxlength="15" placeholder="Inserte IMEI">
                    <a id="btnBuscar" class="btn btn-success">Buscar</a>
                </form> -->
            </ul>

        </nav>

        <div id="infomacion" class="container-fluid">
        
            <div class="row">
                <div class="col-md-12">
                    <h3 style="margin-left: 20px;">Información de registros</h3>
                    <hr style="margin: 10px !important;">
                    <div class="float-right" style="margin-top: -4.3% !important;">
                        <form class="form-inline" class="ml-auto">
                            <input id="fini" class="form-control datepicker" readonly="readonly" style="margin-right: 5px !important;"/>
                            <input id="ffin" class="form-control datepicker" readonly="readonly" style="margin-right: 5px !important;"/>
                            <select id="junta_id" class="form-control">
                                <option value="0">Todas las juntas</option>
                                <option value="1">JLSV Huatabampo</option>
                                <option value="2">JLSV Hermosillo</option>
                                <option value="3">JLSV Yaqui</option>
                                <option value="4">JLSV Guaymas</option>
                                <option value="73">JLSV Navojoa</option>
                            </select>
                            &nbsp;&nbsp;
                            <a id="refresh" class="btn btn-primary"><i class="fas fa-sync"></i></a>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered table-sm" bgcolor="#ffffff" >
                    <thead>
                        <tr>
                            <th width="25%"><b>Campos totales: </b><span id="lblCampostotales" class="right">0</span></th>
                            <th width="25%"><b>Campos revisados: </b><span id="lblCamposrevisados" class="right">0</span></th>
                            <th width="25%"><b>Total de arboles: </b><span id="lblTotalarboles" class="right">0</span></th>
                            <th width="25%"><b>Registros capturados: </b><span id="lblRegistros" class="right">0</span></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th><b>Campos positivos a <span data-toggle="tooltip" title="Huanglongbing de los Cítricos" >HLB</span>: </b><span id="lblCamposHlb" class="right">0</span></th>
                            <th><b>Campos con adultos: </b><span id="lblCamposAdultos" class="right">0</span></th>
                            <th><b>Campos con ninfas: </b><span id="lblCamposNinfas" class="right">0</span></th>
                            <th><b>Campos con <span data-toggle="tooltip" title="Monitoreo Estratificado Alto Adultos" >MEAA</span>: </b><span id="lblCamposMEAA" class="right">0</span></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th><b>Campos con <span data-toggle="tooltip" title="Monitoreo Estratificado Alto Ninfas" >MEAN</span>: </b><span id="lblCamposMEAN" class="right">0</span></th>
                            <th><b>Campos con <span data-toggle="tooltip" title="Monitoreo Estratificado Medio Adultos" >MEMA</span>: </b><span id="lblCamposMEMA" class="right">0</span></th>
                            <th><b>Campos con <span data-toggle="tooltip" title="Monitoreo Estratificado Medio Ninfas" >MEMN</span>: </b><span id="lblCamposMEMN" class="right">0</span></th>
                            <th><b>Campos con <span data-toggle="tooltip" title="Monitoreo Estratificado Bajo Adultos" >MEBA</span>: </b><span id="lblCamposMEBA" class="right">0</span></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th><b>Campos con <span data-toggle="tooltip" title="Monitoreo Estratificado Bajo Ninfas" >MEBN</span>: </b><span id="lblCamposMEBN" class="right">0</span></th>
                            <th><b>Árboles positivos a <span data-toggle="tooltip" title="Huanglongbing de los Cítricos" >HLB</span>: </b><span id="lblArbolesHlb" class="right">0</span></th>
                            <th><b>Árboles con adultos: </b><span id="lblArbolesAdultos" class="right">0</span></th>
                            <th><b>Árboles con ninfas: </b><span id="lblArbolesNinfas" class="right">0</span></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th><b>Árboles con <span data-toggle="tooltip" title="Monitoreo Estratificado Alto Adultos" >MEAA</span>: </b><span id="lblArbolesMEAA" class="right">0</span></th>
                            <th><b>Árboles con <span data-toggle="tooltip" title="Monitoreo Estratificado Alto Ninfas" >MEAN</span>: </b><span id="lblArbolesMEAN" class="right">0</span></th>
                            <th><b>Árboles con <span data-toggle="tooltip" title="Monitoreo Estratificado Medio Adultos" >MEMA</span>: </b><span id="lblArbolesMEMA" class="right">0</span></th>
                            <th><b>Árboles con <span data-toggle="tooltip" title="Monitoreo Estratificado Medio Ninfas" >MEMN</span>: </b><span id="lblArbolesMEMN" class="right">0</span></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th><b>Árboles con <span data-toggle="tooltip" title="Monitoreo Estratificado Bajo Adultos" >MEBA</span>: </b><span id="lblArbolesMEBA" class="right">0</span></th>
                            <th><b>Árboles con <span data-toggle="tooltip" title="Monitoreo Estratificado Bajo Ninfas" >MEBN</span>: </b><span id="lblArbolesMEBN" class="right">0</span></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;">
                <div class="row">
                    <div class="col-md-4">
                        <div id="DivMapHLB">
                            <h3 class="titulos">Exploración síntomas HLB</h3>
                            <div id="mapHLB" class="map"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="DivMapPSI">
                            <h3 class="titulos">Monitoreo de psílidos</h3>
                            <div id="mapPSI" class="map"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="DivMapEST">
                            <h3 class="titulos">Monitoreo estratificado</h3>
                            <div id="mapEST" class="map"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="DivMEInfo" class="col-md-12" style="margin-bottom: 20px;">
                    <h3 class="titulos">Monitoreo estratificado. (árboles con presencia)</h3>
                    <div class="float-right" style="margin-top: -3% !important;">
                        <form class="form-inline" class="ml-auto">
                            <select id="campo_id" class="form-control">
                                <option value="0"> -- TODOS LOS CAMPOS -- </option>
                            </select>
                        </form>
                    </div>
                    <hr style="margin: 10px !important;">

                    <img src="{{ URL::to('/images/') }}/arbolcitrico.png"></img>

                    <hr style="width: 65% !important; border-top: 3px solid red; margin: 10px !important; position: absolute; left: 30%; top: 30%;">
                    <hr style="width: 65% !important; border-top: 3px solid red; margin: 10px !important; position: absolute; left: 30%; top: 55%;">

                    <h2 class="animated bounceInLeft titulos" style="position: absolute; left: 50%; top: 10%;">ADULTOS</h2>
                    <h2 class="animated bounceInLeft titulos" style="position: absolute; left: 75%; top: 10%;">NINFAS</h2>

                    <h3 class="animated bounceInLeft subtitulo giralo90" style="position: absolute; left: 90%; top: 20%;">ALTO</h3>
                    <h3 class="animated bounceInLeft subtitulo giralo90" style="position: absolute; left: 89.3%; top: 42%;">MEDIO</h3>
                    <h3 class="animated bounceInLeft subtitulo giralo90" style="position: absolute; left: 90%; top: 65%;">BAJO</h3>

                    <h1 id="lblME_AA" class="animated bounceInLeft" style="position: absolute; left: 53%; top: 22%;">0</h1>
                    <h1 id="lblME_AN" class="animated bounceInLeft" style="position: absolute; left: 77%; top: 22%;">0</h1>
                    <h1 id="lblME_MA" class="animated bounceInLeft" style="position: absolute; left: 53%; top: 41%;">0</h1>
                    <h1 id="lblME_MN" class="animated bounceInLeft" style="position: absolute; left: 77%; top: 41%;">0</h1>
                    <h1 id="lblME_BA" class="animated bounceInLeft" style="position: absolute; left: 53%; top: 64%;">0</h1>
                    <h1 id="lblME_BN" class="animated bounceInLeft" style="position: absolute; left: 77%; top: 64%;">0</h1>

            </div>

            <div id="DivTblMunicipios" class="col-md-12">
                <h2 class="titulos">Actividad en campo</h2>
                <hr style="margin: 10px !important;">
                <h3 class="titulos">Municipios</h3>
                <table id="tblMunicipios" class="table table-striped table-bordered table-hover table-sm" >
                    <thead class="text-white bg-dark" >
                        <th class="text-white">Municipio</th>
                        <th class="text-white"># campos rev.</th>
                        <th class="text-white"># árboles rev.</th>
                        <th class="text-white"># registros</th>
                    </thead>
                    <tbody id="registros" class="table-light"></tbody>
                </table>
                <hr style="margin: 10px !important;">
            </div>

            <div id="DivTblJuntas" class="col-md-12">
                <h3 class="titulos">Juntas</h3>
                <table id="tblJuntas" class="table table-striped table-bordered table-hover table-sm" >
                    <thead class="text-white bg-dark" >
                        <th class="text-white">JLSV</th>
                        <th class="text-white"># campos rev.</th>
                        <th class="text-white"># árboles rev.</th>
                        <th class="text-white"># registros</th>
                    </thead>
                    <tbody id="registros" class="table-light"></tbody>
                </table>
                <hr style="margin: 10px !important;">
            </div>

            <div id="DivTblTécnicos" class="col-md-12">
                <h3 class="titulos">Técnicos</h3>
                <table id="tblTecnicos" class="table table-striped table-bordered table-hover table-sm" >
                    <thead class="text-white bg-dark" >
                        <th class="text-white" width="30%">Técnico</th>
                        <th class="text-white">JLSV</th>
                        <th class="text-white"># campos rev.</th>
                        <th class="text-white"># árboles rev.</th>
                        <th class="text-white"># registros</th>
                    </thead>
                    <tbody id="registros" class="table-light"></tbody>
                </table>
                <hr style="margin: 10px !important;">
            </div>

        </div>
        
    </div>
</body>
    <script src="{{ URL::to('/') }}/js/app.js"></script>
    <script src="{{ URL::to('/') }}/js/jquery.blockUI.js"></script>
    <!-- DataTables -->
    <script src="{{ URL::to('/') }}/theme/plugins/datatables/jquery.dataTables.js"></script>
    <script src="{{ URL::to('/') }}/theme/plugins/datatables/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert2 -->
    <script src="{{ URL::to('/') }}/theme/plugins/sweetalert2/sweetalert2.min.js"></script>
    <script>

        const base_url = "{{ URL::to('/') }}";
        const base_u    = "";
        var resumenes;
        var arboles;
        var myLatLng = {lat: 23.0000000, lng:  -102.0000000};
        var mapHLB;  
        var mapPSI;  
        var mapEST;  
        var gm_puntosHLB = []; 
        var gm_puntosPSI = []; 
        var gm_puntosEST = []; 
        var iterator;
        var ano = "<?php echo date("Y"); ?>";
        var semana = "<?php echo date("W"); ?>";
        var siembra_id;
        var infoWindow;
        var pol;
        var myTable;
        var junta_id;
        var campo_id = 0;
        var fini;
        var ffin;

        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '< Ant',
            nextText: 'Sig >',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);

        $(document).ready(function () {

            $('[data-toggle="tooltip"]').tooltip(); 
            var $datepicker = $('#ffin');
            $datepicker.datepicker();
            $datepicker.datepicker('setDate', new Date());
            var $datepicker = $('#fini');
            var d = new Date();
            $datepicker.datepicker();
            $datepicker.datepicker('setDate', sumarDias(d, -30));

            $('#tblMunicipios, #tblJuntas, #tblTecnicos ').DataTable({
                language : {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "info": false
            });

            mapHLB = new google.maps.Map(document.getElementById('mapHLB'), { center: myLatLng, zoom: 6, mapTypeId: google.maps.MapTypeId.SATELLITE, mapTypeControl: false });
            mapPSI = new google.maps.Map(document.getElementById('mapPSI'), { center: myLatLng, zoom: 6, mapTypeId: google.maps.MapTypeId.SATELLITE, mapTypeControl: false });
            mapEST = new google.maps.Map(document.getElementById('mapEST'), { center: myLatLng, zoom: 6, mapTypeId: google.maps.MapTypeId.SATELLITE, mapTypeControl: false });

            infoWindow = new google.maps.InfoWindow;

            $("#refresh").click(function(){
                junta_id = $("#junta_id").val();
                fini = $("#fini").val();
                ffin = $("#ffin").val();
                if(fini <= ffin){
                    arboles = [];
                    loadingResumen(junta_id, fini, ffin);
                    obtenerDetalles(junta_id, fini, ffin);
                    getMECampos(junta_id, fini, ffin);
                    getMEResumen(junta_id, fini, ffin, campo_id);
                }else{

                }
            });

            $("#campo_id").change(function(){
                campo_id = $("#campo_id").val();
                getMEResumen(junta_id, fini, ffin, campo_id);
            });

            junta_id = $("#junta_id").val();
            fini = $("#fini").val();
            ffin = $("#ffin").val();
            loadingResumen(junta_id, fini, ffin);
            obtenerDetalles(junta_id, fini, ffin);
            getMECampos(junta_id, fini, ffin);
            getMEResumen(junta_id, fini, ffin, campo_id);
            tblActividadMun(junta_id, fini, ffin);
            tblActividadJunta(junta_id, fini, ffin);
            tblActividadTecnico(junta_id, fini, ffin);

        });

        function tblActividadMun(junta_id, fini, ffin){
            $('#DivTblMunicipios').block({ message: '<img src="' + base_url + '/images/loader.gif"></img><strong> Procesando...</strong>' });
            var json = { junta_id: junta_id, fini: fini, ffin: ffin};
            axios.post(base_url + '/citrixplor/tblActividadMun', json)
            .then(function (response) {
                var d = response.data;
                console.log("tblMunicipio", d);
                var myTable = $('#tblMunicipios').dataTable();       
                myTable.fnClearTable();
                $.each(d, function(i,item){
                    var rowNode = myTable
                    .fnAddData( [ 
                        '<b>' + d[i]['municipio'] + '</b>',
                        '<i>' + d[i]['campos_rev'] + '</i>',
                        '<i>' + d[i]['arboles'] + '</i>',
                        '<i>' + d[i]['registros'] + '</i>'
                    ]);
                });
            })
            .catch(function (error) { console.log("Petición de tblMunicipio: ", error);})
            .finally(function () { });
            $("#DivTblMunicipios").unblock();
        }

        function tblActividadJunta(junta_id, fini, ffin){
            $('#DivTblJuntas').block({ message: '<img src="' + base_url + '/images/loader.gif"></img><strong> Procesando...</strong>' });
            var json = { junta_id: junta_id, fini: fini, ffin: ffin};
            axios.post(base_url + '/citrixplor/tblActividadJunta', json)
            .then(function (response) {
                var d = response.data;
                console.log("tblJunta", d);
                var myTable = $('#tblJuntas').dataTable();       
                myTable.fnClearTable();
                $.each(d, function(i,item){
                    var rowNode = myTable
                    .fnAddData( [ 
                        '<b>' + d[i]['jlsv'] + '</b>',
                        '<i>' + d[i]['campos_rev'] + '</i>',
                        '<i>' + d[i]['arboles'] + '</i>',
                        '<i>' + d[i]['registros'] + '</i>'
                    ]);
                });
            })
            .catch(function (error) { console.log("Petición de tblJunta: ", error);})
            .finally(function () { });
            $("#DivTblJuntas").unblock();
        }

        function tblActividadTecnico(junta_id, fini, ffin){
            $('#DivTblTécnicos').block({ message: '<img src="' + base_url + '/images/loader.gif"></img><strong> Procesando...</strong>' });
            var json = { junta_id: junta_id, fini: fini, ffin: ffin};
            axios.post(base_url + '/citrixplor/tblActividadTecnico', json)
            .then(function (response) {
                var d = response.data;
                console.log("tblTecnico", d);
                var myTable = $('#tblTecnicos').dataTable();       
                myTable.fnClearTable();
                $.each(d, function(i,item){
                    var rowNode = myTable
                    .fnAddData( [ 
                        '<b>' + d[i]['tecnico'] + '</b>',
                        '<b>' + d[i]['jlsv'] + '</b>',
                        '<i>' + d[i]['campos_rev'] + '</i>',
                        '<i>' + d[i]['arboles'] + '</i>',
                        '<i>' + d[i]['registros'] + '</i>'
                    ]);
                });
            })
            .catch(function (error) { console.log("Petición de tblJunta: ", error);})
            .finally(function () { });
            $("#DivTblTécnicos").unblock();
        }

        function loadingResumen(junta_id, fini, ffin){

            $('#lblCampostotales').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblCamposrevisados').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblTotalarboles').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblRegistros').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');

            $('#lblCamposHlb').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblCamposAdultos').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblCamposNinfas').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblCamposMEAA').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');

            $('#lblCamposMEAN').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblCamposMEMA').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblCamposMEMN').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblCamposMEBA').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');

            $('#lblCamposMEBN').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblArbolesHlb').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblArbolesAdultos').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblArbolesNinfas').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');

            $('#lblArbolesMEAA').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblArbolesMEAN').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblArbolesMEMA').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblArbolesMEMN').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');

            $('#lblArbolesMEBA').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');
            $('#lblArbolesMEBN').html('').html('<img src="' + base_url + '/images/loader.gif" height="15" width="15"></img>');

            var json = { junta_id: junta_id, fini: fini, ffin: ffin};
            axios.post(base_url + '/citrixplor/resumen', json)
            .then(function (response) {
                var resumen = response.data;
                //console.log("Resumen", resumen);

                $('#lblCampostotales').html('').html(resumen[0].campos_total);
                $('#lblCamposrevisados').html('').html(resumen[0].campos_rev);
                $('#lblTotalarboles').html('').html(resumen[0].arboles);
                $('#lblRegistros').html('').html(resumen[0].registros);

                $('#lblCamposHlb').html('').html(resumen[0].camposposhlb);
                $('#lblCamposAdultos').html('').html(resumen[0].camposconadultos);
                $('#lblCamposNinfas').html('').html(resumen[0].camposconninfas);
                $('#lblCamposMEAA').html('').html(resumen[0].camposmeaa);

                $('#lblCamposMEAN').html('').html(resumen[0].camposmean);
                $('#lblCamposMEMA').html('').html(resumen[0].camposmema);
                $('#lblCamposMEMN').html('').html(resumen[0].camposmemn);
                $('#lblCamposMEBA').html('').html(resumen[0].camposmeba);

                $('#lblCamposMEBN').html('').html(resumen[0].camposmebn);
                $('#lblArbolesHlb').html('').html(resumen[0].arboleshlb);
                $('#lblArbolesAdultos').html('').html(resumen[0].arbolesconadultos);
                $('#lblArbolesNinfas').html('').html(resumen[0].arbolesconninfas);

                $('#lblArbolesMEAA').html('').html(resumen[0].arbolesmeaa);
                $('#lblArbolesMEAN').html('').html(resumen[0].arbolesmean);
                $('#lblArbolesMEMA').html('').html(resumen[0].arbolesmema);
                $('#lblArbolesMEMN').html('').html(resumen[0].arbolesmemn);

                $('#lblArbolesMEBA').html('').html(resumen[0].arbolesmeba);
                $('#lblArbolesMEBN').html('').html(resumen[0].arbolesmebn);

            })
            .catch(function (error) { console.log("Petición de resumen: ", error);})
            .finally(function () { });

        }

        function getMECampos(junta_id, fini, ffin){
            campo_id = 0;
            var json = { junta_id: junta_id, fini: fini, ffin: ffin};
            axios.post(base_url + '/citrixplor/meresumencampos', json)
            .then(function (response) {
                var campos = response.data;
                //console.log("Campos", campos);
                if(campos.length > 0){
                    $("#campo_id").html('');
                    $("#campo_id").append('<option value="0" selected> -- TODOS LOS CAMPOS -- </option>');
                    $.each(campos, function (i, item) {
                        $("#campo_id").append('<option value=' + campos[i]['value'] + '>' + campos[i]['name'] + '</option>');
                    });
                }
            })
            .catch(function (error) { console.log("Petición de monitoreo estratificado campos: ", error);})
            .finally(function () { });
        }

        function getMEResumen(junta_id, fini, ffin, campo_id){
            $('#DivMEInfo').block({ message: '<img src="' + base_url + '/images/loader.gif"></img><strong> Procesando...</strong>' });
            var json = { junta_id: junta_id, fini: fini, ffin: ffin, campo_id:campo_id};
            axios.post(base_url + '/citrixplor/meresumen', json)
            .then(function (response) {
                var meresumen = response.data;
                //console.log("Resumen ME", meresumen);
                $('#lblME_AA').html('').html(meresumen[0].arbolesmeaa);
                $('#lblME_AN').html('').html(meresumen[0].arbolesmean);
                $('#lblME_MA').html('').html(meresumen[0].arbolesmema);
                $('#lblME_MN').html('').html(meresumen[0].arbolesmemn);
                $('#lblME_BA').html('').html(meresumen[0].arbolesmeba);
                $('#lblME_BN').html('').html(meresumen[0].arbolesmebn);
            })
            .catch(function (error) { console.log("Petición de monitoreo estratificado resumen: ", error);})
            .finally(function () { });
            $("#DivMEInfo").unblock();
        }

        function sumarDias(fecha, dias){
            fecha.setDate(fecha.getDate() + dias);
            return fecha;
        }

        function obtenerDetalles(junta_id, fini, ffin){
            $('#DivMapHLB').block({ message: '<img src="' + base_url + '/images/loader.gif"></img><strong> Procesando...</strong>' });
            $('#DivMapPSI').block({ message: '<img src="' + base_url + '/images/loader.gif"></img><strong> Procesando...</strong>' });
            $('#DivMapEST').block({ message: '<img src="' + base_url + '/images/loader.gif"></img><strong> Procesando...</strong>' });
            var json = { junta_id: junta_id, fini: fini, ffin: ffin};
            axios.post(base_url + '/citrixplor/arboles', json)
            .then(function (response) {
                arboles = [];
                //console.log("Arboles",response);
                arboles = response.data;
                creaPoints(arboles, mapHLB, gm_puntosHLB, "DivMapHLB");
                creaPoints(arboles, mapPSI, gm_puntosPSI, "DivMapPSI");
                creaPoints(arboles, mapEST, gm_puntosEST, "DivMapEST");
            })
            .catch(function (error) { console.log("Petición de registros: ", error);})
            .finally(function () { });
        }

        function setPointMap(map, array){
            var addListenersOnPoint = function(point,id) {
                google.maps.event.addListener(point, 'click', function (event) {
                    var infowindow = new google.maps.InfoWindow({ content: '<iframe src="' + base_url + '/citrixplor/info/' + id + '"></iframe>' });
                    infowindow.open(map, point);
                });  
            }
            console.log("ArrayTot", array.length);
            for(var i = 0;i<array.length;i++){
                //console.log("Arbol", arboles[i]);
                array[i].setMap(map);
                addListenersOnPoint(array[i], arboles[i].detalle_id);
            }
        }

        function clearPointMap(tipo){
            //var iterator = 0;
            if(tipo == "DivMapHLB"){
                for(var i = 0;i<gm_puntosHLB.length;i++){
                    gm_puntosHLB[i].setMap(null);
                }
                gm_puntosHLB = [];
            }else if(tipo == "DivMapPSI"){
                for(var i = 0;i<gm_puntosPSI.length;i++){
                    gm_puntosPSI[i].setMap(null);
                }
                gm_puntosPSI = [];
            }else if(tipo == "DivMapEST"){
                for(var i = 0;i<gm_puntosEST.length;i++){
                    gm_puntosEST[i].setMap(null);
                }
                gm_puntosEST = [];
            }
        }

        function creaPoints(puntos, map, array, tipo){
            //console.log("Puntos: ",puntos);
            var bounds = new google.maps.LatLngBounds();
            clearPointMap(tipo);
            for(var x = 0; x < puntos.length; x++){
                var pos = 0;
                if(tipo == "DivMapHLB"){
                    if(puntos[x]["hlb"]){ pos = pos + 1; }
                }else if(tipo == "DivMapPSI"){
                    if(puntos[x]["psa"]){ pos = pos + 1; }
                    if(puntos[x]["psn"]){ pos = pos + 1; }
                }else if(tipo == "DivMapEST"){
                    if(puntos[x]["eaa"]){ pos = pos + 1; }
                    if(puntos[x]["ean"]){ pos = pos + 1; }
                    if(puntos[x]["ema"]){ pos = pos + 1; }
                    if(puntos[x]["emn"]){ pos = pos + 1; }
                    if(puntos[x]["eba"]){ pos = pos + 1; }
                    if(puntos[x]["ebn"]){ pos = pos + 1; }
                }
                if(pos > 0){ var icon = "FF4500"; }else{ var icon = "00FA9A"; }
                var point = new google.maps.Marker({
                    draggable: false,
                    position: {lat: parseFloat(puntos[x]["lat"]), lng: parseFloat(puntos[x]["lng"])},
                    icon: base_url + '/images/markers_10/' + icon + ".png"
                });
                var latlng = new google.maps.LatLng(puntos[x]["lat"], puntos[x]["lng"]);
                bounds.extend(latlng);
                if(tipo == "DivMapHLB"){
                    gm_puntosHLB.push(point);
                }else if(tipo == "DivMapPSI"){
                    gm_puntosPSI.push(point);
                }else if(tipo == "DivMapEST"){
                    gm_puntosEST.push(point);
                }
            }
            if(tipo == "DivMapHLB"){
                setPointMap(map,gm_puntosHLB);
            }else if(tipo == "DivMapPSI"){
                setPointMap(map,gm_puntosPSI);
            }else if(tipo == "DivMapEST"){
                setPointMap(map,gm_puntosEST);
            }
            //setPointMap(map, array);
            map.fitBounds(bounds);
            $("#" + tipo).unblock();
        }

    </script>
</html>


