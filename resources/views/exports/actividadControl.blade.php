@php
  $rolUser = Session::get('rol');
@endphp
@if(Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado')
    <h3>{{ $estado->estado }}</h3>
@else
    <h3>{{ $junta->junta }}</h3>
@endif
<table>
    <thead>
        <tr>
            <th><strong>Id</strong></th>
            <th><strong>Huerta</strong></th>
            <th><strong>Fecha</strong></th>
            <th><strong>Tipo Control</strong></th>
            <th><strong>Producto</strong></th>
            <th><strong>No. Plantas</strong></th>
            <th><strong>Tipo Aplicación</strong></th>
            <th><strong>Control Biológico</strong></th>
            <th><strong>Dosis</strong></th>
            <th><strong>Metodo</strong></th>
            <th><strong>Estado</strong></th>
        </tr>
    </thead>
    <tbody>
    @foreach($capturas as $c)
        <tr>
            <td>{{ $c->id }}</td>
            <td>{{ $c->name }}</td>
            <td>{{ $c->fecha }}</td>
            <td>{{ $c->tipoControl }}</td>
            <td>{{ $c->producto_text }}</td>
            <td>{{ $c->plantas }}</td>
            <td>{{ $c->tipoAplicacion }}</td>
            <td>{{ $c->controlBiologico }}</td>
            <td>{{ $c->metodo_tex }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
