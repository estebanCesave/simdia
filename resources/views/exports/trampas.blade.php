@php
  $rolUser = Session::get('rol');
@endphp
@if(Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado')
    <h3>{{ $estado->estado }}</h3>
@else
    <h3>{{ $junta->junta }}</h3>
@endif
<table>
    <thead>
    <tr>
        <th><strong>SiembraID</strong></th>
        <th><strong>Trampa</strong></th>
        <th><strong>Latitud</strong></th>
        <th><strong>Longitud</strong></th>
        <th><strong>Campo</strong></th>
        <th><strong>Arco</strong></th>
        @if((Auth::user()->hasRole('Admin Junta') && $rolUser == 'Admin Junta') || (Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado'))
            <th><strong>Técnico</strong></th>
        @endif
        @if(Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado')
            <th><strong>Junta</strong></th>
            <th><strong>Estado</strong></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($trampas as $t)
        <tr>
            <td>{{ $t->siembra_id }}</td>
            <td>{{ $t->name }}</td>
            <td>{{ $t->latitud }}</td>
            <td>{{ $t->longitud }}</td>
            <td>{{ $t->campo }}</td>
            <td>{{ $t->arco }}</td>
             @if((Auth::user()->hasRole('Admin Junta') && $rolUser == 'Admin Junta') || (Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado') )
                <td>{{ $t->nombre }} {{ $t->apellido_paterno }} {{ $t->apellido_materno }} </td>
            @endif
            @if(Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado')
                <td>{{ $t->junta }}</td>
                <td>{{ $t->estado }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>