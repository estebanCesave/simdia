@php
  $rolUser = Session::get('rol');
@endphp
@if(Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado')
    <h3>{{ $estado->estado }}</h3>
@else
    <h3>{{ $junta->junta }}</h3>
@endif
<table>
    <thead>
    <tr>
        <th><strong>Id</strong></th>
        <th><strong>SiembraID</strong></th>
        <th><strong>Fecha</strong></th>
        <th><strong>Ejercicio</strong></th>
        <th><strong>Año</strong></th>
        <th><strong>Semana</strong></th>
        <th><strong>Trampa</strong></th>
        <th><strong>Referencia</strong></th>
        @if((Auth::user()->hasRole('Admin Junta') && $rolUser == 'Admin Junta') || (Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado'))
            <th><strong>Técnico</strong></th>
        @endif
        @if(Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado')
            <th><strong>Estado</strong></th>
            <th><strong>Junta</strong></th>
        @endif
        <th><strong>Latitud</strong></th>
        <th><strong>Longitud</strong></th>
        <th><strong>Precisión</strong></th>
        <th><strong>Fenologia</strong></th>
        <th><strong>Revisada</strong></th>
        <th><strong>Instalada</strong></th>
        <th><strong>Observaciones</strong></th>
        <th><strong>Comentarios</strong></th>
        <th><strong>Captura</strong></th>
        <th><strong>Norte adultos</strong></th>
        <th><strong>Norte ninfas</strong></th>
        <th><strong>Norte fenologia</strong></th>
        <th><strong>Sur adultos</strong></th>
        <th><strong>Sur ninfas</strong></th>
        <th><strong>Sur fenologia</strong></th>
        <th><strong>Este adultos</strong></th>
        <th><strong>Este ninfas</strong></th>
        <th><strong>Este fenologia</strong></th>
        <th><strong>Oeste adultos</strong></th>
        <th><strong>Oeste ninfas</strong></th>
        <th><strong>Oeste fenologia</strong></th>
        <th><strong>Metodo</strong></th>
        <th><strong>Fecha del telefono</strong></th>
        @if((Auth::user()->hasRole('Admin Junta') && $rolUser == 'Admin Junta') || (Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado'))
            <th><strong>Creado</strong></th>
        @endif
        <th><strong>IMEI</strong></th>
        <th><strong>Estado</strong></th>
    </tr>
    </thead>
    <tbody>
    @foreach($capturas as $c)
        <tr>
            <td>{{ $c->id }}</td>
            <td>{{ $c->siembra_id }}</td>
            <td>{{ $c->fecha }}</td>
            <td>{{ $c->ejercicioid }}</td>
            <td>{{ $c->ano }}</td>
            <td>{{ $c->semana }}</td>
            <td>{{ $c->name }}</td>
            <td>{{ $c->campo }} </td>
            @if((Auth::user()->hasRole('Admin Junta') && $rolUser == 'Admin Junta') || (Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado'))
                <td>{{ $c->nombre }} {{ $c->apellidopaterno }} {{ $c->apellidomaterno }} </td>
            @endif
            @if(Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado')
                <td>{{ $c->estado }}</td>
                <td>{{ $c->junta }}</td>
            @endif
            <td>{{ $c->lat }}</td>
            <td>{{ $c->lng }}</td>
            <td>{{ $c->accuracy }}</td>
            <td>{{ $c->fenologia }}</td>
            <td>{{ $c->revisada }}</td>
            <td>{{ $c->instalada }}</td>
            <td>{{ $c->observaciones }}</td>
            <td>{{ $c->comentarios }}</td>
            <td>{{ $c->captura }}</td>
            <td>{{ $c->norte_adultos }}</td>
            <td>{{ $c->norte_ninfas }}</td>
            <td>V{{ $c->norte_fenologia }}</td>
            <td>{{ $c->sur_adultos }}</td>
            <td>{{ $c->sur_ninfas }}</td>
            <td>V{{ $c->sur_fenologia }}</td>
            <td>{{ $c->este_adultos }}</td>
            <td>{{ $c->este_ninfas }}</td>
            <td>V{{ $c->este_fenologia }}</td>
            <td>{{ $c->oeste_adultos }}</td>
            <td>{{ $c->oeste_ninfas }}</td>
            <td>V{{ $c->oeste_fenologia }}</td>
            <td>{{ $c->method }}</td>
            <td>{{ date("Y-m-d H:i:s a", strtotime($c->fecha_cel))  }}</td>
            @if((Auth::user()->hasRole('Admin Junta') && $rolUser == 'Admin Junta') || (Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado'))
                <td>{{ date("Y-m-d H:i:s a", strtotime($c->created)) }}</td>
            @endif
            <td>{{ $c->imei }}</td>
            @if($c->inserted_sicafi == 1)
                <td>ENVIADO SICAFI</td>
            @elseif($c->inserted_sicafi == 2)
                <td>EN PROCESO</td>
            @else
                <td>SIN ENVIAR</td>
            @endif    
        </tr>
    @endforeach
    </tbody>
</table>