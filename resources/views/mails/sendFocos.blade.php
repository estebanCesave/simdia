@php
    $url = 'http://siafeson.com/siafeson7/public/focos/pdf/'. $juntaid .'/'. $semana .'/'.$ano 

@endphp
@component('mail::message')
# Focos detectados 

Hola, ** {{ $junta }}  ** 

Este es un correo de los focos detectados de la semana {{ $semana }} del año {{ $ano }}

@component('mail::table', ['focos' => $focos ] )
    
@endcomponent


Saludos

@endcomponent