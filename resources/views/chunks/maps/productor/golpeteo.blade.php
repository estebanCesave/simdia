<style>
    #marcoGolpeteo { height: 680px; }
    #golpeteo { height: 680px; }
	.leaflet-bottom { bottom: 20px }
	.leaflet-control-attribution { margin-bottom: -10px !important }
    .legend {
        line-height: 18px;
        /*color: #555;*/
        float: right;
    }
    .info legend{
        background-color: #FFFF;
    }
    .legend i {
        width: 18px;
        height: 18px;
        float: left;
        margin-right: 8px;
        opacity: 0.7;
    }
</style>
    <div id="marcoGolpeteo"></div>
     <!-- <div id="golpeteo"></div> -->
<script>
    $(function() {
        var semana = $('#slcSemana').val();
        
        if(semana == null){
           semana = moment().format("W") - 1;
        }
        
        var estado = '{!! $estado->estadoId !!}';
        var campo_id = '{!! $campo->name !!}';
        getDataGolp(estado, campo_id, semana);

        $('#slcSemana').on('change',function(){
            semana = $('#slcSemana').val();
            getDataGolp(estado, campo_id, semana)
        });
     
    });

    function getDataGolp(estado, campo, semana){
         const base_url = "{{ URL::to('/').'/productor/huerta/golpeteo' }}";
        $.getJSON(base_url + '/'+ campo +'/'+ semana +'/2019', function(data){
            if(data.count == 0)
            {
                $('#marcoGolpeteo').html("<br><br><h1 class='text-center'><i class='fas fa-exclamation-triangle fa-5x'></i><h3 class='text-center'>No existe información registrada para esta semana.</h3>" );
            }
            else{
                createMapGolp(data.latitud, data.longitud, data.adulto);
            }
        });
    }
    function createMapGolp(latitud, longitud, adulto)
    {
        document.getElementById('marcoGolpeteo').innerHTML = "<div id='golpeteo'></div>";
        var length = latitud.length;
        var puntosLat = latitud;
        var puntosLon = longitud;
        var puntosAdult = adulto;

        var latitud = '{!! $campo->latitud !!}';
        var longitud = '{!! $campo->longitud !!}';

        var center = [latitud, longitud];
        
        var mapGolp = L.map('golpeteo', {
            center: center,
            zoomControl: false,
            zoom: 15,
            zoomAnimation: true
        });
        L.yandex('yandex#satellite').addTo(mapGolp);

        var greenIcon = L.icon({
            iconUrl: "{{ asset('/images/markers_10/00FF00.png') }}",
            iconSize:     [10, 10], // size of the icon
            iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
        var redIcon = L.icon({
            iconUrl: "{{ asset('/images/markers_10/FF0000.png') }}",
            iconSize:     [10, 10], // size of the icon
            iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
        var yellowIcon = L.icon({
            iconUrl: "{{ asset('/images/markers_10/FFFF00.png') }}",
            iconSize:     [10, 10], // size of the icon
            iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
        var nothing = L.icon({
            iconUrl: "{{ asset('/images/markers_10/00FFFF.png') }}",
            iconSize:     [10, 10], // size of the icon
            iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        }); 

        L.control.zoom({ position: 'topright' }).addTo(mapGolp);

        var legend = L.control({position: 'bottomright'});
        legend.onAdd = function (mapGolp) {
            var div = L.DomUtil.create('div', 'info legend');
                div.innerHTML +='<i style="background:#00FF00"></i> Sin adultos <br> <i style="background:#FF0000"></i> 1 o mas adultos';
            return div;
        };
        legend.addTo(mapGolp);
 
         for(var i = 0; i <= length ; i++ ){
            var icono;
            
            if(puntosAdult[i] == 0 ){
                icono = greenIcon;
            }
            else{
                icono = redIcon;
            }
           
            L.marker([ puntosLat[i], puntosLon[i] ],{ icon: icono }).addTo(mapGolp);
            
           /* L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(mapGolp);*/
            
 
        }
      
    }
</script>
