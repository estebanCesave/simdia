<style>
    #marcoUmbral { height: 680px; }
    #umbral { height: 680px; }
	.leaflet-bottom { bottom: 20px }
	.leaflet-control-attribution { margin-bottom: -10px !important }
    .legend {
        line-height: 18px;
        /*color: #555;*/
        float: right;
    }
    .info legend{
        background-color: #FFFF;
    }
    .legend i {
        width: 18px;
        height: 18px;
        float: left;
        margin-right: 8px;
        opacity: 0.7;
    }
</style>
    <div id="marcoUmbral"></div>
    <!-- <div id="umbral"></div> -->
<script>
    $(function() {
        var semana = $('#slcSemana').val();
        var estado = '{!! $estado->estadoId !!}';
        var campo_id = '{!! $campo->campo_id !!}';
        getDataUmbral(estado, campo_id, semana);

        $('#slcSemana').on('change',function(){
            semana = $('#slcSemana').val();
            getDataUmbral(estado, campo_id, semana)
        });
     
    });
    function  getDataUmbral(estado, campo, semana){
        const base_url = "{{ URL::to('/').'/productor/distribucion/umbral/' }}";
        $.getJSON(base_url + estado +'/'+ campo +'/'+ semana, function(data){
             if(data.count == 0)
            {
                $('#marcoUmbral').html("<br><br><h1 class='text-center'><i class='fas fa-exclamation-triangle fa-5x'></i><h3 class='text-center'>No existe información registrada para esta semana.</h3>" );
            }
            else{
                createMap(data.data, data.umbral);
            }
        });
    }
    function createMap(puntos, umbral)
    {
        document.getElementById('marcoUmbral').innerHTML = "<div id='umbral'></div>";

        var length = puntos.length;
        var puntos = puntos;

        var latitud = '{!! $campo->latitud !!}';
        var longitud = '{!! $campo->longitud !!}';

        var center = [latitud, longitud];
        
        var map = L.map('umbral', {
            center: center,
            zoomControl: false,
            zoom: 15,
            zoomAnimation: true
        });
        L.yandex('yandex#satellite').addTo(map);

        var greenIcon = L.icon({
            iconUrl: "{{ asset('/images/markers_10/00FF00.png') }}",
            iconSize:     [10, 10], // size of the icon
            iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
        var redIcon = L.icon({
            iconUrl: "{{ asset('/images/markers_10/FF0000.png') }}",
            iconSize:     [10, 10], // size of the icon
            iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
        var yellowIcon = L.icon({
            iconUrl: "{{ asset('/images/markers_10/FFFF00.png') }}",
            iconSize:     [10, 10], // size of the icon
            iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
        var nothing = L.icon({
            iconUrl: "{{ asset('/images/markers_10/00FFFF.png') }}",
            iconSize:     [10, 10], // size of the icon
            iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        }); 

        L.control.zoom({ position: 'topright' }).addTo(map);

        var legend = L.control({position: 'bottomright'});
        legend.onAdd = function (map) {
             var div = L.DomUtil.create('div', 'info legend');
                div.innerHTML +='<i style="background:#00FF00"></i> 0 <br> <i style="background:#FFFF00"></i> > 0 < '+ umbral +' <br> <i style="background:#FF0000"></i> >= '+ umbral +' <br> <i style="background:#00FFFF"></i> No revisado';
            return div;
        };
        legend.addTo(map);


        for(var i = 0; i <= length; i++ ){
            var icono;
            
            if(puntos[i]['rango'] == 'g'){
                icono = greenIcon;
            }
            else if(puntos[i]['rango'] == 'y'){
                icono = yellowIcon;
            }
            else if(puntos[i]['rango'] == 'r'){
                icono = redIcon;
            }
            else if(puntos[i]['rango'] == 'c'){
                icono = nothing;
            }
            L.marker([ puntos[i]['latitud'], puntos[i]['longitud'] ],{ icon: icono }).addTo(map);
            
            
 
        }
      
    }
</script>
