<link rel="stylesheet" href="{{ asset('assets/leaflet/leaflet.css') }}">

<style>
    #map { height: 480px; }
    .legend {
        line-height: 18px;
        color: #555;
        float: right;
    }
    .legend i {
        width: 18px;
        height: 18px;
        float: left;
        margin-right: 8px;
        opacity: 0.7;
    }
</style>

<div id="map"></div>
<script src="{{ asset('assets/leaflet/leaflet.js') }}"></script>
<script type="text/javascript" src=" {{ asset('assets/maps/estados.js')  }}"></script>
<script>
      $(document).ready(function(){
        semana = moment().format('w');
        ano = moment().format('YYYY');

        const base_url = "{{ URL::to('/').'/mapas/' }}";
        var media;
 
        //$.getJSON(base_url + 'medias/22/2019', function(data){
        $.getJSON(base_url + 'medias/'+semana+'/'+ano, function(data){
            media = data.pais;  
             $.each(data.pais, function(i, name){
                media = name;
            })
            createMap(data.estados);
        });
        
        function createMap(estados)
        {
            var mapboxAccessToken = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
            var map = L.map('map').setView([19.4326, -99.13], 5);
            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=' + mapboxAccessToken, {
                id: 'mapbox/light-v9',
            }).addTo(map);

            var geojson = L.geoJson(
                countries,
                {
                    onEachFeature: popup,
                    style : style
                }
            ).addTo(map);
        
            function style(feature) {
                $.each(estados, function(i, name){
                    if(name.name == feature.properties.state_name){
                       feature.properties.media = name.data;
                    }
                });
                return {
                    weight: 2,
                    opacity: 1,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.7,
                    fillColor: getColor(feature.properties.media)
                };
            }
            function getColor(d) {
                return  typeof d == 'undefined' ? '#959493' :
                        d > media ? '#f71d0b' : 
                                    '#00801a'; 
            }
            function popup(feature, layer) 
            { 
                if (feature.properties && feature.properties.state_name) 
                { 
                    if(typeof feature.properties.media == 'undefined'){
                        layer.bindPopup('<strong>'+ feature.properties.state_name +'</strong> <br> <p> Sin datos </p>' ); 

                    }
                    else{
                        layer.bindPopup('<strong>'+ feature.properties.state_name +'</strong> <br> <p>'+ feature.properties.media +'</p>' ); 
                    }
                } 
            }
            var legend = L.control({position: 'bottomleft'});
            
            legend.onAdd = function (map) {
                var div = L.DomUtil.create('div', 'info legend');
                div.innerHTML +='<i style="background:#f71d0b"></i> Por arriba de la media ('+ media +') <br> <i style="background:#00801a"></i> Por debajo de la media ('+ media +') <br> <i style="background:#959493"></i> Sin datos';
                return div;
            };

            legend.addTo(map);
        }
      });
</script>