<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SIMDIA - focos</title>
    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        table{
            font-size: x-small;
        }
        tfoot tr td{
            font-weight: bold;
            font-size: x-small;
        }
        .gray {
            background-color: lightgray
        }
    </style>
</head>
<body>
    <table width="100%">
        <tr>
            <td valign="top">
                <span style="color: orange; font-size: 30px !Important; font-family: Courier-Bold;">
                    <img src="{!! public_path('/theme/dist/img/citricos.png') !!}" alt="" width="50"/>
                    SIMDIA
                </span>
            </td>
            <td align="right">
                <h3>{{ $junta->name }}</h3>
                <pre>
                    Semana  {{ $semana }}
                    {{ $ano }}
                </pre>
            </td>
        </tr>
    </table>
    <br/>
    <table width="100%">
        <thead style="background-color: lightgray;">
            <tr>
                <th align="center">Fecha</th>
                <th align="center">Campo</th>
                <th align="center">Asignado</th>
                <th align="center">Promedio</th>
            </tr>
        </thead>
        <tbody>
            @foreach($focos as $f)
            <tr>
                <td align="center">{{ date('d-m-Y',strtotime($f->fecha_det) ) }}</td>
                <td align="center">{{ $f->campo }}</td>
                <td align="center">{{ $f->nombre }} {{ $f->apellido_paterno }} {{ $f->apellido_materno }}</td>
                <td align="center">{{ $f->promedio }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
