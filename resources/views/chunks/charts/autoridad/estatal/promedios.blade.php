
<div class="row">
    <div class='col-md-4'>
        <div class="form-group">
            <select class="form-control" id="slc_anoProm" name="slc_anoProm" placeholder="Selecciona año" >
            </select>
        </div>
    </div>
    <div class='col-md-4'></div>
    <div class='col-md-4'>
        <div class="form-group">
            <select class="form-control" id="slc_cambioProm" name="slc_cambioProm" placeholder="Selecciona">
                <option value='1'>Semanal</option>
                <option value='2' selected>Mensual</option>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <br>
        <div id='promProgress' class="progress">
            <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%">Cargando...</div>
        </div>
        <div id="promedios"></div>
    </div>
</div>
<script>
  $(function() {
    const base_url = "{{ URL::to('/').'/reportes/' }}";
    var ano = $('#slc_anoProm').val();
    var tipo = $('#slc_cambioProm').val();
    var seriesOptionsProm;
    var estado = '{{ $superficie->estado }}';

    if(ano == "" || typeof ano == 'undefined' || ano == null){
        ano = moment().format("YYYY");
    }
    paramsCharts(ano, tipo);
    axios.get(base_url+'anos')
        .then(function (response) {
            crear_lista_base_prom("slc_anoProm", response.data);
        }).catch(function (error) { 
            console.log(error); 
        });
    
    $("#slc_anoProm").on("change", function(){
        ano = $('#slc_anoProm').val();
        tipo = $('#slc_cambioProm').val();
        paramsCharts(ano, tipo);
    });
    $("#slc_cambioProm").on("change", function(){
        ano = $('#slc_anoProm').val();
        tipo = $('#slc_cambioProm').val();
        paramsCharts(ano, tipo);
    });
    function paramsCharts(ano, tipo)
    {
        var cat = [];
        var minProm = [];
        var maxProm = [];
        var texto; 
        if(tipo == 1){
            texto = 'Semanas'
            cat = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','43','44','45','46','47','48','49','50','51','52','53'];
        }
        else{
            texto = 'Meses'
            cat = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        }
        seriesOptionsProm = [],
            yAxisOptionsProm = [],
            xAxisOptionsProm = [],
            seriesCounter = 0,
            //names = ['Pais','Baja California','Baja California Sur','Campeche','Chiapas','Colima','Guerrero','Hidalgo','Jalisco','Michoacán','Morelos','Nayarit','Nuevo León','Oaxaca','Puebla','Querétaro','Quintana Roo','San Luis Potosí','Sinaloa','Sonora','Tabasco','Tamaulipas','Yucatán','Veracruz', 'Zacatecas'],
            /**Obteniendo arcos por estado */          
            $.getJSON(base_url + 'arcos/'+ estado, function(data){
                names = data;
                colors = Highcharts.getOptions().colors;
                $.each(names, function(i, name){
                    $.getJSON(base_url + 'promedios/estado/'+ ano +'/'+ estado +'/'+ tipo +'/'+ name, function(data){
                        minProm = data.min;
                        maxProm = data.max;
                        seriesOptionsProm[i] = {
                            name: name,
                            data: data.data,
                            visible: false
                        };
                        seriesCounter ++;
                        if (seriesCounter == names.length) {
                            createChart();
                        }
                    });
                });
            });
        xAxisOptionsProm = {
            categories: cat, 
            title: {
                text: texto, 
                style: {
                    fontSize: '5',
                    color: 'black'
                }
            }
        };
        yAxisOptionsProm =  {
            title: {
                text: 'Promedios' // nombre del eje de Y
            },
            plotLines: [{
                color: '#808080'
            }],
            ticks:{
                min: minProm,
                max: maxProm,
            }
            
        };
    }
    function createChart() 
    {
        $('#promProgress').hide();

        $("#promedios").highcharts({ 
            credits:{
                text: 'Siafeson.com'
            }, 
            chart: { 
                events:{
                    load: function()
                    {
                        this.credits.element.onclick = function(){
                            window.open('http://siafeson.com','_blank');
                        }
                    }
                }, 
                width: 
                    $('#tab-active').width(), 
                    type: 'line', 
                    className: 'SiafesonChart',
                    style:{
                        fontSize: '18px'
                    },
                    zoomType:'xy' 
            },
            plotOptions: { 
                series: { 
                    lineWidth: 4 
                } 
            },
            legend: {
                itemStyle:{
                    fontSize:'15px'
                }
            },
            title: {
                text: ano
            },
            subtitle: {
                text: ''
            },
            xAxis: xAxisOptionsProm,
            yAxis: yAxisOptionsProm,
            series: seriesOptionsProm,
            exporting: { 
                sourceWidth: 1800, 
                sourceHeight: 800, 
                scale: 1 
            }
        });
    }
    function crear_lista_base_prom(slc, dato, selected){
        $("#" + slc).html('');
        $("#" + slc).append('<option value="0" selected> -- SELECCIONE -- </option>');
        $.each(dato, function (i, item) {
            $("#" + slc).append('<option value=' + dato[i]['ano'] + ' data-file='+ dato[i]['ano'] +'>'+ dato[i]['ano'] + '</option>');
        });
        if (selected){ 
            $("#" + slc).val(selected); 
        }
    }
});
</script>












 
 