<div class="row">
    <div class='col-md-4'>
        <div class="form-group">
            <select class="form-control" id="slc_cambioAnos" name="slc_cambioAnos" placeholder="Selecciona">
                <option value='1'>Semanal</option>
                <option value='2' selected>Mensual</option>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <br>
        <div id='anosProgress' class="progress">
            <div class="progress-bar progress-bar-striped bg-danger progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%">Cargando...</div>
        </div>
        <div id="promediosAnos"></div>
    </div>
</div>
<script>
  $(function() {
    const base_url = "{{ URL::to('/').'/reportes/' }}";
    var tipoAno = $('#slc_cambioAnos').val();
    var seriesOptionsAnos;
    var xAxisOptionsAnos;
    var estado = '{{ $superficie->estado }}';
    
    anoAno = moment().format("YYYY");

    paramsChartsAnos(tipoAno);
  
    $("#slc_cambioAnos").on("change", function(){
        //ano = $('#slc_ano').val();
        tipoAno = $('#slc_cambioAnos').val();
        paramsChartsAnos(tipoAno);
    });
    function paramsChartsAnos(tipo)
    {
        var cat = [];
        var texto; 
        var min = [];
        var max = [];
        if(tipo == 1){
            texto = 'Semanas'
            cat = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','43','44','45','46','47','48','49','50','51','52','53'];
        }
        else{
            texto = 'Meses'
            cat = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        }
        seriesOptionsAnos = [],
            yAxisOptionsAnos = [],
            xAxisOptionsAnos = [],
            seriesCounterAnos = 0,
            $.getJSON(base_url + 'getanos', function(data){
                namesA = data;
                console.log(namesA)
                colors = Highcharts.getOptions().colors;
                $.each(namesA, function(i, name){
                    $.getJSON(base_url + 'promedios/estado/'+ name +'/'+ tipo +'/'+ estado, function(data){
                        cat = data.semanas;
                        min = data.min;
                        max = data.max;
                         var visible
                        if(name == anoAno ){
                            visible = true;
                        }
                        else{
                            visible = false;
                        }
                        seriesOptionsAnos[i] = {
                            name: name,
                            data: data.data,
                            visible: visible
                        };
                        seriesCounterAnos ++;
                        if (seriesCounterAnos == namesA.length) {
                            createChartAnos();
                        }
                    });
                });
            });
        xAxisOptionsAnos = {
            categories: cat, 
            title: {
                text: '', 
                style: {
                    fontSize: '5',
                    color: 'black'
                }
            }
        };
        yAxisOptionsAnos =  [
            {
                min: 0,
                title: {
                    text: 'Promedio', 
                    style: {
                        fontSize: '5',
                        color: 'black'
                    }
                }
            }
        ]
    }
    function createChartAnos() 
    {
         $('#anosProgress').hide();
            
 
         $("#promediosAnos").highcharts({ 
            credits:{
                text: 'Siafeson.com'
            }, 
            chart: { 
                events:{
                    load: function()
                    {
                        this.credits.element.onclick = function(){
                            window.open('http://siafeson.com','_blank');
                        }
                    }
                }, 
                width: 
                    $('#tab-active').width(), 
                    type: 'line', 
                    className: 'SiafesonChart',
                    style:{
                        fontSize: '18px'
                    },
                    zoomType:'xy' 
            },
            plotOptions: { 
                series: { 
                    lineWidth: 4 
                } 
            },
            legend: {
                itemStyle:{
                    fontSize:'15px'
                }
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: xAxisOptionsAnos,
            yAxis: yAxisOptionsAnos,
            series: seriesOptionsAnos,
            exporting: { 
                sourceWidth: 1800, 
                sourceHeight: 800, 
                scale: 1 
            }
        });
    }
});
</script>












 
 