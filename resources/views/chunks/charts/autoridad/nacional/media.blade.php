
<div class="row">
    <div class='col-md-4'>
        <div class="form-group">
            <select class="form-control" id="slc_anoMedia" name="slc_anoMedia" placeholder="Selecciona año" >
            </select>
        </div>
    </div>
    <div class='col-md-4'></div>
    <div class='col-md-4'>
        <div class="form-group">
            <select class="form-control" id="slc_cambioMedia" name="slc_cambioMedia" placeholder="Selecciona">
                <option value='1'>Semanal</option>
                <option value='2' selected>Mensual</option>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <br>
        <div id='mediasProgress' class="progress">
            <div class="progress-bar progress-bar-striped bg-success progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%">Cargando...</div>
        </div>
        <div id="medias"></div>
    </div>
</div>
<script>
  $(function() {
    const base_url = "{{ URL::to('/').'/reportes/' }}";
    var anoMedia = $('#slc_anoMedia').val();
    var tipoMedia = $('#slc_cambioMedia').val();
    var seriesOptionsMedia;
    var xAxisOptionsMedia;

    if(anoMedia == "" || typeof anoMedia == 'undefined' || anoMedia == null){
        anoMedia = moment().format("YYYY");
    }
    paramsChartsMedias(anoMedia, tipoMedia);
    axios.get(base_url+'anos')
        .then(function (response) {
            crear_lista_base_media("slc_anoMedia", response.data);
        }).catch(function (error) { 
            console.log(error); 
        });
    
    $("#slc_anoMedia").on("change", function(){
        anoMedia = $('#slc_anoMedia').val();
        tipotipoMedia = $('#slc_cambioMedia').val();
        paramsChartsMedias(anoMedia, tipotipoMedia);
    });
    $("#slc_cambioMedia").on("change", function(){
        anoMedia = $('#slc_anoMedia').val();
        tipotipoMedia = $('#slc_cambioMedia').val();

        if(anoMedia == 0){
            anoMedia = moment().format("YYYY");
        }
        else{
            anoMedia = $('#slc_anoMedia').val();
        }
        paramsChartsMedias(anoMedia, tipotipoMedia);
    });
    function paramsChartsMedias(ano, tipo)
    {
        var cat = [];
        var minMed = [];
        var maxMed = [];
        var texto; 
        if(tipo == 1){
            texto = 'Semanas'
            cat = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','43','44','45','46','47','48','49','50','51','52','53'];
        }
        else{
            texto = 'Meses'
            cat = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        }
        seriesOptionsMedia = [],
            yAxisOptionsMedia = [],
            xAxisOptionsMedia = [],
            seriesCounterMedia = 0,
            names = ['Pais','Baja California','Baja California Sur','Campeche','Chiapas','Colima','Guerrero','Hidalgo','Jalisco','Michoacán','Morelos','Nayarit','Nuevo León','Oaxaca','Puebla','Querétaro','Quintana Roo','San Luis Potosí','Sinaloa','Sonora','Tabasco','Tamaulipas','Yucatán','Veracruz', 'Zacatecas'],
            colors = Highcharts.getOptions().colors;
        $.each(names, function(i, name){
             $.getJSON(base_url + 'media/'+ ano +'/'+ name +'/'+ tipo, function(data){
                cat = data.semanas;
                minMed = data.min;
                maxMed = data.max;

                 var visible
                 if(name =='Pais'){
                     visible = true;
                 }
                 else{
                     visible = false;
                 }
                 seriesOptionsMedia[i] = {
                    name: name,
                    data: data.data,
                    visible: visible
                };
                
                seriesCounterMedia ++;
                if (seriesCounterMedia == names.length) {
                     createChartMedias();
                }
                
            });
        });
        xAxisOptionsMedia = {
            categories: cat, 
            title: {
                text: texto, 
                style: {
                    fontSize: '5',
                    color: 'black'
                }
            }
        };
        yAxisOptionsMedia = [
            {
                min: 0,
                title: {
                    text: 'Promedio', 
                    style: {
                        fontSize: '5',
                        color: 'black'
                    }
                }
            }
        ]
     }
    function createChartMedias() 
    {
        $('#mediasProgress').hide();

        $("#medias").highcharts({ 
            credits:{
                text: 'Siafeson.com'
            }, 
            chart: { 
                events:{
                    load: function()
                    {
                        this.credits.element.onclick = function(){
                            window.open('http://siafeson.com','_blank');
                        }
                    }
                }, 
                width: 
                    $('#tab-active').width(), 
                    type: 'line', 
                    className: 'SiafesonChart',
                    style:{
                        fontSize: '18px'
                    },
                    zoomType:'xy' 
            },
            plotOptions: { 
                series: { 
                    lineWidth: 4 
                } 
            },
            legend: {
                itemStyle:{
                    fontSize:'15px'
                }
            },
            title: {
                text: anoMedia
            },
            subtitle: {
                text: ''
            },
            xAxis: xAxisOptionsMedia,
            yAxis: yAxisOptionsMedia,
            series: seriesOptionsMedia,
            exporting: { 
                sourceWidth: 1800, 
                sourceHeight: 800, 
                scale: 1 
            }
        });
    }
    function crear_lista_base_media(slc, dato, selected){
        $("#" + slc).html('');
        $("#" + slc).append('<option value="0" selected> -- SELECCIONE -- </option>');
        $.each(dato, function (i, item) {
            $("#" + slc).append('<option value=' + dato[i]['ano'] + ' data-file='+ dato[i]['ano'] +'>'+ dato[i]['ano'] + '</option>');
        });
        if (selected){ 
            $("#" + slc).val(selected); 
        }
    }
});
</script>












 
 