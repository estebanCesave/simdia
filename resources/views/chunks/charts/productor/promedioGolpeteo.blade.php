<div class="row">
    <div class="col-md-12">
        <div id='promProgress' class="progress">
            <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%">Cargando...</div>
        </div>
        <div id="promedios"></div>
    </div>
</div>
<script>
  $(function() {
    const base_url = "{{ URL::to('/') }}";
    var ano = $('#slc_ano').val();
    //var tipo = $('#slc_cambio').val();
    var tipo = 1;
    var seriesOptions;
    var xAxisOptions;
    var anoAno = moment().format("YYYY");
    var campo_id = '{!! $campo->name !!}';


    if(ano == "" || typeof ano == 'undefined' || ano == null){
        ano = moment().format("YYYY");
    }
    paramsCharts(ano, tipo);
    /*axios.get(base_url+'')
        .then(function (response) {
            crear_lista_base("slc_ano", response.data);
        }).catch(function (error) { 
            console.log(error); 
        });*/
    
    $("#slc_ano").on("change", function(){
        ano = $('#slc_ano').val();
        tipo = $('#slc_cambio').val();
        paramsCharts(ano, tipo);
    });
    $("#slc_cambio").on("change", function(){
        //ano = $('#slc_ano').val();
        tipo = $('#slc_cambio').val();
        paramsCharts(ano, tipo);
    });
    function paramsCharts(ano, tipo)
    {
        var cat = [];
        var texto; 
        var min = [];
        var max = [];
        if(tipo == 1){
            texto = 'Semanas'
            cat = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','43','44','45','46','47','48','49','50','51','52','53'];
        }
        else{
            texto = 'Meses'
            cat = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        }
        seriesOptions = [],
            yAxisOptions = [],
            xAxisOptions = [],
            seriesCounter = 0,
            //names = ['2019'],
            colors = Highcharts.getOptions().colors;
        $.getJSON(base_url + '/productor/getanos/'+ campo_id, function(data){
             names = data;
            $.each(names, function(i, name){
                $.getJSON(base_url + '/productor/huerta/promedio/'+ campo_id +'/'+ name, function(data){
                    cat = data.semanas;
                    min = data.min;
                    max = data.max;
                 
                    var visible
                    if(name == anoAno){
                        visible = true;
                    }
                    else{
                        visible = false;
                    }
                    seriesOptions[i] = {
                        name: name,
                        data: data.data,
                        visible: visible
                    };
                    seriesCounter ++;
                    if (seriesCounter == names.length) {
                        createChart();
                    }
                });
            });
        });
        xAxisOptions = {
                categories: cat,
            scrollbar: {
                enabled: false
            },
            title: {
                text: texto 
            }
        };
        yAxisOptions =  {
            title: {
                text: 'Adultos (promedio por árbol)' // nombre del eje de Y
            },
            plotLines: [{
                color: '#808080'
            }],
            ticks:{
                min: min,
                max: max,
            }
            
        };
    }

     
    function createChart() 
    {
        $('#promProgress').hide();

        $('#promedios').highcharts({
		    chart: {
                 type: 'line',  // tipo de gráfica
                 borderWidth: 0, // ancho del borde de la gráfica
                 events:{
                    load: function(){
                        this.credits.element.onclick = function(){
                                window.open('http://siafeson.com','_blank');
                            }
                        }
                    }
            },
            credits:{
              text: 'Siafeson.com'
            },
			title: {
                text: ano, // título
                x: -20 
            },
		    rangeSelector: {
		        selected: 1
		    },
			xAxis: xAxisOptions,
			yAxis: yAxisOptions,
		    plotOptions: {
		    	series: {
		    		compare: 'value'
		    	}
		    },
		    
		    tooltip: {
		    	pointFormat: '<span style="color:{series.color}">{series.name}</span>:<b>{point.y}</b>',
		    	valueDecimals: 6
		    },
		    
		    series: seriesOptions
		});
    }
    function crear_lista_base(slc, dato, selected){
        $("#" + slc).html('');
        $("#" + slc).append('<option value="0" selected> -- SELECCIONE -- </option>');
        $.each(dato, function (i, item) {
            $("#" + slc).append('<option value=' + dato[i]['ano'] + ' data-file='+ dato[i]['ano'] +'>'+ dato[i]['ano'] + '</option>');
        });
        if (selected){ 
            $("#" + slc).val(selected); 
        }
    }
});
</script>












 
 