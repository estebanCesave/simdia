<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    @foreach($trampas as $t)
        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(185)->generate($t->id .'&'.$t->siembra_id.'&'.$t->name.'&'.$t->campo_id.'&'.$t->latitud.'&'.$t->longitud.'&'.$t->campo.'&'.$t->superficie)) !!} ">
        <span style="font-size: 9pt; padding-left: -175px;">{{$t->name}}</span>
    @endforeach
</body>
</html>
