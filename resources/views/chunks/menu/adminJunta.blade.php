<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-th"></i>
        <p>Actividad <i class="fas fa-angle-right right"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('actividad') }}" class="nav-link">
                <p>Registros</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('actividad.control') }}" class="nav-link">
                <p>Registros CQ</p>
            </a>
        </li> 
    </ul>
</li>
<li class="nav-item">
    <a href="{{route('getRoles')}}" class="nav-link">
        <i class=" nav-icon fas fa-users"></i><p>Usuarios</p>
    </a>
</li> 
<li class="nav-item">
    <a href="{{ route('reportes-avances-usuarios') }}" class="nav-link">
        <i class="nav-icon fas fa-chart-pie"></i>
        <p>Avances</p>
    </a>                                   
</li>
<li class="nav-item">
    <a href="{{ route('listadoTrampas') }}" class="nav-link">
        <i class="nav-icon fas fa-list"></i> <p>Etiquetas</p>
    </a>
</li>
<!--<li class="nav-item">
    <a href="{{ route('recuperar') }}" class="nav-link">
        <i class="nav-icon fas fa-download"></i> <p> Recuperar Registros</p>
    </a>
</li>-->
<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-info"></i><p>Ayuda <i class="fas fa-angle-right right"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="http://www.siafeson.com/assets/videos/simdia/app_nueva.mp4" target="_blank" class="nav-link">
                <i class="nav-icon fas fa-video"></i>
                <p>Video tutorial</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="http://siafeson.com/siafeson7/public/docs/simdia/guia_rapida.pdf" target="_blank" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>Guía rápida</p>
            </a>
        </li>                                   
        <li class="nav-item">
            <a href="http://siafeson.com/siafeson7/public/docs/simdia/Manual_de_Uso_Web-Administrador_Junta.pdf" target="_blank" class="nav-link">
                <i class="fas fa-book"></i>
                <p>Manual admin junta</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>Manuales apps <i class="fas fa-angle-right right"></i></p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ asset('docs/simdia/Manual_Usuario_App_Movil_SISTGLOB_SIMDIA.pdf') }}" target="_blank" class="nav-link">
                        <p>SISTGLOB</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset('docs/simdia/Guia_Rapida_SIMDIA_CQ.pdf') }}" target="_blank" class="nav-link">
                        <p>SIMDIA CQ</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ asset('docs/simdia/Guia_Rapida_SIMDIA_CQ_traspatio.pdf') }}" target="_blank" class="nav-link">
                        <p>CQ Traspatios</p>
                    </a>
                </li>
            </ul>       
        </li>
        <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">
                <i class="nav-icon fab fa-android"></i>
                <p>Aplicaciónes <i class="fas fa-angle-right right"></i></p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="https://play.google.com/store/apps/details?id=siafeson.movil.trampeo" target="_blank" class="nav-link">
                        <p>SISTGLOB</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="https://play.google.com/store/apps/details?id=siafeson.movil.simdiacq_2" target="_blank" class="nav-link">
                        <p>SIMDIA CQ</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="https://play.google.com/store/apps/details?id=siafeson.movil.simdiacqt" target="_blank" class="nav-link">
                        <p>CQ Traspatios</p>
                    </a>
                </li>
            </ul>       
        </li>
    </ul>
</li>                                