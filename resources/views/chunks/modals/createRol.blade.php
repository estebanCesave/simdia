<div class="modal fade" id="modalCreateRol" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nuevo Rol</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" placeholder="Nombre del rol" id="rol" name='rol'>
                    <!--<label>Nombre para mostrar</label>
                    <input type="text" class="form-control" placeholder="Nombre para mostrar" id="mostrar" name='mostrar'>-->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="save" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<script>
$(function () {
    $('#save').on('click', function(){
        var json = {
            name : $('#rol').val()
            //mostrar : $('#mostrar').val()
        }
        axios.post(base_url + 'addRol',json)
            .then(function (response) {
                if(response.data.status == 'success')
                {
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: response.data.message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                    $('#rol').val('');
                    $("#modalCreateRol").modal('hide');
                }

            }).catch(function(error){
                console.log(error);
            });
    });
});
</script>