<!-- Modal -->
<div class="modal fade" id="modalAddImei" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Imei</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
             <div class="form-group">
                <label for="exampleInputEmail1">IMEI (15 digitos)</label>
                <input type="text" class="form-control" id="imei" name='imei' placeholder="Agregar IMEI" maxlength="15">
            </div>
            <div class="text-right">
                <button type="submit" id="saveImei" class="btn btn-primary">Agregar</button>
            </div>
         <br>
        <table id="tblImeis" class="table table-striped">
            <thead class="text-center">
                <tr>
                    <th scope="col">IMEI</th>
                    <th scope="col">Status</th>
                    <th scope="col">Opciones</th>
                </tr>
            </thead>
            <tbody class="text-center">
            
            </tbody>
        </table>
      </div>
   </div>
</div>
<script>
$(function () {
      $("#tblImeis").DataTable({
            "info": false,
            "pageLength" : 5,
            language:{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros por página",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "",
                //"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                //"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "",
                "sPrevious": ""
                },
                "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });  
});
</script>

