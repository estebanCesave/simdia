<style>
   .modal-open .ui-datepicker{
     z-index: 2000!important
    }
</style>
<div class="modal fade" id="modal-lg_admin">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="modal_captura">
            <div class="modal-header" style="border-color: orange;">
              <h4 class="modal-title">Capturar</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Fecha:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="far fa-calendar-alt"></i>
                        </span>
                      </div>
                       <input type="text" class="form-control float-right datepicker" id="fecha" name="fecha" value="" />

                    </div>
                  <!-- /.input group -->
                    </div>
                </div>
                @if((Auth::user()->hasRole('Admin Junta') && $rolUser == 'Admin Junta') || (Auth::user()->hasRole('Admin Estado') && $rolUser == 'Admin Estado'))
                <div class='col-md-6'>
                  <div class="form-group">
                    <label>Usuarios</label>
                    <select class="form-control" id="slcUser">
                    </select>
                  </div>
                </div>
                @endif                
                <div class="col-md-6">
                   <div class="form-group">
                    <label>Trampas</label>
                    <select class="form-control" id="slcTrampas">
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                      <label>Fenologías</label>
                      <select class="form-control" id="slcFenologiaMain">
                      </select>
                    </div>
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                      <label>Número de adultos</label>
                      <input type="text" class="form-control is_integer" placeholder="Adultos ..." id="num_insectos">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label>Instalación:</label><br>
                      <label class="switch"><input type="checkbox" id="switchInstalacion"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span></div></label>
                   </div>                
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label>Revisada:</label><br>
                      <label class="switch"><input type="checkbox" id="switchRevisada"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span></div></label>
                   </div>                
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <label>Motivo</label>
                      <select class="form-control" id="slcMotivosMain" disabled>
                      </select>
                    </div>
                </div>
              </div>               
                <hr style="border-top: 1px solid orange;">
                <label style="font-size: 20px; color: orange;">Árbol</label>               
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                     <label>Adultos</label>
                    </div>
                    <div class="col-md-3">
                     <label>Ninfas</label>
                    </div>
                    <div class="col-md-4">
                     <label>Fenología</label>
                    </div>
                  </div>                  
                  <div class="row">                     
                    <div class="col-md-2">
                     <label>Norte:</label>
                    </div>
                    <div class="form-group col-md-3" >
                      <label class="switch"><input type="checkbox" id="switchAdultoNorte"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span><!--END--></div></label>
                      {{-- <input type="checkbox" class="custom-control-input" id="customSwitchAdultoNorte"><div class="slider round"><span class="on">ON</span><span class="off">OFF</span><!--END--></div>
                      <label class="custom-control-label" for="customSwitchAdultoNorte"></label> --}}
                    </div>                   
                     <div class="form-group col-md-3" >
                      <label class="switch"><input type="checkbox" id="switchNinfaNorte"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span></div></label>
                    </div>  
                 <!-- select -->
                    <div class="form-group col-md-4">
                      <select class="form-control" id="slcFenologiaNorte">
                      </select>
                    </div>
                  </div>                  
                  <div class="row">
                     <div class="col-md-2">
                     <label>Sur:</label>
                     </div>
                     <div class="form-group col-md-3" >
                      <label class="switch"><input type="checkbox" id="switchAdultoSur"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span></div></label>
                    </div>                  
                     <div class="form-group col-md-3" >
                      <label class="switch"><input type="checkbox" id="switchNinfaSur"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span></div></label>
                    </div>  
                 <!-- select -->
                    <div class="form-group col-md-4">
                    <select class="form-control" id="slcFenologiaSur">
                    </select>
                    </div>
                  </div>                  
                  <div class="row">
                     <div class="col-md-2">
                     <label>Este:</label>
                     </div>
                     <div class="form-group col-md-3" >
                      <label class="switch"><input type="checkbox" id="switchAdultoEste"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span></div></label>
                    </div>                  
                     <div class="form-group col-md-3" >
                      <label class="switch"><input type="checkbox" id="switchNinfaEste"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span></div></label>
                    </div>  
                    <div class="form-group col-md-4">
                      <select class="form-control" style="height: 33px;" id="slcFenologiaEste">
                      </select>
                    </div>
                  </div>                  
                  <div class="row">
                    <div class="col-md-2">
                    <label>Oeste:</label>
                    </div>
                    <div class="form-group col-md-3" >
                      <label class="switch"><input type="checkbox" id="switchAdultoOeste"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span></div></label>
                    </div>                  
                     <div class="form-group col-md-3" >
                      <label class="switch"><input type="checkbox" id="switchNinfaOeste"><div class="slider round"><!--ADDED HTML --><span class="on">SI</span><span class="off">NO</span></div></label>
                    </div>  
                    <div class="form-group col-md-4" >
                    <select class="form-control"style="height: 33px;" id="slcFenologiaOeste">
                    </select>
                    </div>
                  </div>                  
                  <div class="form-group col-md-3" style="float:none;margin:auto;">
                    <button type="button" class="btn btn-block bg-gradient-default"style="background-color: orange;" id="btn_save">Guardar</button>
                </div>
            </div>
            <div class="modal-footer justify-content-between" style="border-color: orange;">
              <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>              
            </div>
        </div>
       
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
$(function () {
    $("#fecha").datepicker({
        showWeek: false,
        dateFormat: 'yy-mm-dd',
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        dayNamesShort: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        prevText: 'Ant',
        nextText: 'Sig',
        currentText: 'Hoy',
        firstDay: 0,
        changeMonth: true,
        changeYear: true,
        /*onClose: function(selectedDate) {
           if(selectedDate!=""){
              $("#hasta").attr("disabled", false);
              $('#desde').val(selectedDate);
           }
        }*/
      });
 });
</script>