<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">    
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crear nuevo usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id='showErrors'>
                    <ul id="errors">
                               
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <!--<div class="form-group">
                            <label>Usuario</label>
                            <input type="text" class="form-control" placeholder="Nombre de usuario" id="usuario" name='usuario' required>
                        </div>-->
                        <div class="form-group">
                            <label>Nombre(s)</label>
                            <input type="text" class="form-control" placeholder="Nombre" id="nombre" name='nombre' required>
                        </div>
                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <input type="text" class="form-control" placeholder="Apellido Paterno" id="apellidoPaterno" name='apellidoPaterno' required>
                        </div>
                        <div class="form-group">
                            <label>Apellido Materno</label>
                            <input type="text" class="form-control" placeholder="Apellido Materno" id="apellidoMaterno" name='apellidoMaterno' required>
                        </div>
                        <div class="form-group">
                            <label>Sexo</label>
                            <select class="form-control" id="slcSexo">
                                <option value="0" selected> -- SELECCIONE -- </option>
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option> 
                            </select>                        
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="Email" id="email" name='emailAdd' required>
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" placeholder="Contraseña" id="contra" name='contraAdd' required>
                        </div>    
                        <div class="form-group">
                            <label>Rol</label>
                            <select class="form-control" id="slcRol">
                            </select>                         
                        </div>
                        <div class="form-group">
                            <label>Junta</label>
                            <select class="form-control" id="slcJunta">
                            </select>                        
                        </div>
                        <div class="form-group">
                            <label>Personal ID</label>
                            <input type="text" class="form-control" placeholder="Personal ID" id="personalId" name='personalId' required>
                        </div>
                        <div class="form-group">
                            <label>UUID</label>
                            <input type="text" class="form-control" placeholder="UUID" id="uuid" name='uuid'>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="addUser" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
         </div>
    </div>
</div>
<script>
$(function() {
    $("#showErrors").hide();

    const base_url = "{{ URL::to('/')."/" }}";
    var token =  @php  @csrf  @endphp

    $('#addUser').on('click', function(){

        var validar = validar_valores();
        
        if(validar)
        {
            var json = {
                _token :  "{{ csrf_token() }} ",
                username : $('#usuario').val(),
                nombre : $('#nombre').val(),
                apellidoPaterno : $('#apellidoPaterno').val(),
                apellidoMaterno : $('#apellidoMaterno').val(),
                email : $('#email').val(),
                contra: $('#contra').val(),
                rol : $('#slcRol').val(),
                junta: $('#slcJunta').val(),
                personal_id : $('#personalId').val(),
                uuid : $('#uuid').val(),
                sexo : $('#slcSexo').val()
            }

            $.ajax({
                url: base_url + 'usuarios/add',
                type: 'POST',
                datatype: 'json',
                data: json,
                success:function(data){
                    //console.log(data.errors);
                    if(data.status == 'error'){
                        $('#showErrors').show();
                        $.each(data.errors, function(key, value){
                            $('#errors').append('<li>'+value+'</li>');
                        });
                    }
                    else{
                        $('#addUserModal').modal('hide');
                        Swal.fire({
                            position: 'top-end',
                            type: 'success',
                            title: '',
                            text: 'Usuario creado correctamente',
                            showConfirmButton: false,
                            timer: 3000
                        }) 
                    }
                    
                },
                error:function(xhr, status, error){
                    console.log(error);
                }
            })
        }
    });
    
    axios.post(base_url + 'juntas/all')
    .then(function(response){
        crear_lista('slcJunta',response.data);
    }).catch(function(respose){
        console.log(response);
    });
    
    axios.post(base_url + 'roles/all')
    .then(response=>{
        crear_lista('slcRol',response.data);
    }).catch(error=> {
        console.log(error.response);
    });
    function crear_lista(slc, dato, selected) {
        $("#" + slc).html('');
        $("#" + slc).append('<option value="0" selected> -- SELECCIONE -- </option>');
        $.each(dato, function (i, item) {
            $("#" + slc).append('<option value=' + dato[i]['id'] + '>' + dato[i]['short_name'] + '</option>');
        });
        if (selected){ 
            $("#" + slc).val(selected); 
        }
    }
    function validar_valores(){
        var cont = 0;
        if($('#usuario').val()==''){
            $('#usuario').addClass("is-invalid");
            cont ++;
        }else{
            $('#usuario').removeClass("is-invalid");
        }

        if($('#nombre').val()==''){
            $('#nombre').addClass("is-invalid");
            cont ++;
        }else{
            $('#nombre').removeClass("is-invalid");
        }

        if($('#apellidoPaterno').val()==''){
            $('#apellidoPaterno').addClass("is-invalid");
            cont ++;
        }else{
            $('#apellidoPaterno').removeClass("is-invalid");
        }
        
        if($('#apellidoMaterno').val()==''){
            $('#apellidoMaterno').addClass("is-invalid");
            cont ++;
        }else{
            $('#apellidoMaterno').removeClass("is-invalid");
        }

        if($('#email').val()==''){
            $('#email').addClass("is-invalid");
            cont ++;
        }else{
            $('#email').removeClass("is-invalid");
        }
        
        if($('#contra').val()==''){
            $('#contra').addClass("is-invalid");
            cont ++;
        }else{
            $('#contra').removeClass("is-invalid");
        }

        if($('#slcRol').val()==0){
            $('#slcRol').addClass("is-invalid");
            cont ++;
        }else{
            $('#slcRol').removeClass("is-invalid");
        }

        if($('#slcJunta').val()==0){
            $('#slcJunta').addClass("is-invalid");
            cont ++;
        }else{
            $('#slcJunta').removeClass("is-invalid");
        }

        if($('#slcSexo').val()==0){
            $('#slcSexo').addClass("is-invalid");
            cont ++;
        }else{
            $('#slcSexo').removeClass("is-invalid");
        }

        if($('#personalId').val()==''){
            $('#personalId').addClass("is-invalid");
            cont ++;
        }else{
            $('#personalId').removeClass("is-invalid");
        }

        if($('#uuid').val()==''){
            $('#uuid').addClass("is-invalid");
            cont ++;
        }else{
            $('#uuid').removeClass("is-invalid");
        }

        if(cont>0){
            return false; 
        }else{
            return true;    
        }
    }
    $('#email').on('focusout', function () { 
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        if (regex.test($('#email').val().trim())) {
            $('#email').removeClass("is-invalid");
        }else{
             $('#email').addClass("is-invalid");
        }
    });
    $('#personalId').on('input', function () { 
        this.value = this.value.replace(/[^0-9]/g,'');
    });
});
</script>