<div class="modal fade" id="permissionRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Permisos</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <select id='permisosRol' name='permisosRol' class="form-control formulario_general">
            @foreach($permissions as $p)
                <option value="{{ $p->id }}">{{ $p->name }}</option>
            @endforeach    
            </select>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="saveRolPermission">Guardar</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
        </div>
    </div>
</div>