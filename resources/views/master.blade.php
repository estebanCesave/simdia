@php
    $rol = Session::has('rol');
    $rolUser = Session::get('rol');
@endphp
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIMDIA | SIAFESON</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
    <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('theme/plugins/sweetalert2/sweetalert2.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@6.x/css/materialdesignicons.min.css" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <!-- jQuery -->
  <script src="{{ asset('theme/plugins/jquery/jquery.min.js') }}"></script>

  <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/dist/css/adminlte.min.css') }}">
  <link rel="icon" type="image/png" href="{{ asset('images/favicon_simdia.png') }} ">
  <link rel="stylesheet" href="{{ asset('theme/plugins/chart.js/Chart.min.css') }}  ">
  @routes
</head>
<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div  class="wrapper">
      <div></div>
          <!-- Navbar -->
          <nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
              </li>
            </ul>
          </nav>
         <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
          <!-- Brand Logo -->
          <a href="{{ route('simdia') }}" class="brand-link">
              <img src="{{ asset('theme/dist/img/citricos.png') }}" alt="SIMDIA" class="brand-image" style="opacity: .8">
              <span class="brand-text font-weight-light" style="color: orange; font-size: 25px !Important; font-family: Courier-Bold;">SIMDIA</span>
          </a>
          <!-- Sidebar -->
          <div class="sidebar">
              <!-- Sidebar user (optional) -->
              @if(Auth::check())
              <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                  <div class="image">
                    <br>
                    <i class="nav-icon fas fa-user fa-2x" style="color: white;"></i>
                  </div>
                  <div class="info text-center">
                        <a href="{{ route('session') }}" class="d-block">
                            @if($rol)
                                {{ $rolUser }}
                            @else
                                ------
                            @endif
                        </a>
                        @if(isset(Auth::user()->email))
                        <p style="color:#c2c7d0; font-size:10pt;">{{ Auth::user()->persona->name }} {{ Auth::user()->persona->apellido_paterno }} {{ Auth::user()->persona->apellido_materno }} <br> {{ Auth::user()->email }} <br> {{ Auth::user()->persona->personal_id }} </p>
                        @endif
                  </div>
              </div>
             @endif
              <!-- Sidebar Menu -->
              <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @if($rol)
                    @if(Auth::check())
                        @if((Auth::user()->hasRole('Técnico') && $rolUser == 'Técnico'))    
                            @include('chunks.menu.tecnico')
                        @endif
                        @if((Auth::user()->hasRole('Admin Junta') && $rolUser == 'Admin Junta'))
                            @include('chunks.menu.adminJunta')
                        @endif     
                    @endif
                @endif
                @if(!Auth::check())
                <li class="nav-item">         
                    <a href="{{ route('login') }}" class="nav-link"> 
                        <i class="nav-icon fas fa-sign-in-alt"></i>              
                        <p>
                            Iniciar sesión
                        </p>
                    </a>
                </li>
                @else
                <li class="nav-item">         
                    <a href="{{ route('logout') }}" class="nav-link"> 
                        <i class="nav-icon fas fa-power-off"></i> <p>Cerrar Sesión</p>
                    </a>
                </li>
                @endif
                </ul>
              </nav>
              <!-- /.sidebar-menu -->
          </div>
          <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <transition>
            <div class="content-wrapper" id="app">
                @yield('content')
            </div>
        </transition>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <!--<b>Version</b> 3.0.0-beta.1-->            
                <strong>Copyright &copy; <a href="http://www.siafeson.com/">Siafeson</a>. {{ date('Y') }}</strong>
            </div>
        </footer>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        </aside>
          <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="{{ asset('theme/plugins/fastclick/fastclick.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ asset('theme/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('theme/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('theme/dist/js/demo.js') }}"></script>  
    <script src="{{ asset('theme/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
 </body>
</html>