/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css'
import es from 'vuetify/lib/locale/es'
import VueGeolocation from 'vue-browser-geolocation';
import VueQr from 'vue-qr';
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import * as VueGoogleMaps from 'vue2-google-maps'

const moment = require('moment')
require('moment/locale/es')
window.Vue.use(require('vue-moment'),{
    moment
});

Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(VueQr);
Vue.use(VueGeolocation);
Vue.use(Vuex)
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCWqOylp8I-W7YSuNOESDEdZ-Op7HorDA8',
        libraries: 'places', 
        installComponents: true    
    },
});

Vue.component('home', require('./components/HomeComponent.vue').default);
Vue.component('reporte-avance', require('./components/Reportes/AvancesComponent.vue').default);
Vue.component('reporte-arco', require('./components/Reportes/ArcoComponent.vue').default);
Vue.component('actividad-trampeo', require('./components/Actividad/TrampeoComponent.vue').default);
Vue.component('actividad-control', require('./components/Actividad/ControlComponent.vue').default);
Vue.component('etiquetas', require('./components/EtiquetasComponent.vue').default);
Vue.component('usuarios', require('./components/UsuariosComponent.vue').default);

const app = new Vue({
    el: '#app',
    //router,
    vuetify: new Vuetify({
        lang: {
            locales: { es },
            current: 'es',
        },
    })
});

/*const routes = [
    { path : '/actividad', name: 'reporte-avance', component: require('./components/Reportes/AvancesComponent.vue').default },
    { path : '/trampeo',name: 'actividad-trampeo', component: require('./components/Actividad/TrampeoComponent.vue').default },
    { path : '/control',name: 'actividad-control', component: require('./components/Actividad/ControlComponent.vue').default },
    { path : '/etiquetas',name: 'etiquetas', component: require('./components/EtiquetasComponent.vue').default }
]

const router = new VueRouter({
    routes: routes,
    //mode: 'history'
})*/