<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/','apiController@index')->name('index');
//Simdia Normal
Route::get('/fenologia/arbol','apiController@fenologiaArbol')->name('fenologia-arbol');
Route::get('/fenologia/brote','apiController@fenologiaBrote')->name('fenologia-brote');
Route::get('/trampas/{id}','apiController@trampa')->name('trampas');
Route::get('/trampas/primera/{id}','apiController@firstLog')->name('primera');
Route::get('/observaciones','apiController@getObservaciones')->name('observaciones');
Route::post('/captura','apiController@addCaptura')->name('captura');
Route::post('/archivo','apiController@getArchivo')->name('archivo');
//SIMDIA Scoring
Route::post('/scoring/campos','apiController@camposScoring');
Route::post('/scoring/captura','apiController@addScoring')->name('resumen');
Route::post('/scoring/reenvio','apiController@reenviaScoring')->name('reenvio');

//SIMPP
Route::group(['prefix' => 'simpp'], function() {
    Route::get('/', 'Api\simppController@index');
    Route::get('/fenologia','Api\simppController@fenologia')->name('fenologia');
    Route::get('/observaciones','Api\simppController@getObservaciones')->name('observaciones');
    Route::get('/trampas/{id}','Api\simppController@trampa')->name('trampas');
    Route::get('/trampas/primera/{id}','Api\simppController@firstLog')->name('primera');
    Route::post('/captura','Api\simppController@addCaptura')->name('captura');
    Route::get('/variedades','Api\simppController@getVariedades')->name('variedades');
    Route::get('/envio','Envios\simppController@index')->name('sicafi');
});

//SIMET
Route::group(['prefix' => 'simet'], function() {
    Route::get('/', 'Api\simetContoller@index');
    Route::get('/redes','Api\simetContoller@redes')->name('fenologia');
    Route::get('/ubicaciones/{id}','Api\simetContoller@getUbicacion')->name('ubicaciones');
    Route::get('/ubicaciones/primera/{id}','Api\simetContoller@firstLog')->name('primera');
    Route::post('/captura','Api\simetContoller@addCaptura')->name('captura');
    Route::post('/ubicaciones/agregar','Api\simetContoller@addUbicacion')->name('add-ubicaciones');
    Route::get('/medidas','Api\simetContoller@medidas')->name('unidad-medida');
    Route::get('/temperaturas','Api\simetContoller@temperaturas')->name('unidad-temperaturas');
});

//SIMPIAL
Route::group(['prefix' => 'simpial'], function() {
    Route::get('/', 'Api\simpialController@index');
    Route::get('/fenologia','Api\simpialController@fenologia')->name('fenologia');
    Route::get('/ingrediente','Api\simpialController@ingrediente')->name('ingrediente');
    Route::get('/lugar','Api\simpialController@lugar')->name('lugar');
    Route::get('/medida','Api\simpialController@medida')->name('medida');
    Route::get('/variedad','Api\simpialController@variedad')->name('variedad');
    Route::get('/campos/{id}','Api\simpialController@campos')->name('campos');
    Route::get('/primera/{id}','Api\simpialController@firstLog')->name('primera');
    Route::get('/tablas', 'Api\simpialController@tablas')->name('tablas');
    Route::post('/muestreo','Api\simpialController@addMuestreo')->name('muestreo');
    Route::post('/muestreo/update','Api\simpialController@updateMuestreo')->name('muestreo-update');
});