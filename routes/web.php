<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('simdia.home');
})->name('simdia');
Route::group(['middleware' => [ 'auth' ]], function () {
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/logout', 'UsuarioController@logout')->name('logout');

	// Fenologías
	Route::get('/fenologias/arbol','FenologiaController@getFenologiaArbol')->name('fenologias-arbol');
	Route::get('/fenologias/brote','FenologiaController@getFenologiaBrote')->name('fenologias-brote');
	Route::get('/motivos','ObservacionController@getMotivos')->name('motivos');

	// Usuarios
	//Route::get('/usuarios/all','UsuarioController@usuariosAll')->name('usuarios');
	// Trmpas
	Route::get('/trampas', 'TrampaController@index')->name('listadoTrampas'); // Apartado de trampas
	Route::post('/trampas', 'TrampaController@trampasList')->name('trampas-list'); // Apartado de trampas

	Route::get('/trampas/exportar','ExcelController@trampasExportar')->name('trampas-exports');
	Route::post('trampas/all','TrampaController@index'); // Todas las trampas para apartado de trampas
	Route::post('/trampa','TrampaController@getTrampas')->name('getTrampas'); // Todas las trampas para formulario

	/**CQ */
	Route::get('control/tipo/control','Simdiacq\ControlController@index')->name('getTipo.control');
	Route::get('control/tipo/aplicacion','Simdiacq\AplicacionController@index')->name('getTipo.aplicacion');
	Route::get('control/numero/aplicacion','Simdiacq\AplicacionController@NoAplicacion')->name('getNumero.aplicacion');
	Route::get('control/biologico','Simdiacq\BiologicoController@index')->name('getControl.biologico');
	Route::get('control/producto','Simdiacq\ProductoController@index')->name('get.producto');
	Route::post('control/captura','Simdiacq\CapturaController@store')->name('add-captura-control');
	Route::post('control/captura/{id}','Simdiacq\CapturaController@update')->name('update-captura-control');
	Route::post('control/delete','Simdiacq\CapturaController@delete')->name('delete-captura-control');
	Route::post('control/trampa','Simdiacq\UbicacionesController@index')->name('getTrampas.control');
	Route::post('control/capturas','Simdiacq\CapturaController@index')->name('capturas-control');
	Route::post('control/trampa/superficie','Simdiacq\UbicacionesController@superficie')->name('getTrampas.superficie');
	Route::get('actividad/control/exportar/{desde}/{hasta}','ExcelController@ControlQuimicoExportar')->name('actividad-control-exports');

	// GEnerador de QR
	// Capturas - Registros - Actividad
	Route::get('/actividad', 'ActividadController@index')->name('actividad');
	Route::get('/actividad/exportar/{desde}/{hasta}','ExcelController@actividadExportar')->name('actividad-exports');
	Route::get('/recuperar', 'RecuperarController@index')->name('recuperar');
	Route::post('/getImeis', 'RecuperarController@getImeis');
	Route::post('/archivos', 'RecuperarController@getArchivos');
	Route::post('/readFile','RecuperarController@greadFile')->name('readFile');
	Route::post('/saveFile','RecuperarController@saveFile')->name('saveFile');
	Route::post('/capturas','RegistroController@getRegistros')->name('capturas');
	Route::post('/actividad/addCaptura','RegistroController@addCaptura')->name('add-captura');
	Route::post('/actividad/eliminarCaptura','RegistroController@eliminarCaptura')->name('delete-captura');
	Route::post('/actividad/getCaptura','RegistroController@getCaptura');
	Route::post('/actividad/preparacapadatos','RegistroController@preparacapadatos');
	Route::get('/createPasswords','CreateUsersController@createPasswords');
	Route::post('/generarqr','GenerarQRController@index')->name('qrs');


	Route::get('/actividad/control', 'ActividadController@control')->name('actividad.control');
	Route::post('/capturas/control','RegistroController@getRegistrosControl')->name('capturas.control');


	/**Autoridad Estatal */
	Route::get('/reportes', 'ReportesController@index')->name('reportes');
	Route::get('/mapa', 'ReportesEstadoController@index')->name('reportes-estado');
	Route::get('/mapas/medias/{semana}/{ano}', 'ReportesController@mediasEstados')->name('map-estado');
	Route::get('/listado/camposFocos/', 'ExcelController@focosExportar')->name('exports-focos');

	/**Tecnico */
	Route::post('/reportes/avance', 'AvancesController@avance')->name('avance');
	Route::post('/promedio/adultos/trampa', 'CampoController@promedioAdultosTrampa')->name('promedio-adultos-trampas');
	Route::get('/distribucion/espacial/{id}', 'CampoController@distribucionEspacial')->name('distribucion-espacial-trampas');
	

	/**Productor */
	Route::get('/productor/seleccionar/huerta','ProductorController@huertas')->name('huertas');
	Route::get('/productor/huerta/{id}','ProductorController@huertaId')->name('huerta');
	Route::get('/productor/huerta/semanas/{id}','ProductorController@obtenerSemanas')->name('huerta-semanas');
	Route::get('/productor/distribucion/umbral/{estado}/{campo}/{semana}','ProductorController@distribucionEspacialUmbral')->name('distribucion-umbral');
	Route::get('/productor/huerta/golpeteo/{id}/{semana}/{ano}','ProductorController@golpeteoSemana')->name('huerta-semanas-golp');
	Route::get('/productor/huerta/promedio/{campo}/{ano}','ProductorController@promedioGolp')->name('promedio-adultos-golpeteo');
	Route::get('/productor/trampas/promedio/{campo}/{ano}','ProductorController@promedioGolp')->name('promedio-adultos-golpeteo');

	Route::get('/productor/getanos/{campo}','ProductorController@getAnos')->name('reportes-productor-getanos');


	Route::get('/mapa/reporte/{id}', 'ReportesEstadoController@report')->name('estado');
	Route::get('/reportes/media/{ano}/{estado}/{tipo}','ReportesController@getmedia')->name('media-movil');
	Route::get('/reportes/media/estado/{ano}/{estado}/{arco}/{tipo}','ReportesEstadoController@getmedia')->name('media-movil');

	Route::get('/reportes/promedios/{ano}/{estado}/{tipo}','ReportesController@promedios')->name('reportes-promedios');
	Route::get('/reportes/promedios/{ano}/{tipo}','ReportesController@promediosAno')->name('reportes-promedios-anos');
	Route::get('/reportes/promedios/estado/{ano}/{tipo}/{estado}','ReportesEstadoController@promediosAno')->name('reportes-promedios-anos-estados');

	Route::get('/reportes/arcos/{estado}','ReportesEstadoController@getArcos')->name('get-arcos');
	Route::get('/reportes/promedios/estado/{ano}/{estado}/{tipo}/{arco}','ReportesEstadoController@promedios')->name('reportes-promedios-estado');
	Route::post('/tabla/principal', 'ReportesController@tablaPrincipal')->name('tabla-principal'); // Apartado de trampas
	Route::post('/tabla/principal/{id}', 'ReportesEstadoController@tablaPrincipal')->name('tabla-principal-estados'); 
	Route::get('/reportes/anos','ReportesController@anos')->name('reportes-anos');
	Route::get('/reportes/getanos','ReportesController@getAnos')->name('reportes-getanos');

	//Reportes
	Route::get('/reportes/avances/{id}','ReportesController@avances')->name('reportes-avances');
	Route::get('/reportes/usuarios/avances','ReportesController@avancesJuntas')->name('reportes-avances-usuarios');
	Route::get('/ultima/aplicacion/quimica/{id}','CampoController@aplicacionQuimica')->name('aplicacion-quimica');

	
	/** Usuarios y Roles */
	Route::get('/seleccionar','adminController@selectRol')->name('session');
	Route::post('/seleccionar/rol','adminController@rolSelected')->name('save-rol');
	Route::get('/usuarios','adminController@index')->name('getRoles');
	Route::post('/usuarios/addRol','adminController@createRol')->name('create-rol');
	Route::post('/usuarios/addPermiso','adminController@createPermiso')->name('create-permiso');
	Route::post('/roles/all','rolesController@index')->name('roles');
	Route::post('/usuarios/assignRole','adminController@addUserRoles')->name('assing-roles');
	Route::get('/roles','adminController@getRoles')->name('get-roles');
	Route::post('/roles/assignPermission','adminController@addRolesPermission')->name('assing-permission');
	Route::post('/usuarios/imeis','adminController@imeiUser')->name('imeis-users');
	Route::post('/usuarios/addImei','adminController@addImei')->name('add-imei');
	Route::post('/usuarios/deleteImei','adminController@deleteImei')->name('delete-imei');
	Route::post('/usuarios/update','adminController@updateUser')->name('update-user');
	Route::get('/usuarios/getUsers','adminController@getUsers')->name('get-users');
	Route::get('/usuarios/roles/asignar','adminController@assingRol')->name('assing-rol');
	Route::post('/usuarios/add','UsuarioController@store')->name('add-users');

	/**Juntas */
	Route::post('/juntas/all','juntasController@index')->name('juntas');
	/**Focos PDF */
	Route::get('/focos/pdf/{junta}/{semana}/{ano}','FocosController@pdf');


});
//Route::get('/reportes/promedios', 'ReportesController@promedios')->name('reportes-promedios');

//Auth::routes();
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Envio a SICAFI - SON
Route::get('/envio','EnvioSicafi@index');

// Porceso de focos traspatios
Route::get('/procesos/focos','FocosController@procesoFocos');

//CITRIEPLORE
Route::get('/citrixplor', function () { return view('/citriexplore/index'); });
Route::get('/citrixplor/junta', function () { return view('/citriexplore/junta'); });
Route::post('/citrixplor/resumenes','CitrixplorController@getResumenes');
Route::post('/citrixplor/detalles','CitrixplorController@getDetalles');
Route::post('/citrixplor/arboles','CitrixplorController@getArboles');
Route::post('/citrixplor/resumen','CitrixplorController@getResumenActividad');
Route::get('/citrixplor/info/{id}','CitrixplorController@getInfo');
Route::post('/citrixplor/meresumencampos','CitrixplorController@getMECampos');
Route::post('/citrixplor/meresumen','CitrixplorController@getMEResumen');
Route::post('/citrixplor/tblActividadMun','CitrixplorController@getResumenActividadMunicipio');
Route::post('/citrixplor/tblActividadJunta','CitrixplorController@getResumenActividadJunta');
Route::post('/citrixplor/tblActividadTecnico','CitrixplorController@getResumenActividadTecnico');

//SIMDIA REPORTES
Route::get('/tabla/semanal', 'ReportesController@cargarTablaSemanal')->name('cargar-tabla-principal');
Route::get('/tabla/mensual','ReportesController@cargarTablaMensual')->name('cargar-tabla-mensual');
Route::get('/tabla/anual','ReportesController@cargarTablaAnual')->name('cargar-tabla-anual');

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
