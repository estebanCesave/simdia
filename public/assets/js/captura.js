$(function () {
  const base_url = '/siafeson/public';
  //const base_url = '';
  var user_id =  0;
  var edit = 0;
  var captura_id;
  //getUser();
  getFenologiasArbol();
  getFenologiasBrote();
  getMotivos();
  $('#slcTrampas').val('');


  $("#agregar_captura").on("click", function()
  {
      $('#slcTrampas').val('');
      $('#slcUser').val('');     
      $('#slcFenologiaMain').val(0);    
      $('#num_insectos').val(""); 
      $('#switchInstalacion').prop('checked',false);
      $('#switchAdultoNorte').prop('checked',false);         
      $('#switchNinfaNorte').prop('checked',false);
      $('#switchAdultoSur').prop('checked',false);
      $('#switchNinfaSur').prop('checked',false);
      $('#switchAdultoEste').prop('checked',false);
      $('#switchNinfaEste').prop('checked',false);
      $('#switchAdultoOeste').prop('checked',false);
      $('#switchNinfaOeste').prop('checked',false);
      $('#slcFenologiaNorte').val(0); 
      $('#slcFenologiaSur').val(0); 
      $('#slcFenologiaEste').val(0); 
      $('#slcFenologiaOeste').val(0);
      edit = 0; 
      captura_id = 0;

  $('#slcTrampas').removeAttr('disabled');
  $('#slcUser').removeAttr('disabled');

  $('#modal-lg').modal({backdrop: 'static', keyboard: false})  
  $("#modal-lg").modal('show');
  getTrampas();
  getUser();
      
});


$("#agregar_captura_admin").on("click", function()
{
    $('#slcTrampas').val('');
    $('#slcUser').val('');     
    $('#slcFenologiaMain').val(0);    
    $('#num_insectos').val(""); 
    $('#switchInstalacion').prop('checked',false);
    $('#switchAdultoNorte').prop('checked',false);         
    $('#switchNinfaNorte').prop('checked',false);
    $('#switchAdultoSur').prop('checked',false);
    $('#switchNinfaSur').prop('checked',false);
    $('#switchAdultoEste').prop('checked',false);
    $('#switchNinfaEste').prop('checked',false);
    $('#switchAdultoOeste').prop('checked',false);
    $('#switchNinfaOeste').prop('checked',false);
    $('#slcFenologiaNorte').val(0); 
    $('#slcFenologiaSur').val(0); 
    $('#slcFenologiaEste').val(0); 
    $('#slcFenologiaOeste').val(0);
    edit = 0; 
    captura_id = 0;

$('#slcTrampas').removeAttr('disabled');
$('#slcUser').removeAttr('disabled');

$('#fecha').removeAttr('disabled')


$('#modal-lg_admin').modal({backdrop: 'static', keyboard: false})  
$("#modal-lg_admin").modal('show');
getTrampas();
getUser();
    
});



  $("#containerCaptura").on("click",".borrar", function(){
    captura_id = $(this).data("id");
    var json = {
      captura_id : captura_id
    }
    Swal.fire({
      title: 'Eliminar',
      text: "¿Seguro que desea eliminar esta captura?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: "No",
    }).then((result) => {
      if (result.value) {
        axios.post(base_url + '/actividad/eliminarCaptura',json)
        .then(function (response) {
        if(response.data['estatus']){
          Swal.fire({
              position: 'top-end',
              type: 'success',
              title: 'Captura eliminada',
              showConfirmButton: false,
              timer: 3000
          })
          setTimeout(function(){
            window.location.reload(1);
          }, 4000);
        }else{
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Algo salió mal!',
            footer: ''
          })
        }
        }).catch(function (error) { console.log(error); });
      }
    })  
  });
  $('#switchRevisada').on('change',function(){
      var revisada = document.getElementById('switchRevisada').checked;
      if(revisada == false){
        $("#slcMotivosMain").attr("disabled", false);
        $('#num_insectos').val('');
      }
      else{
        $("#slcMotivosMain").attr("disabled", true);
        $("#slcMotivosMain").val(0);
      }
  });
  $("#containerCaptura").on("click",".editar", function(){
      captura_id  = $(this).data("id");
      var json = {
        captura_id : captura_id
      }
       axios.post(base_url + '/actividad/getCaptura',json)
        .then(function (response) {
            var fechaFormat = response.data[0]['fecha'].substr(0,10);
            $('#fecha').val(fechaFormat);    
            $('#slcTrampas').html('<option value='+response.data[0]['trampa_id']+' selected>'+response.data[0]['trampa_text']+'</option>');           
            $('#slcTrampas').attr('disabled','disabled'); 
            $('#slcFenologiaMain').val(response.data[0]['fenologia_trampa_id']);    
            $('#num_insectos').val(response.data[0]['captura']); 
            $('#slcMotivosMain').val(response.data[0]['observaciones']);    

            $('#slcUser').html('<option value='+response.data[0]['id']+' selected>'+response.data[0]['name']+'</option>');           
            $('#slcUser').attr('disabled','disabled'); 

            if(response.data[0]['instalada'] == 1){
              $('#switchInstalacion').prop('checked',true);
            }else{
              $('#switchInstalacion').prop('checked',false);
            }
            if(response.data[0]['revisada'] == 1){
              $('#switchRevisada').prop('checked',true);
              $("#slcMotivosMain").attr("disabled", true);
            }else{
              $('#switchRevisada').prop('checked',false);
              $("#slcMotivosMain").attr("disabled", false);
            }
            if(response.data[0]['noa'] == 1){
              $('#switchAdultoNorte').prop('checked',true);
            }else{
              $('#switchAdultoNorte').prop('checked',false);
            } 
            if(response.data[0]['non'] == 1){
              $('#switchNinfaNorte').prop('checked',true);
            }else{
              $('#switchNinfaNorte').prop('checked',false);
            }  
            if(response.data[0]['sua'] == 1){
              $('#switchAdultoSur').prop('checked',true);
            }else{
              $('#switchAdultoSur').prop('checked',false);
            }  
            if(response.data[0]['sun'] == 1){
              $('#switchNinfaSur').prop('checked',true);
            }else{
              $('#switchNinfaSur').prop('checked',false);
            }  
            if(response.data[0]['esa'] == 1){
              $('#switchAdultoEste').prop('checked',true);
            }else{
              $('#switchAdultoEste').prop('checked',false);
            }  
            if(response.data[0]['esn'] == 1){
              $('#switchNinfaEste').prop('checked',true);
            }else{
              $('#switchNinfaEste').prop('checked',false);
            }  
            if(response.data[0]['oea'] == 1){
              $('#switchAdultoOeste').prop('checked',true);
            }else{
              $('#switchAdultoOeste').prop('checked',false);
            }  
            if(response.data[0]['oen'] == 1){
              $('#switchNinfaOeste').prop('checked',true);
            }else{
              $('#switchNinfaOeste').prop('checked',false);
            }  
            $('#slcFenologiaNorte').val(response.data[0]['nof']); 
            $('#slcFenologiaSur').val(response.data[0]['suf']); 
            $('#slcFenologiaEste').val(response.data[0]['esf']); 
            $('#slcFenologiaOeste').val(response.data[0]['oef']);
            edit = 1; 
            captura_id = response.data[0]['id'];
            $("#modal-lg").modal('show');
        }).catch(function (error) { 
          console.log(error); 
        });
    });
    $("#containerCaptura").on("click",".editarAdmin", function(){
      captura_id  = $(this).data("id");
      var json = {
        captura_id : captura_id
      }
       axios.post(base_url + '/actividad/getCaptura',json)
        .then(function (response) {
            var fechaFormat = response.data[0]['fecha'].substr(0,10);
            $('#fecha').val(fechaFormat);    
            $('#slcTrampas').html('<option value='+response.data[0]['trampa_id']+' selected>'+response.data[0]['trampa_text']+'</option>');           
            $('#slcTrampas').attr('disabled','disabled'); 
            $('#slcFenologiaMain').val(response.data[0]['fenologia_trampa_id']);    
            $('#num_insectos').val(response.data[0]['captura']); 
            $('#slcMotivosMain').val(response.data[0]['observaciones']);    

            $('#slcUser').html('<option value='+response.data[0]['id']+' selected>'+response.data[0]['name']+'</option>');           
            $('#slcUser').attr('disabled','disabled'); 

            $('#fecha').attr('disabled', 'disabled')

            if(response.data[0]['instalada'] == 1){
              $('#switchInstalacion').prop('checked',true);
            }else{
              $('#switchInstalacion').prop('checked',false);
            }
            if(response.data[0]['revisada'] == 1){
              $('#switchRevisada').prop('checked',true);
              $("#slcMotivosMain").attr("disabled", true);
            }else{
              $('#switchRevisada').prop('checked',false);
              $("#slcMotivosMain").attr("disabled", false);
            }
            if(response.data[0]['noa'] == 1){
              $('#switchAdultoNorte').prop('checked',true);
            }else{
              $('#switchAdultoNorte').prop('checked',false);
            } 
            if(response.data[0]['non'] == 1){
              $('#switchNinfaNorte').prop('checked',true);
            }else{
              $('#switchNinfaNorte').prop('checked',false);
            }  
            if(response.data[0]['sua'] == 1){
              $('#switchAdultoSur').prop('checked',true);
            }else{
              $('#switchAdultoSur').prop('checked',false);
            }  
            if(response.data[0]['sun'] == 1){
              $('#switchNinfaSur').prop('checked',true);
            }else{
              $('#switchNinfaSur').prop('checked',false);
            }  
            if(response.data[0]['esa'] == 1){
              $('#switchAdultoEste').prop('checked',true);
            }else{
              $('#switchAdultoEste').prop('checked',false);
            }  
            if(response.data[0]['esn'] == 1){
              $('#switchNinfaEste').prop('checked',true);
            }else{
              $('#switchNinfaEste').prop('checked',false);
            }  
            if(response.data[0]['oea'] == 1){
              $('#switchAdultoOeste').prop('checked',true);
            }else{
              $('#switchAdultoOeste').prop('checked',false);
            }  
            if(response.data[0]['oen'] == 1){
              $('#switchNinfaOeste').prop('checked',true);
            }else{
              $('#switchNinfaOeste').prop('checked',false);
            }  
            $('#slcFenologiaNorte').val(response.data[0]['nof']); 
            $('#slcFenologiaSur').val(response.data[0]['suf']); 
            $('#slcFenologiaEste').val(response.data[0]['esf']); 
            $('#slcFenologiaOeste').val(response.data[0]['oef']);
            edit = 1; 
            captura_id = response.data[0]['id'];
            $("#modal-lg_admin").modal('show');
        }).catch(function (error) { 
          console.log(error); 
        });
    });
    $("#btn_save").on("click", function()
    {
      var noa = document.getElementById('switchAdultoNorte').checked == true ? 1 : 0;
      var non = document.getElementById('switchNinfaNorte').checked == true ? 1 : 0;     
      var sua = document.getElementById('switchAdultoSur').checked == true ? 1 : 0;
      var sun = document.getElementById('switchNinfaSur').checked == true ? 1 : 0;     
      var esa = document.getElementById('switchAdultoEste').checked == true ? 1 : 0;
      var esn = document.getElementById('switchNinfaEste').checked == true ? 1 : 0;
      var oea = document.getElementById('switchAdultoOeste').checked == true ? 1 : 0;
      var oen = document.getElementById('switchNinfaOeste').checked == true ? 1 : 0;
      var instalada = document.getElementById('switchInstalacion').checked == true ? 1 : 0;
      var revisada = document.getElementById('switchRevisada').checked == true ? 1 : 0;
      var ano = $('#fecha').val().substr(0,4);
      //captura_id = $(this).data("id");
        if(validar_valores()){
          Cargando($('#slcFenologiaNorte').val());
         var json = {
            fecha:$('#fecha').val(),
            ano : ano,
            trampa:$('#slcTrampas').val(),
            fenologia:$('#slcFenologiaMain').val(),
            num_insectos:$('#num_insectos').val(),
            nof:$('#slcFenologiaNorte').val(),
            suf:$('#slcFenologiaSur').val(),
            esf:$('#slcFenologiaEste').val(),
            oef:$('#slcFenologiaOeste').val(),
            noa : noa,
            non : non,
            sua : sua,
            sun : sun,
            esa : esa,
            esn : esn,
            oea : oea,
            oen : oen,
            user_id : $('#slcUser').val(),
            instalada : instalada,
            edit : edit,
            captura_id : captura_id,
            revisada : revisada,
            motivo : $('#slcMotivosMain').val()
          };
          axios.post(base_url + '/actividad/addCaptura',json)
            .then(function (response) {
              if(response.data['estatus']){
                if(response.data['edit'] == 1){
                  var confirmText = 'Captura actualizada';
                }else{
                  var confirmText = 'Captura agregada';
                }
                Swal.fire({
                  position: 'top-end',
                  type: 'success',
                  title: confirmText,
                  showConfirmButton: false,
                  timer: 3000
                })
               setTimeout(function(){
                window.location.reload(1);
               }, 4000);
              }else{
                Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algo salió mal!',
                footer: ''
                })
              }
            }).catch(function (error) { console.log(error); });
       }
    });
    $(".is_integer").keyup(function(){
        var valor = $(this).val();
        if(valor != ""){
          if(isNaN(valor)){
            var lon = valor.length;
            valor = valor.substring(0, (lon -1));
            $(this).val(valor);
          }else{
            $(this).val(parseInt(valor));
          } 
        }
    });

  function getUser(){
    axios.get(base_url+'/usuarios/getUsers')
      .then(function (response) {
        crear_listaUsuarios("slcUser", response.data);
      }).catch(function (error) { 
        console.log(error); 
      });
  }
  $('#slcUser').on('change',function(){
    getTrampas();
  });
  function Cargando() {
    $('#modal_captura').block({
      theme: false,
      message: 'Cargando...'
    });
  }
  function getMotivos() {
    axios.get(base_url+'/motivos')
      .then(function (response) {
         crear_lista("slcMotivosMain", response.data);
      }).catch(function (error) { console.log(error); });
  }
  function getFenologiasArbol() {
    axios.get(base_url+'/fenologias/arbol')
      .then(function (response) {
        crear_lista("slcFenologiaMain", response.data);
      }).catch(function (error) { console.log(error); });
  }
  function getFenologiasBrote() {
    var slcFenologia;
    axios.get(base_url+'/fenologias/brote')
      .then(function (response) {
        for (var i = 0; i < 4; i++) {
          if(i == 0){
            slcFenologia = "slcFenologiaNorte";
          }else if(i == 1){
            slcFenologia = "slcFenologiaSur";
          }else if(i == 2){
            slcFenologia = "slcFenologiaEste";
          }else if(i == 3){
            slcFenologia = "slcFenologiaOeste";
          }
            crear_lista(slcFenologia, response.data);
        }                
      }).catch(function (error) { console.log(error); });
  }
  function getTrampas(edit = 0) {
    var json = {
      user_id: $('#slcUser').val(),
      edit : edit
    };
    axios.post(base_url+'/trampa',json)
      .then(function (response) {
         crear_lista("slcTrampas", response.data);
      }).catch(function (error) { console.log(error); });
  }
  function crear_lista(slc, dato, selected) {
    $("#" + slc).html('');
    $("#" + slc).append('<option value="0" selected> -- SELECCIONE -- </option>');
    $.each(dato, function (i, item) {
      $("#" + slc).append('<option value=' + dato[i]['value'] + '>' + dato[i]['name'].toUpperCase() + '</option>');
    });
    if (selected){ 
      $("#" + slc).val(selected); 
    }
  }
  function crear_listaUsuarios(slc, dato, selected) {
    
    $("#" + slc).html('');
    $("#" + slc).append('<option value="0" selected> -- SELECCIONE -- </option>');
    $.each(dato, function (i, item) {
      var nombre = dato[i]['name'] + ' '+ dato[i]['apellido_paterno'] + ' '+ dato[i]['apellido_materno'];
      $("#" + slc).append('<option value=' + dato[i]['id'] + '>' + nombre.toUpperCase() + '</option>');
    });
    if (selected){ 
      $("#" + slc).val(selected); 
    }
  }
  function validar_valores(){
    var revisada = document.getElementById('switchRevisada').checked == true;
    var cont = 0;
 
    if($('#fecha').val()==''){
       $('#fecha').addClass("is-invalid");
       cont ++;
     }else{
       $('#fecha').removeClass("is-invalid");
     }
      if(($('#slcTrampas').val()=='') || ($('#slcTrampas').val()==0) || ($('#slcTrampas').val() == null )){
       $('#slcTrampas').addClass("is-invalid");
       cont ++;
     }else{
       $('#slcTrampas').removeClass("is-invalid");
     }
      if($('#slcFenologiaMain').val()=='' || ($('#slcFenologiaMain').val()==0) || ($('#slcFenologiaMain').val()==null)){
       $('#slcFenologiaMain').addClass("is-invalid");
       cont ++;
     }else{
       $('#slcFenologiaMain').removeClass("is-invalid");
     }
     if(revisada == true)
     {
        if( ( $('#num_insectos').val() == '')  || ( isNaN( $('#num_insectos').val() ) ) ){
          $('#num_insectos').addClass("is-invalid");
          cont ++;
        }else{
          $('#num_insectos').removeClass("is-invalid"); 
        }
     }
     else if(revisada == false)
     {
        if($('#slcMotivosMain').val()=='' || ($('#slcMotivosMain').val()==0)){
          $('#slcMotivosMain').addClass("is-invalid");
          cont ++;
        }else{
          $('#slcMotivosMain').removeClass("is-invalid");
        }
     }
      if(($('#slcFenologiaNorte').val()=='') || ($('#slcFenologiaNorte').val()==0) || ($('#slcFenologiaNorte').val() == null)){
       $('#slcFenologiaNorte').addClass("is-invalid");
       cont ++;
     }else{
       $('#slcFenologiaNorte').removeClass("is-invalid");
     }
     if(($('#slcFenologiaSur').val()=='') || ($('#slcFenologiaSur').val()==0) || ($('#slcFenologiaSur').val() == null)){
       $('#slcFenologiaSur').addClass("is-invalid");
       cont ++;
     }else{
       $('#slcFenologiaSur').removeClass("is-invalid");
     }
     if(($('#slcFenologiaEste').val()=='') || ($('#slcFenologiaEste').val()==0) || ($('#slcFenologiaEste').val() == null)){
       $('#slcFenologiaEste').addClass("is-invalid");
       cont ++;
     }else{
       $('#slcFenologiaEste').removeClass("is-invalid");
     }
     if(($('#slcFenologiaOeste').val()=='') || ($('#slcFenologiaOeste').val()==0) || ($('#slcFenologiaOeste').val() == null)){
       $('#slcFenologiaOeste').addClass("is-invalid");
       cont ++;
     }else{
       $('#slcFenologiaOeste').removeClass("is-invalid");
     }
      
   if(cont>0){
       return false; 
   }else{
       return true;    
   }
 }
 $("#sendsicafi").click(function () { 
    var val = [];
    /**Filas por pagina */
    $(':checkbox.ads_Checkbox:checked').each(function(i){
      val[i] = $(this).val();
    }); 
    /** Todas las filas */
    /*var table = $("#capturas").DataTable();
    table.column(0).nodes().to$().each(function(index) {
      var test = $(this).find(':checkbox.ads_Checkbox:checked').val();
       val[index] = test;
     });*/
    if(val.length > 0){
      sendcapadatos(val);
    }
    else{
      Swal.fire({ 
        position: 'top-end', 
        type: 'warning', 
        title: 'No se seleccionó ningún registro para enviar a la capa de datos.', 
        showConfirmButton: false, 
        timer: 3000
      })
    }
 });
 function sendcapadatos(IDs){
   var json = {ids: IDs};
   axios.post(base_url + '/actividad/preparacapadatos',json).then(function (response) {
       if(response.data.status == "success")
       {
         Swal.fire({ 
           position: 'top-end', 
           type: 'success', 
           title: response.data.message, 
           showConfirmButton: false, 
           timer: 2000 
          });
         setTimeout(function(){ 
           window.location.reload(1); 
          },3000);
       }else{
         Swal.fire({ 
           position: 'top-end', 
           type: 'error', 
           title: "Hubó un error al procesar los registros.", 
           showConfirmButton: false, 
           timer: 3000 
          });
       }
   }).catch(function (error) { console.log(error); });
   
   /*axios.post(base_url + 'preparacapadatos',json).then(function (response) {
       if(response.data.status == "success")
       {
         Swal.fire({ 
             position: 'top-end', 
             type: 'success', 
             title: response.data.message, 
             showConfirmButton: false, 
             timer: 2000 
         });
         setTimeout(function(){ window.location.reload(1); }, 3000);
       }else{
         Swal.fire({ 
           position: 'top-end', 
           type: 'error', 
           title: response.data.message, 
           showConfirmButton: false, 
           timer: 3000 
         });
       }
   }).catch(function (error) { console.log(error); });*/
 }
});