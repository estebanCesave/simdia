<?php  
    function queryTrampas($imei){

        $query = "select t.id, t.name, t.status, t.latitud, t.longitud, t.siembra_id, c.name as campo
        from siafeson_simdia.trampas t
        inner join siafeson_simdia.rel_user_trampa r on t.id = r.trampa_id and t.status = 1 and r.status = 1
        inner join siafeson_simdia.rel_trampa_campo rtc on t.id = rtc.trampa_id and rtc.status = 1
        inner join siafeson_simdia.campos c on rtc.campo_id = c.id and c.status = 1
        inner join siafeson_siafeson.rel_user_imei rui on r.user_id = rui.user_id and rui.status = 1
        inner join siafeson_siafeson.imei i on rui.imei_id = i.id and i.name = '".$imei."'
        where t.tipo_huerta <> 3
        AND t.name NOT LIKE '%G%'";
        
        return $query;
    }

    function queryTrampasUser($user_id){

        $query = "select t.id as value, t.name
        from siafeson_simdia.trampas t
        inner join siafeson_simdia.rel_user_trampa r on t.id = r.trampa_id and t.status = 1 and r.status = 1
        inner join siafeson_simdia.rel_trampa_campo rtc on t.id = rtc.trampa_id and rtc.status = 1
        inner join siafeson_simdia.campos c on rtc.campo_id = c.id and c.status = 1
        where t.tipo_huerta <> 3
        AND r.user_id = ".$user_id."
        AND t.name NOT LIKE '%G%'";
        
        return $query;
    }

    function queryTrampasUserSemana($user_id,$semana,$ano){

         $query = "select t.id as value, t.name
        from siafeson_simdia.trampas t
        inner join siafeson_simdia.rel_user_trampa r on t.id = r.trampa_id and t.status = 1 and r.status = 1
        inner join siafeson_simdia.rel_trampa_campo rtc on t.id = rtc.trampa_id and rtc.status = 1
        inner join siafeson_simdia.campos c on rtc.campo_id = c.id and c.status = 1
        inner join siafeson_simdia.capturas cap on cap.trampa_id = t.id
        where t.tipo_huerta <> 3
        AND r.user_id = ".$user_id."
        AND cap.ano = ".$ano."
        AND cap.semana = ".$semana."
        and cap.status = 1
        AND t.name NOT LIKE '%G%'";
        
        return $query;
    }

    function getCoordenadas($trampa_id){
        $query = "select t.latitud,t.longitud
                from siafeson_simdia.trampas t 
                where t.id = ".$trampa_id;

        return $query;
    }
    function getQueryAdmin($id, $desde, $hasta, $ano, $junta)
    {

        $query = "select
              c.fecha::DATE,
              c.id,
              c.captura,
              c.revisada,
              c.instalada,
              c.ano,
              c.semana,
              c.ejercicioid,
              split_part(trim(c.posicion::text, '()'), ',', 1)::float AS lat,
              split_part(trim(c.posicion::text, '()'), ',', 2)::float AS lng,
              c.posicion,
              c.accuracy,
              c.distancia_qr,
              c.comentarios,
              c.fecha_cel,
              c.imei,
              t.name AS name,
              t.siembra_id,
              f.name AS fenologia,
              p.status,
              p.name as nombre,
              p.apellido_paterno as apellidopaterno,
              p.apellido_materno as apellidomaterno,
              ca.name as campo,
              c.inserted_sicafi,
              o.name as observaciones,
              c.noa as norte_adultos, 
              c.non as norte_ninfas, 
              c.nof as norte_fenologia, 
              c.sua as sur_adultos,
              c.sun as sur_ninfas, 
              c.suf as sur_fenologia, 
              c.esa as este_adultos, 
              c.esn as este_ninfas,
              c.esf as este_fenologia, 
              c.oea as oeste_adultos, 
              c.oen as oeste_ninfas, 
              c.oef as oeste_fenologia,
              c.created,
              case
              when c.method = 1
              then 'Móvil'
              when c.method = 2
              then 'Web'
              when c.method = 3
              then 'Modificado por web'
              when c.method = 4
              then 'Recuperado'
              when c.method = 5
              then 'Ingresado por Admin en semana siguiente'
              else 'N/A'
              end as Method,
              case
              when c.inserted_sicafi = 1
              then 'En SICAFI'
              when c.inserted_sicafi = 2
              then 'En proceso'
              else 'Sin enviar'
              end as sicafi,
              (select count(*) from siafeson_simdia.capturas as c1 where c1.semana = c.semana and c1.ano = c.ano and c1.status = c.status and c1.valido = c.valido and c1.trampa_id = c.trampa_id) as contador
              from
              siafeson_simdia.capturas as c
              inner join siafeson_simdia.trampas as t ON c.trampa_id = t.id
              inner join siafeson_simdia.rel_trampa_campo rc on rc.trampa_id = t.id and rc.status = 1
              inner join siafeson_simdia.campos ca on ca.id = rc.campo_id
              inner join siafeson_simdia.fenologias as f ON c.fenologia_trampa_id = f.id
              inner join siafeson_siafeson.users as u ON u.id = c.user_id
              inner join siafeson_siafeson.persona as p ON u.persona_id = p.id
              left join siafeson_simdia.observaciones as o ON c.observaciones = o.id
              where c.status = 1 
              and c.valido = 1 
              and c.fecha  between '". $desde ."' and '". $hasta ."'"."
              and c.ano = ". $ano . "
              and p.junta_id = ". $junta."
              order by c.fecha::DATE DESC";
        
            return $query;
    }
    function getQueryEstado($id, $desde, $hasta, $ano, $estado)
    {
        $query ="select
        c.fecha::DATE,
        c.id,
        c.ano,
        c.semana,
        c.ejercicioid,
        split_part(trim(c.posicion::text, '()'), ',', 1)::float AS lat,
        split_part(trim(c.posicion::text, '()'), ',', 2)::float AS lng,
        c.captura,
        c.revisada,
        c.instalada,
        c.comentarios,
        c.fecha_cel,
        c.imei,
        c.accuracy,
        t.name AS name,
        t.siembra_id,
        f.name AS fenologia,
        p.status,
        p.name as nombre,
        p.apellido_paterno as apellidopaterno,
        p.apellido_materno as apellidomaterno,
        c.inserted_sicafi,
        j.name as junta,
        ca.name as campo,
        e.name as estado,
        o.name as observaciones,
        c.noa as norte_adultos, 
        c.non as norte_ninfas, 
        c.nof as norte_fenologia, 
        c.sua as sur_adultos,
        c.sun as sur_ninfas, 
        c.suf as sur_fenologia, 
        c.esa as este_adultos, 
        c.esn as este_ninfas,
        c.esf as este_fenologia, 
        c.oea as oeste_adultos, 
        c.oen as oeste_ninfas, 
        c.oef as oeste_fenologia,
        c.created,
        case 
          when c.method = 1 
            then 'Móvil' 
          when c.method = 2 
            then 'Web' 
          when c.method = 3 
            then 'Modificado por web' 
          when c.method = 4 
            then 'Recuperado' 
          when c.method = 5 
            then 'Ingresado por Admin en semana siguiente' 
          else 'N/A' 
        end as Method,
        case 
          when c.inserted_sicafi = 1 
            then 'En SICAFI' 
          when c.inserted_sicafi = 2 
            then 'En proceso' 
          else 'Sin enviar' 
        end as sicafi,
        (select count(*) from siafeson_simdia.capturas as c1 where c1.semana = c.semana and c1.ano = c.ano and c1.status = c.status and c1.valido = c.valido and c1.trampa_id = c.trampa_id) as contador 
        from
        siafeson_simdia.capturas as c
        inner join siafeson_simdia.trampas as t ON c.trampa_id = t.id
        inner join siafeson_simdia.rel_trampa_campo rc on rc.trampa_id = t.id and rc.status = 1
        inner join siafeson_simdia.campos ca on ca.id = rc.campo_id
        inner join siafeson_simdia.fenologias as f ON c.fenologia_trampa_id = f.id
        inner join siafeson_siafeson.users as u ON u.id = c.user_id
        inner join siafeson_siafeson.persona as p ON u.persona_id = p.id
				INNER JOIN siafeson_siafeson.juntas as j ON j.id = p.junta_id
        INNER JOIN siafeson_siafeson.estados as e ON e.id = j.estado_id
        LEFT JOIN siafeson_simdia.observaciones as o ON c.observaciones = o.id
        where c.status = 1 
        and j.estado_id = ". $estado ."
				and c.valido = 1 
        and c.fecha  between '". $desde ."' and '". $hasta ."'"."
        and c.ano = ". $ano . "
        order by c.fecha::DATE DESC";
       
        return $query;
    }
    /**Reportes */
    function reporteSemanal()
    {
      $query = "select 	
                siafeson_siafeson.estados.id AS estado_id, 
                j.id AS junta_id,
                t.amefi as arco,
                ano AS Ano, 	
                semana AS Semana,
                COUNT(CASE WHEN (c.revisada = 1) THEN 1 ELSE NULL END) AS Revisadas,
                COUNT(CASE WHEN (c.revisada = 0) THEN 1 ELSE NULL END) AS NoRevisadas,
                CASE WHEN SUM(c.captura) IS NULL 
                  THEN NULL 
                ELSE SUM(c.captura) 
                END AS Capturas,
                COUNT(
                CASE WHEN (c.status = 1 AND c.valido = 1 AND c.revisada = 1 AND c.captura > 0) 
                  THEN 1 
                ELSE 
                  NULL END
                ) AS TramprasconCapturas
              FROM siafeson_simdia.capturas as c
              INNER JOIN siafeson_simdia.trampas as t ON t.id = c.trampa_id 
              INNER JOIN siafeson_siafeson.users ON c.user_id = users.id
              INNER JOIN siafeson_siafeson.juntas as j ON t.junta_id = j.id
              INNER JOIN siafeson_siafeson.estados ON estados.id = j.estado_id
              WHERE c.status = 1
              AND c.ano >= extract(year from now()) -1
              AND c.valido = 1
              AND t.tipo_huerta = 1
              AND estados.pais_id = 1
              AND c.semana <> 0
              GROUP BY estados.id, j.id, c.semana,c.ano, t.amefi
              ORDER BY estados.id, j.id  ASC, c.ano DESC,c.semana DESC";

      return $query;
    }
    function reporteMensual()
    {
      $query = "select 	
                siafeson_siafeson.estados.id AS estado_id, 
                siafeson_siafeson.juntas.id AS junta_id,
                t.amefi as arco,
                ano AS Ano, 	
                EXTRACT(MONTH FROM c.fecha) AS num_mes,
                CASE 
                  WHEN EXTRACT(MONTH FROM c.fecha) = 1 THEN 'Enero'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 2 THEN 'Febrero'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 3 THEN 'Marzo'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 4 THEN 'Abril'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 5 THEN 'Mayo'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 6 THEN 'Junio'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 7 THEN 'Julio'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 8 THEN 'Agosto'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 9 THEN 'Septiembre'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 10 THEN 'Octubre'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 11 THEN 'Noviembre'
                    WHEN EXTRACT(MONTH FROM c.fecha) = 12 THEN 'Diciembre'
                    ELSE 'N/A' END AS mes,
                COUNT(CASE WHEN (c.revisada = 1) THEN 1 ELSE NULL END) AS Revisadas,
                COUNT(CASE WHEN (c.revisada = 0) THEN 1 ELSE NULL END) AS NoRevisadas,
                CASE WHEN SUM(c.captura) IS NULL 
                  THEN NULL 
                ELSE SUM(c.captura) 
                END AS Capturas,
                COUNT(
                CASE WHEN (c.status = 1 AND c.valido = 1 AND c.revisada = 1 AND c.captura > 0) 
                  THEN 1 
                ELSE 
                  NULL END
                ) AS TramprasconCapturas
              FROM siafeson_simdia.capturas as c
              INNER JOIN siafeson_simdia.trampas as t ON t.id = c.trampa_id 
              INNER JOIN siafeson_siafeson.users ON c.user_id = users.id
              INNER JOIN siafeson_siafeson.juntas ON t.junta_id = juntas.id
              INNER JOIN siafeson_siafeson.estados ON estados.id = juntas.estado_id
              WHERE c.status = 1
              AND c.valido = 1
              AND t.tipo_huerta = 1
              AND estados.pais_id = 1
              AND EXTRACT(MONTH FROM c.fecha) > 0
              GROUP BY estados.id, juntas.id, t.amefi, EXTRACT(MONTH FROM c.fecha) ,c.ano
              ORDER BY estados.id, juntas.id, t.amefi, c.ano DESC, EXTRACT(MONTH FROM c.fecha) DESC";

          return $query;
    }
    function reporteAnual()
    {
        $query = "select 	
                  siafeson_siafeson.estados.id AS estado_id, 
                  siafeson_siafeson.juntas.id AS junta_id,
                  t.amefi as arco,
                  ano AS Ano, 	
                  COUNT(CASE WHEN (c.revisada = 1) THEN 1 ELSE NULL END) AS Revisadas,
                  COUNT(CASE WHEN (c.revisada = 0) THEN 1 ELSE NULL END) AS NoRevisadas,
                  CASE WHEN SUM(c.captura) IS NULL 
                    THEN NULL 
                  ELSE SUM(c.captura) 
                  END AS Capturas,
                  COUNT(
                  CASE WHEN (c.status = 1 AND c.valido = 1 AND c.revisada = 1 AND c.captura > 0) 
                    THEN 1 
                  ELSE 
                    NULL END
                  ) AS TrampasconCapturas
                FROM  siafeson_simdia.capturas as c
                INNER JOIN siafeson_simdia.trampas as t ON t.id = c.trampa_id 
                INNER JOIN siafeson_siafeson.users ON c.user_id = users.id
                INNER JOIN siafeson_siafeson.juntas ON t.junta_id = juntas.id
                INNER JOIN siafeson_siafeson.estados ON estados.id = juntas.estado_id
                WHERE c.status = 1
                AND c.valido = 1
                AND t.tipo_huerta = 1
                AND estados.pais_id = 1
                AND c.semana <> 0
                GROUP BY estados.id, juntas.id, c.ano, t.amefi
                ORDER BY estados.id, juntas.id  ASC, c.ano DESC";

        return $query;
    }
   /**Fin de reportes */
   function promedioAdultosTrampa($campo_id, $ano, $arco)
   {
        $query = "select
                  ARCO.ano ,
                  ARCO.semana,
                  ARCO.prom as arco,
                  CAMPO.prom as huerta
              FROM
                  (select
                      cpt.ano,
                      cpt.semana,
                      count(1),
                      avg(cpt.captura::decimal) as prom
                  from
                      siafeson_simdia.capturas cpt 
                      inner join siafeson_simdia.trampas t on cpt.trampa_id = t.id
                      inner join siafeson_simdia.rel_trampa_campo rtc on rtc.trampa_id = t.id
                      inner join siafeson_simdia.campos cmp on rtc.campo_id = cmp.id
                  where
                      t.amefi = '$arco'
                      and t.junta_id = (select junta_id from siafeson_simdia.campos where campo_id = $campo_id)
                      and cpt.status = 1
                      and cpt.valido = 1
                      and cpt.ano = $ano
                  group by
                      1,2
                  order by
                      1,2
                  ) as ARCO
                  LEFT join
                  (
                      select
                          cpt.ano,
                          cpt.semana,
                          avg(cpt.captura::decimal) as prom
                      from
                          siafeson_simdia.capturas cpt 
                          inner join siafeson_simdia.trampas t on cpt.trampa_id = t.id
                          inner join siafeson_simdia.rel_trampa_campo rtc on rtc.trampa_id = t.id
                          inner join siafeson_simdia.campos cmp on rtc.campo_id = cmp.id
                      where
                          cmp.campo_id = $campo_id
                          and cpt.status = 1
                          and cpt.valido = 1
                          and cpt.ano = $ano
                      group by
                          1,2
                  )CAMPO on ARCO.ano = CAMPO.ano and ARCO.semana = CAMPO.semana";

          return $query;
    }
    //SIMPP
    function queryTrampasSimpp($imei)
    {
        $query = "select
            t.id,
            t.id_sicafi,
            t.`name`,
            t.productor,
            t.latitud,
            t.longitud,
            t.status,
            t.variedad_id
        from
        siafeson_palomilla.trampas as t
        inner join siafeson_palomilla.rel_user_trampa as rut on t.id = rut.trampa_id and rut.status = 1
        inner join siafeson_siafeson.`user` as u on rut.user_id = u.id
        inner join siafeson_siafeson.rel_user_imei as rui on u.id = rui.user_id and rui.status = 1
        inner join siafeson_siafeson.imeis as i on i.id = rui.imei_id and i.name = '".$imei."'";
        
        return $query;
    }

    function queryTrampasSimgbn($imei){
      $query = "select
          t.id,
          t.id_sicafi,
          t.`name`,
          t.latitud,
          t.longitud,
          t.status
      from
      siafeson_simgbn.trampas as t
      inner join siafeson_simgbn.rel_user_trampa as rut on t.id = rut.trampa_id
      inner join siafeson_siafeson.`user` as u on rut.user_id = u.id
      inner join siafeson_siafeson.rel_user_imei as rui on u.id = rui.user_id
      inner join siafeson_siafeson.imeis as i on i.id = rui.imei_id and i.name = '".$imei."'";
  
      return $query;
    }
    function queryFocos(){
      $query = "
        select CONCAT(UPPER(LEFT(campos.name,1)),SUBSTR(LOWER(campos.name),2)) as campo, capturas.semana as semana, capturas.ano as ano, SUM(capturas.captura) AS capturas from siafeson_simdia.capturas
        INNER JOIN siafeson_simdia.trampas ON trampas.id = capturas.trampa_id AND trampas.tipo_huerta = 1 AND capturas.ano = 2019 AND capturas.status = 1 
        INNER JOIN siafeson_siafeson.juntas ON juntas.id = trampas.junta_id AND juntas.estado_id = 10
        INNER JOIN siafeson_simdia.campos ON campos.id = trampas.campo_id
        WHERE capturas.status = 1 AND capturas.valido = 1
        GROUP BY semana, ano, campo
        ORDER BY campo asc";

        return $query;
    }
