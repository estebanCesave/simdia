<?php
namespace siafeson;
use Illuminate\Database\Eloquent\Model;
 
class Persona extends Model
{
 
    protected $table = 'siafeson_siafeson.persona';
    public $timestamps = false;
   
   /* public function users()
    {
        return $this->belongsTo(User::class,'id','persona_id');
    }*/
    public function scopeSearch($query, $id)
	{
		return $query->where('junta_id',$id);
    }
    public function users()
    {
        return $this->hasMany('siafeson\User','persona_id');
    }
    public function junta()
    {
        return $this->belongsTo('siafeson\Models\Junta','junta_id','id');
    }
}