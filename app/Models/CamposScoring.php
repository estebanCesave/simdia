<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class CamposScoring extends Model
{
    protected $table = 'siafeson_simdia.campos_scoring';
    public $timestamps = false;
}
