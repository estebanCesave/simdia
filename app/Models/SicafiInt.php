<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class SicafiInt extends Model{
    protected $connection = 'sqlsrv_estados';
    protected $table = 'son.i_cprcMonitoreo_Psilido';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}
