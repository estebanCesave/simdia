<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Detalle extends Model
{
    protected $table = 'siafeson_simdia.detalle';
    public $timestamps = false;
}
