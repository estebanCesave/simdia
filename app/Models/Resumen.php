<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Resumen extends Model
{
    protected $table = 'siafeson_simdia.resumen';
    public $timestamps = false;
}
