<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Foco extends Model
{
    protected $table = 'siafeson_simdia.focos';
    public $timestamps = false;
}
