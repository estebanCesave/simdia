<?php
namespace siafeson\Models\SIMPIAL;
use Illuminate\Database\Eloquent\Model;

class Detalle extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_simpial.muestreo_detalle';
    public $timestamps = false;
}
