<?php
namespace siafeson\Models\SIMPIAL;
use Illuminate\Database\Eloquent\Model;

class Variedad extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_simpial.catVariedades';
    public $timestamps = false;
}
