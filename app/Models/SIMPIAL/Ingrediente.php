<?php
namespace siafeson\Models\SIMPIAL;
use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_simpial.catIngredientes';
    public $timestamps = false;
}
