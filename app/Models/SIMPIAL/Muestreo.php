<?php
namespace siafeson\Models\SIMPIAL;
use Illuminate\Database\Eloquent\Model;

class Muestreo extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_simpial.muestreo';
    public $timestamps = false;
}
