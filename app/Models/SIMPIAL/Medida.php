<?php
namespace siafeson\Models\SIMPIAL;
use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_simpial.medidas';
    public $timestamps = false;
}
