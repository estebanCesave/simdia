<?php
namespace siafeson\Models\SIMPIAL;
use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_simpial.catLugares';
    public $timestamps = false;
}
