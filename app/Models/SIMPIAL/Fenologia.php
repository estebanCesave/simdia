<?php
namespace siafeson\Models\SIMPIAL;
use Illuminate\Database\Eloquent\Model;

class Fenologia extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_simpial.catFenologias';
    public $timestamps = false;
}
