<?php
namespace siafeson\Models\Fenologia;
use Illuminate\Database\Eloquent\Model;

class Brote extends Model
{
    protected $table = 'siafeson_simdia.fenologia_brote';
    public $timestamps = false;
}
