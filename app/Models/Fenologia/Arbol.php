<?php
namespace siafeson\Models\Fenologia;
use Illuminate\Database\Eloquent\Model;

class Arbol extends Model
{
    protected $table = 'siafeson_simdia.fenologias';
    public $timestamps = false;
}
