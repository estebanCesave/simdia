<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class SonoraInt extends Model{
    protected $table = 'siafeson_simdia.son_i_cprcmonitoreo_psilido';
    public $timestamps = false;
}
