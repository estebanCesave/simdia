<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class CreateUsers extends Model
{
    protected $table = 'siafeson_siafeson.users';
    public $timestamps = false;
}
