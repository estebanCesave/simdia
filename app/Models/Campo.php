<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Campo extends Model
{
    protected $table = 'siafeson_simdia.campos';
    public $timestamps = false;
}
