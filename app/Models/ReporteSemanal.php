<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class ReporteSemanal extends Model
{
    protected $table = 'siafeson_simdia.reporte_semanal';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}
