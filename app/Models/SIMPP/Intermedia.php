<?php
namespace siafeson\Models\SIMPP;
use Illuminate\Database\Eloquent\Model;

class Intermedia extends Model
{
    protected $connection = 'mysql';
    protected $table = 'siafeson_palomilla.int_i_son_mfpapaTrampeo';
    public $timestamps = false;
}
