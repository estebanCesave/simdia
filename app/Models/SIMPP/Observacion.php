<?php
namespace siafeson\Models\SIMPP;
use Illuminate\Database\Eloquent\Model;

class Observacion extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_palomilla.observaciones';
    public $timestamps = false;
}
