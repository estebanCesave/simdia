<?php
namespace siafeson\Models\SIMPP;
use Illuminate\Database\Eloquent\Model;

class SicafiSon extends Model
{
    protected $connection = 'sqlsrv_sonora';
    protected $table = 'son.i_son_mfpapaTrampeo'; 
    protected $primaryKey = 'ID';
    public $timestamps = false;
}
