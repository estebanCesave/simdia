<?php
namespace siafeson\Models\SIMPP;
use Illuminate\Database\Eloquent\Model;

class Captura extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_palomilla.capturas';
    public $timestamps = false;
}
