<?php
namespace siafeson\Models\SIMPP;
use Illuminate\Database\Eloquent\Model;

class Variedad extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_palomilla.variedades';
    public $timestamps = false;
}
