<?php
namespace siafeson\Models\SIMPP;
use Illuminate\Database\Eloquent\Model;

class Fenologia extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_palomilla.fenologias';
    public $timestamps = false;
}
