<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Observacion extends Model
{
    protected $table = 'siafeson_simdia.observaciones';
    public $timestamps = false;
}
