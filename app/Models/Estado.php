<?php
namespace siafeson\Model;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'siafeson_siafeson.estados';
    public $timestamps = false;
}
