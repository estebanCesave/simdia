<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class rel_user_rol_sitio extends Model
{
    protected $table = 'siafeson_siafeson.rel_user_rol_sitio';
    public $timestamps = false;
}
