<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class ReporteAnual extends Model
{
    protected $table = 'siafeson_simdia.reporte_anual';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}
