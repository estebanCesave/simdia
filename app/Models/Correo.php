<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Correo extends Model
{
    protected $table = 'siafeson_simdia.control_email';
    public $timestamps = false;
}
