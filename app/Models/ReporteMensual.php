<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class ReporteMensual extends Model
{
    protected $table = 'siafeson_simdia.reporte_mensual';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}
