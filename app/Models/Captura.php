<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Captura extends Model
{
    protected $table = 'siafeson_simdia.capturas';
    public $timestamps = false;
}
