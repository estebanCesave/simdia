<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Umbral extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_simdia.umbrales';
    public $timestamps = false;
}
