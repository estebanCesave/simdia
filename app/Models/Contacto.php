<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    protected $table = 'siafeson_simdia.contactos_juntas';
    public $timestamps = false;
}
