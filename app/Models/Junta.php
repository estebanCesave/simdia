<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Junta extends Model
{
    protected $table = 'siafeson_siafeson.juntas';
    public $timestamps = false;
}
