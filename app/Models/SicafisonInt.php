<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class SicafisonInt extends Model{
    protected $connection = 'sqlsrv_sonora';
    protected $table = 'son.i_cprcMonitoreo_Psilido';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}
