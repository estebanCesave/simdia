<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class rel_user_imei extends Model
{
    protected $table = 'siafeson_siafeson.rel_user_imei';
    public $timestamps = false;
}
