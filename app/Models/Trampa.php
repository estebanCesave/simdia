<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Trampa extends Model{
    protected $table = 'siafeson_simdia.trampas';
    public $timestamps = false;
}
