<?php
namespace siafeson\Models\SIMET;
use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_pluviometros.ubicaciones';
    public $timestamps = false;
}
