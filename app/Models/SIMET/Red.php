<?php
namespace siafeson\Models\SIMET;
use Illuminate\Database\Eloquent\Model;

class Red extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_pluviometros.redes';
    public $timestamps = false;
}
