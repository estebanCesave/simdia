<?php
namespace siafeson\Models\SIMET;
use Illuminate\Database\Eloquent\Model;

class Captura extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_pluviometros.capturas';
    public $timestamps = false;
}
