<?php

namespace siafeson\Models\SIMET;

use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{
    //
    protected $connection= 'mysql';
    protected $table = 'siafeson_pluviometros.catMedidas';
    public $timestamps = false;
}
