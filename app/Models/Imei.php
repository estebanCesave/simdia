<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class Imei extends Model
{
    protected $table = 'siafeson_siafeson.imei';
    public $timestamps = false;
}
