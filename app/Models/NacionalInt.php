<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class NacionalInt extends Model{
    protected $table = 'siafeson_simdia.nac_i_cprcmonitoreo_psilido';
    public $timestamps = false;
}
