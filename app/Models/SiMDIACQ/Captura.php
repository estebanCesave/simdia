<?php
namespace siafeson\Models\SIMDIACQ;
use Illuminate\Database\Eloquent\Model;

class Captura extends Model
{
    protected $table = 'siafeson_simdia.cq_capturas';
    public $timestamps = false;
}
