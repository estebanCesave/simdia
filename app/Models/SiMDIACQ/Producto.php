<?php
namespace siafeson\Models\SiMDIACQ;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'siafeson_simdia.catproductos';
    public $timestamps = false;    
}
