<?php
namespace siafeson\Models\SiMDIACQ;
use Illuminate\Database\Eloquent\Model;

class Aplicacion extends Model
{
    protected $table = 'siafeson_simdia.cattipoaplicacion';
    public $timestamps = false;  
}
