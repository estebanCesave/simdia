<?php
namespace siafeson\Models\SiMDIACQ;
use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    protected $table = 'siafeson_simdia.cattipocontrol';
    public $timestamps = false;
}
