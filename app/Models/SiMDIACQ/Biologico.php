<?php
namespace siafeson\Models\SiMDIACQ;
use Illuminate\Database\Eloquent\Model;

class Biologico extends Model
{
    protected $table = 'siafeson_simdia.catcontrolbiologico';
    public $timestamps = false;
}
