<?php
namespace siafeson\Models\SiMDIACQ;
use Illuminate\Database\Eloquent\Model;

class NOAplicacion extends Model
{
    protected $table = 'siafeson_simdia.cq_catnoaplicacion';
    public $timestamps = false;
}
