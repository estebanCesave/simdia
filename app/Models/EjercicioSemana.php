<?php
namespace siafeson\Models;
use Illuminate\Database\Eloquent\Model;

class EjercicioSemana extends Model
{
    protected $connection= 'mysql';
    protected $table = 'siafeson_siafeson.ejerciciosemana';
    public $timestamps = false;
    
    public function scopeSemana($query, $varFecha)
	{
        return  $query->whereRaw("(? >= inicio AND ? <= fin)",[$varFecha, $varFecha]);
    }
}
