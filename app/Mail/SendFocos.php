<?php
namespace siafeson\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendFocos extends Mailable
{
    use Queueable, SerializesModels;
    public $focos;
    public $semana;
    public $ano;
    public $junta;
    public $juntaid;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($focos, $semana, $ano, $junta, $juntaid)
    {
        set_time_limit(60000);
        ini_set('max_execution_time', 60000);
        $this->focos = $focos;
        $this->semana = $semana;
        $this->ano = $ano;
        $this->junta = $junta;
        $this->juntaid = $juntaid;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->markdown('mails.sendFocos')
            ->with('name','Simdia')
            ->subject('SIMDIA Focos semana '. $this->semana.' - '.$this->ano);
    }
}
