<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Models\ReporteSemanal;
use siafeson\Models\ReporteMensual;
use siafeson\Models\ReporteAnual;
use siafeson\Models\EjercicioSemana;
use siafeson\Models\Estado;
use Yajra\Datatables\Datatables;
use Auth;
use DB;

class ReportesController extends Controller
{
    public $ejercicio;

    public function __construct()
    {
          ini_set('max_execution_time', 30000000);
          ini_set('memory_limit', '1024M');
          //ini_set('set_time_limit', 120000);
    }
    public function index()
    {
        $anos =  date("Y");
          /**General */
        $estados = DB::table('siafeson_simdia.trampas as t')->select('e.name')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.tipo_huerta',1)
            ->where('e.pais_id',1)
            ->where('e.id','<>',82)
            ->groupBy('e.id')->orderBy('e.name','ASC')
            ->get();

        $arcos = DB::table('siafeson_simdia.trampas as t')->select('e.name','t.arco')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.tipo_huerta',1)
            ->where('e.pais_id',1)
            ->where('e.id','<>',82)
            ->where('t.arco',DB::raw('NOT LIKE'),'%ZA%')
            ->where('t.arco',DB::raw('NOT LIKE'),'%HU%')
            ->groupBy('e.id')->groupBy('t.arco')
            ->get();

        $traspatios = DB::table('siafeson_simdia.trampas as t')->select('e.name','t.arco')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.tipo_huerta',2)
            ->where('e.pais_id',1)
            ->where('e.id','<>',82)
            ->where('t.arco',DB::raw('NOT LIKE'),'%ZA%')
            ->where('t.arco',DB::raw('NOT LIKE'),'%HU%')
            ->groupBy('e.id')->groupBy('t.arco')
            ->get();

        $superficie = DB::table('siafeson_simdia.trampas')->select(DB::raw('ROUND(SUM(superficie))'))->where('status', 1)->whereIn('tipo_huerta',array(1,2))->first();

        /**Trampas */
        $activas = DB::table('siafeson_simdia.trampas')->select('*')->where('status', 1)->where('tipo_huerta',1)->count();
        $trampaArcos = DB::table('siafeson_simdia.trampas')->select('*')->count();
        $urbanas = DB::table('siafeson_simdia.trampas')->select('*')->where('status', 1)->where('tipo_huerta',2)->count();
        /**Tecnicos */

        $tecnicosArcos =  DB::table('siafeson_simdia.capturas as c')->select('c.user_id')
            ->join('siafeson_simdia.trampas as t','t.id','=','c.trampa_id')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.tipo_huerta',1)
            ->where('c.status',1)
            ->where('c.valido',1)
            ->where('e.pais_id',1)        
            ->where('c.ano',$anos)
            ->groupBy('c.user_id')->having('c.user_id','>',0)
            ->get();

        $tecnicosZU = DB::table('siafeson_simdia.capturas as c')->select('c.user_id')
            ->join('siafeson_simdia.trampas as t','t.id','=','c.trampa_id')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.tipo_huerta',2)
            ->where('c.status',1)
            ->where('c.valido',1)
            ->where('e.pais_id',1)        
            ->where('c.ano',$anos)
            ->groupBy('c.user_id')->having('c.user_id','>',0)
            ->get();

        return view('simdia.autoridad.nacional.index', compact('estados', 'arcos', 'traspatios', 'superficie', 'activas', 'trampaArcos', 'urbanas', 'tecnicosArcos', 'tecnicosZU' ));
    }
    public function getmedia($ano, $estado, $tipo)
    {
        $pila = [];
        $semanas = [];
        $estado_id = Estado::where('name',$estado)->first();
          
        if($tipo == 1)
        {
            if($estado == 'Pais')
            {
                $promedio = DB::table('siafeson_simdia.reporte_semanal AS rs')
                    ->select(DB::raw('ROUND(SUM(rs.capturas) / ( SUM(rs.trampasconcapturas) + (SUM(rs.revisadas))  )::NUMERIC, 2 ) as data'), 's.semana')
                    ->rightJoin('public.semanas AS s', function($join) use($ano, $estado_id){   
                        $join->on('rs.semana','=','s.semana')->where('rs.ano',$ano);   
                    })
                    ->groupBy('s.semana')
                    ->orderBy('s.semana','ASC')
                    ->get();
            }
            else
            {
                $promedio = DB::table('siafeson_simdia.reporte_semanal AS rs')
                    ->select(DB::raw('ROUND(SUM(rs.capturas) / ( SUM(rs.trampasconcapturas) + (SUM(rs.revisadas))  )::NUMERIC, 2 ) as data'), 's.semana')
                    ->rightJoin('public.semanas AS s', function($join) use($ano, $estado_id){   
                        $join->on('rs.semana','=','s.semana')->where('rs.ano',$ano)->where('rs.estado_id',$estado_id->id);   
                    })
                    ->groupBy('s.semana')
                    ->orderBy('s.semana','ASC')
                    ->get();
            }
        }
        else
        {
            if($estado == 'Pais')
            {
                 $promedio = DB::table('siafeson_simdia.reporte_mensual AS rm')
                ->select(DB::raw('ROUND(SUM(rm.capturas) / ( SUM(rm.trampasconcapturas) + (SUM(rm.revisadas))  )::NUMERIC, 2 ) as data'), 'm.numero')
                ->rightJoin('public.meses AS m', function($join) use($ano, $estado_id){   
                    $join->on('rm.num_mes','=','m.numero')->where('rm.ano',$ano);
                })
                ->groupBy('m.numero')
                ->orderBy('m.numero','ASC')
                ->get();
            }
            else
            {
                $promedio = DB::table('siafeson_simdia.reporte_mensual AS rm')
                    ->select(DB::raw('ROUND(SUM(rm.capturas) / ( SUM(rm.trampasconcapturas) + (SUM(rm.revisadas))  )::NUMERIC, 2 ) as data'), 'm.numero')
                    ->rightJoin('public.meses AS m', function($join) use($ano, $estado_id){   
                        $join->on('rm.num_mes','=','m.numero')->where('rm.ano',$ano)->where('rm.estado_id',$estado_id->id);
                    })
                    ->groupBy('m.numero')
                    ->orderBy('m.numero','ASC')
                    ->get();
            }        
        }
        foreach($promedio as $p)
        {
            if(is_null($p->data)){
                 $p->data = '';
                array_push($pila,$p->data);
            }
            else{
                array_push($pila,(float)$p->data);
            }
            if($tipo == 2){
                array_push($semanas, $p->numero);
            }
            else{
                array_push($semanas, $p->semana);
            }
        }           
        return response()->json([
            'data' => $pila,
            'semanas' => $semanas,
            'min' =>  min($pila),
            'max' => max($pila)
        ]);
    }
    public function promedios($ano, $estado, $tipo)
    {
        $pila = [];
        $semanas = [];
        $estado_id = Estado::where('name',$estado)->first();
 
        if($tipo == 1)
        {
            if($estado == 'Pais')
            {
                $promedio = DB::table('siafeson_simdia.reporte_semanal AS rs')
                    ->select(DB::raw('ROUND( SUM(rs.capturas) / ( SUM(rs.revisadas) + SUM(rs.norevisadas) )::NUMERIC, 2 ) as data'), 's.semana' )
                    ->rightJoin('public.semanas AS s', function($join) use($ano, $estado_id){   
                        $join->on('rs.semana','=','s.semana')->where('rs.ano',$ano);   
                    })
                    ->groupBy('s.semana')
                    ->orderBy('s.semana','ASC')
                    ->get();
            }
            else
            {
                $promedio = DB::table('siafeson_simdia.reporte_semanal AS rs')
                    ->select(DB::raw('ROUND( SUM(rs.capturas) / ( SUM(rs.revisadas) + SUM(rs.norevisadas) )::NUMERIC, 2 ) as data'), 's.semana' )
                    ->rightJoin('public.semanas AS s', function($join) use($ano, $estado_id){   
                        $join->on('rs.semana','=','s.semana')->where('rs.ano',$ano)->where('rs.estado_id',$estado_id->id);   
                    })
                    ->groupBy('s.semana')
                    ->orderBy('s.semana','ASC')
                    ->get();
            }
        }
        else
        {
            if($estado == 'Pais')
            {
                $promedio = DB::table('siafeson_simdia.reporte_mensual as rm')
                    ->select(DB::raw('ROUND( SUM(rm.capturas) / ( SUM(rm.revisadas) + SUM(rm.norevisadas) )::NUMERIC, 2 ) as data'), 'm.mes')
                    ->rightJoin('public.meses as m', function($join) use($ano, $estado_id){
                        $join->on('rm.num_mes','=','m.numero')->where('rm.ano',$ano);
                    })
                    ->groupBy('m.mes','m.numero')
                    ->orderBy('m.numero','ASC')
                    ->get();
            }
            else
            {
                $promedio = DB::table('siafeson_simdia.reporte_mensual as rm')
                    ->select(DB::raw('ROUND( SUM(rm.capturas) / ( SUM(rm.revisadas) + SUM(rm.norevisadas) )::NUMERIC, 2 ) as data'), 'm.mes')
                    ->rightJoin('public.meses as m', function($join) use($ano, $estado_id){
                        $join->on('rm.num_mes','=','m.numero')->where('rm.ano',$ano)->where('rm.estado_id',$estado_id->id);
                    })
                    ->groupBy('m.mes','m.numero')
                    ->orderBy('m.numero','ASC')
                    ->get();
            }
        }        
        foreach($promedio as $p)
        {
            if(is_null($p->data)){
                $p->data = '';
                array_push($pila,$p->data);
            }
            else{
               array_push($pila,(float)$p->data);
            }
            if($tipo == 2){
                array_push($semanas, $p->mes);
            }
            else{
                array_push($semanas, $p->semana);
            }
        }           
        return response()->json([
            'data' => $pila,
            'semanas' => $semanas,
            'min' =>  min($pila),
            'max' => max($pila) 
        ]);
    }
    public function promediosAno($ano, $tipo)
    {
        $pila = [];
        $semanas = [];
        $min;
        $max;

        if($tipo == 1)
        {
            $promedio = DB::table('siafeson_simdia.reporte_semanal AS rs')
                ->select(DB::raw('ROUND( SUM(rs.capturas) / ( SUM(rs.revisadas) + SUM(rs.norevisadas) )::NUMERIC, 2 ) as data'), 's.semana' )
                ->rightJoin('public.semanas AS s', function($join) use($ano){   
                     $join->on('rs.semana','=','s.semana')->where('rs.ano',$ano);   
                })
                ->groupBy('s.semana')
                ->orderBy('s.semana','ASC')
                ->get();
        }
        else
        {
            $promedio = DB::table('siafeson_simdia.reporte_mensual as rm')
                ->select(DB::raw('ROUND( SUM(rm.capturas) / ( SUM(rm.revisadas) + SUM(rm.norevisadas) )::NUMERIC, 2 ) as data'), 'm.mes')
                ->rightJoin('public.meses as m', function($join) use($ano){
                    $join->on('rm.num_mes','=','m.numero')->where('rm.ano',$ano);
                })
                ->groupBy('m.mes','m.numero')
                ->orderBy('m.numero','ASC')
                ->get();
        } 
        foreach($promedio as $p)
        {
            if(is_null($p->data)){
                $p->data = '';
                array_push($pila,$p->data);
            }
            else{
               array_push($pila,(float)$p->data);
            }
            if($tipo == 2){
                array_push($semanas, $p->mes);
            }
            else{
                array_push($semanas, $p->semana);
            }
        }           
        if(empty($pila)){
            $min = 0;
            $max = 0;
        }
        else{
            $min = min($pila);
            $max = max($pila);
        }  
        return response()->json([
            'data' => $pila,
            'semanas' => $semanas,
            'min' =>  $min,
            'max' => $max 
        ]);
    }
    /** Tablas proncipales */
    public function tablaPrincipal()
    {
        $tablaPrincipal = DB::table('siafeson_simdia.reporte_semanal')->select(
            'ano', 
            'semana',
            DB::raw('sum(revisadas) as revisadas'),
            DB::raw('sum(norevisadas) as noRevisadas'),
            DB::raw('sum(capturas) as adultosDiaforina'),
            DB::raw('sum(trampasconcapturas) as trampasAdultos'))
            ->where('status',1)
            ->groupBy('ano','semana')
            ->orderBy('ano','DESC')
            ->orderBy('semana','DESC');
 
        return datatables()
            ->query($tablaPrincipal)
            ->make(true);
    }
    public function cargarTablaSemanal(){
        $query = reporteSemanal();    
        $trampas = DB::select(DB::raw($query));

        $nuevos = 0;
        $data = [];
       
        foreach ($trampas as $t) 
        {
            $existe = ReporteSemanal::where('semana',$t->semana)->where('ano',$t->ano)->where('estado_id',$t->estado_id)->where('junta_id',$t->junta_id)->where('arco',$t->arco)->delete();
           
            $reporte = new ReporteSemanal;
            $reporte->estado_id = $t->estado_id;
            $reporte->junta_id = $t->junta_id;
            $reporte->arco = $t->arco;
            $reporte->ano = $t->ano;
            $reporte->semana = $t->semana;
            $reporte->revisadas = $t->revisadas;
            $reporte->norevisadas = $t->norevisadas;
            $reporte->capturas = $t->capturas;
            $reporte->trampasconcapturas = $t->tramprasconcapturas;
            $reporte->status = 1;
            $reporte->created = date("Y-m-d H:i:s");
            $reporte->modified = date("Y-m-d H:i:s");
            $reporte->save();
            $nuevos = $nuevos + 1;
            array_push($data, $reporte);
        }

        return view('simdia.reportes.semanal', compact('data'));       
    }
    public function cargarTablaMensual(){
        $query = reporteMensual();    
        $trampas = DB::select(DB::raw($query));
        $nuevos = 0;
        $data = [];
  
        foreach ($trampas as $t) 
        {
            $existe = ReporteMensual::where('ano',$t->ano)->where('num_mes',$t->num_mes)->where('estado_id',$t->estado_id)->where('junta_id',$t->junta_id)->where('arco',$t->arco)->delete();
            
            $reporte = new ReporteMensual;
            $reporte->estado_id = $t->estado_id;
            $reporte->junta_id = $t->junta_id;
            $reporte->arco = $t->arco;
            $reporte->ano = $t->ano;
            $reporte->num_mes = $t->num_mes;
            $reporte->mes = $t->mes;
            $reporte->revisadas = $t->revisadas;
            $reporte->norevisadas = $t->norevisadas;
            $reporte->capturas = $t->capturas;
            $reporte->trampasconcapturas = $t->tramprasconcapturas;
            $reporte->status = 1;
            $reporte->created = date("Y-m-d H:i:s");
            $reporte->modified = date("Y-m-d H:i:s");
            $reporte->save();
            $nuevos = $nuevos + 1;
            array_push($data, $reporte);         
        }
        return view('simdia.reportes.mensual', compact('data'));       
    }
    public function cargarTablaAnual(){
        $query = reporteAnual();    
        $trampas = DB::select(DB::raw($query));
        $nuevos = 0;
        $actualizados = 0;

        foreach ($trampas as $t) 
        {
            $existe = ReporteAnual::where('ano',$t->ano)->where('arco',$t->arco)->where('estado_id',$t->estado_id)->where('junta_id',$t->junta_id)->first();
            if(!$existe)
            {
                $reporte = new ReporteAnual;
                $reporte->estado_id = $t->estado_id;
                $reporte->junta_id = $t->junta_id;
                $reporte->arco = $t->arco;
                $reporte->ano = $t->ano;
                $reporte->revisadas = $t->revisadas;
                $reporte->norevisadas = $t->norevisadas;
                $reporte->capturas = $t->capturas;
                $reporte->trampasconcapturas = $t->trampasconcapturas;
                $reporte->status = 1;
                $reporte->created = date("Y-m-d H:i:s");
                $reporte->modified = date("Y-m-d H:i:s");
                $reporte->save();
                $nuevos = $nuevos + 1;
            }
            else{
                //var_dump('Existe: ', $t->junta_id);
            }
        }
        return response()->json([
            'status' => 'success',
            'nuevos' => 'Se insertaron un total de: '.$nuevos.' registros nuevos',
            'actualizados' => 'Se actualizaron un total de: '.$actualizados.' registros',
        ]); 
    }
    public function anos(){
        $query = DB::table('siafeson_simdia.reporte_semanal')->select('ano')
            ->groupBy('ano')
            ->orderBy('ano','DESC')->get();
        return response()->json($query);
    }
    public function getAnos()
    {
        $pila = [];
        $query = DB::table('siafeson_simdia.reporte_semanal')->select('ano')
            ->groupBy('ano')
            ->orderBy('ano','ASC')->get();

        foreach ($query as $a) 
        {
            array_push($pila,$a->ano);
        }

        return $pila;    
    }
    public function mediasEstados($semana, $ano)
    {
        $promedio = DB::table('siafeson_simdia.reporte_semanal AS rs')
                ->select(DB::raw('ROUND(SUM(rs.capturas) / ( SUM(rs.trampasconcapturas) + (SUM(rs.revisadas))  )::NUMERIC, 2 ) as data'), 'e.name')
                ->Join('siafeson_siafeson.estados AS e','e.id','=','rs.estado_id')
                ->where('rs.semana',$semana)->where('rs.ano',$ano)
                ->groupBy('e.name')
                ->orderBy('e.name','ASC')
                ->get();

        $pais =  DB::table('siafeson_simdia.reporte_semanal AS rs')
                ->select(DB::raw('ROUND(SUM(rs.capturas) / ( SUM(rs.trampasconcapturas) + (SUM(rs.revisadas))  )::NUMERIC, 2 ) as data'))
                ->where('rs.semana',$semana)->where('rs.ano',$ano)
                ->first();

        return response()->json([
                    'status' => 'success',
                    'estados' => $promedio,
                    'pais' => $pais
                ]);
    }


    public function avances($id){
       return view('simdia.reportes.avances',compact('id'));        
    }    
    public function avancesJuntas(){
        return view('simdia.reportes.avancesjuntas');
    }    
}