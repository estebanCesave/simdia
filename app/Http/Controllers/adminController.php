<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\User;
use siafeson\Persona;
use siafeson\Models\Imei;
use siafeson\Models\rel_user_imei;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;
use DB;

class adminController extends Controller
{
    public function index(Request $request)
    {
        if(Auth::user()->hasrole('Admin Junta') && $request->session()->get('rol') == 'Admin Junta' )
        {
            return view('simdia.usuarios.index');  
        }
        else if(Auth::user()->hasrole('Admin Estado') && $request->session()->get('rol') == 'Admin Estado')
        {
            /** Trayendo Junta del administrador */        
            $junta = DB::table('siafeson_siafeson.persona')
                ->select('siafeson_siafeson.juntas.name as junta', 'siafeson_siafeson.juntas.id as juntaId')
                ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
                ->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
                ->where('siafeson_siafeson.users.id',Auth::user()->id)
                ->first();
            
            $estado = DB::table('siafeson_siafeson.persona')
                ->select('siafeson_siafeson.estados.name as estado','siafeson_siafeson.estados.id as estadoId')
                ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
                ->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
                ->join('siafeson_siafeson.estados','siafeson_siafeson.estados.id','=','siafeson_siafeson.juntas.estado_id')
                ->where('siafeson_siafeson.users.id',Auth::user()->id)
                ->first();
             
            $usuarios = DB::table('siafeson_siafeson.persona as p')
                ->select(
                    'u.id',
                    'u.username',
                    'u.email',
                    'p.name',
                    'p.apellido_paterno',
                    'p.apellido_materno',
                    'p.status',
                    'p.personal_id',
                    'j.name as junta', 
                    'e.name as estado')
                ->leftjoin('siafeson_siafeson.users as u','u.persona_id','=','p.id')
                ->join('siafeson_siafeson.juntas as j','j.id','=','p.junta_id')
                ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
                ->where('e.id',$estado->estadoId)
                ->get();

            $roles = Role::all();

            return view('admin.usuarios.index', compact('usuarios','roles','estado'));
        }
        else if(Auth::user()->hasrole('Super Admin') && $request->session()->get('rol') == 'Super Admin')
        {
            $usuarios = DB::table('siafeson_siafeson.persona as p')
            ->select(
                'u.id',
                'u.username',
                'u.email',
                'p.name',
                'p.apellido_paterno',
                'p.apellido_materno',
                'p.status',
                'p.personal_id',
                'j.name as junta', 
                'e.name as estado')
            ->leftjoin('siafeson_siafeson.users as u','u.persona_id','=','p.id')
            ->join('siafeson_siafeson.juntas as j','j.id','=','p.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->get();

            $roles = Role::all();

            return view('admin.usuarios.index', compact('usuarios','roles'));

        }
        else{
            abort(403);
        }
    }
    public function create()
    {
        return view('admin.create');
    }
    public function createRol(Request $request)
    {
        $role = Role::create(['name' => $request->name ]);

        if($role)
        {
            return response()->json([
                'status' => 'success',
                'message' => 'Rol creado correctamente'

            ]);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Ocurrio un error al crear el rol'

            ]);
        }
    }
    public function createPermiso(Request $request)
    {
        $permiso = Permission::create(['name' => $request->name]);

        if($permiso)
        {
            return response()->json([
                'status' => 'success',
                'message' => 'Permiso creado correctamente'

            ]);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Ocurrio un error al crear el permiso'

            ]);
        }
    }
    public function getRoles(Request $request)
    {
        if(Auth::user()->hasrole('Super Admin'))
        {
            $roles = Role::all();
            $permissions = Permission::all();
            return view('admin.roles.index', compact('roles','permissions'));
        }
        else{
            abort(403);
        }
    }
    public function addRolesPermission(Request $request)
    {
        $rol = Role::find($request->rolId);
        $permiso = Permission::find($request->permissionId);
        $test = $rol->hasPermissionTo($permiso->name);
        
        if($test)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Ya tienes permisos para ejecutar esta accion'
            ]);
        }
        else
        {
            $rol->givePermissionTo($permiso->name);
            return response()->json([
                'status' => 'success',
                'message' => 'Permiso asignado correctamente'
            ]);
        }
    }
    public function addUserRoles(Request $request)
    {
        $user = User::find($request->userId);
        $rol = Role::find($request->rolId);
        $test = $user->hasRole($rol->name);
        
        if($test)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Ya tienes asignado este rol'
            ]);
        }
        else
        {
            $user->assignRole($rol->name);
            return response()->json([
                'status' => 'success',
                'message' => 'Usuario asignado correctamente'
            ]);
        }
        
    }
    public function updateUser(Request $request)
    {
        if($request->contra == ''){
            return response()->json([
                'status' => 'error',
                'message' => 'Inserte contraseña nueva'
            ]);
        }
        else
        {
            $user = User::find($request->userId);
            $user->password = bcrypt($request->contra);
            $user->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Contraseña cambiada correctamente'
            ]);
        }
    }
    public function imeiUser(Request $request)
    {
        $imeis = DB::table('siafeson_siafeson.rel_user_imei')
            ->select('siafeson_siafeson.imei.id','siafeson_siafeson.imei.name','siafeson_siafeson.rel_user_imei.status')
            ->join('siafeson_siafeson.imei','siafeson_siafeson.imei.id','=','siafeson_siafeson.rel_user_imei.imei_id')
            ->where('siafeson_siafeson.rel_user_imei.status',1)
            ->where('siafeson_siafeson.rel_user_imei.user_id',$request->userId)
            ->get();

            return response()->json([
                'status'  => 'success',
                'rol' => 'Tecnico',
                'data' => $imeis
            ]);
    }
    public function addImei(Request $request)
    {
        $longitud = strlen($request->name);
        if($longitud < 15){
            return response()->json([
                'status' => 'error',
                'message' => 'Deben de ser 15 digitos'
            ]);
        }
        else
        {
            $existe =  Imei::where('name',$request->name)->first();
            if(!$existe)
            {

                $imei = new Imei();
                $imei->name = $request->name;
                $imei->save();
        
                $relacion = new rel_user_imei();
                $relacion->user_id = $request->userId;
                $relacion->imei_id = $imei->id;
                $relacion->status = 1;
                $relacion->created =  date("Y-m-d H:i:s");
                $relacion->modified =  date("Y-m-d H:i:s");
                $relacion->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Imei agregado correctamente'
                ]);

            }
            else
            {
                $existeRelacion =  rel_user_imei::where('imei_id',$existe->id)->first();
 
                if(!$existeRelacion)
                {
                    $relacion = new rel_user_imei();
                    $relacion->user_id = $request->userId;
                    $relacion->imei_id = $existe->id;
                    $relacion->status = 1;
                    $relacion->created =  date("Y-m-d H:i:s");
                    $relacion->modified =  date("Y-m-d H:i:s");
                    $relacion->save();

                    return response()->json([
                        'status' => 'success',
                        'message' => 'Imei agregado correctamente'
                    ]);

                }
                else
                {
                    if($existeRelacion->status == 0)
                    {
                        $existeRelacion->user_id = $request->userId;
                        $existeRelacion->imei_id = $existe->id;
                        $existeRelacion->status = 1;
                        $existeRelacion->modified =  date("Y-m-d H:i:s");
                        $existeRelacion->save();
                        
                        return response()->json([
                            'status' => 'success',
                            'message' => 'El imei ha sido asignado'
                        ]);
                    }
                    else
                    {
                        $persona =  DB::table('siafeson_siafeson.rel_user_imei')
                            ->select('siafeson_siafeson.persona.name', 'siafeson_siafeson.persona.apellido_paterno','siafeson_siafeson.persona.apellido_materno')
                            ->join('siafeson_siafeson.users','siafeson_siafeson.rel_user_imei.user_id','=','siafeson_siafeson.users.id')
                            ->join('siafeson_siafeson.persona','siafeson_siafeson.users.persona_id','=','siafeson_siafeson.persona.id')
                            ->where('siafeson_siafeson.rel_user_imei.status',1)
                            ->where('siafeson_siafeson.rel_user_imei.imei_id',$existe->id)
                            ->first();

                        $nombre = $persona->name.' '.$persona->apellido_paterno.' '.$persona->apellido_materno;     

                        return response()->json([
                            'status' => 'error',
                            'message' => 'Imei ya esta asignado a: '.$nombre
                        ]);
                    }
                   
                }

            }    
        }
   }
   public function deleteImei(Request $request)
   {
        $existeRelacion =  rel_user_imei::where('imei_id',$request->imei_id)->first();
        if($existeRelacion)
        {
            $existeRelacion->status = 0;
            $existeRelacion->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Imei desactivado correctamente'
            ]);
        }
   }
   public function getUsers(Request $request)
   {
        if(Auth::user()->hasrole('Admin Junta') && $request->session()->get('rol') == 'Admin Junta')
        {
            $usuarios = DB::table('public.roles as r')
                ->select('u.id','u.username','p.personal_id','r.name','u.email','p.name','p.apellido_paterno','p.apellido_materno','p.status', DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"))
                ->join('public.model_has_roles as m','m.role_id','=','r.id')
                ->join('siafeson_siafeson.users as u','u.id','=','m.model_id')
                ->join('siafeson_siafeson.persona as p','p.id','=','u.persona_id')
                ->where('p.junta_id',Auth::user()->persona->junta_id)->where('r.id',1)
                ->get();
        
            return response()->json($usuarios);
            
        }
        else if(Auth::user()->hasrole('Admin Estado') && $request->session()->get('rol') == 'Admin Estado')
        {
            $usuarios = DB::table('public.roles as r')
                ->select('u.id','u.username','r.name','u.email','p.name','p.apellido_paterno','p.apellido_materno','p.status', 'j.name as junta', 'e.name as estado', DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"))
                ->join('public.model_has_roles as m','m.role_id','=','r.id')
                ->join('siafeson_siafeson.users as u','u.id','=','m.model_id')
                ->join('siafeson_siafeson.persona as p','p.id','=','u.persona_id')
                ->join('siafeson_siafeson.juntas as j','j.id','=','p.junta_id')
                ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
                ->where('e.id',Auth::user()->persona->junta->estado_id)->where('r.id',1)
                ->get();

            return response()->json($usuarios);
        }
		else if(Auth::user()->hasrole('Técnico') && $request->session()->get('rol') == 'Técnico')
		{
            $usuarios = DB::table('public.roles as r')
                ->select('u.id','u.username','r.name','u.email','p.name','p.apellido_paterno','p.apellido_materno','p.status', DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"))
                ->join('public.model_has_roles as m','m.role_id','=','r.id')
                ->join('siafeson_siafeson.users as u','u.id','=','m.model_id')
                ->join('siafeson_siafeson.persona as p','p.id','=','u.persona_id')
                ->where('u.id',Auth::user()->id)
                ->get();

            return response()->json($usuarios);
        }    
   }
   public function assingRol()
   {
        set_time_limit(0);
        $relacionLevels = DB::table('siafeson_siafeson.aux_user_sitio')->select('*')->get();
        //Recorrer
        foreach ($relacionLevels as $r) 
        {
            /** Checa que el usuario no haya sido insertado anteriormente */
            $buscarUsuario = DB::table('public.model_has_roles')->select('*')
            ->where('role_id',$r->id_level)
            ->where('model_id',$r->id_user)    
            ->first();

            if(!$buscarUsuario)
            {
                $insertar = DB::table('public.model_has_roles')->insert(
                    ['role_id' => $r->id_level, 'model_type' => 'siafeson\User', 'model_id' => $r->id_user]
                );
            }
        }

        return ('Se han asignado rol en paqueteria Spatie!');
        \Log::info('Se han asignado rol en paqueteria Spatie!');
   }
   public function selectRol(Request $request)
   {
        $roles = Auth::user()->roles;
        $count = Auth::user()->roles->count();
 
        if($count > 1)
        {
            return view('simdia.roles', compact('roles'));
        }
        else{
            $nombre;
            
            foreach ($roles as $r) 
            {
                $nombre = $r->name;
            }
            $request->session()->put('rol', $nombre);

            if($request->session()->get('rol') == 'Admin Junta' || $request->session()->get('rol') == 'Admin Estado' )
            {
                return redirect('/actividad');
            }
            else if($request->session()->get('rol') == 'Técnico')
            {
                return redirect('reportes/avances/'.Auth::user()->id);
            }
            else if($request->session()->get('rol') == 'Super Admin')
            {
                return redirect('/usuarios');
            }
            else if($request->session()->get('rol') == 'Autoridad Nacional'){
                return redirect('/reportes');
            }
            else if($request->session()->get('rol') == 'Autoridad Estatal')
            {
                return redirect('/mapa/reporte/'.Auth::user()->persona->junta->estado_id);
            }
        }
   }
   public function rolSelected(Request $request)
   {
       $request->session()->put('rol', $request->rolUser);
 
       if($request->session()->get('rol') == 'Admin Junta' || $request->session()->get('rol') == 'Admin Estado' )
       {
           return redirect('/actividad');
       }
       else if($request->session()->get('rol') == 'Técnico')
       {
           return redirect('reportes/avances/'.Auth::user()->id);
       }
       else if($request->session()->get('rol') == 'Super Admin')
       {
           return redirect('/usuarios');
       }
       else if($request->session()->get('rol') == 'Autoridad Nacional'){
            return redirect('/reportes');
       }
       else if($request->session()->get('rol') == 'Productor'){
            return redirect('/productor/seleccionar/huerta');
       }
       else if($request->session()->get('rol') == 'Autoridad Estatal')
       {
            return redirect('/mapa/reporte/'.Auth::user()->persona->junta->estado_id);
       }
    }
}