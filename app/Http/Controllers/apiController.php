<?php
//namespace App\Http\Controllers;
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Models\Fenologia\Arbol;
use siafeson\Models\Fenologia\Brote;
use siafeson\Models\Trampa;
use siafeson\Models\Captura;
use siafeson\Models\Cap;
use siafeson\User;
use siafeson\Models\Detalle;
use siafeson\Models\CamposScoring;
use siafeson\Models\Resumen;
use siafeson\Models\Observacion;
use Spatie\Dropbox\Client;
use DB;
  
class apiController extends Controller
{
    public function __construct()
    {
        $this->dropbox = \Storage::disk('dropbox')->getDriver()->getAdapter()->getClient();   
    }
    public function index(){
       return 'SIMDIA Siafeson API';
    }
    public function fenologiaArbol(){
        $fenologia = Arbol::all();
        
        if($fenologia)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla árbol actualizada correctamente',
                    'data'=> $fenologia
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener los datos',
                    'data'=> null,
                    'log' =>  \Log::error('Ocurrio un error al obtener los datos (arbol)')
                   
                ]
            );
        }
        return response()->json($fenologia);
    }
    public function fenologiaBrote(){
        $fenologia = Brote::all();
        if($fenologia)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla brotes actualizada correctamente',
                    'data'=> $fenologia
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrio un error al obtener los datos',
                    'data'=> null,
                    'log' =>  \Log::error('Ocurrio un error al obtener los datos (Brotes)')

                ]
            );
        }
    }
    public function trampa($id){
        $query = queryTrampas($id);       
        $trampas = DB::select(DB::raw($query));
 
        if(!$trampas)
        {
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Sin trampas asignadas para este IMEI',
                    'data'=> null,
                    'log' =>  \Log::error('Sin trampas asignadas para este IMEI: '. $id)
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Ubicaciones actualizadas correctamente',
                    'count' => count($trampas),
                    'data' => $trampas,
                    'log' =>  \Log::info('Ubicaciones actualizadas correctamente: '. $id)
                ]
            );
        }
    }
    public function firstLog($id){
        $query = queryTrampas($id);       
        $trampas = DB::select(DB::raw($query));

        $fenologiaBrote = Brote::all();
        $fenologiaArbol = Arbol::all();
        $observaciones = Observacion::all();


        return response()->json(
            [
                'status'=>'success',
                'message' => 'Tablas actualizadas correctamente',
                'trampas'=> $trampas,
                'arbol' => $fenologiaArbol,  
                'brote' => $fenologiaBrote,
                'observaciones' => $observaciones,
                'log' =>  \Log::info('Ubicaciones actualizadas correctamente (primera vez): '. $id)

            ]
        );        
    }
    public function addCaptura(Request $request)
    {
        if(is_null($request->tipo)){
            $request->tipo = 'Movil';
        }
        
        $user = DB::table('siafeson_siafeson.rel_user_imei')
            ->select('siafeson_siafeson.users.id as user_id')
            ->join('siafeson_siafeson.users','siafeson_siafeson.users.id','=','siafeson_siafeson.rel_user_imei.user_id')
            ->join('siafeson_siafeson.imei','siafeson_siafeson.imei.id','=','siafeson_siafeson.rel_user_imei.imei_id')
            ->where('siafeson_siafeson.imei.name',$request->imei)
            ->where('siafeson_siafeson.rel_user_imei.status',1)
            ->first(); 
               
            if($user)
            {
                $existe = Captura::where('fecha',$request->fecha)->where('ano',$request->ano)->where('trampa_id',$request->trampa_id)->where('status',$request->status)->where('user_id',$user->user_id)->first();

                if(!$existe)
                {
                    $captura = new Captura();
                    $captura->fecha = $request->fecha;
                    $captura->trampa_id = $request->trampa_id;
                    $captura->user_id = $user->user_id;
                    $captura->posicion = $request->posicion;
                    $captura->accuracy = $request->accuracy;
                    $captura->fenologia_trampa_id = $request->fenologia_trampa_id;
                    $captura->captura = $request->captura;
                    $captura->imei = $request->imei;
                    $captura->noa = $request->noa;
                    $captura->non = $request->non;
                    $captura->nof = $request->nof;
                    $captura->sua = $request->sua;
                    $captura->sun = $request->sun;
                    $captura->suf = $request->suf;
                    $captura->esa = $request->esa;
                    $captura->esn = $request->esn;
                    $captura->esf = $request->esf;
                    $captura->oea = $request->oea;
                    $captura->oen = $request->oen;
                    $captura->oef = $request->oef;
                    $captura->status = $request->status;
    
                    if(is_null($request->id_bd_cel)){
                        $captura->id_bd_cel = 0;
                    }
                    else{
                        $captura->id_bd_cel = $request->id_bd_cel;
                    }
        
                    $captura->fecha_cel = $request->fecha_cel;
                    $captura->method = $request->method;
                    $captura->instalada = $request->instalada;
        
                    if(is_null($request->distancia_qr)){
                        $captura->distancia_qr = 0;
                    }
                    else{
                        $captura->distancia_qr = $request->distancia_qr;
                    }
                    
                    if(is_null($request->revisada)){
                        $captura->revisada = 1;
                    }
                    else{
                        $captura->revisada = $request->revisada;
                    }
                    $captura->observaciones = $request->observacion;
                    $captura->created =  date("Y-m-d H:i:s");
                    $captura->modified =  date("Y-m-d H:i:s");
                    $captura->save();
                                               
                    return response()->json(
                        [
                            'status'=>'success',
                            'data'=>$captura,
                            'log' =>  \Log::info('Captura agregada correctamente ('.$request->tipo.'): '. $captura)
                         ]
                    );
                }
                else{

                    return response()->json(
                        [
                            'status'=>'success',
                            'data'=> 'El registro ya existe',
                            'log' => \Log::error('El registro ya existe: '. $request->imei.' user_id: '.$user->user_id.' trampa_id: '.$request->trampa_id. ' fecha: '.$request->fecha.' año:'.$request->ano.' semana: '.$request->semana.' status: '.$request->status)
                        ]
                    );
                }    
            }
            else
            {
                return response()->json(
                    [
                        'status'=>'error',
                        'data'=> 'No se encontró usuario con tu IMEI',
                        'log' => \Log::error('No se encontró usuario con tu IMEI: '. $request->imei)
                    ]
                );
            }
    }
    public function getArchivo(Request $request)
    {
        try {
            $file = $request->file('file');
            $mimetype = \File::mimeType($file);
            $nombre = $file->getClientOriginalName();
            $imei = substr($nombre, 10, -25 );

            \Storage::disk('local')->put('android/simdia/'.$imei.'/'.$nombre,  \File::get($file));
           
            return response()->json(
                [
                    'status'=>'success',
                    'message'=> 'Archivo subido correctamente',
                    'name' => $nombre,
                    'log' => \Log::info('Archivo subido correctamente: '. $imei)
                ]
            );
        } catch (\Throwable $th) {
        
            \Storage::disk('dropbox')->putFileAs('/', 
                $request->file('file'), 
                $request->file('file')->getClientOriginalName()
            );

            return response()->json(
                [
                    'status'=>'error',
                    'message'=> 'Ocurrió un error al subir archivo: '.$th->getmessage(),
                    'request' => $request->file('file')->getClientOriginalName(),
                    'log' => \Log::error('Ocurrió un error al subir archivo: '. $request->file('file')->getClientOriginalName())

                ]
            );
        }
      
    }
    public function getObservaciones()
    {
        $observaciones = Observacion::all();
        if($observaciones)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla observaciones actualizada correctamente',
                    'data'=> $observaciones
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrio un error al obtener los datos',
                    'data'=> null,
                    'log' =>  \Log::error('Ocurrio un error al obtener los datos (observaciones)')

                ]
            );
        }

    }
    // ----------------------------------------------------
    // --------- S I M D I A  S C O R I N G ---------------
    // ----------------------------------------------------

    public function camposScoring(Request $request){

        $u = DB::table('siafeson_siafeson.rel_user_imei')
                ->select('siafeson_siafeson.persona.junta_id as junta_id')
                ->join('siafeson_siafeson.users','siafeson_siafeson.users.id','=','siafeson_siafeson.rel_user_imei.user_id')
                ->join('siafeson_siafeson.imei','siafeson_siafeson.imei.id','=','siafeson_siafeson.rel_user_imei.imei_id')
                ->join('siafeson_siafeson.persona','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
                ->where('siafeson_siafeson.rel_user_imei.status',1)
                ->where('siafeson_siafeson.imei.name',$request->imei)
                ->first();

        if($u){ 
            
            $junta_id = $u->junta_id; 

            $c = CamposScoring::select('siafeson_simdia.campos_scoring.id as id',
                'siafeson_simdia.campos_scoring.name as nombre',
                'siafeson_simdia.campos_scoring.latitud',
                'siafeson_simdia.campos_scoring.longitud',
                'siafeson_simdia.campos_scoring.superficie',
                'siafeson_simdia.variedades.nombre as variedad')
                ->join('siafeson_simdia.variedades','siafeson_simdia.variedades.id','=','siafeson_simdia.campos_scoring.variedad_id')
                ->where('siafeson_simdia.campos_scoring.junta_id',$junta_id)
                ->get(); 
        
        }else{
            $c = CamposScoring::select('siafeson_simdia.campos_scoring.id as id',
                'siafeson_simdia.campos_scoring.name as nombre',
                'siafeson_simdia.campos_scoring.latitud',
                'siafeson_simdia.campos_scoring.longitud',
                'siafeson_simdia.campos_scoring.superficie',
                'siafeson_simdia.variedades.nombre as variedad')
                ->join('siafeson_simdia.variedades','siafeson_simdia.variedades.id','=','siafeson_simdia.campos_scoring.variedad_id')
                ->join('siafeson_simdia.rel_imei_campo','siafeson_simdia.rel_imei_campo.campo_id','=','siafeson_simdia.campos_scoring.id')
                ->join('siafeson_siafeson.imei','siafeson_siafeson.imei.id','=','siafeson_simdia.rel_imei_campo.imei_id')
                ->where('siafeson_simdia.rel_imei_campo.status',1)
                ->where('siafeson_siafeson.imei.name',$request->imei)
                ->get(); 
        }
        return response()->json($c);
    }


    public function addScoring(Request $request){
        //return $request;
        try {
            \Log::info('CITRIXPLOR INSERT - DATO A INSERTAR: ' . json_encode($request, true) . ' \\n');
            //Revisar si ya existe
            $e = Resumen::where('fecha',$request->fecha)
                        ->where('campo_id',$request->campo_id)
                        ->where('id_bd_cel',$request->id)
                        ->where('imei',$request->imei)
                        ->first();

            if($e){
                return response()->json(
                    [
                        'status'=> 1,
                        'msj'=> "Guardado en linea",
                        'id_db_cel' => $request->id
                    ]
                );
            }else{

                $u = DB::table('siafeson_siafeson.rel_user_imei')
                ->select('siafeson_siafeson.users.id as user_id')
                ->join('siafeson_siafeson.users','siafeson_siafeson.users.id','=','siafeson_siafeson.rel_user_imei.user_id')
                ->join('siafeson_siafeson.imei','siafeson_siafeson.imei.id','=','siafeson_siafeson.rel_user_imei.imei_id')
                ->where('siafeson_siafeson.imei.name',$request->imei)
                ->first(); 

                if($u){ $user_id = $u->user_id; }else{ $user_id = 0; }

                //Corregir registros que se terminaron desde apartado de registros.
                if(!(isset($request->latitud)) && !(isset($request->longitud))){
                    $request->latitud = 1;
                    $request->longitud = -1;
                    $request->accuracy = 0;
                    $request->distancia_qr = 0;
                }

                $resumen = new Resumen();
                $resumen->fecha = $request->fecha;
                $resumen->campo_id = $request->campo_id;
                $resumen->posicion = $request->latitud.",".$request->longitud;
                $resumen->accuracy = $request->accuracy;
                $resumen->distancia_qr = $request->distancia_qr;
                $resumen->id_bd_cel = $request->id;
                $resumen->imei = $request->imei;
                $resumen->status = 1;
                $resumen->fecha_cel = $request->created;
                $resumen->method = 1;
                $resumen->tot_sintomas_hlb = $request->tot_sintomas_hlb;
                $resumen->tot_psilidos_adultos = $request->tot_psilidos_adultos;
                $resumen->tot_psilidos_ninfas = $request->tot_psilidos_ninfas;
                $resumen->tot_alto_adultos = $request->tot_alto_adultos;
                $resumen->tot_alto_ninfas = $request->tot_alto_ninfas;
                $resumen->tot_medio_adultos = $request->tot_medio_adultos;
                $resumen->tot_medio_ninfas = $request->tot_medio_ninfas;
                $resumen->tot_bajo_adultos = $request->tot_bajo_adultos;
                $resumen->tot_bajo_ninfas = $request->tot_bajo_ninfas;
                $resumen->user_id = $user_id;
                $resumen->created =  date("Y-m-d H:i:s");
                $resumen->modified =  date("Y-m-d H:i:s");
                $resumen->save();

                foreach($request->detalle as $d => $key){
                    $detalle = new Detalle();
                    $detalle->resumen_id = $resumen->id;
                    $detalle->posicion = $key['latitud'].",".$key['longitud'];
                    $detalle->accuracy = $key['accuracy'];
                    $detalle->sintomas_hlb = $key['simtomas_hlb'];
                    $detalle->psilidos_adultos = $key['psilidos_adultos'];
                    $detalle->psilidos_ninfas = $key['psilidos_ninfas'];
                    $detalle->alto_adultos = $key['alto_adultos'];
                    $detalle->alto_ninfas = $key['alto_ninfas'];
                    $detalle->medio_adultos = $key['medio_adultos'];
                    $detalle->medio_ninfas = $key['medio_ninfas'];
                    $detalle->bajo_adultos = $key['bajo_adultos'];
                    $detalle->bajo_ninfas = $key['bajo_ninfas'];
                    $detalle->status = 1;
                    $detalle->fecha_cel = $key['created'];
                    $detalle->secuencial = $key['secuencial'];
                    $detalle->created =  date("Y-m-d H:i:s");
                    $detalle->modified =  date("Y-m-d H:i:s");
                    $detalle->save();
                }
                \Log::info('CITRIXPLOR - Se inserto correctamente \\n');    
                return response()->json(
                    [
                        'status'=> 1,
                        'msj'=> "Guardado en linea",
                        'id_db_cel' => $request->id
                    ]
                );

            }

        }catch (\Illuminate\Database\QueryException $e) {
            \Log::error('CITRIXPLOR - No se inserto correctamente: '. $e->getMessage() .' \\n');
            return response()->json(
                [
                    'status'=> 0,
                    'msj'=>"No se pudo insetar el registro (reenvio try)",
                    'id_db_cel' => 0
                ]
            );
        }
        
    }

    public function reenviaScoring(Request $request){
        try {
            \Log::info('CITRIXPLOR REEINSERT - DATO A INSERTAR: ' . json_encode($request, true) . ' \\n');
            $insersiones = 0;

            $u = DB::table('siafeson_siafeson.rel_user_imei')
                ->select('siafeson_siafeson.users.id as user_id')
                ->join('siafeson_siafeson.users','siafeson_siafeson.users.id','=','siafeson_siafeson.rel_user_imei.user_id')
                ->join('siafeson_siafeson.imei','siafeson_siafeson.imei.id','=','siafeson_siafeson.rel_user_imei.imei_id')
                ->where('siafeson_siafeson.imei.name',$request->imei)
                ->first(); 

            if($u){ $user_id = $u->user_id; }else{ $user_id = 0; }      
            
            //Revisar si ya existe
            $e = Resumen::where('fecha',$request->fecha)->where('campo_id',$request->campo_id)->where('id_bd_cel',$request->id)->where('imei',$request->imei)->first();
            if($e){
                // No se inserta nada
                $resumen_id = $e->id;
            }else{
                //Corregir registros que se terminaron desde apartado de registros.
                if(!(isset($request->latitud)) && !(isset($request->longitud))){
                    $request->latitud = 1;
                    $request->longitud = -1;
                    $request->accuracy = 0;
                    $request->distancia_qr = 0;
                }
                //Se inserta en Resumen
                $resumen = new Resumen();
                $resumen->fecha = $request->fecha;
                $resumen->campo_id = $request->campo_id;
                $resumen->posicion = $request->latitud.",".$request->longitud;
                $resumen->accuracy = $request->accuracy;
                $resumen->distancia_qr = $request->distancia_qr;
                $resumen->id_bd_cel = $request->id;
                $resumen->imei = $request->imei;
                $resumen->status = 1;
                $resumen->fecha_cel = $request->created;
                $resumen->method = 1;
                $resumen->tot_sintomas_hlb = $request->tot_sintomas_hlb;
                $resumen->tot_psilidos_adultos = $request->tot_psilidos_adultos;
                $resumen->tot_psilidos_ninfas = $request->tot_psilidos_ninfas;
                $resumen->tot_alto_adultos = $request->tot_alto_adultos;
                $resumen->tot_alto_ninfas = $request->tot_alto_ninfas;
                $resumen->tot_medio_adultos = $request->tot_medio_adultos;
                $resumen->tot_medio_ninfas = $request->tot_medio_ninfas;
                $resumen->tot_bajo_adultos = $request->tot_bajo_adultos;
                $resumen->tot_bajo_ninfas = $request->tot_bajo_ninfas;
                $resumen->user_id = $user_id;
                $resumen->created =  date("Y-m-d H:i:s");
                $resumen->modified =  date("Y-m-d H:i:s");
                $resumen->save();
                $insersiones = $insersiones + 1;
                $resumen_id = $resumen->id;
            }
            //Detalles faltantes
            foreach($request->detalle as $d => $key){

                $d = Detalle::where('resumen_id',$resumen_id)
                ->where('secuencial',$key['secuencial'])
                ->first();

                if($d){
                    // No se inserta nada
                }else{
                    $detalle = new Detalle();
                    $detalle->resumen_id = $resumen_id;
                    $detalle->posicion = $key['latitud'].",".$key['longitud'];
                    $detalle->accuracy = $key['accuracy'];
                    $detalle->sintomas_hlb = $key['simtomas_hlb'];
                    $detalle->psilidos_adultos = $key['psilidos_adultos'];
                    $detalle->psilidos_ninfas = $key['psilidos_ninfas'];
                    $detalle->alto_adultos = $key['alto_adultos'];
                    $detalle->alto_ninfas = $key['alto_ninfas'];
                    $detalle->medio_adultos = $key['medio_adultos'];
                    $detalle->medio_ninfas = $key['medio_ninfas'];
                    $detalle->bajo_adultos = $key['bajo_adultos'];
                    $detalle->bajo_ninfas = $key['bajo_ninfas'];
                    $detalle->status = 1;
                    $detalle->fecha_cel = $key['created'];
                    $detalle->secuencial = $key['secuencial'];
                    $detalle->created =  date("Y-m-d H:i:s");
                    $detalle->modified =  date("Y-m-d H:i:s");
                    $detalle->save();
                    $insersiones = $insersiones + 1;
                }
                if($insersiones > 0){
                    \Log::info('CITRIXPLOR - Se inserto correctamente \\n');  
                    return response()->json(
                        [
                            'status'=> 1,
                            'msj'=> "Guardado en linea",
                            'id_db_cel' => $request->id
                        ]
                    );
                }else{
                    \Log::info('CITRIXPLOR - Ya existia en CITRIXPLOR \\n');  
                    return response()->json(
                        [
                            'status'=> 0,
                            'msj'=> "Ya existia en CITRIXPLOR",
                            'id_db_cel' => $request->id
                        ]
                    );
                }
            }


        }catch (\Illuminate\Database\QueryException $e) {
            \Log::error('CITRIXPLOR REENVIO - No se inserto correctamente: '. $e->getMessage() .' \\n');
            return response()->json(
                [
                    'status'=> 0,
                    //'msj'=>$e->getMessage(),
                    'msj'=>"Error al enviar el registro (try)",
                    'id_db_cel' => 0
                ]
            );
        }
    }
}