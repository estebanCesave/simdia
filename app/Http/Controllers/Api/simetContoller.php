<?php
namespace siafeson\Http\Controllers\Api;
use Illuminate\Http\Request;
use siafeson\Http\Controllers\Controller;
use siafeson\Models\SIMET\Captura;
use siafeson\Models\SIMET\Red;
use siafeson\Models\SIMET\Ubicacion;
use siafeson\Models\SIMET\Medida;
use DB;

class simetContoller extends Controller
{
    public function index(){
        return 'SIMET API LARAVEL';
    }
    public function redes(){
        $redes = Red::all();
        
        if($redes)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla redes actualizada correctamente',
                    'data'=> $redes
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener los datos',
                    'data'=> null,
                   
                ]
            );
        }
    }
    public function addCaptura(Request $request)
    {
        $fecha = date("Y-m-d", strtotime($request->fechaHora));
        $existe = Captura::where('ubicacion_id',$request->ubicacion_id)->where(DB::raw('DATE(fecha_hora_cel)'),$fecha)->where('status',1)->first();
        
        if(!$existe)
        {
            $captura = new Captura();
            $captura->ubicacion_id = $request->ubicacion_id;
            $captura->fecha_hora_cel = $request->fechaHora;
            $captura->imei = $request->imei;
            $captura->id_bd_cel = $request->id_bd_cel;
            $captura->latitud = $request->latitud;
            $captura->longitud = $request->longitud;
            $captura->accuracy = $request->accuracy;
            $captura->distancia = $request->distancia;
            if($request->medida == 2){
                $captura->valor_pre = $request->presipitacion * 25.4;
            }
            else{
                $captura->valor_pre = $request->presipitacion;
            }
            if($request->temperatura == 4){
                $captura->valor_tmax = ($request->maximo - 32) * 5 / 9;
                $captura->valor_tmin = ($request->minimo - 32) * 5 / 9;
            }
            else{
                $captura->valor_tmax = $request->maximo;
                $captura->valor_tmin = $request->minimo;
            }
            $captura->status = $request->status;
            $captura->created =  date("Y-m-d H:i:s");
            $captura->modified =  date("Y-m-d H:i:s");
            $captura->save();   

            return response()->json(
                [
                    'status'=>'success',
                    'data' => $captura,
                    'message' => 'Registro guardado localmente y en línea'
                ]
            );
        }
        else
        {
            return response()->json(
                [
                    'status'=>'warning',
                    'data'=> null,
                    'message' => 'Ya existe un registro en este dia'
                ]
            );
        }
    }   
    public function firstLog($id){
        $redes =  Red::all();
        $ubicacion = Ubicacion::where('imei',$id)->get();
        $medidas = Medida::where('tipo_medida_id',2)->get();
        $temperaturas = Medida::where('tipo_medida_id',1)->get();

        return response()->json(
            [
                'status'=>'success',
                'message' => 'Tablas actualizadas correctamente',
                'redes'=> $redes,
                'ubicaciones' => $ubicacion,
                'medidas' => $medidas,
                'temperaturas' => $temperaturas
            ]
        );        
    }
    public function getUbicacion($id){
        $ubicacion = Ubicacion::where('imei',$id)->get();

        if(!$ubicacion)
        {
            return response()->json(
                [
                    'status'=>'warning',
                    'message' => 'Sin trampas asignadas para este IMEI',
                    'data'=> null,
                 ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Ubicaciones actualizadas correctamente',
                    'count' => count($ubicacion),
                    'data' => $ubicacion,
                 ]
            );
        }

    }
    public function addUbicacion(Request $request)
    {
        $ubicacion = Ubicacion::find($request->id_sim);
        if(!$ubicacion)
        {
            $ubicacion = new Ubicacion();
            $ubicacion->name = $request->name;
            $ubicacion->latitud = $request->latitud;
            $ubicacion->longitud = $request->longitud;
            $ubicacion->red_id = $request->red;
            $ubicacion->status = $request->status;
            $ubicacion->imei = $request->imei;
            $ubicacion->medida_temp_id = $request->temperatura;
            $ubicacion->medida_prec_id = $request->medida;
            $ubicacion->created =  date("Y-m-d H:i:s");
            $ubicacion->modified =  date("Y-m-d H:i:s");
            $ubicacion->save(); 
        }
        else
        {
            $ubicacion->name = $request->name;
            $ubicacion->latitud = $request->latitud;
            $ubicacion->longitud = $request->longitud;
            $ubicacion->red_id = $request->red;
            $ubicacion->status = $request->status;
            $ubicacion->imei = $request->imei;
            $ubicacion->medida_temp_id = $request->temperatura;
            $ubicacion->medida_prec_id = $request->medida;
            $ubicacion->modified =  date("Y-m-d H:i:s");
            $ubicacion->save();   
        }    
        return response()->json(
            [
                'status'=>'success',
                'data' => $ubicacion,
                'id_simet' => $ubicacion->id,
                'message' => 'Registro guardado localmente y en línea'
            ]
        );
    }
    public function medidas(){
        $medidas = Medida::where('tipo_medida_id',2)->get();
        if($medidas)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla medidas actualizada correctamente',
                    'data'=> $medidas
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener los datos',
                    'data'=> null,
                   
                ]
            );
        }
    }
    public function temperaturas(){
        $temperaturas = Medida::where('tipo_medida_id',1)->get();
        if($temperaturas)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla temperaturas actualizada correctamente',
                    'data'=> $temperaturas
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener los datos',
                    'data'=> null,
                   
                ]
            );
        }
    
    }
}
