<?php
namespace siafeson\Http\Controllers\Api;
use Illuminate\Http\Request;
use siafeson\Http\Controllers\Controller;
use siafeson\Models\SIMPIAL\Fenologia;
use siafeson\Models\SIMPIAL\Ingrediente;
use siafeson\Models\SIMPIAL\Lugar;
use siafeson\Models\SIMPIAL\Medida;
use siafeson\Models\SIMPIAL\Variedad;
use siafeson\Models\SIMPIAL\Muestreo;
use siafeson\Models\SIMPIAL\Detalle;
use DB;

class simpialController extends Controller
{
    public function index(){
        return 'SIMPIAL API LARAVEL';
    }
    public function fenologia()
    {
    $fenologia = Fenologia::all();
        
        if($fenologia)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla fenologia actualizada correctamente',
                    'data'=> $fenologia
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener los datos',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPIAL: Ocurrio un error al obtener las fenologias')
                
                ]
            );
        }
        return response()->json($fenologia);
    }
    public function ingrediente()
    {
        $ingrediente = Ingrediente::all();
        
        if($ingrediente)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla ingredientes actualizada correctamente',
                    'data'=> $ingrediente
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener los datos',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPIAL: Ocurrio un error al obtener los ingredientes')
                
                ]
            );
        }
    }
    public function lugar()
    {
        $lugar = Lugar::all();
        
        if($lugar)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla lugares actualizada correctamente',
                    'data'=> $lugar
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener los datos',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPIAL: Ocurrio un error al obtener los lugares')
                
                ]
            );
        
        }
    }
    public function medida()
    {
        $medida = Medida::all();
        
        if($medida)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla lugares actualizada correctamente',
                    'data'=> $medida
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener las medidas',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPIAL: Ocurrio un error al obtener las medidas')
                
                ]
            );
        
        }
    }
    public function variedad()
    {
        $variedad = Variedad::all();
        
        if($variedad)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla variedades actualizada correctamente',
                    'data'=> $variedad
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener las medidas',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPIAL: Ocurrio un error al obtener las variedades')
                
                ]
            );
        
        }
    }
    public function campos($id)
    {
            
        $campos = DB::connection('mysql')->table('siafeson_simpial.rel_campo_user as r')
            ->select('c.*')
            ->join('siafeson_simpial.catCampos as c', 'c.id','=','r.campo_id')
            ->join('siafeson_siafeson.user as u', 'u.id','=','r.user_id')
            ->join('siafeson_siafeson.rel_user_imei as ri', 'ri.user_id','=','u.id')
            ->join('siafeson_siafeson.imeis as i', 'i.id','=','ri.imei_id')
            ->where('r.status',1)
            ->where('ri.status',1)
            ->where('i.name',$id)
            ->get();
            
        if(count($campos) == 0)
        {
            return response()->json(
                [
                    'status'=>'warning',
                    'message' => 'No hay campos asignados para este IMEI',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPIAL: No hay campos asignados para este IMEI: '. $id)
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Campos actualizados correctamente',
                    'count' => count($campos),
                    'data' => $campos,
                    'log' =>  \Log::info('SIMPIAL Campos actualizados correctamente: '. $id)
                ]
            );
        }
    }
    public function tablas()
    {
        $fenologia = Fenologia::all();
        $ingrediente = Ingrediente::all();
        $lugar = Lugar::all();
        $medida = Medida::all();
        $variedades = Variedad::all();

        return response()->json(
            [
                'status'=>'success',
                'message' => 'Tablas actualizadas correctamente',
                'fenologias' => $fenologia,  
                'ingredientes' => $ingrediente,  
                'lugares' => $lugar,
                'medida' => $medida,
                'variedades' => $variedades,
                'log' =>  \Log::info('SIMPIAL: Tablas actualizadas correctamente')
            ]
        );        
    }
    public function firstLog($id)
    {
        $campos = DB::connection('mysql')->table('siafeson_simpial.rel_campo_user as r')
        ->select('c.*')
        ->join('siafeson_simpial.catCampos as c', 'c.id','=','r.campo_id')
        ->join('siafeson_siafeson.user as u', 'u.id','=','r.user_id')
        ->join('siafeson_siafeson.rel_user_imei as ri', 'ri.user_id','=','u.id')
        ->join('siafeson_siafeson.imei as i', 'i.id','=','ri.imei_id')
        ->where('r.status',1)
        ->where('ri.status',1)
        ->where('i.name',$id)
        ->get();    
       

        $fenologia = Fenologia::all();
        $ingrediente = Ingrediente::all();
        $lugar = Lugar::all();
        $medida = Medida::all();
        $variedades = Variedad::all();

        return response()->json(
            [
                'status'=>'success',
                'message' => 'Tablas actualizadas correctamente',
                'campos'=> $campos,
                'fenologias' => $fenologia,  
                'ingredientes' => $ingrediente,  
                'lugares' => $lugar,
                'medida' => $medida,
                'variedades' => $variedades,
                'log' =>  \Log::info('SIMPIAL: Campos actualizados correctamente (primera vez): '. $id)

            ]
        );        
    }
    public function addMuestreo(Request $request)
    {
        $user = DB::connection('mysql')->table('siafeson_siafeson.rel_user_imei')
            ->select('siafeson_siafeson.user.id as user_id')
            ->join('siafeson_siafeson.user','siafeson_siafeson.user.id','=','siafeson_siafeson.rel_user_imei.user_id')
            ->join('siafeson_siafeson.imeis','siafeson_siafeson.imeis.id','=','siafeson_siafeson.rel_user_imei.imei_id')
            ->where('siafeson_siafeson.imeis.name',$request->imei)
            ->where('siafeson_siafeson.rel_user_imei.status',1)
            ->first();


        foreach ($request->muestreo as $m => $key) 
        {
            $muestreo = new Muestreo();
            $muestreo->imei = $key['imei'];
            $muestreo->fecha = $key['fecha'];
            $muestreo->fechaHora_cel = $key['fechaHora_cel'];
            $muestreo->fechaHora_sat = $key['fechaHora_sat'];
            $muestreo->latitud = $key['latitud'];
            $muestreo->longitud = $key['longitud'];
            $muestreo->accuracy = $key['accuracy'];
            $muestreo->distancia_qr = $key['distancia_qr'];
            $muestreo->campo_id = $key['campo_id'];
            $muestreo->plantas_muestreadas = $key['plantas_muestreadas'];
            $muestreo->plantas_afectadas = $key['plantas_afectadas'];
            $muestreo->sup_afectada = $key['sup_afectada'];
            $muestreo->infestacion = $key['infestacion'];
            $muestreo->diseminacion = $key['diserminacion'];
            $muestreo->fenologia_id = $key['fenologia_id'];
            $muestreo->observacion = $key['observacion'];
            $muestreo->id_bd_cel = $key['id_bd_cel'];
            $muestreo->user_id = $user->user_id;
            $muestreo->ejercicio = $key['ejercicio'];
            $muestreo->ano = $key['ano'];
            $muestreo->semana = $key['semana'];
            $muestreo->status =  1;
            $muestreo->created =  date("Y-m-d H:i:s");
            $muestreo->modified =  date("Y-m-d H:i:s");
            $muestreo->save();

        }
        foreach ($request->detalle as $detalle => $key) 
        {
            $detalle = new Detalle();
            $detalle->muestreo_id = $muestreo->id;
            $detalle->secuencial_detalle = $key['secuencial_detalle'] + 1;
            $detalle->fechaHora_cel = $key['fechaHora_cel'];
            $detalle->fechaHora_sat = $key['fechaHora_sat'];
            $detalle->id_bd_cel = $key['id_bd_cel'];
            $detalle->latitud = $key['latitud'];
            $detalle->longitud = $key['longitud'];
            $detalle->accuracy = $key['accuracy'];
            $detalle->presencia = $key['presencia'];
            $detalle->status =  1;
            $detalle->created =  date("Y-m-d H:i:s");
            $detalle->modified =  date("Y-m-d H:i:s");
            $detalle->save();
        }

        return response()->json(
            [
                'status'=>'success',
                'message' => 'Registro guardado localmente y en línea',
                'log' =>  \Log::info('SIMPIAL: Muestreo agregado correctamente: ')

            ]
        );        
    }
    public function updateMuestreo(Request $request)
    {
        foreach ($request->muestreo as $m => $key) 
        {
            $user = DB::connection('mysql')->table('siafeson_siafeson.rel_user_imei')
                ->select('siafeson_siafeson.user.id as user_id')
                ->join('siafeson_siafeson.user','siafeson_siafeson.user.id','=','siafeson_siafeson.rel_user_imei.user_id')
                ->join('siafeson_siafeson.imeis','siafeson_siafeson.imeis.id','=','siafeson_siafeson.rel_user_imei.imei_id')
                ->where('siafeson_siafeson.imeis.name',$request->imei)
                ->where('siafeson_siafeson.rel_user_imei.status',1)
                ->first(); 
                
            $existe = Muestreo::where('imei',$key['imei'])->where('fecha',$key['fecha'])->where('campo_id',$key['campo_id'])->first();

            if(!$existe)
            {
                $muestreo = new Muestreo();
                $muestreo->imei = $key['imei'];
                $muestreo->fecha = $key['fecha'];
                $muestreo->fechaHora_cel = $key['fechaHora_cel'];
                $muestreo->fechaHora_sat = $key['fechaHora_sat'];
                $muestreo->latitud = $key['latitud'];
                $muestreo->longitud = $key['longitud'];
                $muestreo->accuracy = $key['accuracy'];
                $muestreo->distancia_qr = $key['distancia_qr'];
                $muestreo->campo_id = $key['campo_id'];
                $muestreo->plantas_muestreadas = $key['plantas_muestreadas'];
                $muestreo->plantas_afectadas = $key['plantas_afectadas'];
                $muestreo->sup_afectada = $key['sup_afectada'];
                $muestreo->infestacion = $key['infestacion'];
                $muestreo->diseminacion = $key['diserminacion'];
                $muestreo->fenologia_id = $key['fenologia_id'];
                $muestreo->observacion = $key['observacion'];
                $muestreo->id_bd_cel = $key['id_bd_cel'];
                $muestreo->user_id = $user->user_id;
                $muestreo->ejercicio = 21;
                $muestreo->ano = 2020;
                $muestreo->semana = $key['semana'];
                $muestreo->status =  1;
                $muestreo->created =  date("Y-m-d H:i:s");
                $muestreo->modified =  date("Y-m-d H:i:s");
                $muestreo->save();
            }
            else
            {

                $detalle = new Detalle();
                $detalle->muestreo_id = $existe->id;
                $detalle->secuencial_detalle = $key['secuencial_detalle'] + 1;
                $detalle->fechaHora_cel = $key['d_fechaHora_cel'];
                $detalle->fechaHora_sat = $key['d_fechaHora_sat'];
                $detalle->id_bd_cel = $key['d_id_bd_cel'];
                $detalle->latitud = $key['d_latitud'];
                $detalle->longitud = $key['d_longitud'];
                $detalle->accuracy = $key['d_accuracy'];
                $detalle->presencia = $key['presencia'];
                $detalle->status =  1;
                $detalle->created =  date("Y-m-d H:i:s");
                $detalle->modified =  date("Y-m-d H:i:s");
                $detalle->save();
            }
        }
               
        return response()->json([
            'status' => 'success',
            'message' => 'Registro guardado localmente y en línea',
            'log' =>  \Log::info('SIMPIAL: Muestreo agregado correctamente subir archivo'),
            'data' => $request->all(),
        ]);
    }
}