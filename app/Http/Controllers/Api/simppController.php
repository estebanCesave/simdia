<?php
namespace siafeson\Http\Controllers\Api;
use Illuminate\Http\Request;
use siafeson\Http\Controllers\Controller;
use siafeson\Models\SIMPP\Fenologia;
use siafeson\Models\SIMPP\Observacion;
use siafeson\Models\SIMPP\Captura;
use siafeson\Models\SIMPP\Variedad;
use DB;
use siafeson\Models\EjercicioSemana;

class simppController extends Controller
{
    public $ejercicio;

    public function index(){
        return 'SIMPP API LARAVEL';
    }
    public function fenologia(){
        $fenologia = fenologia::all();
        
        if($fenologia)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla fenologia actualizada correctamente',
                    'data'=> $fenologia
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrió un error al obtener los datos',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPP: Ocurrio un error al obtener las fenologias')
                   
                ]
            );
        }
        return response()->json($fenologia);
    }
    public function getObservaciones()
    {
        $observaciones = Observacion::all();
        if($observaciones)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla observaciones actualizada correctamente',
                    'data'=> $observaciones
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrio un error al obtener los datos',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPP: Ocurrio un error al obtener los datos (observaciones)')

                ]
            );
        }
    }
    public function getVariedades()
    {
        $variedades = Variedad::all();
        
        if($variedades)
        {
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Tabla variedades actualizada correctamente',
                    'data'=> $variedades
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'error',
                    'message' => 'Ocurrio un error al obtener los datos',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPP: Ocurrio un error al obtener los datos (variedades)')

                ]
            );
        }

    }
    public function trampa($id){
        $query = queryTrampasSimpp($id);    
        $trampas = DB::connection('mysql')->select(DB::raw($query));
        
        if(!$trampas)
        {
            return response()->json(
                [
                    'status'=>'warning',
                    'message' => 'Sin trampas asignadas para este IMEI',
                    'data'=> null,
                    'log' =>  \Log::error('SIMPP: Sin trampas asignadas para este IMEI: '. $id)
                ]
            );
        }
        else{
            return response()->json(
                [
                    'status'=>'success',
                    'message' => 'Ubicaciones actualizadas correctamente',
                    'count' => count($trampas),
                    'data' => $trampas,
                    'log' =>  \Log::info('SIMPP Ubicaciones actualizadas correctamente: '. $id)
                ]
            );
        }
    }
    public function firstLog($id){
        $query = queryTrampasSimpp($id);    
        $trampas = DB::connection('mysql')->select(DB::raw($query));

        $fenologia = fenologia::all();
        $observaciones = Observacion::all();
        $variedades = Variedad::all();

        return response()->json(
            [
                'status'=>'success',
                'message' => 'Tablas actualizadas correctamente',
                'trampas'=> $trampas,
                'fenologia' => $fenologia,  
                'observaciones' => $observaciones,
                'variedades' => $variedades,
                'log' =>  \Log::info('SIMPP: Ubicaciones actualizadas correctamente (primera vez): '. $id)

            ]
        );        
    }
    public function addCaptura(Request $request)
    {
        if(is_null($request->tipo)){
            $request->tipo = 'Movil';
        }
       
        $user = DB::connection('mysql')->table('siafeson_siafeson.rel_user_imei')
            ->select('siafeson_siafeson.user.id as user_id')
            ->join('siafeson_siafeson.user','siafeson_siafeson.user.id','=','siafeson_siafeson.rel_user_imei.user_id')
            ->join('siafeson_siafeson.imeis','siafeson_siafeson.imeis.id','=','siafeson_siafeson.rel_user_imei.imei_id')
            ->where('siafeson_siafeson.imeis.name',$request->imei)
            ->where('siafeson_siafeson.rel_user_imei.status',1)
            ->first(); 
              
            if($user)
            {
                //$ejercicio = 21; //ejerciciosemana($request->fecha);
                $this->ejercicio = EjercicioSemana::select('semana', 'ejercicioid')->semana($request->fecha)->first();

                //$ejercicio['EjercicioID']
                $existe = Captura::where('ejercicio',$this->ejercicio->ejercicioid)->where('ano',$request->ano)->where('semana',$this->ejercicio->semana)->where('trampa_id',$request->trampa_id)->where('status',$request->status)->where('user_id',$user->user_id)->first();
                
                if(!$existe)
                {
                    $captura = new Captura();
                    $captura->trampa_id = $request->trampa_id;
                    $captura->fecha = $request->fecha;
                    $captura->ejercicio = $this->ejercicio->ejercicioid;
                    $captura->semana = $this->ejercicio->semana;
                    $captura->ano = $request->ano;
                    $captura->latitud = $request->latitud;
                    $captura->longitud = $request->longitud;
                    $captura->accuracy = $request->accuracy;
                    if(is_null($request->distancia_qr)){
                        $captura->distancia_qr = 0;
                    }
                    else{
                        $captura->distancia_qr = $request->distancia_qr;
                    }
                    $captura->capturas = $request->captura;
                    $captura->fenologia_id = $request->fenologia_id;
                    $captura->trampas_instaladas = $request->trampas_instaladas;
                    $captura->trampas_revisadas = $request->trampas_revisadas;
                    $captura->cambio_atrayente = $request->cambio_atrayente;
                    $captura->observacion_id = $request->observacion_id;
                    $captura->method = $request->method;
                    $captura->user_id = $user->user_id;
                    $captura->imei = $request->imei;
                    if(is_null($request->id_bd_cel)){
                        $captura->id_bd_cel = 0;
                    }
                    else{
                        $captura->id_bd_cel = $request->id_bd_cel;
                    }
                    $captura->fechaHora_cel = $request->fechaHora_cel;
                    $captura->fechaHora_sat = $request->fechaHora_sat;
                    $captura->status = $request->status;
                    $captura->created =  date("Y-m-d H:i:s");
                    $captura->modified =  date("Y-m-d H:i:s");
                    $captura->save();

                    $intermedia = Captura::find($captura->id);
                    $intermedia->inserted_sicafi = 0;
                    $intermedia->save();
        
                    return response()->json(
                        [
                            'status'=>'success',
                            'data' => $captura,
                            'message' => 'Registro guardado localmente y en línea',
                            'log' =>  \Log::info('SIMPP: Captura agregada correctamente ('.$request->tipo.'): '. $captura)
                        ]
                    );
                }
                else{
                     return response()->json(
                        [
                            'status'=>'warning',
                            'data'=> null,
                            'message' => 'El registro ya existe en el servidor',
                            'log' => \Log::error('SIMPP: El registro ya existe: '. $request->imei.' user_id: '.$user->user_id.' trampa_id: '.$request->trampa_id. ' fecha: '.$request->fecha.' año:'.$request->ano.' semana: '.$request->semana.' status: '.$request->status)
                        ]
                    );
                }    
            }
            else
            {
                return response()->json(
                    [
                        'status'=>'error',
                        'data'=> null,
                        'message' => 'No se encontró usuario con tu IMEI',
                        'log' => \Log::error('SIMPP: No se encontró usuario con tu IMEI: '. $request->imei)
                    ]
                );
            }
    }
}