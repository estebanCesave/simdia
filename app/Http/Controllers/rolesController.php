<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class rolesController extends Controller
{
    public function index(){
        $roles = Role::select('name as short_name','id')->get();
        return response()->json($roles);
    }
}
