<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Models\ReporteSemanal;
use siafeson\Models\Estado;

use DB;

class ReportesEstadoController extends Controller
{
    public function __construct()
    {
      	ini_set('max_execution_time', 60000);
    }
    public function index()
    {
        return view('simdia.autoridad.estatal.mapa');
    }
    public function report($id)
    {
        $anos =  date("Y");
        $estado_id = $id;

        $arcos = DB::table('siafeson_simdia.trampas as t')->select('e.name','t.arco')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.tipo_huerta',1)
            ->where('e.pais_id',1)
            ->where('e.id','=',$id)
            ->where('t.arco',DB::raw('NOT LIKE'),'%ZA%')
            ->where('t.arco',DB::raw('NOT LIKE'),'%HU%')
            ->groupBy('e.id')->groupBy('t.arco')
            ->get();
        
        $traspatios = DB::table('siafeson_simdia.trampas as t')->select('e.name','t.arco')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.tipo_huerta',2)
            ->where('e.pais_id',1)
            ->where('e.id','=', $id)
            ->where('t.arco',DB::raw('NOT LIKE'),'%ZA%')
            ->where('t.arco',DB::raw('NOT LIKE'),'%HU%')
            ->groupBy('e.id')->groupBy('t.arco')
            ->get();

        $superficie = DB::table('siafeson_simdia.trampas as t')
                ->select(DB::raw('ROUND(SUM(t.superficie))'), 'e.name as estado')
                ->where('t.status', 1)
                ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
                ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
                ->where('e.id','=', $id)->groupBy('estado')
                ->whereIn('t.tipo_huerta',array(1,2))->first();

        /**Trampas */
        $activas = DB::table('siafeson_simdia.trampas as t')->select('*')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.status', 1)->where('e.id','=',$id)->where('t.tipo_huerta',1)->count();

        $trampaArcos = DB::table('siafeson_simdia.trampas as t')->select('*')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')        
            ->where('t.status', 1)->where('e.id','=',$id)->where('t.tipo_huerta',1)->count();

         $urbanas = DB::table('siafeson_simdia.trampas as t')->select('*')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.status', 1)->where('e.id','=',$id)
            ->where('t.tipo_huerta',2)->count();
 
        /**Tecnicos */
        $tecnicosArcos =  DB::table('siafeson_simdia.capturas as c')->select('c.user_id')
            ->join('siafeson_simdia.trampas as t','t.id','=','c.trampa_id')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.tipo_huerta',1)
            ->whereNotNull('c.user_id')
            ->where('c.status',1)
            ->where('c.valido',1)
            ->where('e.id',$id)        
             ->where('c.ano',$anos)
            ->groupBy('c.user_id')->having('c.user_id','>',0)
            ->get();
        
        $tecnicosZU = DB::table('siafeson_simdia.capturas as c')->select('c.user_id')
            ->join('siafeson_simdia.trampas as t','t.id','=','c.trampa_id')
            ->join('siafeson_siafeson.juntas as j','j.id','=','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->where('t.tipo_huerta',2)
            ->whereNotNull('c.user_id')
            ->where('c.status',1)
            ->where('c.valido',1)
            ->where('e.id',$id)
            ->where('c.ano',$anos)
            ->groupBy('c.user_id')->having('c.user_id','>',0)
            ->get();
            

        return view('simdia.autoridad.estatal.index', compact('arcos', 'traspatios', 'superficie', 'activas', 'trampaArcos', 'urbanas', 'tecnicosArcos', 'tecnicosZU', 'estado_id'));

    }
    /** Tablas proncipales */
    public function tablaPrincipal($id)
    {
        $tablaPrincipal = DB::table('siafeson_simdia.reporte_semanal')->select(
            'ano', 
            'semana',
            DB::raw('sum(revisadas) as revisadas'),
            DB::raw('sum(norevisadas) as noRevisadas'),
            DB::raw('sum(capturas) as adultosDiaforina'),
            DB::raw('sum(trampasconcapturas) as trampasAdultos'))
            ->where('status',1)->where('estado_id',$id)
            ->groupBy('ano','semana')
            ->orderBy('ano','DESC')
            ->orderBy('semana','DESC');
 
        return datatables()
            ->query($tablaPrincipal)
            ->make(true);
    }
    public function getArcos($estado)
    {
        $pila = [];
        $arcos = DB::table('siafeson_simdia.reporte_semanal as rs')
                ->select('rs.arco')
                ->join('siafeson_siafeson.estados as e','e.id','=','rs.estado_id')
                ->where('e.name',$estado)
                ->groupBy('rs.arco')
                ->orderBy('rs.arco','asc')
                ->get();
        
        foreach($arcos as $a)
        {
            array_push($pila,$a->arco);
        }
        return $pila;

    }
    public function getmedia($ano,$estado, $arco, $tipo)
    {
        $pila =[];
        $semanas = [];
        $estado_id = Estado::where('name',$estado)->first();

        if($tipo == 1)
        {
            $promedio = DB::table('siafeson_simdia.reporte_semanal AS rs')
                ->select(DB::raw('ROUND(SUM(rs.capturas) / ( SUM(rs.trampasconcapturas) + (SUM(rs.revisadas))  )::NUMERIC, 2 ) as data'), 's.semana' )
                ->rightJoin('public.semanas AS s', function($join) use($ano, $arco, $estado_id){   
                    $join->on('rs.semana','=','s.semana')->where('rs.ano',$ano)->where('rs.arco',$arco)->where('rs.estado_id',$estado_id->id);   
                })
                ->groupBy('s.semana')->groupBy('rs.arco')
                ->orderBy('s.semana','ASC')
                ->get(); 
        }
        else
        {
            $promedio = DB::table('siafeson_simdia.reporte_mensual AS rm')
                ->select(DB::raw('ROUND(SUM(rm.capturas) / ( SUM(rm.trampasconcapturas) + (SUM(rm.revisadas))  )::NUMERIC, 2 ) as data'), 'm.mes')
                ->rightJoin('public.meses AS m', function($join) use($ano, $arco,  $estado_id){   
                    $join->on('rm.num_mes','=','m.numero')->where('rm.ano',$ano)->where('rm.arco',$arco)->where('rm.estado_id',$estado_id->id);   
                })
                ->leftJoin('siafeson_siafeson.estados as e', function($join) use($estado){
                    $join->on('e.id','=','rm.estado_id')->where('e.name',$estado);
                })
                ->groupBy('m.mes')->groupBy('rm.num_mes')->groupBy('rm.arco')
                ->orderBy('rm.num_mes','ASC')
                ->get();

        }
        foreach($promedio as $p)
        {
            if(is_null($p->data)){
                $p->data = '';
                array_push($pila,$p->data);
            }
            else{
               array_push($pila,(float)$p->data);
            }            
            if($tipo == 2){
                array_push($semanas, $p->mes);
            }
            else{
                array_push($semanas, $p->semana);
            }
        }           
        return response()->json([
            'data' => $pila,
            'semanas' => $semanas
        ]);
    }
    public function promedios($ano, $estado, $tipo, $arco)
    {
        $pila =[];
        $semanas = [];
        $estado_id = Estado::where('name',$estado)->first();

        if($tipo == 1)
        {
            $promedio = DB::table('siafeson_simdia.reporte_semanal AS rs')
                ->select(DB::raw('ROUND( SUM(rs.capturas) / ( SUM(rs.revisadas) + SUM(rs.norevisadas) )::NUMERIC, 2 ) as data'), 's.semana' )
                ->rightJoin('public.semanas AS s', function($join) use($ano, $arco, $estado_id){   
                    $join->on('rs.semana','=','s.semana')->where('rs.ano',$ano)->where('rs.arco',$arco)->where('rs.estado_id',$estado_id->id);   
                })
                ->groupBy('s.semana')->groupBy('rs.semana')->groupBy('rs.arco')
                ->orderBy('s.semana','ASC')
                ->get(); 
        }
        else
        {
            $promedio = DB::table('siafeson_simdia.reporte_mensual AS rm')
                ->select(DB::raw('ROUND( SUM(rm.capturas) / ( SUM(rm.revisadas) + SUM(rm.norevisadas) )::NUMERIC, 2 ) as data'), 'm.mes' )
                ->rightJoin('public.meses AS m', function($join) use($ano, $arco, $estado_id){   
                    $join->on('rm.num_mes','=','m.numero')->where('rm.ano',$ano)->where('rm.arco',$arco)->where('rm.estado_id',$estado_id->id);   
                })
                ->leftJoin('siafeson_siafeson.estados as e', function($join) use($estado){
                    $join->on('e.id','=','rm.estado_id')->where('e.name',$estado);
                })
                ->groupBy('m.mes')->groupBy('rm.num_mes')->groupBy('rm.arco')
                ->orderBy('rm.num_mes','ASC')
                ->get(); 
        }
        foreach($promedio as $p)
        {
            if(is_null($p->data)){
                $p->data = '';
                array_push($pila,$p->data);
            }
            else{
               array_push($pila,(float)$p->data);
            }            
            if($tipo == 2){
                array_push($semanas, $p->mes);
            }
            else{
                array_push($semanas, $p->semana);
            }
        }           
        return response()->json([
            'data' => $pila,
            'semanas' => $semanas
        ]);
        
    }
    public function promediosAno($ano, $tipo, $estado)
    {
        $pila = [];
        $semanas = [];
        $estado_id = Estado::where('name',$estado)->first();
        $min;
        $max;

        if($tipo == 1)
        {
            $promedio = DB::table('siafeson_simdia.reporte_semanal AS rs')
                ->select(DB::raw('ROUND( SUM(rs.capturas) / ( SUM(rs.revisadas) + SUM(rs.norevisadas) )::NUMERIC, 2 ) as data'), 's.semana' )
                ->rightJoin('public.semanas AS s', function($join) use($ano){   
                     $join->on('rs.semana','=','s.semana')->where('rs.ano',$ano);   
                })
                ->where('rs.estado_id',$estado_id->id)
                ->groupBy('s.semana')
                ->orderBy('s.semana','ASC')
                ->get();
        }
        else
        {
            $promedio = DB::table('siafeson_simdia.reporte_mensual as rm')
                ->select(DB::raw('ROUND( SUM(rm.capturas) / ( SUM(rm.revisadas) + SUM(rm.norevisadas) )::NUMERIC, 2 ) as data'), 'm.mes')
                ->rightJoin('public.meses as m', function($join) use($ano){
                    $join->on('rm.num_mes','=','m.numero')->where('rm.ano',$ano);
                })
                ->where('rm.estado_id',$estado_id->id)
                ->groupBy('m.mes','m.numero')
                ->orderBy('m.numero','ASC')
                ->get();
        }
        foreach($promedio as $p)
        {
            if(is_null($p->data)){
                $p->data = '';
                array_push($pila,$p->data);
            }
            else{
               array_push($pila,(float)$p->data);
            }
            if($tipo == 2){
                array_push($semanas, $p->mes);
            }
            else{
                array_push($semanas, $p->semana);
            }
        }
        if(empty($pila)){
            $min = 0;
            $max = 0;
        }
        else{
            $min = min($pila);
            $max = max($pila);
        }  
        return response()->json([
            'data' => $pila,
            'semanas' => $semanas,
            'min' =>  $min,
            'max' => $max 
        ]);
    }
}
