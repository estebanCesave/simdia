<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Models\Trampa;
use siafeson\Models\Captura;
use Illuminate\Support\Facades\Auth;
use siafeson\Models\EjercicioSemana;
use DB;

class TrampaController extends Controller
{
	public function __construct()
    {
      	ini_set('max_execution_time', 300);
    }
	public function index()
	{
		return view('simdia.listadotrampas');
	}
	public function trampasList(Request $request)
	{
		if(Auth::user()->hasrole('Admin Junta') && $request->session()->get('rol') == 'Admin Junta') 
		{
			$junta = $this->getJunta();

			$trampas =  DB::table('siafeson_simdia.rel_trampa_campo as r')
				->select(
					't.id',
					't.siembra_id',
					't.name',
					't.latitud',
					't.longitud',
					't.arco',
					'c.name as campo',
					DB::raw("CONCAT(p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"))  
				->join('siafeson_simdia.trampas as t','r.trampa_id','=','t.id')
				->join('siafeson_simdia.campos as c','c.id','=','r.campo_id')
				->join('siafeson_simdia.rel_user_trampa as ru','t.id','=','ru.trampa_id')
				->join('siafeson_siafeson.users as u','ru.user_id','=','u.id')
				->join('siafeson_siafeson.persona as p','p.id','=','u.persona_id')
				->where('t.status',1)
				->where('t.tipo_huerta','<>', 3)
				->where('ru.status',1)
				->where('c.status',1)
				->where('p.junta_id', $junta);
					
				return datatables()
					->query($trampas)
					->addColumn('slc','chunks.datatable.trampas.select')
					->rawColumns(['slc'])
					->filterColumn('nombre', function($query, $keyword) { 
						$sql = "CONCAT( p.name,'-',p.apellido_paterno,'-',p.apellido_materno)  like ?";
						$query->whereRaw($sql, ["%{$keyword}%"]);
					})
					->make(true);	
		}
		else if(Auth::user()->hasrole('Técnico') && $request->session()->get('rol') == 'Técnico' )
		{	
			$trampas = DB::table('siafeson_simdia.trampas as t')
				->select(
					't.id', 
					't.siembra_id',
					't.name',
					't.latitud',
					't.longitud',
					'c.name as campo',
					't.arco')
				->join('siafeson_simdia.rel_trampa_campo as rtc','rtc.trampa_id','t.id')
				->join('siafeson_simdia.campos as c','c.id','rtc.campo_id')
				->join('siafeson_simdia.rel_user_trampa as rut','rut.trampa_id','t.id')
				->where('t.status',1)
				->where('t.tipo_huerta','<>', 3)
				->where('rtc.status',1)
				->where('rut.status',1)
				->where('c.status',1)
				->where('rut.user_id', Auth::user()->id );
				
				return datatables()->query($trampas)->make(true);
		}
        else if(Auth::user()->hasrole('Admin Estado') && $request->session()->get('rol') == 'Admin Estado' )
		{
			$estado = $this->getEstado();
			$trampas =  DB::table('siafeson_simdia.rel_trampa_campo as r')
				->select(
					't.id',
					't.siembra_id',
					't.name',
					't.latitud',
					't.longitud',
					't.arco',
					'c.name as campo',
 					DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"),  
 					'e.name as estado',
					'j.name as junta')  
				->join('siafeson_simdia.trampas as t','r.trampa_id','=','t.id')
				->join('siafeson_simdia.campos as c','c.id','=','r.campo_id')
				->join('siafeson_simdia.rel_user_trampa as ru','t.id','=','ru.trampa_id')
				->join('siafeson_siafeson.users as u','ru.user_id','=','u.id')
				->join('siafeson_siafeson.persona as p','p.id','=','u.persona_id')
				->join('siafeson_siafeson.juntas as j','j.id','=','c.junta_id')
				->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
				->where('t.status',1)
				->where('t.tipo_huerta','<>', 3)
				->where('ru.status',1)
				->where('c.status',1)
				->where('e.id',$estado);

				return datatables()
					->query($trampas)
					->addColumn('slc','chunks.datatable.trampas.select')
					->rawColumns(['slc'])
					->filterColumn('nombre', function($query, $keyword) { 
						$sql = "CONCAT( p.name,'-',p.apellido_paterno,'-',p.apellido_materno)  like ?";
						$query->whereRaw($sql, ["%{$keyword}%"]);
					})
					->make(true);
		}
        else if(Auth::user()->hasrole('Super Admin') && $request->session()->get('rol') == 'Super Admin' )
		{	
			$trampas =  DB::table('siafeson_simdia.rel_trampa_campo as r')
				->select(
					't.id',
					't.siembra_id',
					't.name',
					't.latitud',
					't.longitud',
					't.arco',
					'c.name as campo',
                	DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"),  
					'e.name as estado',
					'j.name as junta')  
				->join('siafeson_simdia.trampas as t','r.trampa_id','=','t.id')
				->join('siafeson_simdia.campos as c','c.id','=','r.campo_id')
				->join('siafeson_simdia.rel_user_trampa as ru','t.id','=','ru.trampa_id')
				->join('siafeson_siafeson.users as u','ru.user_id','=','u.id')
				->join('siafeson_siafeson.persona as p','p.id','=','u.persona_id')
				->join('siafeson_siafeson.juntas as j','j.id','=','c.junta_id')
				->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
				->where('t.status',1)
				->where('ru.status',1)
				->where('c.status',1);
 
				return datatables()
					->query($trampas)
					->addColumn('slc','chunks.datatable.trampas.select')
					->rawColumns(['slc'])
					->filterColumn('nombre', function($query, $keyword) { 
						$sql = "CONCAT( p.name,'-',p.apellido_paterno,'-',p.apellido_materno)  like ?";
						$query->whereRaw($sql, ["%{$keyword}%"]);
					})
					->make(true);
		}
		else{
			abort(403);
		}
	}
	public function getTrampas(request $request)
    {
		$fecha = date('Y-m-d');
		$this->ejercicio = EjercicioSemana::select('semana', 'ejercicioid','ano')->semana($fecha)->first();
		$semana = $this->ejercicio->semana;
		
		if(Auth::user()->hasrole('Técnico') && $request->session()->get('rol') == 'Técnico' )
		{			
			$user = new Trampa;
			$user->user_id = Auth::user()->id;
			$trampasTotal = array();
			$trampasCapturas = array();
			$trampasDisponibles = array();
			$trampasFinales = array();
			$ano = substr($fecha,0,4);
			$flag = 0;
			
			$query = queryTrampasUser(Auth::user()->id);
			$trampasUser = DB::select(DB::raw($query));
			
			$query2 = queryTrampasUserSemana(Auth::user()->id, $semana, $ano);
			$trampas = DB::select(DB::raw($query2));

        	dd(Auth::user()->persona->junta_id);
			
	  
			if($request->edit == 0){
				foreach ($trampasUser as $key) {
					array_push($trampasTotal, $key->value);
				}	
				foreach ($trampas as $key) {
					array_push($trampasCapturas, $key->value);
				}
      			$trampasDisponibles = array_diff($trampasTotal, $trampasCapturas);
      		  	foreach ($trampasDisponibles as $key => $value){
				  	foreach ($trampasUser as $key2){
						if($value == $key2->value){
          					$trampasFinales[$flag] = array("value" => $key2->value,"name" => $key2->name);
          					$flag = $flag + 1;
         				}
      				}
      			}
   	    		return $trampasFinales;
	  		}
			else{
        		return $trampasUser;
      		}
		}
		else{
 			if($request->user_id){
				$user_id = $request->user_id;
 				$fecha = date('Y-m-d');
				$user = new Trampa;
				$user->user_id = $user_id;
				$trampasTotal = array();
				$trampasCapturas = array();
				$trampasDisponibles = array();
				$trampasFinales = array();
				$ano = substr($fecha,0,4);
				$flag = 0;
				$query = queryTrampasUser($user_id);
				$trampasUser = DB::select(DB::raw($query));

				$query2 = queryTrampasUserSemana($user_id, $semana, $ano);
				$trampas = DB::select(DB::raw($query2));
		
				if($request->edit == 0)	{
					foreach ($trampasUser as $key) {
						array_push($trampasTotal, $key->value);
					}			
					foreach ($trampas as $key) {
						array_push($trampasCapturas, $key->value);
					}			
					$trampasDisponibles = array_diff($trampasTotal, $trampasCapturas);
			
					foreach ($trampasDisponibles as $key => $value){
						foreach ($trampasUser as $key2){
							if($value == $key2->value){
								$trampasFinales[$flag] = array("value" => $key2->value,"name" => $key2->name);
								$flag = $flag + 1;
							}
						}
					}
					return $trampasFinales;
				}
			}		 
			else{
        		return 0;
      		}
		}
	}
	function getJunta()
    {
      $junta =  DB::table('siafeson_siafeson.persona')
        ->select('siafeson_siafeson.juntas.name as junta', 'siafeson_siafeson.juntas.id as juntaId')
        ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
        ->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
        ->where('siafeson_siafeson.users.id',Auth::user()->id)
        ->first();

        return $junta->juntaId;
	}
	function getEstado()
	{
		$estado =  DB::table('siafeson_siafeson.persona')
        ->select('siafeson_siafeson.estados.name as estado','siafeson_siafeson.estados.id as estadoId')
        ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
		->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
		->join('siafeson_siafeson.estados','siafeson_siafeson.estados.id','=','siafeson_siafeson.juntas.estado_id')
        ->where('siafeson_siafeson.users.id',Auth::user()->id)
        ->first();

	    return $estado->estadoId;
	}
}