<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use siafeson\Models\Captura;

class RecuperarController extends Controller
{
    public function index(){
        return view('simdia.recuperar');
    }
    public function getImeis(Request $request)
    {
        if(Auth::user()->hasrole('Admin Junta') && $request->session()->get('rol') == 'Admin Junta' )
        {
            $user_id = $request->userId;

            $query = 'Select i.id as value,i.name from siafeson_siafeson.imei i 
                        INNER JOIN siafeson_siafeson.rel_user_imei rel on rel.imei_id = i.id AND rel.status = 1
                        INNER JOIN siafeson_siafeson.users u on u.id = rel.user_id
                        WHERE u.id = '.$user_id;
            $imeis = DB::select(DB::raw($query));
            return $imeis;
        }
        else if(Auth::user()->hasrole('Técnico') && $request->session()->get('rol') == 'Técnico')
        {
            $user_id = Auth::user()->id;
            $query = 'Select i.id as value,i.name from siafeson_siafeson.imei i 
                        INNER JOIN siafeson_siafeson.rel_user_imei rel on rel.imei_id = i.id AND rel.status = 1
                        INNER JOIN siafeson_siafeson.users u on u.id = rel.user_id
                        WHERE u.id = '.$user_id;
            $imeis = DB::select(DB::raw($query));
            return $imeis;
        }
        else if(Auth::user()->hasrole('Admin Estado') && $request->session()->get('rol') == 'Admin Estado' )
        {
            $user_id = $request->userId;

            $query = 'Select i.id as value,i.name from siafeson_siafeson.imei i 
                        INNER JOIN siafeson_siafeson.rel_user_imei rel on rel.imei_id = i.id AND rel.status = 1
                        INNER JOIN siafeson_siafeson.users u on u.id = rel.user_id
                        WHERE u.id = '.$user_id;
            $imeis = DB::select(DB::raw($query));
            return $imeis;
        }
    }
    public function getArchivos(request $request)
    {
        $imei =  $request->imei;
        $path = base_path().'/storage/app/android/simdia/'.$imei.'';
        if(is_dir($path)){
            $files = array_diff(scandir($path,1), array('..', '.'));
            return response()->json([
                'status'=>'success',
                'data' => $files
            ]);
        }
        else{
            return response()->json([
                'status'=>'error',
                'data' => 'Carpeta Vacia o no existe'
            ]);
        }
    }
    public function greadFile(Request $request)
    {
        $pila = [];
        $data = file_get_contents(base_path().'/storage/app/android/simdia/'.$request->imei.'/'.$request->file);
        $files = json_decode($data, false);
        $week = date("W");

        return response()->json($files);
    }
    public function saveFile(Request $request)
    {
        if(empty($request->all()))
        {
            return response()->json(
                [
                    'status'=>'error',
                    'data'=> 'No se ha seleccionado ningun registro',
                ]
            );
        }
        else
        {
            $pila = [];
            /**Recorriendo lo que esta dentro de files */
            foreach($request->all() as $f => $key)
            {
                 /**Trayendo Usuario ID */
                $user = DB::table('siafeson_siafeson.rel_user_imei')
                    ->select('siafeson_siafeson.users.id as user_id')
                    ->join('siafeson_siafeson.users','siafeson_siafeson.users.id','=','siafeson_siafeson.rel_user_imei.user_id')
                    ->join('siafeson_siafeson.imei','siafeson_siafeson.imei.id','=','siafeson_siafeson.rel_user_imei.imei_id')
                    ->where('siafeson_siafeson.imei.name',$key['imei'])
                    ->where('siafeson_siafeson.rel_user_imei.status',1)
                    ->first(); 

                $existe = Captura::where('fecha', $key['fecha'])->where('ano',$key['ano'])->where('semana',$key['semana'])->where('trampa_id',$key['trampa_id'])->where('status',1)->where('user_id',$user->user_id)->first();

                if(!$existe)
                {
                    /**Semana actual */
                    $captura = new Captura();
                    $captura->fecha = $key['fecha'];
                    $captura->semana = $key['semana'];
                    $captura->ano = $key['ano'];
                    $captura->trampa_id = $key['trampa_id'];
                    $captura->user_id = $user->user_id;
                    $captura->posicion = $key['latitud'].','.$key['longitud'];
                    $captura->accuracy = $key['accuracy'];
                    if($key['distancia_qr'] == 'null'){
                        $captura->distancia_qr = 0;
                    }
                    else{
                        $captura->distancia_qr = $key['distancia_qr'];
                    }
                    $captura->fenologia_trampa_id = $key['fenologia_id'];
                    $captura->captura = $key['capturas'];
                    $captura->imei = $key['imei'];
                    $captura->noa = $key['noa'];
                    $captura->non = $key['non'];
                    $captura->nof = $key['nof'];
                    $captura->sua = $key['sua'];
                    $captura->sun = $key['sun'];
                    $captura->suf = $key['suf'];
                    $captura->esa = $key['esa'];
                    $captura->esn = $key['esn'];
                    $captura->esf = $key['esf'];
                    $captura->oea = $key['oea'];
                    $captura->oen = $key['oen'];
                    $captura->oef = $key['oef'];
                    $captura->status = 1;
                    $captura->id_bd_cel = $key['id_bd_cel'];
                    $captura->fecha_cel = $key['fechaHora'];
                    $captura->instalada = $key['instalada'];
                    $captura->method = 4;
                    $captura->revisada = 1;
                    $captura->created =  date("Y-m-d H:i:s");
                    $captura->modified =  date("Y-m-d H:i:s");
                    $captura->save();
                    array_push($pila,$f);
                }
                else
                {
                    return response()->json(
                        [
                            'status'=>'success',
                            'data'=> 'El registro ya existe',
                        ]
                    );
                }   
            }
            return response()->json([
                'status'=>'success',
                'data' => count($pila),
                'log' =>  \Log::info('Captura agregada correctamente (Web Recuperar): ')
            ]);
        }        
    }
}
