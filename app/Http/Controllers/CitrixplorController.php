<?php

namespace siafeson\Http\Controllers;
use siafeson\Models\Detalle;
use siafeson\Models\Resumen;
use siafeson\Models\CamposScoring;
use DB;
use Illuminate\Http\Request;

class CitrixplorController extends Controller{
    // 867970030568428
    function getResumenes(Request $request){
        try {
            // DB::enableQueryLog();
            $reg = DB::table('siafeson_simdia.resumen')
                    ->select('siafeson_simdia.resumen.id as resumen_id',
                    'siafeson_simdia.campos_scoring.id as campo_id',
                    'siafeson_simdia.campos_scoring.name as campo',
                    'siafeson_simdia.resumen.fecha as fecha')
                    ->join('siafeson_simdia.campos_scoring','siafeson_simdia.campos_scoring.id','=','siafeson_simdia.resumen.campo_id')
                    ->where('siafeson_simdia.resumen.status',1)
                    ->where('siafeson_simdia.resumen.imei',$request->imei)
                    ->get(); 
            // $query = DB::getQueryLog();
            // print_r($query);
            return $reg;
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['status'=>0, 'data'=>$e->getmessage() ]);
        }
    }

    function getDetalles(Request $request){
        try {
            // DB::enableQueryLog();
            $arboles = DB::table('siafeson_simdia.resumen')
                    ->select('siafeson_simdia.resumen.id as resumen_id',
                    'siafeson_simdia.detalle.id as detalle_id',
                    'siafeson_simdia.campos_scoring.id as campo_id',
                    'siafeson_simdia.campos_scoring.name as campo',
                    DB::raw("split_part(trim(siafeson_simdia.detalle.posicion::text, '()'), ',', 1) as lat"),
                    DB::raw("split_part(trim(siafeson_simdia.detalle.posicion::text, '()'), ',', 2) as lng"),
                    'siafeson_simdia.detalle.secuencial',
                    'siafeson_simdia.detalle.sintomas_hlb as hlb',
                    'siafeson_simdia.detalle.psilidos_adultos as psa',
                    'siafeson_simdia.detalle.psilidos_ninfas as psn',
                    'siafeson_simdia.detalle.alto_adultos as eaa',
                    'siafeson_simdia.detalle.alto_ninfas as ean',
                    'siafeson_simdia.detalle.medio_adultos as ema',
                    'siafeson_simdia.detalle.medio_ninfas as emn',
                    'siafeson_simdia.detalle.bajo_adultos as eba',
                    'siafeson_simdia.detalle.bajo_ninfas as ebn')
                    ->join('siafeson_simdia.detalle','siafeson_simdia.detalle.resumen_id','=','siafeson_simdia.resumen.id')
                    ->join('siafeson_simdia.campos_scoring','siafeson_simdia.campos_scoring.id','=','siafeson_simdia.resumen.campo_id')
                    ->where('siafeson_simdia.resumen.status',1)
                    ->where('siafeson_simdia.detalle.status',1)
                    ->where('siafeson_simdia.resumen.imei',$request->imei)
                    ->get(); 
            // $query = DB::getQueryLog();
            // print_r($query);
            return $arboles;
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['status'=>0, 'data'=>$e->getmessage() ]);
        }
    }

    function getResumenActividad(Request $request){
        try {
            //return $request;            
            $from = date($request->fini);
            $to = date($request->ffin);
            $campos_total = "0";
            $where_junta = "";
            $where_campos = "";
            $where_imei = "";

            if($request->junta_id == 0 || $request->junta_id != 0){
                if($request->junta_id != 0){
                    $where_junta = "AND c.junta_id = ".$request->junta_id;
                    $campos_total = "(select count(cs.id) as total from siafeson_simdia.campos_scoring cs where cs.status = 1 and cs.junta_id = ".$request->junta_id." )";
                }else{
                    $campos_total = "(select count(cs.id) as total from siafeson_simdia.campos_scoring cs where cs.status = 1)";
                }
            }

            if($request->campo_id == 0 || $request->campo_id != 0){
                if($request->campo_id != 0){
                    $where_campos = "AND c.id = ".$request->campo_id;
                }
            }

            if($request->imei){

                $u = DB::table('siafeson_siafeson.rel_user_imei')
                ->select('siafeson_siafeson.persona.junta_id as junta_id')
                ->join('siafeson_siafeson.users','siafeson_siafeson.users.id','=','siafeson_siafeson.rel_user_imei.user_id')
                ->join('siafeson_siafeson.imei','siafeson_siafeson.imei.id','=','siafeson_siafeson.rel_user_imei.imei_id')
                ->join('siafeson_siafeson.persona','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
                ->where('siafeson_siafeson.rel_user_imei.status',1)
                ->where('siafeson_siafeson.imei.name',$request->imei)
                ->first();

                if($u){ 
                    $junta_id = $u->junta_id; 
                    $campos_total = "(select count(cs.id) as total from siafeson_simdia.campos_scoring cs where cs.status = 1 and cs.junta_id = ".$junta_id." )";
                }else{
                    $campos_total = "(SELECT count(cs.id) as total 
                    FROM siafeson_simdia.campos_scoring cs 
                    INNER JOIN siafeson_simdia.rel_imei_campo rel On rel.campo_id = cs.id
                    INNER JOIN siafeson_siafeson.imei i ON i.id = rel.imei_id
                    WHERE cs.status = 1 and i.name = '".$request->imei."' )";
                }
                $where_imei = "AND r.imei = '".$request->imei."'";
            }

            $qry = "SELECT ".$campos_total." as campos_total,
            count(distinct c.id) as campos_rev,
            count(d.id) as arboles,
            count(distinct r.id) as registros,
            count(distinct case when d.sintomas_hlb = 1 then c.id else null end) as camposposhlb,
            count(distinct case when d.psilidos_adultos = 1 then c.id else null end) as camposconadultos,
            count(distinct case when d.psilidos_ninfas = 1 then c.id else null end) as camposconninfas,
            count(distinct case when d.alto_adultos = 1 then c.id else null end) as camposmeaa,
            count(distinct case when d.alto_ninfas = 1 then c.id else null end) as camposmean,
            count(distinct case when d.medio_adultos = 1 then c.id else null end) as camposmema,
            count(distinct case when d.medio_ninfas = 1 then c.id else null end) as camposmemn,
            count(distinct case when d.bajo_adultos = 1 then c.id else null end) as camposmeba,
            count(distinct case when d.bajo_ninfas = 1 then c.id else null end) as camposmebn,
            sum(case when d.sintomas_hlb = 1 then 1 else 0 end) as arboleshlb,
            sum(case when d.psilidos_adultos = 1 then 1 else 0 end) as arbolesconadultos,
            sum(case when d.psilidos_ninfas = 1 then 1 else 0 end) as arbolesconninfas,
            sum(case when d.alto_adultos = 1 then 1 else 0 end) as arbolesmeaa,
            sum(case when d.alto_ninfas = 1 then 1 else 0 end) as arbolesmean,
            sum(case when d.medio_adultos = 1 then 1 else 0 end) as arbolesmema,
            sum(case when d.medio_ninfas = 1 then 1 else 0 end) as arbolesmemn,
            sum(case when d.bajo_adultos = 1 then 1 else 0 end) as arbolesmeba,
            sum(case when d.bajo_ninfas = 1 then 1 else 0 end) as arbolesmebn
            FROM siafeson_simdia.resumen as r
            INNER JOIN siafeson_simdia.detalle as d ON d.resumen_id = r.id
            INNER JOIN siafeson_simdia.campos_scoring as c ON c.id = r.campo_id
            INNER JOIN siafeson_simdia.variedades as v ON v.id = c.variedad_id
            WHERE r.status = 1 AND d.status = 1
            AND r.fecha between ? AND ? ".$where_junta." ".$where_campos." ".$where_imei." ";
            
            $regs = DB::select($qry, [$from, $to]);
            return $regs;
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['status'=>0, 'data'=>$e->getmessage() ]);
        }
    }

    function getArboles(Request $request){
        try {

            $from = date($request->fini);
            $to = date($request->ffin);
            $where_junta = "";
            $where_campos = "";
            $where_imei = "";

            if($request->junta_id == 0 || $request->junta_id != 0){
                if($request->junta_id != 0){
                    $where_junta = "AND c.junta_id = ".$request->junta_id;
                }
            }

            if($request->campo_id == 0 || $request->campo_id != 0){
                if($request->campo_id != 0){
                    $where_campos = "AND c.id = ".$request->campo_id;
                }
            }

            if($request->imei){
                $where_imei = "AND r.imei = '".$request->imei."'";
            }

            $qry = "SELECT r.id as resumen_id,
            d.id as detalle_id,
            c.id as campo_id,
            c.name as campo,
            split_part(trim(d.posicion::text, '()'), ',', 1) as lat,
            split_part(trim(d.posicion::text, '()'), ',', 2) as lng,
            d.secuencial,
            d.sintomas_hlb as hlb,
            d.psilidos_adultos as psa,
            d.psilidos_ninfas as psn,
            d.alto_adultos as eaa,
            d.alto_ninfas as ean,
            d.medio_adultos as ema,
            d.medio_ninfas as emn,
            d.bajo_adultos as eba,
            d.bajo_ninfas as ebn
            FROM siafeson_simdia.resumen as r
            INNER JOIN siafeson_simdia.detalle as d ON d.resumen_id = r.id
            INNER JOIN siafeson_simdia.campos_scoring as c ON c.id = r.campo_id
            INNER JOIN siafeson_simdia.variedades as v ON v.id = c.variedad_id
            WHERE r.status = 1
            AND d.status = 1
            AND r.fecha between ? AND ? ".$where_junta." ".$where_campos." ".$where_imei." "; 

            $regs = DB::select($qry, [$from, $to]);
            return $regs;

        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['status'=>0, 'data'=>$e->getmessage() ]);
        }
    }

    function getMECampos(Request $request){
        try {

            $from = date($request->fini);
            $to = date($request->ffin);
            $where_junta = "";
            $where_imei = "";

            if($request->junta_id == 0 || $request->junta_id != 0){
                if($request->junta_id != 0){
                    $where_junta = "AND c.junta_id = ".$request->junta_id;
                }
            }

            if($request->imei){
                $where_imei = "AND r.imei = '".$request->imei."'";
            }

            $qry = "SELECT * FROM (SELECT DISTINCT c.id as value, (c.name || ' - ' || c.propietario || ' (' || c.superficie || ' Ha.)') AS name
            FROM siafeson_simdia.resumen as r
            INNER JOIN siafeson_simdia.detalle as d on d.resumen_id = r.id
            INNER JOIN siafeson_simdia.campos_scoring as c on c.id = r.campo_id
            INNER JOIN siafeson_simdia.variedades as v on v.id = c.variedad_id
            WHERE r.status = 1
            AND d.status = 1
            AND r.fecha between ? AND ? ".$where_junta." ".$where_imei.") t
            ORDER BY t.name";
            
            $regs = DB::select($qry, [$from, $to]);
            return $regs;

        } catch (\Illuminate\Database\QueryException $e) {

            return response()->json(['status'=>0, 'data'=>$e->getmessage() ]);

        }
    }

    function getMEResumen(Request $request){
        try {

            $from = date($request->fini);
            $to = date($request->ffin);
            $where_junta = "";
            $where_campos = "";
            $where_imei = "";

            if($request->junta_id == 0 || $request->junta_id != 0){
                if($request->junta_id != 0){
                    $where_junta = "AND c.junta_id = ".$request->junta_id;
                }
            }

            if($request->campo_id == 0 || $request->campo_id != 0){
                if($request->campo_id != 0){
                    $where_campos = "AND c.id = ".$request->campo_id;
                }
            }

            if($request->imei){
                $where_imei = "AND r.imei = '".$request->imei."'";
            }

            $qry = "SELECT sum(case when d.alto_adultos = 1 then 1 else 0 end) as arbolesmeaa,
            sum(case when d.alto_ninfas = 1 then 1 else 0 end) as arbolesmean,
            sum(case when d.medio_adultos = 1 then 1 else 0 end) as arbolesmema,
            sum(case when d.medio_ninfas = 1 then 1 else 0 end) as arbolesmemn,
            sum(case when d.bajo_adultos = 1 then 1 else 0 end) as arbolesmeba,
            sum(case when d.bajo_ninfas = 1 then 1 else 0 end) as arbolesmebn
            FROM siafeson_simdia.resumen as r
            INNER JOIN siafeson_simdia.detalle as d on d.resumen_id = r.id
            INNER JOIN siafeson_simdia.campos_scoring as c on c.id = r.campo_id
            INNER JOIN siafeson_simdia.variedades as v on v.id = c.variedad_id
            WHERE r.status = 1
            AND d.status = 1
            AND r.fecha between ? AND ? ".$where_junta." ".$where_campos." ".$where_imei." ";
            
            $regs = DB::select($qry, [$from, $to]);
            return $regs;

        } catch (\Illuminate\Database\QueryException $e) {

            return response()->json(['status'=>0, 'data'=>$e->getmessage() ]);

        }
    }

    function getInfo($id){
        $arbol = DB::table('siafeson_simdia.resumen')
        ->select('siafeson_simdia.resumen.id as resumen_id',
        'siafeson_simdia.detalle.id as detalle_id',
        'siafeson_simdia.campos_scoring.id as campo_id',
        'siafeson_simdia.campos_scoring.name as campo',
        DB::raw("split_part(trim(siafeson_simdia.detalle.posicion::text, '()'), ',', 1) as lat"),
        DB::raw("split_part(trim(siafeson_simdia.detalle.posicion::text, '()'), ',', 2) as lng"),
        'siafeson_simdia.detalle.secuencial',
        'siafeson_simdia.resumen.fecha as fecha',
        'siafeson_simdia.detalle.sintomas_hlb as hlb',
        'siafeson_simdia.detalle.psilidos_adultos as psa',
        'siafeson_simdia.detalle.psilidos_ninfas as psn',
        'siafeson_simdia.detalle.alto_adultos as eaa',
        'siafeson_simdia.detalle.alto_ninfas as ean',
        'siafeson_simdia.detalle.medio_adultos as ema',
        'siafeson_simdia.detalle.medio_ninfas as emn',
        'siafeson_simdia.detalle.bajo_adultos as eba',
        'siafeson_simdia.detalle.bajo_ninfas as ebn',
        'siafeson_simdia.campos_scoring.propietario as propietario',
        'siafeson_simdia.variedades.nombre as variedad',
        'siafeson_simdia.campos_scoring.superficie as superficie')
        ->join('siafeson_simdia.detalle','siafeson_simdia.detalle.resumen_id','=','siafeson_simdia.resumen.id')
        ->join('siafeson_simdia.campos_scoring','siafeson_simdia.campos_scoring.id','=','siafeson_simdia.resumen.campo_id')
        ->join('siafeson_simdia.variedades','siafeson_simdia.variedades.id','=','siafeson_simdia.campos_scoring.variedad_id')
        ->where('siafeson_simdia.detalle.id',$id)
        ->first();
        return view('citriexplore/info')->with('arbol', $arbol);
    }

    // ------- A C T I V I D A D  D E  C A M P O

    function getResumenActividadMunicipio(Request $request){
        try {
                      
            $from = date($request->fini);
            $to = date($request->ffin);
            $where_junta = "";
            $where_campos = "";
            $where_imei = "";

            if($request->junta_id == 0 || $request->junta_id != 0){
                if($request->junta_id != 0){
                    $where_junta = "AND c.junta_id = ".$request->junta_id;
                }
            }

            $qry = "SELECT m.nombre_completo as municipio,
            count(distinct c.id) as campos_rev,
            count(d.id) as arboles,
            count(distinct r.id) as registros,
            count(distinct case when d.sintomas_hlb = 1 then c.id else null end) as camposposhlb,
            count(distinct case when d.psilidos_adultos = 1 then c.id else null end) as camposconadultos,
            count(distinct case when d.psilidos_ninfas = 1 then c.id else null end) as camposconninfas,
            count(distinct case when d.alto_adultos = 1 then c.id else null end) as camposmeaa,
            count(distinct case when d.alto_ninfas = 1 then c.id else null end) as camposmean,
            count(distinct case when d.medio_adultos = 1 then c.id else null end) as camposmema,
            count(distinct case when d.medio_ninfas = 1 then c.id else null end) as camposmemn,
            count(distinct case when d.bajo_adultos = 1 then c.id else null end) as camposmeba,
            count(distinct case when d.bajo_ninfas = 1 then c.id else null end) as camposmebn,
            sum(case when d.sintomas_hlb = 1 then 1 else 0 end) as arboleshlb,
            sum(case when d.psilidos_adultos = 1 then 1 else 0 end) as arbolesconadultos,
            sum(case when d.psilidos_ninfas = 1 then 1 else 0 end) as arbolesconninfas,
            sum(case when d.alto_adultos = 1 then 1 else 0 end) as arbolesmeaa,
            sum(case when d.alto_ninfas = 1 then 1 else 0 end) as arbolesmean,
            sum(case when d.medio_adultos = 1 then 1 else 0 end) as arbolesmema,
            sum(case when d.medio_ninfas = 1 then 1 else 0 end) as arbolesmemn,
            sum(case when d.bajo_adultos = 1 then 1 else 0 end) as arbolesmeba,
            sum(case when d.bajo_ninfas = 1 then 1 else 0 end) as arbolesmebn
            FROM siafeson_simdia.resumen as r
            INNER JOIN siafeson_simdia.detalle as d ON d.resumen_id = r.id
            INNER JOIN siafeson_simdia.campos_scoring as c ON c.id = r.campo_id
            INNER JOIN siafeson_simdia.variedades as v ON v.id = c.variedad_id
            INNER join siafeson_siafeson.juntas as j on j.id = c.junta_id
            inner join siafeson_siafeson.catmunicipiossicafi as m on m.municipioid = c.municipio_id
            WHERE r.status = 1 AND d.status = 1
            AND r.fecha between ? AND ? ".$where_junta." group by m.nombre_completo";
            
            $regs = DB::select($qry, [$from, $to]);
            return $regs;
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['status'=>0, 'data'=>$e->getmessage() ]);
        }
    }

    function getResumenActividadJunta(Request $request){
        try {
                      
            $from = date($request->fini);
            $to = date($request->ffin);
            $where_junta = "";
            $where_campos = "";
            $where_imei = "";

            if($request->junta_id == 0 || $request->junta_id != 0){
                if($request->junta_id != 0){
                    $where_junta = "AND c.junta_id = ".$request->junta_id;
                }
            }

            $qry = "SELECT j.short_name as jlsv,
            count(distinct c.id) as campos_rev,
            count(d.id) as arboles,
            count(distinct r.id) as registros,
            count(distinct case when d.sintomas_hlb = 1 then c.id else null end) as camposposhlb,
            count(distinct case when d.psilidos_adultos = 1 then c.id else null end) as camposconadultos,
            count(distinct case when d.psilidos_ninfas = 1 then c.id else null end) as camposconninfas,
            count(distinct case when d.alto_adultos = 1 then c.id else null end) as camposmeaa,
            count(distinct case when d.alto_ninfas = 1 then c.id else null end) as camposmean,
            count(distinct case when d.medio_adultos = 1 then c.id else null end) as camposmema,
            count(distinct case when d.medio_ninfas = 1 then c.id else null end) as camposmemn,
            count(distinct case when d.bajo_adultos = 1 then c.id else null end) as camposmeba,
            count(distinct case when d.bajo_ninfas = 1 then c.id else null end) as camposmebn,
            sum(case when d.sintomas_hlb = 1 then 1 else 0 end) as arboleshlb,
            sum(case when d.psilidos_adultos = 1 then 1 else 0 end) as arbolesconadultos,
            sum(case when d.psilidos_ninfas = 1 then 1 else 0 end) as arbolesconninfas,
            sum(case when d.alto_adultos = 1 then 1 else 0 end) as arbolesmeaa,
            sum(case when d.alto_ninfas = 1 then 1 else 0 end) as arbolesmean,
            sum(case when d.medio_adultos = 1 then 1 else 0 end) as arbolesmema,
            sum(case when d.medio_ninfas = 1 then 1 else 0 end) as arbolesmemn,
            sum(case when d.bajo_adultos = 1 then 1 else 0 end) as arbolesmeba,
            sum(case when d.bajo_ninfas = 1 then 1 else 0 end) as arbolesmebn
            FROM siafeson_simdia.resumen as r
            INNER JOIN siafeson_simdia.detalle as d ON d.resumen_id = r.id
            INNER JOIN siafeson_simdia.campos_scoring as c ON c.id = r.campo_id
            INNER JOIN siafeson_simdia.variedades as v ON v.id = c.variedad_id
            INNER join siafeson_siafeson.juntas as j on j.id = c.junta_id
            WHERE r.status = 1 AND d.status = 1
            AND r.fecha between ? AND ? ".$where_junta." group by j.id";
            
            $regs = DB::select($qry, [$from, $to]);
            return $regs;
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['status'=>0, 'data'=>$e->getmessage() ]);
        }
    }

    function getResumenActividadTecnico(Request $request){
        try {
                      
            $from = date($request->fini);
            $to = date($request->ffin);
            $where_junta = "";
            $where_campos = "";
            $where_imei = "";

            if($request->junta_id == 0 || $request->junta_id != 0){
                if($request->junta_id != 0){
                    $where_junta = "AND c.junta_id = ".$request->junta_id;
                }
            }

            $qry = "SELECT case when u.id is null then 'PRODUCTOR' else (p.name || ' ' || p.apellido_paterno || ' ' || p.apellido_materno) end as tecnico,
            j.short_name as jlsv,
            count(distinct c.id) as campos_rev,
            count(d.id) as arboles,
            count(distinct r.id) as registros,
            count(distinct case when d.sintomas_hlb = 1 then c.id else null end) as camposposhlb,
            count(distinct case when d.psilidos_adultos = 1 then c.id else null end) as camposconadultos,
            count(distinct case when d.psilidos_ninfas = 1 then c.id else null end) as camposconninfas,
            count(distinct case when d.alto_adultos = 1 then c.id else null end) as camposmeaa,
            count(distinct case when d.alto_ninfas = 1 then c.id else null end) as camposmean,
            count(distinct case when d.medio_adultos = 1 then c.id else null end) as camposmema,
            count(distinct case when d.medio_ninfas = 1 then c.id else null end) as camposmemn,
            count(distinct case when d.bajo_adultos = 1 then c.id else null end) as camposmeba,
            count(distinct case when d.bajo_ninfas = 1 then c.id else null end) as camposmebn,
            sum(case when d.sintomas_hlb = 1 then 1 else 0 end) as arboleshlb,
            sum(case when d.psilidos_adultos = 1 then 1 else 0 end) as arbolesconadultos,
            sum(case when d.psilidos_ninfas = 1 then 1 else 0 end) as arbolesconninfas,
            sum(case when d.alto_adultos = 1 then 1 else 0 end) as arbolesmeaa,
            sum(case when d.alto_ninfas = 1 then 1 else 0 end) as arbolesmean,
            sum(case when d.medio_adultos = 1 then 1 else 0 end) as arbolesmema,
            sum(case when d.medio_ninfas = 1 then 1 else 0 end) as arbolesmemn,
            sum(case when d.bajo_adultos = 1 then 1 else 0 end) as arbolesmeba,
            sum(case when d.bajo_ninfas = 1 then 1 else 0 end) as arbolesmebn
            FROM siafeson_simdia.resumen as r
            INNER JOIN siafeson_simdia.detalle as d ON d.resumen_id = r.id AND d.status = 1
            INNER JOIN siafeson_simdia.campos_scoring as c ON c.id = r.campo_id
            INNER JOIN siafeson_simdia.variedades as v ON v.id = c.variedad_id
            INNER join siafeson_siafeson.juntas as j on j.id = c.junta_id
            inner join siafeson_siafeson.catmunicipiossicafi as m on m.municipioid = c.municipio_id
            left join siafeson_siafeson.users as u on u.id = r.user_id
            left join siafeson_siafeson.persona as p on p.id = u.persona_id 
            WHERE r.status = 1
            AND r.fecha between ? AND ? ".$where_junta." group by u.id, p.name, p.apellido_paterno, p.apellido_materno, j.short_name";
            
            $regs = DB::select($qry, [$from, $to]);
            return $regs;
            
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['status'=>0, 'data'=>$e->getmessage() ]);
        }
    }

}
