<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActividadController extends Controller
{
    public function index()
    {
        return view('simdia.actividad.trampeo');
    }
    public function control()
    {
        return view('simdia.actividad.control');
    }
}
