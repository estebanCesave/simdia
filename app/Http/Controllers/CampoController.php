<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use DB;

class CampoController extends Controller
{
    public function aplicacionQuimica($id)
    {       
        $campo = DB::table('siafeson_simdia.cq_capturas as cq')
            ->select('cq.fecha','cq.semana','cq.ano','p.name AS producto','cq.dosis','a.name AS tipo')
            ->join('siafeson_simdia.catproductos as p','p.id','=','cq.producto_id')
            ->join('siafeson_simdia.cattipoaplicacion as a','a.id','=','cq.tipo_id')
            ->where('cq.campo_id',$id)
            ->where('cq.status',1)
            ->first(); 
                      
        return response()->json([
            'status' => 'success',
            'aplicacion' => $campo,
        ]);

    }
    public function promedioAdultosTrampa(Request $request)
    {
        $query = promedioAdultosTrampa($request->campo_id, $request->ano, $request->arco);    
        $trampas = DB::select(DB::raw($query));
        $arco = [];
        $huerta = [];
        $semanas = [];

        foreach ($trampas as $key => $value) {
            array_push($arco, $value->arco);
            array_push($semanas, $value->semana);
            array_push($huerta, $value->huerta);
        }

        return response()->json([
            'status' => 'success',
            'arco' => $arco,
            'huerta' => $huerta,
            'semanas' => $semanas,

        ]);
    }
    public function distribucionEspacial($id)
    {
        $trampas = DB::table('siafeson_simdia.trampas as t')->select('t.name','t.latitud','t.longitud')->where('t.campo_id',$id)->where('t.status',1)->get(); 
       
        return response()->json([
            'status' => 'success',
            'trampas' => $trampas,
         ]);
    }

}
