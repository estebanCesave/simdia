<?php

namespace siafeson\Http\Controllers;
use siafeson\Models\SicafisonInt;
use siafeson\Models\SicafiInt;
use siafeson\Models\SonoraInt;
use siafeson\Models\NacionalInt;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class EnvioSicafi extends Controller{

    public function index(){

        // -------------------------
        // ------ S O N O R A ------
        // -------------------------
        $n_arreglar_son = $this->ArreglarSonora();
        echo "Sonora (Arreglar): ".$n_arreglar_son."<br>";
        $n_reg_son = $this->sonora();
        echo "Sonora: ".$n_reg_son."<br>";

        // -------------------------
        // ---- N A C I O N A L ----
        // -------------------------
        $n_arreglar_nac = $this->ArreglarNacional();
        echo "Nacional (Arreglar): ".$n_arreglar_nac."<br>";
        $n_reg_nac = $this->nacional();
        echo "Nacional: ".$n_reg_nac."<br>";

    }

    private function ArreglarSonora(){
        $regs = SonoraInt::select('id AS ReplicaID')->where("sim_status_sicafi","=","3")->get()->toArray();
        $total = 0;
        if($regs > 0){
            for($x = 0; $x < count($regs); $x++){
                $existe = SicafisonInt::where("ReplicaID","=",$regs[$x]['ReplicaID'])->first();
                if($existe){
                    $existe = $existe->toArray();
                    try {
                        $reg = SonoraInt::find($regs[$x]['ReplicaID']);
                        $reg->sim_status_sicafi = 1;
                        $reg->sim_id_sicafi = $existe['ID'];
                        if($reg->save()){
                            \Log::info('Se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Sonora \\n');
                            $total = $total + 1;
                        }else{
                            \Log::error('No se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Sonora \\n');
                        }
                    }catch (\Exception $e) {
                        \Log::critical('ERROR al actualizar registro intermedia (Arreglar) de Sonora ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 1 -->' . $e->getMessage().' \\n');
                    }
                }else{
                    try {
                        $reg = SonoraInt::find($regs[$x]['ReplicaID']);
                        $reg->sim_status_sicafi = 2;
                        $reg->sim_id_sicafi = null;
                        if($reg->save()){
                            \Log::info('Se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Sonora \\n');
                            $total = $total + 1;
                        }else{
                            \Log::error('No se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Sonora \\n');
                        }
                    }catch (\Exception $e) {
                        \Log::critical('ERROR al actualizar registro intermedia (Arreglar) de Sonora ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 2 -->' . $e->getMessage().' \\n');
                    }
                }
            }
        }else{
            \Log::warning('La consulta de arreglar intermedia de Sonora no trajo registros con status = 3 \\n');
        }
        \Log::info('Se arreglarón un total de '.$total.' de registros \\n');
        return $total;
    }

    private function sonora(){
        $total = 0;
        // $affected = SonoraInt::where("sim_status_sicafi","=","2")->take(50)->update(['sim_status_sicafi' => 3]);
        try {
            $affected = DB::update(DB::raw("UPDATE siafeson_simdia.son_i_cprcmonitoreo_psilido SET sim_status_sicafi = 3 WHERE id in (SELECT id FROM siafeson_simdia.son_i_cprcmonitoreo_psilido WHERE  sim_status_sicafi = 2 LIMIT  50)"));
        }catch (\Exception $e) {
            \Log::critical('ERROR al actualizar para preparar registros --> : ' . $e->getMessage().' \\n');
        }
        // echo $affected; exit();
        if($affected > 0){
            $regs = SonoraInt::select('ejercicioid AS EjercicioID', 
            'juntaid AS JuntaID', 
            'semana AS Semana', 
            DB::raw("to_char(fecha, 'DD/MM/YYYY') as Fecha"), 
            'siembraid AS SiembraID', 
            'personalid AS PersonalID', 
            'sitioid AS SitioID',
            'ruta AS Ruta', 
            'noproductores AS noProductores', 
            'nohuertas AS noHuertas', 
            'noregistrohuerto AS NoRegistroHuerto', 
            'superficie AS Superficie', 
            'notrampasinst AS NoTrampasInst', 
            'trampas_revisadas AS Trampas_revisadas', 
            //'nopsilido AS noPsilidos', Ya no se envía
            'fenologia_brote_1 AS FenologiaID_Brote_1', // NORTE 
            'adultos_brote_1 AS Adultos_Brote_1', // NORTE 
            'ninfas_brote_1 AS Ninfas_Brote_1', // NORTE
            'fenologia_brote_2 AS FenologiaID_Brote_2', // SUR
            'adultos_brote_2 AS Adultos_Brote_2', // SUR
            'ninfas_brote_2 AS Ninfas_Brote_2', // SUR
            'fenologia_brote_3 AS FenologiaID_Brote_3', // ESTE
            'adultos_brote_3 AS Adultos_Brote_3', // ESTE
            'ninfas_brote_3 AS Ninfas_Brote_3', // ESTE
            'fenologia_brote_4 AS FenologiaID_Brote_4', // OESTE
            'adultos_brote_4 AS Adultos_Brote_4', // OESTE
            'ninfas_brote_4 AS Ninfas_Brote_4', // OESTE   
            'copyid AS CopyID', 
            'plagaid AS PlagaID', 
            'grupoid AS GrupoID', 
            'userid AS UserID', 
            'no_amefi AS No_AMEFI', 
            'fenologiaid AS FenologiaID',
            DB::raw("to_char(addrecord, 'DD/MM/YYYY HH:MI:SS') as Addrecord"),
            'campanaid AS CampanaID', 
            'smartphone AS SMARTPHONE', 
            'diaphorinaxtrampa as DiaphorinaXTrampa',
            'id AS ReplicaID', 
            'externo AS Externo')
            ->where("sim_status_sicafi","=","3")->get()->toArray();
            if($regs > 0){
                try {
                    if(SicafisonInt::insert($regs)){
                        \Log::info('Se insertaron registros en SICAFISON \\n');
                        for($x = 0; $x < count($regs); $x++){
                            $existe = SicafisonInt::where("ReplicaID","=",$regs[$x]['ReplicaID'])->first();
                            if($existe){
                                $existe = $existe->toArray();
                                try {
                                    $reg = SonoraInt::find($regs[$x]['ReplicaID']);
                                    $reg->sim_status_sicafi = 1;
                                    $reg->sim_id_sicafi = $existe['ID'];
                                    if($reg->save()){
                                        \Log::info('Se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Sonora \\n');
                                        $total = $total + 1;
                                    }else{
                                        \Log::error('No se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Sonora \\n');
                                    }
                                }catch (\Exception $e) {
                                    \Log::critical('ERROR al actualizar registro intermedia de Sonora ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 1 -->' . $e->getMessage().' \\n');
                                }
                            }else{
                                try {
                                    $reg = SonoraInt::find($regs[$x]['ReplicaID']);
                                    $reg->sim_status_sicafi = 2;
                                    if($reg->save()){
                                        \Log::info('Se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Sonora \\n');
                                    }else{
                                        \Log::error('No se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Sonora \\n');
                                    }
                                }catch (\Exception $e) {
                                    \Log::critical('ERROR al actualizar registro intermedia de Sonora ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 2 -->' . $e->getMessage().' \\n');
                                }
                            }
                        }
                    }else{
                        \Log::error('No se insertarón los registros en SICAFISON \\n');
                    }
                }catch (\Exception $e) {
                    \Log::critical('ERROR en Envio a SICAFISON (Bulk Insertion) -->' . $e->getMessage().' \\n');
                }
            }else{
                \Log::warning('La consulta a intermedia de Sonora no trajo registros con status = 3 \\n');
            }
        }else{
            \Log::warning('No habían registros en la intermedia de Sonora por enviar \\n');
        }
        // DB::enableQueryLog();
        // $last_query = DB::getQueryLog();
        // if($last_query){
        //     $info = $last_query[0];
        //     \Log::info('Query: '.$info['query'].' \n');
        //     \Log::info('Parámetros (bindings): '.implode(",",$info['bindings']).' \n');
        // }
        // print_r(DB::getQueryLog());
        // DB::disableQueryLog();
        \Log::info('Se enviaron un total de '.$total.' de registros \\n');
        return $total;
    }

    private function ArreglarNacional(){
        $regs = NacionalInt::select('id AS ReplicaID')->where("sim_status_sicafi","=","3")->get()->toArray();
        $total = 0;
        if($regs > 0){
            for($x = 0; $x < count($regs); $x++){
                $existe = SicafiInt::where("ReplicaID","=",$regs[$x]['ReplicaID'])->first();
                if($existe){
                    $existe = $existe->toArray();
                    try {
                        $reg = NacionalInt::find($regs[$x]['ReplicaID']);
                        $reg->sim_status_sicafi = 1;
                        $reg->sim_id_sicafi = $existe['ID'];
                        if($reg->save()){
                            \Log::info('Se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Nacional \\n');
                            $total = $total + 1;
                        }else{
                            \Log::error('No se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Nacional \\n');
                        }
                    }catch (\Exception $e) {
                        \Log::critical('ERROR al actualizar registro intermedia (Arreglar) de Nacional ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 1 -->' . $e->getMessage().' \\n');
                    }
                }else{
                    try {
                        $reg = NacionalInt::find($regs[$x]['ReplicaID']);
                        $reg->sim_status_sicafi = 2;
                        $reg->sim_id_sicafi = null;
                        if($reg->save()){
                            \Log::info('Se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Nacional \\n');
                            $total = $total + 1;
                        }else{
                            \Log::error('No se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Nacional \\n');
                        }
                    }catch (\Exception $e) {
                        \Log::critical('ERROR al actualizar registro intermedia (Arreglar) de Nacional ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 2 -->' . $e->getMessage().' \\n');
                    }
                }
            }
        }else{
            \Log::warning('La consulta de arreglar intermedia de Nacional no trajo registros con status = 3 \\n');
        }
        \Log::info('Se arreglarón un total de '.$total.' de registros Nacional \\n');
        return $total;
    }

    private function nacional(){
        $total = 0;
        // $affected = SonoraInt::where("sim_status_sicafi","=","2")->take(50)->update(['sim_status_sicafi' => 3]);
        try {
            $affected = DB::update(DB::raw("UPDATE siafeson_simdia.nac_i_cprcmonitoreo_psilido SET sim_status_sicafi = 3 WHERE id in (SELECT id FROM siafeson_simdia.nac_i_cprcmonitoreo_psilido WHERE  sim_status_sicafi = 2 LIMIT  50)"));
        }catch (\Exception $e) {
            \Log::critical('ERROR al actualizar para preparar registros Nacional --> : ' . $e->getMessage().' \\n');
        }
        // echo $affected; exit();
        if($affected > 0){
            $regs = NacionalInt::select('ejercicioid AS EjercicioID', 
            'juntaid AS JuntaID', 
            'semana AS Semana', 
            DB::raw("to_char(fecha, 'DD/MM/YYYY') as Fecha"), 
            'siembraid AS SiembraID', 
            'personalid AS PersonalID', 
            'sitioid AS SitioID',
            'ruta AS Ruta', 
            'noproductores AS noProductores', 
            'nohuertas AS noHuertas', 
            'noregistrohuerto AS NoRegistroHuerto', 
            'superficie AS Superficie', 
            'notrampasinst AS NoTrampasInst', 
            'trampas_revisadas AS Trampas_revisadas', 
            //'nopsilido AS noPsilidos', Ya no se envía
            'fenologia_brote_1 AS FenologiaID_Brote_1', // NORTE 
            'adultos_brote_1 AS Adultos_Brote_1', // NORTE 
            'ninfas_brote_1 AS Ninfas_Brote_1', // NORTE
            'fenologia_brote_2 AS FenologiaID_Brote_2', // SUR
            'adultos_brote_2 AS Adultos_Brote_2', // SUR
            'ninfas_brote_2 AS Ninfas_Brote_2', // SUR
            'fenologia_brote_3 AS FenologiaID_Brote_3', // ESTE
            'adultos_brote_3 AS Adultos_Brote_3', // ESTE
            'ninfas_brote_3 AS Ninfas_Brote_3', // ESTE
            'fenologia_brote_4 AS FenologiaID_Brote_4', // OESTE
            'adultos_brote_4 AS Adultos_Brote_4', // OESTE
            'ninfas_brote_4 AS Ninfas_Brote_4', // OESTE   
            'copyid AS CopyID', 
            'plagaid AS PlagaID', 
            'grupoid AS GrupoID', 
            'userid AS UserID', 
            'no_amefi AS No_AMEFI', 
            'fenologiaid AS FenologiaID',
            DB::raw("to_char(addrecord, 'DD/MM/YYYY HH:MI:SS') as Addrecord"),
            'campanaid AS CampanaID', 
            'smartphone AS SMARTPHONE', 
            'diaphorinaxtrampa as DiaphorinaXTrampa',
            'id AS ReplicaID', 
            'externo AS Externo')
            ->where("sim_status_sicafi","=","3")->get()->toArray();
            if($regs > 0){
                try {
                    if(SicafiInt::insert($regs)){
                        \Log::info('Se insertaron registros en SICAFISON \\n');
                        for($x = 0; $x < count($regs); $x++){
                            $existe = SicafiInt::where("ReplicaID","=",$regs[$x]['ReplicaID'])->first();
                            if($existe){
                                $existe = $existe->toArray();
                                try {
                                    $reg = NacionalInt::find($regs[$x]['ReplicaID']);
                                    $reg->sim_status_sicafi = 1;
                                    $reg->sim_id_sicafi = $existe['ID'];
                                    if($reg->save()){
                                        \Log::info('Se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Nacional \\n');
                                        $total = $total + 1;
                                    }else{
                                        \Log::error('No se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Nacional \\n');
                                    }
                                }catch (\Exception $e) {
                                    \Log::critical('ERROR al actualizar registro intermedia de Nacional ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 1 -->' . $e->getMessage().' \\n');
                                }
                            }else{
                                try {
                                    $reg = NacionalInt::find($regs[$x]['ReplicaID']);
                                    $reg->sim_status_sicafi = 2;
                                    if($reg->save()){
                                        \Log::info('Se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Nacional \\n');
                                    }else{
                                        \Log::error('No se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Nacional \\n');
                                    }
                                }catch (\Exception $e) {
                                    \Log::critical('ERROR al actualizar registro intermedia de Nacional ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 2 -->' . $e->getMessage().' \\n');
                                }
                            }
                        }
                    }else{
                        \Log::error('No se insertarón los registros en SICAFI \\n');
                    }
                }catch (\Exception $e) {
                    \Log::critical('ERROR en Envio a SICAFI (Bulk Insertion) -->' . $e->getMessage().' \\n');
                }
            }else{
                \Log::warning('La consulta a intermedia de Nacional no trajo registros con status = 3 \\n');
            }
        }else{
            \Log::warning('No habían registros en la intermedia de Nacional por enviar \\n');
        }
        \Log::info('Se enviaron un total de '.$total.' de registros Nacional \\n');
        return $total;
    }

}
