<?php
namespace siafeson\Http\Controllers;
use siafeson\Models\Captura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use siafeson\User;

class RegistroController extends Controller
{
    public function __construct()
    {
      //ini_set('max_execution_time', 300);
    }
    public function getRegistros(request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300000);
        $desde = $request->desde;
        $hasta = $request->hasta;
        $anoDesde = substr($desde,0,4);
        $anoHasta = substr($hasta,0,4);
        $user_id = Auth::user()->id;
        
        if(Auth::user()->hasrole('Admin Junta') && $request->session()->get('rol') == 'Admin Junta' )
        {
          $junta = $this->getJunta();
          
          $captura = DB::table('siafeson_simdia.capturas as c')->select(
                'c.id',
                "c.fecha",
                'c.captura',
                'c.instalada',
                'c.revisada',
                'c.inserted_sicafi as sicafi',
                'c.method as method',
                'c.fenologia_trampa_id AS fenologia',
                'c.observaciones as motivo',
                'c.noa',
                'c.non',
                'c.nof',
                'c.sua',
                'c.sun',
                'c.suf',
                'c.esa',
                'c.esn',
                'c.esf',
                'c.oea',
                'c.oen',
                'c.oef',
                't.siembra_id',
                't.id as trampa',
                't.name AS name',
                'f.name as fenologia_text',
                'p.status',
                DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"),
                DB::raw("
                  CASE
                    WHEN c.method = 1 THEN 'Móvil'
                    WHEN c.method = 2 THEN 'Web'
                    WHEN c.method = 3 THEN 'Modificado por web'
                    WHEN c.method = 4 THEN 'Recuperado'
                    WHEN c.method = 5 THEN 'Ingresado por Admin en semana siguiente'
                    ELSE 'N/A'
                  END as metodo_tex               
                "),
                'u.id as user',
                'j.name as junta')
            ->join('siafeson_simdia.trampas as t','c.trampa_id','t.id')
            ->join('siafeson_siafeson.users as u','u.id','c.user_id')
            ->join('siafeson_siafeson.persona as p','u.persona_id','p.id')
            ->join('siafeson_siafeson.juntas as j','j.id','p.junta_id')
            ->join('siafeson_simdia.fenologias as f','f.id','c.fenologia_trampa_id')
            ->where('c.status',1)
            ->where('c.valido',1)
            ->whereBetween('c.fecha', [ $request->desde, $request->hasta ])
            ->whereBetween('c.ano', [ substr($desde,0,4), substr($hasta,0,4) ])
            ->where('j.id',Auth::user()->persona->junta_id)
            ->orderBy('sicafi','ASC');
          
            return datatables()->query($captura)->addColumn('fecha','chunks.datatable.fecha')->rawColumns(['fenologia'])->make(true);
        }
        else if(Auth::user()->hasrole('Técnico') && $request->session()->get('rol') == 'Técnico')
        {
            $junta = $this->getJunta();
            $captura = DB::table('siafeson_simdia.capturas as c')->select(
                'c.id',
                "c.fecha",
                'c.captura',
                'c.instalada',
                'c.revisada',
                'c.inserted_sicafi as sicafi',
                'c.method as method',
                'c.fenologia_trampa_id AS fenologia',
                'c.observaciones as motivo',
                'c.noa',
                'c.non',
                'c.nof',
                'c.sua',
                'c.sun',
                'c.suf',
                'c.esa',
                'c.esn',
                'c.esf',
                'c.oea',
                'c.oen',
                'c.oef',
                't.siembra_id',
                't.id as trampa',
                't.name AS name',
                'f.name as fenologia_text',
                'p.status',
                DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"),
                DB::raw("
                  CASE
                    WHEN c.method = 1 THEN 'Móvil'
                    WHEN c.method = 2 THEN 'Web'
                    WHEN c.method = 3 THEN 'Modificado por web'
                    WHEN c.method = 4 THEN 'Recuperado'
                    WHEN c.method = 5 THEN 'Ingresado por Admin en semana siguiente'
                    ELSE 'N/A'
                  END as metodo_tex               
                "),
                'u.id as user',
                'j.name as junta')
            ->join('siafeson_simdia.trampas as t','c.trampa_id','t.id')
            ->join('siafeson_siafeson.users as u','u.id','c.user_id')
            ->join('siafeson_siafeson.persona as p','u.persona_id','p.id')
            ->join('siafeson_siafeson.juntas as j','j.id','p.junta_id')
            ->join('siafeson_simdia.fenologias as f','f.id','c.fenologia_trampa_id')
            ->leftJoin('siafeson_simdia.observaciones as o','o.id','c.observaciones')
            ->leftJoin('siafeson_simdia.campos as ca','ca.campo_id','t.campo_id')
            ->where('c.status',1)
            ->where('c.valido',1)
            ->whereBetween('c.fecha', [ $desde, $hasta ])
            ->whereBetween('c.ano', [ $anoDesde, $anoHasta ])
            ->where('c.user_id',$user_id)
            ->where('j.id',$junta)
            ->orderBy('sicafi','ASC');

            return datatables()->query($captura)->addColumn('fecha','chunks.datatable.fecha')->rawColumns(['fenologia'])->make(true);
        }
        else if(Auth::user()->hasrole('Admin Estado') && $request->session()->get('rol') == 'Admin Estado' )
        {
            $estado = $this->getEstado();
            $captura = DB::table('siafeson_simdia.capturas as c')->select(
                'c.id',
                "c.fecha",
                'c.captura',
                'c.instalada',
                't.name AS name',
                'c.fenologia_trampa_id AS fenologia',
                'p.status',
                't.siembra_id',
                DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"),  
                'c.inserted_sicafi as sicafi',
                'j.name as junta',
                'e.name as estado',
                'c.method as method')
            ->join('siafeson_simdia.trampas as t','c.trampa_id','t.id')
            ->join('siafeson_siafeson.users as u','u.id','c.user_id')
            ->join('siafeson_siafeson.persona as p','u.persona_id','p.id')
            ->join('siafeson_siafeson.juntas as j','j.id','p.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','j.estado_id')
            ->where('c.status',1)
            ->where('j.estado_id',$estado)
            ->where('c.valido',1)
            ->whereBetween('c.fecha', [ $desde, $hasta ])
            ->whereBetween('c.ano', [ $anoDesde, $anoHasta ])
            ->orderBy('sicafi','ASC');
                      
            return datatables()
              ->query($captura)
              ->addColumn('slc','chunks.datatable.select')
              ->addColumn('fecha','chunks.datatable.fecha')
              ->addColumn('method','chunks.datatable.method')
              ->addColumn('status','chunks.datatable.status')
              ->addColumn('fenologia','chunks.datatable.fenologia')
              ->addColumn('btn','chunks.datatable.actions')
              ->rawColumns(['btn','slc','status','method','fecha'])
              ->filterColumn('nombre', function($query, $keyword) { 
                $sql = "CONCAT( p.name,'-',p.apellido_paterno,'-',p.apellido_materno)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
              })
              ->make(true);           
        }
        else if(Auth::user()->hasrole('Super Admin') && $request->session()->get('rol') == 'Super Admin')
        {
            $captura = DB::table('siafeson_simdia.capturas as c')->select(
              'c.id',
              "c.fecha",
              'c.captura',
              'c.instalada',
              't.name AS name',
              'c.fenologia_trampa_id AS fenologia',
              'p.status',
              'p.name as nombre',
              't.siembra_id',
              'p.apellido_paterno as apellidopaterno',
              'p.apellido_materno as apellidomaterno',
              'c.inserted_sicafi as sicafi',
              'j.name as junta',
              'e.name as estado',
              'c.method as method')
          ->join('siafeson_simdia.trampas as t','c.trampa_id','t.id')
          ->join('siafeson_siafeson.users as u','u.id','c.user_id')
          ->join('siafeson_siafeson.persona as p','u.persona_id','p.id')
          ->join('siafeson_siafeson.juntas as j','j.id','p.junta_id')
          ->join('siafeson_siafeson.estados as e','e.id','j.estado_id')
          ->leftJoin('siafeson_simdia.campos as ca','ca.campo_id','t.campo_id')
          ->join('siafeson_simdia.fenologias as f','f.id','c.fenologia_trampa_id')
          ->leftJoin('siafeson_simdia.observaciones as o','o.id','c.observaciones')
          ->where('c.status',1)
          ->where('c.valido',1)
          ->whereBetween('c.fecha', [ $desde, $hasta ])
          ->whereBetween('c.ano', [ $anoDesde, $anoHasta ])
          ->orderBy('sicafi','ASC');

          return datatables()
            ->query($captura)
            ->addColumn('slc','chunks.datatable.select')
            ->addColumn('fecha','chunks.datatable.fecha')
            ->addColumn('nombre','chunks.datatable.names')
            ->addColumn('method','chunks.datatable.method')
            ->addColumn('status','chunks.datatable.status')
            ->addColumn('fenologia','chunks.datatable.fenologia')
            ->addColumn('btn','chunks.datatable.actions')
            ->rawColumns(['btn','slc','status','method','fecha'])
            ->make(true);
        }
        else{
          abort(403);
        }
        return datatables()->collection($captura)->toJson();
    }
    public function addCaptura(request $request)
    {
      if((Auth::user()->hasrole('Admin Junta') && $request->session()->get('rol') == 'Admin Junta') || (Auth::user()->hasrole('Admin Estado') && $request->session()->get('rol') == 'Admin Estado'))
      {
          $query = getCoordenadas($request->trampa);
          $coordenadas = DB::select(DB::raw($query));
          $coordenadas2 = '('.$coordenadas[0]->latitud.','.$coordenadas[0]->longitud.')';
          if($request->edit == 1){
              $captura = Captura::find($request->captura_id);
              $captura->method = 3;
            }else{
              $captura = new Captura;
              $captura->trampa_id = $request->trampa;
              $captura->user_id = $request->user_id;
              $captura->revisada = 1;
              $captura->status = 1;
              $captura->method = 2;
              $captura->created = "NOW()";
              $captura->posicion = $coordenadas2;
            }
            $captura->fecha = $request->fecha;   		
            $captura->fenologia_trampa_id = $request->fenologia;   		
            $captura->captura = $request->num_insectos;
            $captura->noa = $request->noa;
            $captura->non = $request->non;
            $captura->nof = $request->nof;
            $captura->sua = $request->sua;
            $captura->sun = $request->sun;
            $captura->suf = $request->suf;
            $captura->esa = $request->esa;
            $captura->esn = $request->esn;
            $captura->esf = $request->esf;
            $captura->oea = $request->oea;
            $captura->oen = $request->oen;
            $captura->oef = $request->oef;   	   		
            $captura->instalada = $request->instalada;
            $captura->revisada = $request->revisada;   	   		
            $captura->observaciones = $request->motivo;   		
            $captura->modified = date("Y-m-d H:i:s");
            $captura->save();
            
            if($captura){
              return array("estatus" => true,"edit" => $request->edit, 'data' => $captura);
            }else{
              return array("estatus" => false, 'data' => $captura);
            }          
        
      }
      else if(Auth::user()->hasrole('Técnico') && $request->session()->get('rol') == 'Técnico')
      {
        $user_id = Auth::user()->id;
        $query = getCoordenadas($request->trampa);
        // return $request->trampa;
          $coordenadas = DB::select(DB::raw($query));
          $coordenadas2 = '('.$coordenadas[0]->latitud.','.$coordenadas[0]->longitud.')';
          if($request->edit == 1){
            $captura = Captura::find($request->captura_id);
            $captura->method = 3;
          }else{
            $captura = new Captura;
            $captura->trampa_id = $request->trampa;
            $captura->user_id = $user_id;
            $captura->revisada = 1;
            $captura->status = 1;
            $captura->method = 2;
            $captura->created = date("Y-m-d H:i:s");
            $captura->posicion = $coordenadas2;
          }
          
          $captura->fecha = $request->fecha;   		
          $captura->fenologia_trampa_id = $request->fenologia;   		
          $captura->captura = $request->num_insectos;
          $captura->noa = $request->noa;
          $captura->non = $request->non;
          $captura->nof = $request->nof;
          $captura->sua = $request->sua;
          $captura->sun = $request->sun;
          $captura->suf = $request->suf;
          $captura->esa = $request->esa;
          $captura->esn = $request->esn;
          $captura->esf = $request->esf;
          $captura->oea = $request->oea;
          $captura->oen = $request->oen;
          $captura->oef = $request->oef;   	   		
          $captura->instalada = $request->instalada;   		
          $captura->modified = date("Y-m-d H:i:s");
          $captura->revisada = $request->revisada;   	   		
          $captura->observaciones = $request->motivo;
          $captura->save();

          if($captura){
            return array("estatus" => true,"edit" => $request->edit);
          }else{
            return array("estatus" => false);
          }
      }      		
    }
    public function eliminarCaptura(request $request)
    {
      $captura = Captura::find($request->captura_id);
      if($captura){
        $captura->status = 0;
      if($captura->save()){
        return array("estatus" => true);
      }else{
        return array("estatus" => false);
      }
      }else{
         return array("estatus" => false);
      }
    }
    /**POR ID DE CAPTURA */
    public function getCaptura(request $request)
    {
      if(Auth::user()->hasrole('Técnico') && $request->session()->get('rol') == 'Técnico')
      {
        return Captura::select('siafeson_simdia.capturas.id',
          'siafeson_simdia.capturas.fecha',
          'siafeson_simdia.capturas.trampa_id',
          'siafeson_simdia.capturas.fenologia_trampa_id',
          'siafeson_simdia.capturas.captura',
          'siafeson_simdia.capturas.instalada',
          'siafeson_simdia.capturas.revisada',
          'siafeson_simdia.capturas.observaciones',
          'siafeson_simdia.capturas.noa',
          'siafeson_simdia.capturas.non',
          'siafeson_simdia.capturas.nof',
          'siafeson_simdia.capturas.sua',
          'siafeson_simdia.capturas.sun',
          'siafeson_simdia.capturas.suf',
          'siafeson_simdia.capturas.esa',
          'siafeson_simdia.capturas.esn',
          'siafeson_simdia.capturas.esf',
          'siafeson_simdia.capturas.oea',
          'siafeson_simdia.capturas.oen',
          'siafeson_simdia.capturas.oef',
          't.name as trampa_text')
          ->join('siafeson_simdia.trampas as t','t.id','siafeson_simdia.capturas.trampa_id')
          ->leftJoin('siafeson_simdia.observaciones as o','o.id','siafeson_simdia.capturas.observaciones')
          ->where('siafeson_simdia.capturas.id',$request->captura_id)->get();
      }
      else if((Auth::user()->hasrole('Admin Junta') && $request->session()->get('rol') == 'Admin Junta') || (Auth::user()->hasrole('Admin Estado') && $request->session()->get('rol') == 'Admin Estado'))
      {
          $capturas = Captura::select('siafeson_simdia.capturas.id',
            'siafeson_simdia.capturas.fecha',
            'siafeson_simdia.capturas.trampa_id',
            'siafeson_simdia.capturas.fenologia_trampa_id',
            'siafeson_simdia.capturas.captura',
            'siafeson_simdia.capturas.instalada',
            'siafeson_simdia.capturas.revisada',
            'siafeson_simdia.capturas.observaciones',
            'siafeson_simdia.capturas.noa',
            'siafeson_simdia.capturas.non',
            'siafeson_simdia.capturas.nof',
            'siafeson_simdia.capturas.sua',
            'siafeson_simdia.capturas.sun',
            'siafeson_simdia.capturas.suf',
            'siafeson_simdia.capturas.esa',
            'siafeson_simdia.capturas.esn',
            'siafeson_simdia.capturas.esf',
            'siafeson_simdia.capturas.oea',
            'siafeson_simdia.capturas.oen',
            'siafeson_simdia.capturas.oef',
            'p.name',
            'u.id as userId',
            't.name as trampa_text')
            ->join('siafeson_simdia.trampas as t','t.id','siafeson_simdia.capturas.trampa_id')
            ->join('siafeson_siafeson.users as u','u.id','siafeson_simdia.capturas.user_id')
            ->join('siafeson_siafeson.persona as p','u.persona_id','p.id')            
            ->leftJoin('siafeson_simdia.observaciones as o','o.id','siafeson_simdia.capturas.observaciones')
            ->where('siafeson_simdia.capturas.id',$request->captura_id)->get();
          
          return $capturas;
      }

    }
    public function preparacapadatos(request $request)
    {
      $pila = [];
      $fecha = date('m');
      $semana = date('W');

      foreach ($request->ids as $id) 
      {
        $enproceso = Captura::find($id);
        $fechaRegistro = date('m',strtotime($enproceso->fecha));
        $fechaRegistroSemana = date('W',strtotime($enproceso->fecha));

          // ESTO NO ES VALIDO: if($fechaRegistro == $fecha || $fechaRegistroSemana == $semana){
          if($fechaRegistro == $fecha)
          {
            if($enproceso->inserted_sicafi == 0)
            {
              $enproceso->inserted_sicafi = 2;
              \Log::info('El registro ('.$enproceso->id.') cambio a dos');
              \Log::error($enproceso);
              $enproceso->save();
              
              array_push($pila, $enproceso);
            }
            else{
              \Log::info('El registro ('.$enproceso->id.') no esta en 0');
              \Log::error($enproceso);
            }
            
          }
          else{
            return response()->json([
              'status' => 'error',
              'message' => ' ',
            ]); 
          }
      }
      return response()->json([
        'status' => 'success',
        'message' =>  count($pila) .' registros seleccionados están en proceso de envío a capa de datos'
      ]); 
    }
    function getJunta()
    {
      $junta =  DB::table('siafeson_siafeson.persona')
        ->select('siafeson_siafeson.juntas.name as junta', 'siafeson_siafeson.juntas.id as juntaId')
        ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
        ->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
        ->where('siafeson_siafeson.users.id',Auth::user()->id)
        ->first();

        return $junta->juntaId;
    }
    function getEstado()
    {
      $estado =  DB::table('siafeson_siafeson.users')
      ->select('siafeson_siafeson.juntas.estado_id')
      ->join('siafeson_siafeson.persona','siafeson_siafeson.users.persona_id','=','siafeson_siafeson.persona.id')
      ->join('siafeson_siafeson.juntas','siafeson_siafeson.juntas.id','=','siafeson_siafeson.persona.junta_id')
      ->where('siafeson_siafeson.users.id',Auth::user()->id)
      ->first();

      return $estado->estado_id;
    }
    
}