<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Models\EjercicioSemana;
use Auth;
use DB;

class AvancesController extends Controller
{
    public function avance(Request $request)
    {
        $this->ejercicio = EjercicioSemana::select('semana', 'ejercicioid','ano')->semana($request->desde)->first();

        $semana = $this->ejercicio->semana;
        $ano = $this->ejercicio->ano;
        
        $registradas = DB::table('siafeson_simdia.capturas AS c')->select('c.id')->join('siafeson_simdia.trampas as t','t.id','c.trampa_id')->where('c.semana',$semana)->where('c.ano',$ano)->where('c.status',1)->where('t.status',1)->where('c.user_id',$request->id)->count();
        $revisadas = DB::table('siafeson_simdia.capturas AS c')->select('c.id')->join('siafeson_simdia.trampas as t','t.id','c.trampa_id')->where('c.semana',$semana)->where('c.ano',$ano)->where('c.status',1)->where('t.status',1)->where('c.user_id',$request->id)->where('c.revisada',1)->count();
        $noRevisadas = DB::table('siafeson_simdia.capturas AS c')->select('c.id')->join('siafeson_simdia.trampas as t','t.id','c.trampa_id')->where('c.semana',$semana)->where('c.ano',$ano)->where('c.status',1)->where('t.status',1)->where('c.user_id',$request->id)->where('c.revisada',0)->count();
        $enSicafi = DB::table('siafeson_simdia.capturas AS c')->select('c.id')->join('siafeson_simdia.trampas as t','t.id','c.trampa_id')->where('c.semana',$semana)->where('c.ano',$ano)->where('c.status',1)->where('t.status',1)->where('c.user_id',$request->id)->whereIn('c.inserted_sicafi', [1,4])->count();        
        $trampas = DB::table('siafeson_simdia.trampas AS t')->select('t.id')->join('siafeson_simdia.rel_user_trampa as r','t.id','=','r.trampa_id')->where('t.status',1)->where('t.tipo_huerta','<>',3)->where('r.user_id',$request->id)->where('r.status',1)->count();
        
        $telefono =  DB::table('siafeson_simdia.capturas AS c')->select('c.id')->join('siafeson_simdia.trampas as t','t.id','c.trampa_id')->where('c.semana',$semana)->where('c.ano',$ano)->where('c.status',1)->where('t.status',1)->where('c.user_id',$request->id)->whereIn('c.method',[1,4])->count();
        $web =  DB::table('siafeson_simdia.capturas AS c')->select('c.id')->join('siafeson_simdia.trampas as t','t.id','c.trampa_id')->where('c.semana',$semana)->where('c.ano',$ano)->where('c.status',1)->where('t.status',1)->where('c.user_id',$request->id)->whereIn('c.method',[2,5])->count();
        $telefono_web =  DB::table('siafeson_simdia.capturas AS c')->select('c.id')->join('siafeson_simdia.trampas as t','t.id','c.trampa_id')->where('c.semana',$semana)->where('c.ano',$ano)->where('c.status',1)->where('t.status',1)->where('c.user_id',$request->id)->where('c.method',3)->count();
        $eliminadas =  DB::table('siafeson_simdia.capturas AS c')->select('c.id')->join('siafeson_simdia.trampas as t','t.id','c.trampa_id')->where('c.semana',$semana)->where('c.ano',$ano)->where('c.status',0)->where('c.user_id',$request->id)->count();

        
        $avance =  $registradas / $trampas * 100;
        $avanceSicafi = $enSicafi / $trampas * 100;
        $faltantes = $trampas - $registradas;

        $huertos = DB::table('siafeson_simdia.trampas AS t')
            ->select('c.latitud','c.longitud','e.name as estado','t.arco', 'm.name as municipio','c.id','c.campo_id','c.name as huerta',DB::raw('COUNT(t.id) as trampas'),DB::raw('SUM(t.superficie) as superficie'))
            ->join("siafeson_simdia.campos as c",function($join){
                $join->on("c.campo_id","=","t.campo_id")->orOn("c.id","=","t.campo_id");
            })
            ->join('siafeson_simdia.rel_user_trampa as r','t.id','r.trampa_id')
            ->join('siafeson_siafeson.juntas as j','j.id','t.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','=','j.estado_id')
            ->leftJoin('siafeson_siafeson.catmunicipiossicafi as m','m.municipioid','c.municipio_id')
            ->where('t.tipo_huerta','<>',3)
            ->where('t.status',1)->where('r.status',1)
            ->where('t.junta_id',Auth::user()->persona->junta_id)
            ->where('r.user_id',$request->id)->groupBy('c.name','c.id','c.campo_id','m.name','e.name','t.arco','c.latitud','c.longitud')->get();
            
       
        return response()->json([
            'status' => 'success',
            'semana' => $semana,
            'ano' => $ano,
            'telefono' => $telefono,
            'web' => $web,
            'telefonoWeb' => $telefono_web,
            'eliminadas' => $eliminadas,
            'registradas' => $registradas,
            'revisadas' => $revisadas,
            'noRevisadas' => $noRevisadas,
            'trampas' => $trampas,
            'faltantes' => $faltantes,
            'avance' => number_format($avance,2),
            'avanceSicafi' => number_format($avanceSicafi,2),
            'huertos' => $huertos
        ]);
    }
}
