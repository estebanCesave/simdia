<?php
namespace siafeson\Http\Controllers\SimdiaCq;
use Illuminate\Http\Request;
use siafeson\Http\Controllers\Controller;
use siafeson\Models\Simdiacq\Aplicacion;
use siafeson\Models\Simdiacq\NOAplicacion;

class AplicacionController extends Controller
{
    public function index()
    {
        return Aplicacion::select('id as value','name as name')->where('status',1)->get();
    }
    public function NoAplicacion()
    {
        return NOAplicacion::select('id as value','name as name')->where('status',1)->get();
    }
}
