<?php
namespace siafeson\Http\Controllers\SimdiaCq;
use Illuminate\Http\Request;
use siafeson\Http\Controllers\Controller;
use siafeson\Models\Simdiacq\Control;

class ControlController extends Controller
{
    public function index()
    {
        return Control::select('id as value','name as name')->where('status',1)->get();
    }
}
