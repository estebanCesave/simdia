<?php
namespace siafeson\Http\Controllers\SimdiaCq;
use Illuminate\Http\Request;
use siafeson\Http\Controllers\Controller;
use siafeson\Models\EjercicioSemana;
use DB;
use Auth;

class UbicacionesController extends Controller
{
    public function index(Request $request)
    {
        $this->ejercicio = EjercicioSemana::select('semana', 'ejercicioid', 'ano')->semana($request->fecha)->first();

        $ubicaciones = DB::table('siafeson_simdia.trampas as t')
            ->select('t.id AS Id',
                    'c.latitud',
                    'c.longitud',
                    'c.name AS Huerta',
                    't.id AS id_trampa',                    
                    't.name AS Trampa',
                    't.superficie AS Superficie',
                    't.siembra_id as SiembraID')	   
            ->join('siafeson_simdia.campos as c', 'c.campo_id', '=', 't.campo_id')
            ->join('siafeson_siafeson.juntas as j', 'j.id', '=', 't.junta_id')
            ->where('t.superficie','>',0)
            ->where('t.status',1)
            ->where('t.junta_id',Auth::user()->persona->junta_id)
            ->whereNotIn('t.campo_id', DB::table('siafeson_simdia.cq_capturas as cq')->select('cq.campo_id')->where('cq.semana',$this->ejercicio->semana)->where('cq.ano',$this->ejercicio->ano))           
            ->get();

        return response()->json($ubicaciones);
     }    
     public function superficie(Request $request)
     {
        $this->ejercicio = EjercicioSemana::select('semana', 'ejercicioid', 'ano')->semana($request->fecha)->first();

        $ubicaciones = DB::table('siafeson_simdia.trampas as t')
        ->select('t.id AS Id',
                'c.latitud',
                'c.longitud',
                'c.name AS Huerta',
                't.id AS id_trampa',                    
                't.name AS Trampa',
                't.superficie AS Superficie',
                't.siembra_id as SiembraID')	   
        ->join('siafeson_simdia.campos as c', 'c.campo_id', '=', 't.campo_id')
        ->join('siafeson_siafeson.juntas as j', 'j.id', '=', 't.junta_id')
        ->where('t.id',$request->id)
        ->where('t.superficie','>',0)
        ->where('t.status',1)
        ->where('t.junta_id',Auth::user()->persona->junta_id)
        ->whereNotIn('t.campo_id', DB::table('siafeson_simdia.cq_capturas as cq')->select('cq.campo_id')->where('cq.semana',$this->ejercicio->semana)->where('cq.ano',$this->ejercicio->ano))           
        ->first();

        return response()->json($ubicaciones);

    }

}
