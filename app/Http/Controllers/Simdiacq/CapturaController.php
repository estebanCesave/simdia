<?php
namespace siafeson\Http\Controllers\Simdiacq;
use Illuminate\Http\Request;
use siafeson\Http\Controllers\Controller;
use siafeson\Models\Trampa;
use siafeson\Models\Simdiacq\Captura;
use siafeson\Models\EjercicioSemana;
use Auth;
use DB;

class CapturaController extends Controller
{
    public function index(request $request)
    {              
        $captura = DB::table('siafeson_simdia.cq_capturas as cq')->select(
            'cq.id',
            'cq.fecha',
            'cq.dosis',
            'cq.superficie',
            'cq.no_plantas as plantas',
            'cq.no_productores as productores',
            'cq.inserted_sicafi as sicafi',
            'tc.name as tipoControl',
            'tc.id as control',
            'ta.name as tipoAplicacion',
            'ta.id as tipo',
            'cp.name as producto_text',
            'cp.id as producto',
            'cb.name as controlBiologico',
            'cb.id as biologico',
            'na.name as noAplicacion',
            'na.id as aplicacion',
            'u.id as user',
            't.siembra_id',
            'c.name AS name',
            't.id as huerta',
            DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"),
            DB::raw("
              CASE
                WHEN cq.method = 1 THEN 'Móvil'
                WHEN cq.method = 2 THEN 'Web'
                WHEN cq.method = 3 THEN 'Modificado por web'
                WHEN cq.method = 4 THEN 'Recuperado'
                WHEN cq.method = 5 THEN 'Ingresado por Admin en semana siguiente'
                ELSE 'N/A'
              END as metodo_tex               
            "),
            'u.id as user')
        ->join('siafeson_simdia.trampas as t','cq.trampa_id','t.id')
        ->join('siafeson_siafeson.users as u','u.id','cq.user_id')
        ->join('siafeson_siafeson.persona as p','u.persona_id','p.id')
        ->join('siafeson_siafeson.juntas as j','j.id','p.junta_id')
        ->join('siafeson_siafeson.estados as e','e.id','j.estado_id')
        ->join('siafeson_simdia.cattipocontrol as tc','cq.tipo_control_id','tc.id')
        ->join('siafeson_simdia.cattipoaplicacion as ta','cq.tipo_id','ta.id')
        ->join('siafeson_simdia.catproductos as cp','cq.producto_id','cp.id')
        ->join('siafeson_simdia.catcontrolbiologico as cb','cq.c_biologico_id','cb.id')
        ->join('siafeson_simdia.cq_catnoaplicacion as na','cq.noaplicacion_id','na.id')
        ->join('siafeson_simdia.campos as c','cq.campo_id','c.campo_id')
        ->where('cq.status',1)
        ->whereBetween('cq.fecha', [ $request->desde, $request->hasta ])
        ->whereBetween('cq.ano', [ substr($request->desde,0,4), substr($request->hasta,0,4) ]);

        if(Auth::user()->hasrole('Técnico') && $request->session()->get('rol') == 'Técnico'){
          $captura->where('cq.user_id',Auth::user()->id);
        }
        else if(Auth::user()->hasrole('Admin Junta') && $request->session()->get('rol') == 'Admin Junta' ){
          $captura->where('j.id',Auth::user()->persona->junta_id);
        }
        else if(Auth::user()->hasrole('Admin Estado') && $request->session()->get('rol') == 'Admin Estado' ){
          $captura->where('j.estado_id',Auth::user()->persona->junta->estado_id);
        }
       
        $captura->orderBy('sicafi','ASC');

        return datatables()->query($captura)->addColumn('fecha','chunks.datatable.fecha')->make(true);
    }
    public function store(request $request)
    {
        $this->ejercicio = EjercicioSemana::select('semana', 'ejercicioid')->semana($request->fecha)->first();
        $campo = Trampa::find($request->trampa_id);

        $captura = new Captura;
        $captura->fecha = $request->fecha;
        $captura->ejercicio = $this->ejercicio->ejercicioid;
        $captura->semana = $this->ejercicio->semana;
        $captura->mes = $request->mes;
        $captura->ano = $request->ano;
        $captura->trampa_id = $request->trampa_id;
        $captura->campo_id = $campo->campo_id;
        $captura->user_id = $request->user_id;
        $captura->latitud = $request->latitud;
        $captura->longitud = $request->longitud;
        $captura->id_bd_cel =  $request->id_bd_cel;
        $captura->accuracy = $request->accuracy;
        $captura->imei = '--N/A--';
        $captura->producto_id =  $request->producto;
        $captura->superficie =  $request->superficie;
        $captura->no_plantas =  $request->plantas;
        $captura->tipo_control_id =  $request->tipoControl;
        $captura->c_biologico_id =  $request->control;
        $captura->method = 2;
        $captura->tipo_id = $request->aplicacion;
        $captura->noaplicacion_id = $request->no_aplicacion;
        $captura->dosis = $request->dosis;
        $captura->no_productores = $request->productores;
        $captura->semana_created = $request->semana;
        $captura->status = 1;
        $captura->created = date("Y-m-d H:i:s");
        $captura->modified = date("Y-m-d H:i:s");
        $captura->save();

        return response()->json([
            'status' => 'success',            
            'message' => 'Captura creada correctamente',
            'data' => $captura
        ]);
    }
    public function update(request $request, $id)
    {
        $this->ejercicio = EjercicioSemana::select('semana', 'ejercicioid')->semana($request->fecha)->first();
        $campo = Trampa::find($request->trampa_id);

        $captura = Captura::find($id);
        $captura->fecha = $request->fecha;
        $captura->ejercicio = $this->ejercicio->ejercicioid;
        $captura->semana = $this->ejercicio->semana;
        $captura->mes = $request->mes;
        $captura->ano = $request->ano;
        $captura->trampa_id = $request->trampa_id;
        $captura->campo_id = $campo->campo_id;
        $captura->user_id = $request->user_id;
        $captura->latitud = $request->latitud;
        $captura->longitud = $request->longitud;
        $captura->id_bd_cel =  $request->id_bd_cel;
        $captura->accuracy = $request->accuracy;
        $captura->imei = '--N/A--';
        $captura->producto_id =  $request->producto;
        $captura->superficie =  $request->superficie;
        $captura->no_plantas =  $request->plantas;
        $captura->tipo_control_id =  $request->tipoControl;
        $captura->c_biologico_id =  $request->control;
        $captura->method = 3;
        $captura->tipo_id = $request->aplicacion;
        $captura->noaplicacion_id = $request->no_aplicacion;
        $captura->dosis = $request->dosis;
        $captura->no_productores = $request->productores;
        $captura->semana_created = $request->semana;
        $captura->status = 1;
        $captura->created = date("Y-m-d H:i:s");
        $captura->modified = date("Y-m-d H:i:s");
        $captura->save();
               
        return response()->json([
            'status' => 'success',
            'message' => 'Captura actualizada correctamente',
            'data' => $captura
        ]);
    }
    public function delete(request $request)
    {
      $captura = Captura::find($request->captura_id);
      if($captura){
        $captura->status = 0;
      if($captura->save()){
        return array("estatus" => true);
      }else{
        return array("estatus" => false);
      }
      }else{
         return array("estatus" => false);
      }
    }
}
