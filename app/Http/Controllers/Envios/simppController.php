<?php
namespace siafeson\Http\Controllers\Envios;
use Illuminate\Http\Request;
use siafeson\Http\Controllers\Controller;
use siafeson\Models\SIMPP\SicafiSon;
use siafeson\Models\SIMPP\Intermedia;
use DB;

class simppController extends Controller
{
    public function index()
    {
       // $n_arreglar_son = $this->ArreglarSonora();
       // echo "Sonora (Arreglar): ".$n_arreglar_son."<br>";
        $n_reg_son = $this->sonora();
        //echo "Sonora: ".$n_reg_son."<br>";
    }
    private function ArreglarSonora()
    {
        $regs = Intermedia::select('id AS ReplicaID')->where("sim_status_sicafi","=","3")->get()->toArray();
        $total = 0;

        if($regs > 0)
        {
            for($x = 0; $x < count($regs); $x++)
            {
                $existe = SicafiSon::where("ID","=",$regs[$x]['ReplicaID'])->first();

                if($existe)
                {
                    $existe = $existe->toArray();

                    try{
                        $reg = Intermedia::find($regs[$x]['ReplicaID']);
                        $reg->sim_status_sicafi = 1;
                        $reg->ID = $existe['ID'];
                        $reg->save();

                        if($save)
                        {
                            \Log::info('SIMPP: Se actualizó el Estatus a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Sonora');
                            $total = $total + 1;
                        }   
                        else 
                        {
                            \Log::error('SIMPP: No se actualizó el Estatus a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Sonora');        
                        }
                    }catch(\Exception $e){
                        \Log::critical('SIMPP: ERROR al actualizar registro intermedia (Arreglar) de Sonora ('.$regs[$x]['ReplicaID'].') a Estatus = 1 -->' . $e->getMessage().'');
                    }
                }
                else
                {
                    try{
                        $regs = Intermedia::find($regs[$x]['ReplicaID']);
                        $reg->Estatus = 1;
                        $reg->ID = null;
                        $reg->save();

                        if($reg)
                        {
                            \Log::info('SIMPP: Se actualizó el Estatus a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Sonora');
                            $total = $total + 1;
                        }
                        else {
                            \Log::error('SIMPP: No se actualizó el Estatus a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" del arreglar intermedia de Sonora');
                        }

                    }catch (\Exception $e) {
                        \Log::critical('ERROR al actualizar registro intermedia (Arreglar) de Sonora ('.$regs[$x]['ReplicaID'].') a Estatus = 2 -->' . $e->getMessage().'');
                    }
                }

            }
        }
        else
        {
            \Log::warning('SIMPP: La consulta de arreglar intermedia de Sonora no trajo registros con status = 3');
        }

        \Log::info('SIMPP: Se arreglarón un total de '.$total.' de registros');
        return $total;
    }
    private function sonora(){
        $total = 0;
        
        $affected = DB::connection('mysql')->update(DB::raw("update siafeson_palomilla.int_i_son_mfpapaTrampeo set sim_status_sicafi = 3 where sim_status_sicafi = 2 limit 50"));
        
        if($affected > 0)
        {
            $regs = Intermedia::select(
                    'SiembraID',
                    'JuntaID',
                    'PersonalID',
                    DB::raw("DATE_FORMAT(Fecha,'%Y-%m-%dT%T') as Fecha"), 
                    'EjercicioID',
                    'Semana',
                    'FenologiaID',
                    'SuperficieTrampeada',
                    'NoTrampasInstaladas',
                    'NoTrampasRevisadas',
                    'CapturasTrampas',
                    'PlagaID',
                    'Observacion',
                    'Externo',
                    DB::raw("DATE_FORMAT(AddRecord,'%Y-%m-%dT%T') as AddRecord"),
                    'UserID',
                    'CampanaID',
                    'GrupoID',
                    'smartphone as SMARTPHONE',
                    'CambioAtrayente',
                    'ID as ReplicaID')
            ->where("sim_status_sicafi","=","3")->get()->toArray();
        
            if($regs > 0)
            {
                $guardar = SicafiSon::insert($regs);
                //$guardar = true;
                if($guardar)
                {
                    \Log::info('SIMPP: Se insertaron registros en SICAFISON');
                    for($x = 0; $x < count($regs); $x++)
                    {
                        $existe = SicafiSon::where("ReplicaID","=",$regs[$x]['ReplicaID'])->first();
                        if($existe)
                        {
                            $existe = $existe->toArray();
                            try{
                                $reg = Intermedia::find($regs[$x]['ReplicaID']);
                                $reg->sim_status_sicafi = 1;
                                $reg->sim_id_sicafi = $existe['ReplicaID'];
                                $reg->save();
                                
                                if($reg)
                                {
                                    \Log::info('Se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Sonora');
                                    $total = $total + 1;
                                }
                                else
                                {
                                    \Log::error('SIMPP: No se actualizó el sim_status_sicafi a 1 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Sonora');
                                }
                            }catch(\Exception $e) {
                                \Log::critical('SIMPP: ERROR al actualizar registro intermedia de Sonora ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 1 -->' . $e->getMessage().'');
                            }
                        }
                        else
                        {
                            try{
                                $reg = Intermedia::find($regs[$x]['ReplicaID']);
                                $reg->sim_status_sicafi = 2;
                                $reg->save();

                                if($reg)
                                {
                                    \Log::info('SIMPP: Se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Sonora');
                                }
                                else
                                {
                                    \Log::error('SIMPP: No se actualizó el sim_status_sicafi a 2 el Registro ID: "'.$regs[$x]['ReplicaID'].'" de la intermedia de Sonora');
                                }
                            }catch(\Exception $e){
                                \Log::critical('SIMPP: ERROR al actualizar registro intermedia de Sonora ('.$regs[$x]['ReplicaID'].') a sim_status_sicafi = 2 -->' . $e->getMessage().'');
                            }
                        }
                    }
                }
                else{
                    \Log::error('SIMPP: No se insertarón los registros en SICAFISON');
                }
            }
        }
        else
        {
            \Log::warning('SIMPP: No habían registros en la intermedia de Sonora por enviar');
        }
        
        \Log::info('SIMPP: Se enviaron un total de '.$total.' de registros');
        return $total;
    }
}
