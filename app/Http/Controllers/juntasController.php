<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Models\Junta;
use DB;

class juntasController extends Controller
{
    public function index(){
        $junta = DB::table('siafeson_siafeson.juntas as j')
                    ->select('j.*')
                    ->join('siafeson_siafeson.estados as e', 'e.id','=','j.estado_id')
                    ->where('e.pais_id',1)
                    ->orderBy('j.name','ASC')
                    ->get();

        return response()->json($junta);
     }    
}
