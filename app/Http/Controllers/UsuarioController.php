<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Http\Requests\UsuarioCreateRequest;
use Illuminate\Support\Facades\Auth;
use siafeson\User;
use siafeson\Persona;
use siafeson\Models\rel_user_rol_sitio;
use Spatie\Permission\Models\Role;

class UsuarioController extends Controller
{
    public function getUserId(){
        return Auth::user();
    }  
    public function logout(Request $request)
    {
        $request->session()->flush();    	
        Auth::logout();
  		return redirect('/');
    }
    public function store(Request $request)
    {
         $validator = \Validator::make($request->all(), [
            //'username' => 'unique:pgsql.siafeson_siafeson.users',
            'email' => 'unique:pgsql.siafeson_siafeson.users|email',
            'personal_id' => 'unique:pgsql.siafeson_siafeson.persona',

        ]);
        
        if($validator->fails())
        {
            return response()->json(
                [
                    'status' => 'error', 
                    'errors'=> $validator->errors()->all()
                ]);
        }
        else
        {
            $persona = new Persona();
            $persona->name = $request->nombre;
            $persona->apellido_paterno = $request->apellidoPaterno;
            $persona->apellido_materno = $request->apellidoMaterno;
            $persona->status = 1;
            $persona->junta_id = $request->junta;
            $persona->personal_id = $request->personal_id;
            $persona->created = date("Y-m-d H:i:s");
            $persona->modified = date("Y-m-d H:i:s");
            $persona->control = 0;
            $persona->uuid = $request->uuid;
            $persona->save();

            if($persona)
            {
                $usuario = new User();
                $usuario->username = $request->email;
                $usuario->password = bcrypt($request->contra);
                $usuario->email = $request->email;
                $usuario->modified = date("Y-m-d H:i:s");
                $usuario->persona_id = $persona->id;
                $usuario->status = 1;
                $usuario->save();
                
                if($usuario){
                    $rol = Role::find($request->rol);
                    $usuario->assignRole($rol->name);
    
                    $rel = new rel_user_rol_sitio();
                    $rel->user_id = $usuario->id;
                    $rel->rol_id = $request->rol;
                    $rel->sitio_id = 1;
                    $rel->status = 1;
                    $rel->created = date("Y-m-d H:i:s");
                    $rel->modified = date("Y-m-d H:i:s");
                    $rel->save();
                }
                else{
                    return response()->json(
                        [
                            'status' => 'error', 
                            'errors'=> 'Ocurrio un error al crear el usuario'
                        ]);
                }
            }
            else{
                return response()->json(
                    [
                        'status' => 'error', 
                        'errors'=> 'Ocurrio un error al crear la persona'
                    ]);
            }

            return response()->json(
                [
                    'status' => 'success', 
                    'message'=> 'usuario creado correctamente',
                    'persona' => $persona,
                    'usuario' => $usuario,
                    'rol' => $rel
                ]);

        }
    }
}
