<?php
namespace siafeson\Http\Controllers;
use siafeson\Models\Trampa;
use Illuminate\Http\Request;
use DB;

class GenerarQRController extends Controller
{
    public function index(request $request)
    {
        if($request->selector)
        {
            $lista = [];
            foreach ($request->selector as $key => $value) {
                $test = [
                    'id' => $value['id'],
                ];
                array_push($lista, $test);
            }
            $trampas = DB::table('siafeson_simdia.trampas')->select('siafeson_simdia.trampas.id', 
            'siafeson_simdia.trampas.siembra_id', 
            'siafeson_simdia.trampas.name',
            'c.id as campo_id', 
            'siafeson_simdia.trampas.latitud', 
            'siafeson_simdia.trampas.longitud', 
            'c.name as campo', 
            'siafeson_simdia.trampas.superficie')
            ->join('siafeson_simdia.rel_trampa_campo as rtc','rtc.trampa_id','siafeson_simdia.trampas.id')
            ->join('siafeson_simdia.campos as c','c.id','rtc.campo_id')
            ->where('rtc.status',1)
            ->whereIn('siafeson_simdia.trampas.id', $lista)->get();
            
            $view = \View::make('chunks.qrGenerate', compact('trampas'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            $pdf->setPaper('letter');
            return $pdf->stream('trampas.pdf');
        }   
    }
}