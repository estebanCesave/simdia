<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Exports\CapturasExport;
use siafeson\Exports\TrampasExport;
use siafeson\Exports\focosExport;
use siafeson\Exports\Simdiacq\CapturasExport as Control;
use siafeson\Models\Captura;
use Excel;

class ExcelController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '-1');
    }
    public function actividadExportar($desde, $hasta)
    {
        ini_set('memory_limit', '-1');
        return Excel::download(new CapturasExport($desde, $hasta), 'capturas.xlsx');
    }
    public function ControlQuimicoExportar($desde, $hasta)
    {
        ini_set('memory_limit', '-1');
        return Excel::download(new Control($desde, $hasta), 'capturas_cq.xlsx');
    }
    public function trampasExportar()
    {
        return Excel::download(new TrampasExport, 'trampas.xlsx');
    }
    public function focosExportar()
    {
       return Excel::download(new focosExport, 'listadoCamposFocos.xlsx');
    }

}

