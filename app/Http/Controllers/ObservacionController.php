<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Models\Observacion;

class ObservacionController extends Controller
{
    public function getMotivos(){
        return Observacion::select('id as value','name as name')->where('status',1)->get();
    }
}
