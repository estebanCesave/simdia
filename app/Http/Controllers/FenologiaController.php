<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Models\Fenologia\Arbol;
use siafeson\Models\Fenologia\Brote;

class FenologiaController extends Controller
{
   public function getFenologiaArbol(){
   	return Arbol::select('id as value','name as name')->where('status',1)->get();
   } 
   public function getFenologiaBrote(){
   	return Brote::select('id as value','name as name')->where('status',1)->get();
   }
}
