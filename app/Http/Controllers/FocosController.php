<?php

namespace siafeson\Http\Controllers;
use siafeson\Models\Foco;
use siafeson\Models\Contacto;
use siafeson\Models\Correo;
use DB;
use Illuminate\Http\Request;
use siafeson\Mail\SendFocos as sendFocos;

class FocosController extends Controller{

    function procesoFocos(){

        $semana = date("W");
        $ano = date('Y');

        $focos = $this->obtenerFocos($semana,$ano);
        
        $contactos = DB::table('siafeson.siafeson_simdia.contactos_juntas as cj')
            ->select('cj.junta_id', 'cj.name as jlsv', 'cj.contactos', 'cj.correos_general')
            ->join('siafeson.siafeson_simdia.focos as f', function ($join) use ($semana, $ano) {
                $join->on('f.junta_id', '=', 'cj.junta_id')
                    ->where('f.semana_det', $semana)
                    ->where('f.ano_det', $ano);
            })
            ->groupBy('cj.junta_id', 'cj.name', 'cj.contactos', 'cj.correos_general')
            ->get(); 

        $correos_generales = array('ajilagunes@gmail.com', 'octavio.saavedra@cesaveson.com', 'avelino.valenzuela@cesaveson.com', 'francisco.berumen@cesaveson.com', 'linda.herrera@cesaveson.com');

        foreach ($contactos as &$c) 
        {
            $se_envio = DB::table('siafeson.siafeson_simdia.control_email')->select('id')->where('junta_id', $c->junta_id)->where('semana', $semana)->where('ano', $ano)->first();
    
            $contac = preg_split("/[\s,]+/", $c->contactos);
            $g = preg_split("/[\s,]+/", $c->correos_general);

            $focos = DB::table('siafeson.siafeson_simdia.focos as f')
                ->select('f.fecha_det', 'f.nivel', 'f.promedio', 'p.name as nombre', 'p.apellido_paterno', 'p.apellido_materno', 'c.name as campo')
                ->join('siafeson.siafeson_siafeson.users as u', 'u.id', '=', 'f.user_id')
                ->join('siafeson.siafeson_siafeson.persona as p', 'p.id', '=', 'u.persona_id')
                ->join('siafeson.siafeson_simdia.campos as c', 'c.id', '=', 'f.campo_id')
                ->where('f.status', 1)
                ->where('f.ano_det', $ano)
                ->where('f.semana_det', $semana)
                ->where('f.junta_id', $c->junta_id)
                ->get();
            
            if(!$se_envio && count($focos) > 0)
            {
                // AQUI ENVIAR CORREO Y GUARDAR REGISTRO EN CONTROL EMAIL
                \Mail::to($g, $c->jlsv)
                    ->cc($contac)
                    ->bcc($correos_generales)
                    ->send(new sendFocos($focos, $semana, $ano, $c->jlsv, $c->junta_id ));
                    sleep(5);

                //GUARDAR CORREO EN CONTROL EMAIL
                $control = new Correo;
                $control->junta_id = $c->junta_id;
                $control->plaga_id = 16;
                $control->semana = $semana;
                $control->ano = $ano;
                $control->enviado = 1;
                $control->created = date('Y-m-d h:i:s');
                $control->modified = date('Y-m-d h:i:s');
                $control->save(); 

                //MENSAJE
                echo "ENVIO DE CORREO: Se envío correo para la semana ".$semana."/".$ano." a la junta ".$c->jlsv.".<br>";
            }else{
                echo "ENVIO DE CORREO: Ya se envío correo para la semana ".$semana."/".$ano." a la junta ".$c->jlsv.".<br>";
            }
        }
           
        // return $contactos;
    }
    function obtenerFocos($semana, $ano){
        // DB::enableQueryLog();

        $hora = date('G'); //Extrae la hora actual
        $dia = date('N'); //Extrae el día
        if($dia == 7 and $hora >= 18){

            $hay_focos = DB::table('siafeson.siafeson_simdia.focos')
            ->select('id')
            ->where('status', 1)
            ->where('ano_det', $ano)
            ->where('semana_det', $semana)
            ->get(); 

 
            if(count($hay_focos) > 0){
                echo "GENERACIÓN DE FOCOS: Ya hay focos para esta semana.<br>";
            }else{
                $query = "select A.*
                from(select MIN(r.fecha) as fecha_det,
                    r.semana as semana_det,
                    r.ano as ano_det,
                    c.id as campo_id,
                    r.user_id as user_id,
                    t.tipo_huerta as tipo_huerta,
                    sum(r.captura) as capturas,
                    count(t.id) as trampas,
                    (sum(r.captura)::float / count(t.id)::float) as promedio,
                    r.user_id as user_asignado,
                    1 as nivel,
                    c.junta_id
                    from siafeson.siafeson_simdia.capturas r
                    join siafeson.siafeson_simdia.trampas t on t.id = r.trampa_id 
                    and t.tipo_huerta = 2
                    join siafeson.siafeson_simdia.campos c on c.id = t.campo_id
                    where r.status = 1
                    and r.valido = 1
                    and r.ano = $ano
                    and r.semana = $semana
                    group by r.semana, r.ano, c.id, r.user_id, t.tipo_huerta, c.junta_id) A
                where A.promedio > 0"; 
                $focos = DB::select(DB::raw($query));
                foreach ($focos as &$item) {
                    $foco = new Foco;
                    $foco->fecha_det = $item->fecha_det;
                    $foco->semana_det = $item->semana_det;
                    $foco->ano_det = $item->ano_det;
                    $foco->campo_id = $item->campo_id;
                    $foco->user_id = $item->user_id;
                    $foco->tipo_huerta = $item->tipo_huerta;
                    $foco->promedio = $item->promedio;
                    $foco->user_asignado = $item->user_asignado;
                    $foco->nivel = $item->nivel;
                    $foco->junta_id = $item->junta_id;
                    $foco->status = 1;
                    $foco->created = date('Y-m-d h:i:s');
                    $foco->modified = date('Y-m-d h:i:s');
                    $foco->save();
                }
                echo 'GENERACIÓN DE FOCOS: Total de focos insertados: ' . count($focos) . '.<br>';
            }

        }else{
            echo "GENERACIÓN DE FOCOS: Fuera del periodo establecido.<br>";
        }

        // $query = DB::getQueryLog();
        // print_r($query);
    }
    public function pdf($id, $semana, $ano)
    {
        $focos = DB::table('siafeson.siafeson_simdia.focos as f')
            ->select('f.fecha_det', 'f.nivel', 'f.promedio', 'p.name as nombre', 'p.apellido_paterno', 'p.apellido_materno', 'c.name as campo')
            ->join('siafeson.siafeson_siafeson.users as u', 'u.id', '=', 'f.user_id')
            ->join('siafeson.siafeson_siafeson.persona as p', 'p.id', '=', 'u.persona_id')
            ->join('siafeson.siafeson_simdia.campos as c', 'c.id', '=', 'f.campo_id')
            ->where('f.status', 1)
            ->where('f.ano_det', $ano)
            ->where('f.semana_det', $semana)
            ->where('f.junta_id', $id)
            ->get();

        $junta = DB::table('siafeson.siafeson_siafeson.juntas')->select('name')->where('id', $id)->first();
        

        $view = \View::make('chunks.focosPdf', compact('focos', 'semana', 'ano', 'junta'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('letter');
        $pdf->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        return $pdf->stream('focos.pdf');
    }

}
