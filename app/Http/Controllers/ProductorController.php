<?php
namespace siafeson\Http\Controllers;
use Illuminate\Http\Request;
use siafeson\Models\Campo;
use siafeson\Models\Umbral;
use DB;

class ProductorController extends Controller
{
    public function __construct()
    {
          ini_set('max_execution_time', 120000);
          //ini_set('set_time_limit', 120000);
    }
    public function huertas(){
        $campos = Campo::all();
         return view('simdia.productor.index', compact('campos'));
    }
    public function huertaId($id)
    {
        $campo = Campo::where('campo_id',$id)->first();

        $estado = DB::table('siafeson_siafeson.estados as e')
                ->select('e.name as estado','e.id as estadoId')
                ->join('siafeson_siafeson.juntas as j','j.estado_id','=','e.id')
                ->join('siafeson_simdia.campos as c','c.junta_id','=','j.id')
                ->where('c.campo_id',$id)
                ->first();

        $superficie = DB::table('siafeson_simdia.trampas as t')
            ->select(DB::raw('ROUND( SUM(superficie)::NUMERIC, 2) as superficie'))
            ->where('t.campo_id',$id)
            ->where('t.status',1)
            ->where('t.trampa',1)
            ->first();

        $trampas = DB::table('siafeson_simdia.trampas')->select('*')->where('campo_id',$id)->where('status', 1)->where('trampa',1)->count();

        $arco = DB::table('siafeson_simdia.trampas as t')->select('t.arco')->where('campo_id',$id)->where('campo_id',$id)->where('status', 1)->where('trampa',1)->first();

        $aplicacionQuimica = DB::connection('mysql')->table('siafeson_simdia.cq_capturas as c')
                ->select('c.fecha','c.semana','c.ano','p.name AS producto','ca.name AS tipo')
                ->join('siafeson_simdia.catProductos as p', 'p.id','=','c.producto_id')
                ->join('siafeson_simdia.catTipoAplicacion as ca','ca.id','=','c.tipo_id')
                ->where('c.campo_id',$id)
                ->where('c.status',1)
                ->orderBy('c.fecha','DESC')->orderBy('c.ano','DESC')->orderBy('c.semana','DESC')
                ->first();

        return view('simdia.productor.huerta', compact('campo','superficie','trampas','arco', 'estado', 'aplicacionQuimica'));
    }
    public function distribucionEspacialUmbral($estado, $campo, $semana)
    {
        $umbral = Umbral::where('estado_id',$estado)->first();

        $distribucion = DB::connection('mysql')->table('siafeson_simdia.capturas as c')
                ->select('c.id',
                        't.name as trampa_id', 
                        't.name as Trampa',
                        'c.longitud',
                        'c.latitud', 
                        DB::raw("
                            ( 
                                case 
                                    when (c.captura = 0) then 'g' 
                                    when (c.captura > 0 and c.captura < ". $umbral->umbral ." ) then 'y'
                                    when (c.captura >= ". $umbral->umbral .") then 'r'
                                    when (c.captura IS NULL) then 'c'
                                    else 'b' 
                                end 
                            ) as rango")
                        )
                ->join('siafeson_simdia.trampas as t','t.id','=','c.trampa_id')
                ->where('t.campo_id',$campo)
                ->where('c.semana',$semana)
                ->where('c.ano',2019)
                ->whereNotNull('c.user_id')
                ->orderBy('t.name','ASC')
                ->get();

                return response()->json(
                    [
                        'status' => 'success',
                        'data' => $distribucion,
                        'count' => count($distribucion),
                        'umbral' => $umbral->umbral
                    ]);
    }
    public function golpeteoSemana($campo, $semana, $ano)
    {
        $campoId = DB::connection('mysql')->table('siafeson_simdiagolp.campos as c')
            ->select('c.id as campoId')->where('c.referencia','LIKE','%'.$campo.'%')->first();

        $golpeteo = DB::connection('mysql')->table('siafeson_simdiagolp.registros as r')
                ->select('r.pt_lat','r.pt_lon','pt_adultos')
                ->where('r.campo_id',$campoId->campoId)
                ->where('r.semana',$semana)
                ->where('r.ano',$ano)->first();

                if(is_null($golpeteo)){
                    $latitudes = 0;
                    $longitudes = 0;
                    $adultos = 0;
                }
                else{
                    $latitudes = preg_split("/[\s,]+/", $golpeteo->pt_lat);
                    $longitudes = preg_split("/[\s,]+/", $golpeteo->pt_lon);
                    $adultos = preg_split("/[\s,]+/", $golpeteo->pt_adultos);
                }

                return response()->json([
                    'status' => 'success',
                    'latitud' =>  $latitudes,
                    'longitud' =>  $longitudes,
                    'adulto' =>  $adultos,
                    'count' => count($golpeteo)
                ]);

    }
    public function obtenerSemanas($campo){
        
        $semanas = DB::table('siafeson_simdia.capturas as c')->select('c.semana')
                    ->join('siafeson_simdia.trampas as t','t.id','=','c.trampa_id')
                    ->join('siafeson_simdia.campos as ca','ca.campo_id','=','t.campo_id')
                    ->where('ca.campo_id',$campo)
                    ->groupBy('c.semana')
                    ->orderBy('c.semana','ASC')
                    ->get();

        return response()->json($semanas);
    }
    public function promedioGolp($campo, $ano)
    {
        $pila = [];
        $semanas = [];
        
        $campoId = DB::connection('mysql')->table('siafeson_simdiagolp.campos as c')
        ->select('c.id as campoId')->where('c.referencia','LIKE','%'.$campo.'%')->first();

        $promedio = DB::connection('mysql')->table('siafeson_simdiagolp.registros as g')
            ->select(DB::raw('g.tot_adultos / g.tot_arboles as data'), 's.semana')
            ->rightJoin('siafeson_siafeson.semanas AS s', function($join) use($ano, $campoId){   
                $join->on('g.semana','=','s.semana')->where('g.ano',$ano)->where('g.campo_id', $campoId->campoId);   
            })
            ->groupBy('s.semana')
            ->get();

        foreach($promedio as $p){
            array_push($pila, (float)$p->data);
            array_push($semanas, $p->semana);
        }

        return response()->json([
            'data' => $pila,
            'semanas' => $semanas,
            'min' =>  min($pila),
            'max' => max($pila) 
        ]);
    }
    public function promedioTrampa($campo, $ano)
    {
        $pila = [];
        $semanas = [];
        
        $campoId = DB::connection('mysql')->table('siafeson_simdiagolp.campos as c')
        ->select('c.id as campoId')->where('c.referencia','LIKE','%'.$campo.'%')->first();

        $promedio = DB::connection('mysql')->table('siafeson_simdiagolp.registros as g')
            ->select(DB::raw('g.tot_adultos / g.tot_arboles as data'), 's.semana')
            ->rightJoin('siafeson_siafeson.semanas AS s', function($join) use($ano, $campoId){   
                $join->on('g.semana','=','s.semana')->where('g.ano',$ano)->where('g.campo_id', $campoId->campoId);   
            })
            ->groupBy('s.semana')
            ->get();

        foreach($promedio as $p){
            array_push($pila, (float)$p->data);
            array_push($semanas, $p->semana);
        }

        return response()->json([
            'data' => $pila,
            'semanas' => $semanas,
            'min' =>  min($pila),
            'max' => max($pila) 
        ]);
    }
    public function getAnos($campo)
    {
        $pila = [];
        $campoId = DB::connection('mysql')->table('siafeson_simdiagolp.campos as c')
        ->select('c.id as campoId')->where('c.referencia','LIKE','%'.$campo.'%')->first();

        $query = DB::connection('mysql')->table('siafeson_simdiagolp.registros')
            ->select('ano')
            ->where('campo_id', $campoId->campoId)
             ->groupBy('ano')
            ->get();

        foreach ($query as $a) 
        {
            array_push($pila,$a->ano);
        }

        return $pila;    
    }
}
