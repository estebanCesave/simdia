<?php
namespace siafeson\Console\Commands;
use Illuminate\Console\Command;
use DB;

class AsignarRol extends Command
{
   
    protected $signature = 'asignar:rol';
    protected $description = 'Recorre la tabla rel_user_sitio e inserta los roles e id de usuario en la tabla model_has_role que se encuentra en el esquema public y es de spatie';

    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $relacionLevels = DB::table('siafeson_siafeson.aux_user_sitio')->select('*')->get();
        //Recorrer
        foreach ($relacionLevels as $r) 
        {
            /** Checa que el usuario no haya sido insertado anteriormente */
            $buscarUsuario = DB::table('public.model_has_roles')->select('*')
            ->where('role_id',$r->id_level)
            ->where('model_id',$r->id_user)    
            ->first();
            if(!$buscarUsuario)
            {
                $insertar = DB::table('public.model_has_roles')->insert(
                    ['role_id' => $r->id_level, 'model_type' => 'siafeson\User', 'model_id' => $r->id_user]
                );
            }
        }

        $this->info('Se han asignado rol en paqueteria Spatie!');
        \Log::info('Se han asignado rol en paqueteria Spatie!');
    }
}
