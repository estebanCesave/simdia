<?php
namespace siafeson\Console\Commands;
use Illuminate\Console\Command;
use siafeson\Models\ReporteAnual;
use DB;

class Anual extends Command
{
    protected $signature = 'command:anual';
    protected $description = 'Comando para insertar las capturas en la tabla reporte anual';

    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $query = reporteAnual();    
        $trampas = DB::select(DB::raw($query));
        $nuevos = 0;
        $actualizados = 0;

        foreach ($trampas as $t) 
        {
            $existe = ReporteAnual::where('ano',$t->ano)->where('arco',$t->arco)->where('estado_id',$t->estado_id)->where('junta_id',$t->junta_id)->first();
            if(!$existe)
            {
                $reporte = new ReporteAnual;
                $reporte->estado_id = $t->estado_id;
                $reporte->junta_id = $t->junta_id;
                $reporte->arco = $t->arco;
                $reporte->ano = $t->ano;
                $reporte->revisadas = $t->revisadas;
                $reporte->norevisadas = $t->norevisadas;
                $reporte->capturas = $t->capturas;
                $reporte->trampasconcapturas = $t->trampasconcapturas;
                $reporte->status = 1;
                $reporte->created = date("Y-m-d H:i:s");
                $reporte->modified = date("Y-m-d H:i:s");
                $reporte->save();
                $nuevos = $nuevos + 1;
            }
            else{
                //var_dump('Existe: ', $t->junta_id);
            }
        }
        
        $this->info('Se insertaron un total de: '.$nuevos.' registros nuevos');
        $this->info('Se actualizaron un total de: '.$actualizados.' registros');
    }
}
