<?php
namespace siafeson\Console\Commands;
use Illuminate\Console\Command;
use siafeson\Models\ReporteMensual;
use DB;

class Mensual extends Command
{
    protected $signature = 'command:mensual';
    protected $description = 'Comando para insertar las capturas en la tabla reporte mensual';

    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $query = reporteMensual();    
        $trampas = DB::select(DB::raw($query));
        $nuevos = 0;
        $actualizados = 0;
   
        foreach ($trampas as $t) 
        {
            $existe = ReporteMensual::where('ano',$t->ano)->where('num_mes',$t->num_mes)->where('estado_id',$t->estado_id)->where('junta_id',$t->junta_id)->first();
            if(!$existe)
            {
                $reporte = new ReporteMensual;
                $reporte->estado_id = $t->estado_id;
                $reporte->junta_id = $t->junta_id;
                $reporte->arco = $t->arco;
                $reporte->ano = $t->ano;
                $reporte->num_mes = $t->num_mes;
                $reporte->mes = $t->mes;
                $reporte->revisadas = $t->revisadas;
                $reporte->norevisadas = $t->norevisadas;
                $reporte->capturas = $t->capturas;
                $reporte->trampasconcapturas = $t->tramprasconcapturas;
                $reporte->status = 1;
                $reporte->created = date("Y-m-d H:i:s");
                $reporte->modified = date("Y-m-d H:i:s");
                $reporte->save();
                $nuevos = $nuevos + 1;
            }
            else{
                //var_dump('EXISTE',$t->num_mes);
            }
        }
        
        $this->info('Se insertaron un total de: '.$nuevos.' registros nuevos');
        $this->info('Se actualizaron un total de: '.$actualizados.' registros');
    }
}
