<?php
namespace siafeson\Console\Commands;
use Illuminate\Console\Command;
use siafeson\Models\ReporteSemanal;
use DB;

class Semanal extends Command
{
    protected $signature = 'command:semanal';
    protected $description = 'Comando para insertar las capturas en la tabla reporte semanal';

    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $query = reporteSemanal();    
        $trampas = DB::select(DB::raw($query));
        $nuevos = 0;
        $actualizados = 0;
         
        foreach ($trampas as $t) 
        {
            $existe = ReporteSemanal::where('semana',$t->semana)->where('ano',$t->ano)->where('estado_id',$t->estado_id)->where('junta_id',$t->junta_id)->first();
            
            if(!$existe)
            {
                $reporte = new ReporteSemanal;
                $reporte->estado_id = $t->estado_id;
                $reporte->junta_id = $t->junta_id;
                $reporte->arco = $t->arco;
                $reporte->ano = $t->ano;
                $reporte->semana = $t->semana;
                $reporte->revisadas = $t->revisadas;
                $reporte->norevisadas = $t->norevisadas;
                $reporte->capturas = $t->capturas;
                $reporte->trampasconcapturas = $t->tramprasconcapturas;
                $reporte->status = 1;
                $reporte->created = date("Y-m-d H:i:s");
                $reporte->modified = date("Y-m-d H:i:s");
                $reporte->save();
                $nuevos = $nuevos + 1;
            }
        }
        
        $this->info('Se insertaron un total de: '.$nuevos.' registros nuevos');
        $this->info('Se actualizaron un total de: '.$actualizados.' registros');
    }
}
