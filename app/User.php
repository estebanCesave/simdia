<?php
namespace siafeson;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use siafeson\ModelsUser;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;
    protected $table = 'siafeson_siafeson.users';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /*public function persona()
    {
        return $this->hasOne('siafeson\Persona', 'persona_id','id');
    }*/
    public function getAuthPassword()
    {
        return $this->password;
    }
    public function persona()
    {
        return $this->belongsTo('siafeson\Persona','persona_id','id');
    }
   
}
