<?php
namespace siafeson\Exports;
use siafeson\Models\ReporteSemanal;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class focosExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $query = queryFocos();
        $captura = DB::select(DB::raw($query));
        $campos = [];
        $semanas = [];
        $test = [];
        $data = [];

        foreach($captura as $r){
            $test['semanas'] = $r->semana;
            $test['captura'] = $r->capturas;
           
            array_push($campos, $r->campo);
            array_push($semanas, $r->semana);
            array_push($data,$test);
            
        }
        $campos = array_unique($campos);
        sort($campos);
        $semanas = array_unique($semanas);
        sort($semanas);
          // dd($test);
        return view('exports.focos',[
            'campos' => $campos,
            'semanas' => $semanas,
            'data' => $data  
        ]);
        
        //return $captura;
        //return ReporteSemanal::all();
    }
}
