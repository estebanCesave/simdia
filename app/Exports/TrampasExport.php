<?php
namespace siafeson\Exports;
use siafeson\Models\Trampa;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use DB;
use Auth;
use Session;

class TrampasExport implements FromView
{
    public function view(): View
    {
        $junta = $this->getJunta();
        $trampas = $this->query();
        $estado = $this->getEstado();

        return view('exports.trampas',[
            'trampas' => $trampas,
            'junta' => $junta,
            'estado' => $estado
        ]);
    }
    function query()
    {
        if(Auth::user()->hasrole('Admin Junta') && Session::get('rol') == 'Admin Junta')
        {
            $junta = $this->getJunta();

            $trampas =  DB::table('siafeson_simdia.rel_trampa_campo')
                ->select('siafeson_simdia.trampas.id','siafeson_simdia.trampas.siembra_id','siafeson_simdia.trampas.name','siafeson_simdia.trampas.latitud','siafeson_simdia.trampas.longitud','siafeson_simdia.trampas.arco','siafeson_simdia.campos.name as campo','siafeson_siafeson.persona.name as nombre','siafeson_siafeson.persona.apellido_paterno','siafeson_siafeson.persona.apellido_materno')  
                ->join('siafeson_simdia.trampas','siafeson_simdia.rel_trampa_campo.trampa_id','=','siafeson_simdia.trampas.id')
                ->join('siafeson_simdia.campos','siafeson_simdia.campos.id','=','siafeson_simdia.rel_trampa_campo.campo_id')
                ->join('siafeson_simdia.rel_user_trampa','siafeson_simdia.trampas.id','=','siafeson_simdia.rel_user_trampa.trampa_id')
                ->join('siafeson_siafeson.users','siafeson_simdia.rel_user_trampa.user_id','=','siafeson_siafeson.users.id')
                ->join('siafeson_siafeson.persona','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
                ->where('siafeson_simdia.trampas.status',1)
                ->where('siafeson_simdia.rel_user_trampa.status',1)
                ->where('siafeson_simdia.campos.status',1)
                ->where('siafeson_siafeson.persona.junta_id', $junta->juntaId)
                ->get();

            return $trampas;    
        
        }
        else if(Auth::user()->hasrole('Admin Estado') && Session::get('rol') == 'Admin Estado' )
		{
            $estado = $this->getEstado();

			$trampas =  DB::table('siafeson_simdia.rel_trampa_campo')
				->select('siafeson_simdia.trampas.id','siafeson_simdia.trampas.siembra_id','siafeson_simdia.trampas.name','siafeson_simdia.trampas.latitud','siafeson_simdia.trampas.longitud','siafeson_simdia.trampas.arco','siafeson_simdia.campos.name as campo','siafeson_siafeson.persona.name as nombre','siafeson_siafeson.persona.apellido_paterno','siafeson_siafeson.persona.apellido_materno', 'siafeson_siafeson.estados.name as estado','siafeson_siafeson.juntas.name as junta')  
				->join('siafeson_simdia.trampas','siafeson_simdia.rel_trampa_campo.trampa_id','=','siafeson_simdia.trampas.id')
				->join('siafeson_simdia.campos','siafeson_simdia.campos.id','=','siafeson_simdia.rel_trampa_campo.campo_id')
				->join('siafeson_simdia.rel_user_trampa','siafeson_simdia.trampas.id','=','siafeson_simdia.rel_user_trampa.trampa_id')
				->join('siafeson_siafeson.users','siafeson_simdia.rel_user_trampa.user_id','=','siafeson_siafeson.users.id')
				->join('siafeson_siafeson.persona','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
				->join('siafeson_siafeson.juntas','siafeson_siafeson.juntas.id','=','siafeson_simdia.campos.junta_id')
				->join('siafeson_siafeson.estados','siafeson_siafeson.estados.id','=','siafeson_siafeson.juntas.estado_id')
				->where('siafeson_simdia.trampas.status',1)
				->where('siafeson_simdia.rel_user_trampa.status',1)
				->where('siafeson_simdia.campos.status',1)
				->where('siafeson_siafeson.estados.id',$estado->estadoId)
                ->get();
                
            return $trampas;
        }
    }
    function getJunta()
    {
      $junta =  DB::table('siafeson_siafeson.persona')
        ->select('siafeson_siafeson.juntas.name as junta', 'siafeson_siafeson.juntas.id as juntaId')
        ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
        ->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
        ->where('siafeson_siafeson.users.id',Auth::user()->id)
        ->first();

        return $junta;
    }
    function getEstado()
	{
		$estado =  DB::table('siafeson_siafeson.persona')
        ->select('siafeson_siafeson.estados.name as estado','siafeson_siafeson.estados.id as estadoId')
        ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
		->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
		->join('siafeson_siafeson.estados','siafeson_siafeson.estados.id','=','siafeson_siafeson.juntas.estado_id')
        ->where('siafeson_siafeson.users.id',Auth::user()->id)
        ->first();

	    return $estado;
	}
}
