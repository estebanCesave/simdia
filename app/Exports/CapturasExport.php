<?php
namespace siafeson\Exports;
use siafeson\Models\Captura;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Http\Request;
use DB;
use Auth;
use Session;

class CapturasExport implements FromView
{
    public function __construct($desde, $hasta)
    {
        ini_set('max_execution_time', 30000000);
        ini_set('memory_limit', '-1');
        $this->desde = $desde;
        $this->hasta = $hasta;
    }
    public function view(): View
    {
        $capturas = $this->query();
        $junta = $this->getJunta();
        $estado = $this->getEstado();

        return view('exports.actividad', [
            'capturas' => $capturas,
            'junta' => $junta,
            'estado' => $estado
        ]);
    }   
    function query()
    {
        $desde = $this->desde; //date('Y-m-d');
        $hasta = $this->hasta;
        $desdeSemana = ejerciciosemana($desde);
        $hastaSemana = ejerciciosemana($hasta);
        $ano = substr($desde,0,4);
        $user_id = Auth::user()->id;

        if(Auth::user()->hasrole('Admin Junta') && Session::get('rol') == 'Admin Junta')
        {
          $junta = $this->getJunta();
          //$query = getQueryAdmin($user_id, $desdeSemana['Semana'], $hastaSemana['Semana'], $ano, $junta->juntaId);       
          $query = getQueryAdmin($user_id, $desde, $hasta, $ano, $junta->juntaId);
          $captura = DB::select(DB::raw($query));
             
          return $captura;
            
        }
        else if(Auth::user()->hasrole('Admin Estado') && Session::get('rol') == 'Admin Estado')
        {
          $estado = $this->getEstado();
          //$query = getQueryEstado($user_id, $desdeSemana['Semana'], $hastaSemana['Semana'], $ano, $estado->estadoId);
          $query = getQueryEstado($user_id, $desde, $hasta, $ano, $estado->estadoId);
          $captura = DB::select(DB::raw($query));
          return $captura;
        }
        else if(Auth::user()->hasrole('Técnico') && Session::get('rol') == 'Técnico')
        {
              $captura = Captura::select('siafeson_simdia.capturas.id',
              DB::raw("siafeson_simdia.capturas.fecha::DATE"),
              DB::raw("split_part(trim(siafeson_simdia.capturas.posicion::text, '()'), ',', 1)::float AS lat"),
              DB::raw("split_part(trim(siafeson_simdia.capturas.posicion::text, '()'), ',', 2)::float AS lng"),
              DB::raw("siafeson_simdia.capturas.fecha::DATE"),
              't.name as name',
              't.siembra_id',
              'siafeson_simdia.capturas.id',
              'siafeson_simdia.capturas.captura',
              'siafeson_simdia.capturas.revisada',
              'siafeson_simdia.capturas.instalada',
              'siafeson_simdia.capturas.ano',
              'siafeson_simdia.capturas.semana',
              'siafeson_simdia.capturas.ejercicioid',
              'siafeson_simdia.capturas.accuracy',
              'siafeson_simdia.capturas.distancia_qr',
              'siafeson_simdia.capturas.observaciones',
              'siafeson_simdia.capturas.comentarios',
              'siafeson_simdia.capturas.fecha_cel',
              'siafeson_simdia.capturas.imei',
              'siafeson_simdia.capturas.noa as norte_adultos', 
              'siafeson_simdia.capturas.non as norte_ninfas', 
              'siafeson_simdia.capturas.nof as norte_fenologia', 
              'siafeson_simdia.capturas.sua as sur_adultos',
              'siafeson_simdia.capturas.sun as sur_ninfas', 
              'siafeson_simdia.capturas.suf as sur_fenologia', 
              'siafeson_simdia.capturas.esa as este_adultos', 
              'siafeson_simdia.capturas.esn as este_ninfas',
              'siafeson_simdia.capturas.esf as este_fenologia', 
              'siafeson_simdia.capturas.oea as oeste_adultos', 
              'siafeson_simdia.capturas.oen as oeste_ninfas', 
              'siafeson_simdia.capturas.oef as oeste_fenologia',
              'ca.name as campo',
              'f.name as fenologia',
              'o.name as observaciones',
              'siafeson_simdia.capturas.inserted_sicafi',
              DB::raw("case when siafeson_simdia.capturas.method = 1 then 'Móvil' when siafeson_simdia.capturas.method = 2 then 'Web' when siafeson_simdia.capturas.method = 3 then 'Modificado por web' when siafeson_simdia.capturas.method = 4 then 'Recuperado' when siafeson_simdia.capturas.method = 5 then 'Ingresado por Admin en semana siguiente' else 'N/A' end as Method"),
              DB::raw("case when siafeson_simdia.capturas.inserted_sicafi = 1 then 'En SICAFI' when siafeson_simdia.capturas.inserted_sicafi = 2 then 'En proceso' else 'Sin enviar' end as sicafi"),
              DB::raw("(select count(*) from siafeson_simdia.capturas c1 where c1.semana = siafeson_simdia.capturas.semana and c1.ano = siafeson_simdia.capturas.ano and c1.status = siafeson_simdia.capturas.status and c1.valido = siafeson_simdia.capturas.valido and c1.trampa_id = siafeson_simdia.capturas.trampa_id) as contador"))
              ->join('siafeson_simdia.trampas as t','t.id','siafeson_simdia.capturas.trampa_id')
              ->join('siafeson_siafeson.users as u','u.id','siafeson_simdia.capturas.user_id')
              ->join('siafeson_siafeson.persona as p','u.persona_id','p.id')
              ->join('siafeson_siafeson.juntas as j','j.id','p.junta_id')
              ->join('siafeson_simdia.fenologias as f','f.id','siafeson_simdia.capturas.fenologia_trampa_id')
              ->leftJoin('siafeson_simdia.observaciones as o','o.id','siafeson_simdia.capturas.observaciones')
              ->leftJoin('siafeson_simdia.campos as ca','ca.campo_id','t.campo_id')
              ->where('siafeson_simdia.capturas.status',1)
              ->where('siafeson_simdia.capturas.valido',1)
              ->where('siafeson_simdia.capturas.user_id',$user_id)
              ->whereBetween('siafeson_simdia.capturas.semana', [ date('W',strtotime($desde)) , date('W', strtotime($hasta)) ] )
              ->where('siafeson_simdia.capturas.ano',$ano)  
              ->orderBy('siafeson_simdia.capturas.fecha','DESC')
              ->get();

          return $captura;
        }
        else if(Auth::user()->hasrole('Super Admin') && Session::get('rol') == 'Super Admin')
        {
            $anoDesde = substr($desde,0,4);
            $anoHasta = substr($hasta,0,4);

            $captura = DB::table('siafeson_simdia.capturas as c')->select(
              'c.id',
              DB::raw("c.fecha::DATE"),
              DB::raw("split_part(trim(c.posicion::text, '()'), ',', 1)::float AS lat"),
              DB::raw("split_part(trim(c.posicion::text, '()'), ',', 2)::float AS lng"),
              DB::raw("c.fecha::DATE"),
              't.name as name',
              't.siembra_id',
              'c.id',
              'c.captura',
              'c.revisada',
              'c.instalada',
              'c.ano',
              'c.semana',
              'c.ejercicioid',
              'c.accuracy',
              'c.distancia_qr',
              'c.observaciones',
              'c.comentarios',
              'c.fecha_cel',
              'c.imei',
              'c.noa as norte_adultos', 
              'c.non as norte_ninfas', 
              'c.nof as norte_fenologia', 
              'c.sua as sur_adultos',
              'c.sun as sur_ninfas', 
              'c.suf as sur_fenologia', 
              'c.esa as este_adultos', 
              'c.esn as este_ninfas',
              'c.esf as este_fenologia', 
              'c.oea as oeste_adultos', 
              'c.oen as oeste_ninfas', 
              'c.oef as oeste_fenologia',
              'ca.name as campo',
              'f.name as fenologia',
              'o.name as observaciones',
              'c.inserted_sicafi',
              DB::raw("case when c.method = 1 then 'Móvil' when c.method = 2 then 'Web' when c.method = 3 then 'Modificado por web' when c.method = 4 then 'Recuperado' when c.method = 5 then 'Ingresado por Admin en semana siguiente' else 'N/A' end as Method"),
              DB::raw("case when c.inserted_sicafi = 1 then 'En SICAFI' when c.inserted_sicafi = 2 then 'En proceso' else 'Sin enviar' end as sicafi"))
            ->join('siafeson_simdia.trampas as t','c.trampa_id','t.id')
            ->join('siafeson_siafeson.users as u','u.id','c.user_id')
            ->join('siafeson_siafeson.persona as p','u.persona_id','p.id')
            ->join('siafeson_siafeson.juntas as j','j.id','p.junta_id')
            ->join('siafeson_siafeson.estados as e','e.id','j.estado_id')
            ->leftJoin('siafeson_simdia.campos as ca','ca.campo_id','t.campo_id')
            ->join('siafeson_simdia.fenologias as f','f.id','c.fenologia_trampa_id')
            ->leftJoin('siafeson_simdia.observaciones as o','o.id','c.observaciones')
            ->where('c.status',1)
            ->where('c.valido',1)
            ->whereBetween('c.fecha', [ $desde, $hasta ])
            ->whereBetween('c.ano', [ $anoDesde, $anoHasta ])
            ->get();

            return $captura;

        }
    }
    function getJunta()
    {
      $junta =  DB::table('siafeson_siafeson.persona')
        ->select('siafeson_siafeson.juntas.name as junta', 'siafeson_siafeson.juntas.id as juntaId')
        ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
        ->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
        ->where('siafeson_siafeson.users.id',Auth::user()->id)
        ->first();

        return $junta;
    }
    function getEstado()
    {
      $estado = DB::table('siafeson_siafeson.persona')
        ->select('siafeson_siafeson.estados.name as estado','siafeson_siafeson.estados.id as estadoId')
        ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
        ->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
        ->join('siafeson_siafeson.estados','siafeson_siafeson.estados.id','=','siafeson_siafeson.juntas.estado_id')
        ->where('siafeson_siafeson.users.id',Auth::user()->id)
        ->first();

        return $estado;
    }
}