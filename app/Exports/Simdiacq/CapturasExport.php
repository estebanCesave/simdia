<?php
namespace siafeson\Exports\Simdiacq;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Session;
use Auth;
use DB;

class CapturasExport implements FromView
{
    public function __construct($desde, $hasta)
    {
        ini_set('max_execution_time', 30000000);
        ini_set('memory_limit', '-1');
        $this->desde = $desde;
        $this->hasta = $hasta;
    }
    public function view(): View
    {
        $capturas = $this->query();
        $junta = $this->getJunta();
        $estado = $this->getEstado();

        return view('exports.actividadControl', [ 
            'capturas' => $capturas,
            'junta' => $junta,
            'estado' => $estado
        ]);
    }
    function query()
    {
        $desde = $this->desde; //date('Y-m-d');
        $hasta = $this->hasta;
        $anoDesde = substr($desde,0,4);
        $anoHasta = substr($hasta,0,4);

        if(Auth::user()->hasrole('Técnico') && Session::get('rol') == 'Técnico')
        {
            $capturas = DB::table('siafeson_simdia.cq_capturas as cq')->select(
                'cq.id',
                'cq.fecha',
                'cq.dosis',
                'cq.superficie',
                'cq.no_plantas as plantas',
                'cq.no_productores as productores',
                'cq.inserted_sicafi as sicafi',
                'tc.name as tipoControl',
                'tc.id as control',
                'ta.name as tipoAplicacion',
                'ta.id as tipo',
                'cp.name as producto_text',
                'cp.id as producto',
                'cb.name as controlBiologico',
                'cb.id as biologico',
                'na.name as noAplicacion',
                'na.id as aplicacion',
                'u.id as user',
                't.siembra_id',
                'c.name AS name',
                't.id as huerta',
                DB::raw("CONCAT( p.name,' ',p.apellido_paterno,' ',p.apellido_materno) AS nombre"),
                DB::raw("
                  CASE
                    WHEN cq.method = 1 THEN 'Móvil'
                    WHEN cq.method = 2 THEN 'Web'
                    WHEN cq.method = 3 THEN 'Modificado por web'
                    WHEN cq.method = 4 THEN 'Recuperado'
                    WHEN cq.method = 5 THEN 'Ingresado por Admin en semana siguiente'
                    ELSE 'N/A'
                  END as metodo_tex               
                "),
                'u.id as user')
            ->join('siafeson_simdia.trampas as t','cq.trampa_id','t.id')
            ->join('siafeson_siafeson.users as u','u.id','cq.user_id')
            ->join('siafeson_siafeson.persona as p','u.persona_id','p.id')
            ->join('siafeson_siafeson.juntas as j','j.id','p.junta_id')
            ->join('siafeson_simdia.cattipocontrol as tc','cq.tipo_control_id','tc.id')
            ->join('siafeson_simdia.cattipoaplicacion as ta','cq.tipo_id','ta.id')
            ->join('siafeson_simdia.catproductos as cp','cq.producto_id','cp.id')
            ->join('siafeson_simdia.catcontrolbiologico as cb','cq.c_biologico_id','cb.id')
            ->join('siafeson_simdia.cq_catnoaplicacion as na','cq.noaplicacion_id','na.id')
            ->join('siafeson_simdia.campos as c','cq.campo_id','c.campo_id')
            ->where('cq.status',1)
            ->whereBetween('cq.fecha', [ $desde, $hasta ])
            ->whereBetween('cq.ano', [ $anoDesde, $anoHasta ])
            ->where('cq.user_id',Auth::user()->id)
            ->orderBy('cq.fecha','DESC')
            ->get();

            return $capturas;
        }
    }   
    function getJunta()
    {
      $junta =  DB::table('siafeson_siafeson.persona')
        ->select('siafeson_siafeson.juntas.name as junta', 'siafeson_siafeson.juntas.id as juntaId')
        ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
        ->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
        ->where('siafeson_siafeson.users.id',Auth::user()->id)
        ->first();

        return $junta;
    }
    function getEstado()
    {
      $estado = DB::table('siafeson_siafeson.persona')
        ->select('siafeson_siafeson.estados.name as estado','siafeson_siafeson.estados.id as estadoId')
        ->join('siafeson_siafeson.users','siafeson_siafeson.persona.id','=','siafeson_siafeson.users.persona_id')
        ->join('siafeson_siafeson.juntas','siafeson_siafeson.persona.junta_id','=','siafeson_siafeson.juntas.id')
        ->join('siafeson_siafeson.estados','siafeson_siafeson.estados.id','=','siafeson_siafeson.juntas.estado_id')
        ->where('siafeson_siafeson.users.id',Auth::user()->id)
        ->first();

        return $estado;
    }
}
